package com.lcsb.echeck.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-06-16T13:15:34")
@StaticMetamodel(Checkdept.class)
public class Checkdept_ { 

    public static volatile SingularAttribute<Checkdept, String> date;
    public static volatile SingularAttribute<Checkdept, String> timescan;
    public static volatile SingularAttribute<Checkdept, String> deptID;
    public static volatile SingularAttribute<Checkdept, String> notel;
    public static volatile SingularAttribute<Checkdept, String> name;
    public static volatile SingularAttribute<Checkdept, String> id;
    public static volatile SingularAttribute<Checkdept, String> staffID;
    public static volatile SingularAttribute<Checkdept, String> timeout;
    public static volatile SingularAttribute<Checkdept, String> status;

}