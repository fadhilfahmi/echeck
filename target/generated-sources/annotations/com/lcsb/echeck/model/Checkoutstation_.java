package com.lcsb.echeck.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-06-16T13:15:34")
@StaticMetamodel(Checkoutstation.class)
public class Checkoutstation_ { 

    public static volatile SingularAttribute<Checkoutstation, String> date;
    public static volatile SingularAttribute<Checkoutstation, String> reason;
    public static volatile SingularAttribute<Checkoutstation, String> locationID;
    public static volatile SingularAttribute<Checkoutstation, String> approvedate;
    public static volatile SingularAttribute<Checkoutstation, String> id;
    public static volatile SingularAttribute<Checkoutstation, String> checkID;
    public static volatile SingularAttribute<Checkoutstation, String> timeout;
    public static volatile SingularAttribute<Checkoutstation, String> approvetime;
    public static volatile SingularAttribute<Checkoutstation, String> staffID;
    public static volatile SingularAttribute<Checkoutstation, String> timein;
    public static volatile SingularAttribute<Checkoutstation, String> approveID;
    public static volatile SingularAttribute<Checkoutstation, String> status;

}