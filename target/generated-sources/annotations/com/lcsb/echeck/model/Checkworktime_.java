package com.lcsb.echeck.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-06-16T13:15:34")
@StaticMetamodel(Checkworktime.class)
public class Checkworktime_ { 

    public static volatile SingularAttribute<Checkworktime, String> start;
    public static volatile SingularAttribute<Checkworktime, String> end;
    public static volatile SingularAttribute<Checkworktime, String> id;
    public static volatile SingularAttribute<Checkworktime, String> descp;

}