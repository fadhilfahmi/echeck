package com.lcsb.echeck.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-06-16T13:15:34")
@StaticMetamodel(LeaveRequest.class)
public class LeaveRequest_ { 

    public static volatile SingularAttribute<LeaveRequest, String> daterequest;
    public static volatile SingularAttribute<LeaveRequest, String> requestto;
    public static volatile SingularAttribute<LeaveRequest, String> leaveID;
    public static volatile SingularAttribute<LeaveRequest, Integer> id;
    public static volatile SingularAttribute<LeaveRequest, String> timerequest;
    public static volatile SingularAttribute<LeaveRequest, Boolean> status;
    public static volatile SingularAttribute<LeaveRequest, String> datetochange;

}