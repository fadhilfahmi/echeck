package com.lcsb.echeck.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-06-16T13:15:34")
@StaticMetamodel(Login.class)
public class Login_ { 

    public static volatile SingularAttribute<Login, String> password;
    public static volatile SingularAttribute<Login, String> level;
    public static volatile SingularAttribute<Login, Integer> levelId;
    public static volatile SingularAttribute<Login, String> staffName;
    public static volatile SingularAttribute<Login, Integer> id;
    public static volatile SingularAttribute<Login, String> user;
    public static volatile SingularAttribute<Login, String> staffId;
    public static volatile SingularAttribute<Login, String> email;

}