package com.lcsb.echeck.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-06-16T13:15:34")
@StaticMetamodel(LeaveInfo.class)
public class LeaveInfo_ { 

    public static volatile SingularAttribute<LeaveInfo, Integer> bf;
    public static volatile SingularAttribute<LeaveInfo, String> year;
    public static volatile SingularAttribute<LeaveInfo, Integer> mc;
    public static volatile SingularAttribute<LeaveInfo, Integer> id;
    public static volatile SingularAttribute<LeaveInfo, Integer> eligibleleave;
    public static volatile SingularAttribute<LeaveInfo, String> staffID;

}