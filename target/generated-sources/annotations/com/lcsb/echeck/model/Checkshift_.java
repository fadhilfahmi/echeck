package com.lcsb.echeck.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-06-16T13:15:34")
@StaticMetamodel(Checkshift.class)
public class Checkshift_ { 

    public static volatile SingularAttribute<Checkshift, String> approvedbyDate;
    public static volatile SingularAttribute<Checkshift, Boolean> approve;
    public static volatile SingularAttribute<Checkshift, String> syifID;
    public static volatile SingularAttribute<Checkshift, Boolean> active;
    public static volatile SingularAttribute<Checkshift, String> approvedbyID;
    public static volatile SingularAttribute<Checkshift, Integer> id;
    public static volatile SingularAttribute<Checkshift, String> staffID;

}