package com.lcsb.echeck.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-06-16T13:15:34")
@StaticMetamodel(EstateInfo.class)
public class EstateInfo_ { 

    public static volatile SingularAttribute<EstateInfo, String> address;
    public static volatile SingularAttribute<EstateInfo, String> estatecode;
    public static volatile SingularAttribute<EstateInfo, String> city;
    public static volatile SingularAttribute<EstateInfo, String> phone;
    public static volatile SingularAttribute<EstateInfo, String> district;
    public static volatile SingularAttribute<EstateInfo, String> postcode;
    public static volatile SingularAttribute<EstateInfo, String> estatedescp;
    public static volatile SingularAttribute<EstateInfo, String> state;
    public static volatile SingularAttribute<EstateInfo, String> fax;

}