package com.lcsb.echeck.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-06-16T13:15:34")
@StaticMetamodel(LeaveComment.class)
public class LeaveComment_ { 

    public static volatile SingularAttribute<LeaveComment, String> date;
    public static volatile SingularAttribute<LeaveComment, String> leaveID;
    public static volatile SingularAttribute<LeaveComment, String> comment;
    public static volatile SingularAttribute<LeaveComment, Integer> id;
    public static volatile SingularAttribute<LeaveComment, String> time;
    public static volatile SingularAttribute<LeaveComment, String> staffID;

}