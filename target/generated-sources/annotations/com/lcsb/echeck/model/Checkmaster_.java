package com.lcsb.echeck.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-06-16T13:15:34")
@StaticMetamodel(Checkmaster.class)
public class Checkmaster_ { 

    public static volatile SingularAttribute<Checkmaster, String> date;
    public static volatile SingularAttribute<Checkmaster, String> timescan;
    public static volatile SingularAttribute<Checkmaster, String> timevalid;
    public static volatile SingularAttribute<Checkmaster, Double> temperature;
    public static volatile SingularAttribute<Checkmaster, String> id;
    public static volatile SingularAttribute<Checkmaster, String> staffID;
    public static volatile SingularAttribute<Checkmaster, String> timeout;
    public static volatile SingularAttribute<Checkmaster, String> status;

}