package com.lcsb.echeck.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-06-16T13:15:34")
@StaticMetamodel(Leave.class)
public class Leave_ { 

    public static volatile SingularAttribute<Leave, String> reason;
    public static volatile SingularAttribute<Leave, String> period;
    public static volatile SingularAttribute<Leave, String> dateapply;
    public static volatile SingularAttribute<Leave, String> year;
    public static volatile SingularAttribute<Leave, String> hrDate;
    public static volatile SingularAttribute<Leave, String> dateend;
    public static volatile SingularAttribute<Leave, String> type;
    public static volatile SingularAttribute<Leave, String> supervisorID;
    public static volatile SingularAttribute<Leave, String> checkDate;
    public static volatile SingularAttribute<Leave, String> headID;
    public static volatile SingularAttribute<Leave, String> datestart;
    public static volatile SingularAttribute<Leave, String> hrID;
    public static volatile SingularAttribute<Leave, String> supervisorDate;
    public static volatile SingularAttribute<Leave, Integer> days;
    public static volatile SingularAttribute<Leave, String> headDate;
    public static volatile SingularAttribute<Leave, Integer> stafflevel;
    public static volatile SingularAttribute<Leave, String> leaveID;
    public static volatile SingularAttribute<Leave, String> checkID;
    public static volatile SingularAttribute<Leave, String> staffID;
    public static volatile SingularAttribute<Leave, String> status;

}