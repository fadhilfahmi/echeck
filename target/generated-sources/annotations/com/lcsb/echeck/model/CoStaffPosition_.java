package com.lcsb.echeck.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-06-16T13:15:34")
@StaticMetamodel(CoStaffPosition.class)
public class CoStaffPosition_ { 

    public static volatile SingularAttribute<CoStaffPosition, Integer> level;
    public static volatile SingularAttribute<CoStaffPosition, Integer> id;
    public static volatile SingularAttribute<CoStaffPosition, Integer> dorder;
    public static volatile SingularAttribute<CoStaffPosition, String> descp;

}