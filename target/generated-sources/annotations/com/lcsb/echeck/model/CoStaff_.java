package com.lcsb.echeck.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-06-16T13:15:34")
@StaticMetamodel(CoStaff.class)
public class CoStaff_ { 

    public static volatile SingularAttribute<CoStaff, String> flag;
    public static volatile SingularAttribute<CoStaff, String> city;
    public static volatile SingularAttribute<CoStaff, String> hp;
    public static volatile SingularAttribute<CoStaff, String> title;
    public static volatile SingularAttribute<CoStaff, String> oldic;
    public static volatile SingularAttribute<CoStaff, String> imageURL;
    public static volatile SingularAttribute<CoStaff, String> ic;
    public static volatile SingularAttribute<CoStaff, String> pposition;
    public static volatile SingularAttribute<CoStaff, String> pobirth;
    public static volatile SingularAttribute<CoStaff, Integer> id;
    public static volatile SingularAttribute<CoStaff, String> state;
    public static volatile SingularAttribute<CoStaff, String> fax;
    public static volatile SingularAttribute<CoStaff, String> department;
    public static volatile SingularAttribute<CoStaff, String> staffid;
    public static volatile SingularAttribute<CoStaff, String> email;
    public static volatile SingularAttribute<CoStaff, String> workertype;
    public static volatile SingularAttribute<CoStaff, String> citizen;
    public static volatile SingularAttribute<CoStaff, String> address;
    public static volatile SingularAttribute<CoStaff, String> race;
    public static volatile SingularAttribute<CoStaff, String> sex;
    public static volatile SingularAttribute<CoStaff, String> departmentID;
    public static volatile SingularAttribute<CoStaff, String> postcode;
    public static volatile SingularAttribute<CoStaff, String> birth;
    public static volatile SingularAttribute<CoStaff, String> religion;
    public static volatile SingularAttribute<CoStaff, String> marital;
    public static volatile SingularAttribute<CoStaff, String> phone;
    public static volatile SingularAttribute<CoStaff, String> name;
    public static volatile SingularAttribute<CoStaff, String> location;
    public static volatile SingularAttribute<CoStaff, String> position;
    public static volatile SingularAttribute<CoStaff, String> category;
    public static volatile SingularAttribute<CoStaff, String> datejoin;
    public static volatile SingularAttribute<CoStaff, String> remarks;
    public static volatile SingularAttribute<CoStaff, String> status;

}