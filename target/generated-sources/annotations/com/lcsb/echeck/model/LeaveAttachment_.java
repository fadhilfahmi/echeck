package com.lcsb.echeck.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-06-16T13:15:34")
@StaticMetamodel(LeaveAttachment.class)
public class LeaveAttachment_ { 

    public static volatile SingularAttribute<LeaveAttachment, String> filename;
    public static volatile SingularAttribute<LeaveAttachment, String> leaveID;
    public static volatile SingularAttribute<LeaveAttachment, Integer> id;
    public static volatile SingularAttribute<LeaveAttachment, String> sessionID;
    public static volatile SingularAttribute<LeaveAttachment, String> staffID;

}