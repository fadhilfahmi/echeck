package com.lcsb.echeck.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-06-16T13:15:34")
@StaticMetamodel(Members.class)
public class Members_ { 

    public static volatile SingularAttribute<Members, String> abb;
    public static volatile SingularAttribute<Members, String> compID;
    public static volatile SingularAttribute<Members, Integer> level;
    public static volatile SingularAttribute<Members, String> imageURL;
    public static volatile SingularAttribute<Members, String> memberName;
    public static volatile SingularAttribute<Members, Integer> id;
    public static volatile SingularAttribute<Members, String> position;
    public static volatile SingularAttribute<Members, String> email;
    public static volatile SingularAttribute<Members, String> memberID;

}