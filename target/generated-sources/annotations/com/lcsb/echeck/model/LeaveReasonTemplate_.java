package com.lcsb.echeck.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-06-16T13:15:34")
@StaticMetamodel(LeaveReasonTemplate.class)
public class LeaveReasonTemplate_ { 

    public static volatile SingularAttribute<LeaveReasonTemplate, String> reason;
    public static volatile SingularAttribute<LeaveReasonTemplate, Boolean> active;
    public static volatile SingularAttribute<LeaveReasonTemplate, Integer> id;
    public static volatile SingularAttribute<LeaveReasonTemplate, Integer> dorder;

}