<%-- 
    Document   : report_staff_list
    Created on : Mar 11, 2020, 9:01:28 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.eleave.model.CoStaffDepartment"%>
<%@page import="com.lcsb.eleave.dao.GeneralTerm"%>
<%@page import="com.lcsb.eleave.dao.AccountingPeriod"%>
<%@page import="com.lcsb.eleave.dao.LeaveDAO"%>
<%@page import="com.lcsb.eleave.model.Leave"%>
<%@page import="com.lcsb.eleave.dao.StaffDAO"%>
<%@page import="com.lcsb.eleave.model.CoStaff"%>
<%@page import="com.lcsb.eleave.model.LoginProfile"%>
<%@page import="com.lcsb.eleave.model.Car"%>
<%@page import="com.lcsb.eleave.dao.CarDAO"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

%>

<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {


$('#change-view').change(function (e) {
            e.preventDefault();
            var id = $(this).val();
            $(location).attr('href', 'report_staff_list.jsp?filter='+id);

            return false;
        });
        $('#gotochecklist').click(function (e) {
            e.preventDefault();
            $(location).attr('href', 'leave_list_approval.jsp');

            return false;
        });

        $('.leave-view').click(function (e) {

            var id = $(this).attr('id');

            $(location).attr('href', 'PathController?process=viewleavedetail&id=' + id);

            return false;
        });

        $('#datatable tbody').on('click', 'tr', function (e) {
            e.preventDefault();

            var id = $(this).attr('id');
            $.ajax({
                async: false,
                url: "PathController?process=addmodal&carID=" + id,
                success: function (result) {
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });


            $('#myModal').modal('toggle')
            return false;
        });

        $('#modalhere').on('click', '#editCarButton', function (e) {
            e.preventDefault();

            var id = $(this).attr('title');
            $(location).attr('href', 'conf_car_edit.jsp?id=' + id);

            return false;
        });

        $('#modalhere').on('click', '#deleteCarButton', function (e) {
            e.preventDefault();

            var id = $(this).attr('title');

            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: 'Yes, delete it!',
                closeOnConfirm: false,
            }, function () {

                $.ajax({
                    async: false,
                    url: "PathController?process=deletecar&carID=" + id,
                    success: function (result) {
                        //swal("Deleted!", "Car has been deleted.", "success");
                        $(location).attr('href', 'conf_car.jsp');
                    }
                });

            });


            return false;
        });


    });

</script>
<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>


        <div class="content-wrapper"> 

            <div class="page-content fade-in-up">
                <!--<div class="alert alert-danger alert-dismissable fade show alert-outline has-icon"><i class="la la-info-circle alert-icon"></i>

                    <div class="d-flex align-items-center justify-content-between">
                        <div><strong>Perhatian</strong><br>Ada beberapa permohonan yang perlu diambil tindakan.</div>
                <!--<div>
                    <button class="btn btn-sm btn-danger btn-rounded" id="gotochecklist"><%//= LeaveDAO.getPreparedLeave(log)%> Permohonan</button>
                </div>
            </div>
        </div>-->
                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title"><i class="fa fa-users" aria-hidden="true"></i> Senarai Anggota</div>
                        <div>
                            
                            <select class="form-control" id="change-view">
                                    <option value="none">Semua</option>
                                    <%
                                        List<CoStaffDepartment> listAlldept = (List<CoStaffDepartment>) StaffDAO.getAllDepartmentWithoutGMMD(log);
                                        int l = 0;

                                        for (CoStaffDepartment k : listAlldept) {
                                            l++;
                                            String selected = "";

                                            if (k.getId().equals(request.getParameter("filter"))) {
                                                selected = "selected";
                                            }
                                    %>
                                    <option value="<%= k.getId()%>" <%= selected%>><%= k.getDescp()%></option>
                                    <%}%>
                                </select>
                                </div>
                                
                    </div>
                    <div class="ibox-body text-center">
                        <h5><span id="countall"></span> anggota perkhidmatan</h5>

                        <%
                            List<CoStaff> listAll = (List<CoStaff>) StaffDAO.getAllStaffFilteredBy(log, "departmentID", request.getParameter("filter"));
                            int i = 0;

                           

                            for (CoStaff j : listAll) {
                                i++;
                        %>

                        <div class="col-lg-5 col-sm-12 mb-4 centered">
                            <a class="card bg-primary centered" href="javascript:;">
                                <div class="card-body flexbox">
                                    <img class="img-circle mr-3" src="<%= j.getImageURL() %>" alt="image" width="48" />
                                    <div class="text-right">
                                        <h6 class="mb-1 text-white"><%= j.getName() %></h6><small class="text-muted"><%= j.getPosition() %></small><br><small class="text-muted"><%= j.getDepartment() %></small></div>
                                </div>
                            </a>
                        </div>
                        <%}
                        %>
                        <input type="hidden" id="countall-holder" value="<%=i%>">
                    </div>
                </div>


            </div>
            <!-- END PAGE CONTENT-->
            <div id="modalhere"></div>
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
        </div>



        <jsp:include page='layout/bottom.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>

        <script>
            $(function () {
                $("[name='timeline1-option']").change(function () {
                    +this.value ? $('.timeline-1').addClass('center-orientation') : $('.timeline-1').removeClass('center-orientation');
                });
                $("[name='timeline2-option']").change(function () {
                    +this.value ? $('.timeline-2').addClass('center-orientation') : $('.timeline-2').removeClass('center-orientation');
                });
            })
        </script>
</body>
<script>
    $(function () {
        
        $('#countall').html($('#countall-holder').val());
        
        $('#datatable').DataTable({
            pageLength: 10,
            fixedHeader: true,
            responsive: true,
            "sDom": 'rtip',
            columnDefs: [{
                    targets: 'no-sort',
                    orderable: false
                }]
        });

        var table = $('#datatable').DataTable();
        $('#key-search').on('keyup', function () {
            table.search(this.value).draw();
        });
        $('#type-filter').on('change', function () {
            table.column(4).search($(this).val()).draw();
        });
    });
</script>
</html>







