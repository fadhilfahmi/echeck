<%-- 
    Document   : intro_user
    Created on : Jan 22, 2020, 10:57:48 PM
    Author     : fadhilfahmi
--%>
<%@page import="com.lcsb.echeck.dao.CheckDAO"%>
<%@page import="com.lcsb.echeck.dao.EstateDAO"%>
<%@page import="com.lcsb.echeck.model.LoginProfile"%>
<%@page import="com.lcsb.echeck.dao.StaffDAO"%>
<%@page import="com.lcsb.echeck.model.CoStaff"%>
<%@page import="java.util.List"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width initial-scale=1.0">
        <title>Sistem Permohonan Cuti LCSB</title>
        <!-- GLOBAL MAINLY STYLES-->
        <link href="./assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
        <link href="./assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
        <link href="./assets/vendors/line-awesome/css/line-awesome.min.css" rel="stylesheet" />
        <link href="./assets/vendors/themify-icons/css/themify-icons.css" rel="stylesheet" />
        <link href="./assets/vendors/animate.css/animate.min.css" rel="stylesheet" />
        <link href="./assets/vendors/toastr/toastr.min.css" rel="stylesheet" />
        <link href="./assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
        <link href="./assets/vendors/bootstrap-sweetalert/dist/sweetalert.css" rel="stylesheet" />
        <!-- PLUGINS STYLES-->
        <!-- THEME STYLES-->
        <link href="assets/css/main.css" rel="stylesheet" />


        <!-- PAGE LEVEL STYLES-->
    </head>

    <body>
        <div class="page-wrapper">
            <!-- START HEADER-->
            <header class="header  align-middle">
                <ul class="nav navbar-toolbar">
                    <li>
                        <a class="nav-link sidebar-toggler js-sidebar-toggler" href="javascript:;">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </a>
                    </li>


                    <a class="page-brand align-middle" href="Login"><img src="./assets/img/logolcsbwithtitle.png" width="85%"></a>
                    <li>
                        <a class="nav-link">
                            <span>eDaftar</span>
                        </a>
                    </li>
                </ul>
            </header>
            <!-- END HEADER-->
            <!-- START SIDEBAR-->

            <!-- END SIDEBAR-->
            <div class="content-wrapper">
                <!-- START PAGE CONTENT-->

                <div class="page-content fade-in-up col-lg-6 col-sm-12 centered">

                    <form id="form-wizard" action="javascript:;" novalidate="novalidate" name="form_intro">
                        <input type="hidden" name="deptID" value="<%= request.getParameter("deptID")%>">
                        <h6>Gejala</h6>
                        <section>
                            <h3>Adakah anda mempunyai gejala berikut :</h3>

                            <div class="form-group" id="list_staff">
                                <div class="ibox-body">
                                    <div class="form-group">
                                        <label class="checkbox">
                                            <input type="checkbox" name="gejala" value="Demam">
                                            <span class="input-span"></span>Demam</label>
                                    </div>
                                    <div class="form-group">
                                        <label class="checkbox">
                                            <input type="checkbox" name="gejala" value="Batuk Kering">
                                            <span class="input-span"></span>Batuk Kering</label>
                                    </div>
                                    <div class="form-group">
                                        <label class="checkbox">
                                            <input type="checkbox" name="gejala" value="Sakit Tekak">
                                            <span class="input-span"></span>Sakit Tekak</label>
                                    </div>
                                    <div class="form-group">
                                        <label class="checkbox">
                                            <input type="checkbox" name="gejala" value="Selsema">
                                            <span class="input-span"></span>Selsema</label>
                                    </div>
                                    <div class="form-group">
                                        <label class="checkbox">
                                            <input type="checkbox" name="gejala" value="Sesak Nafas">
                                            <span class="input-span"></span>Sesak Nafas</label>
                                    </div>
                                </div>
                                <small> <i class="fa fa-info-circle" aria-hidden="true"></i> Abaikan jika tiada dan tekan 'Seterusnya'</small>
                            </div>

                        </section>
                        <h6>Kontak</h6>
                        <section>
                            <p>Adakah anda pernah berhubung rapat dengan sebarang kluster COVID-19 yang dinyatakan oleh Kementerian
                                Kesihatan Malaysia atau Pesakit Di Bawah Siasatan (PUI) atau pesakit COVID-19 positif dalam masa 14 hari lalu?</p>
                            <div class="form-group">
                                <label class="radio radio-danger">
                                    <input type="radio" name="question2" value="Ya">
                                    <span class="input-span"></span>Ya</label>
                            </div>
                            <div class="form-group">
                                <label class="radio radio-success">
                                    <input type="radio" name="question2" value="Tidak">
                                    <span class="input-span"></span>Tidak</label>
                            </div>


                            <!--<button class="btn btn-danger btn-rounded mr-2">Ya</button><button class="btn btn-success btn-rounded">Tidak</button> -->
                            <div id="form_1">

                            </div>
                        </section>
                        <h6>Rentas</h6>
                        <section>
                            <p>Pernahkah anda ke Negara atau kawasan yang terjejas COVID-19 dalam masa 14 hari lalu?</p>
                            <div class="form-group">
                                <label class="radio radio-danger">
                                    <input type="radio" name="question3" value="Ya">
                                    <span class="input-span"></span>Ya</label>
                            </div>
                            <div class="form-group">
                                <label class="radio radio-success">
                                    <input type="radio" name="question3" value="Tidak">
                                    <span class="input-span"></span>Tidak</label>
                            </div>
                            <div id="form_2">
                            </div>
                        </section>
                        <%
                        if(CheckDAO.isStaffCheckInOutsideTheirBase(log, log.getUserID(), request.getParameter("deptID"))){
                        %>
                        <h6>K. Luar</h6>
                        <section>
                            <p>Sistem akan merekod anda sedang bekerja di luar (IBU PEJABAT). Nyatakan sebab dan tujuan anda ke sini (<%= EstateDAO.getEstateInfo(log, request.getParameter("deptID"), "estatecode").getEstatedescp() %>).</p>
                            <div class="form-group mb-4">
                                    <textarea class="form-control form-control-air" rows="3" name="reason-out-station"></textarea>
                                </div>
                        </section>
                             <h6>Suhu</h6>
                        <section>
                            <section>
                            <div class="form-group mb-4 mt-4">
                                <input class="form-control form-control-lg form-control-rounded form-control-air" type="text" name="temp" placeholder="Suhu">
                            </div>
                        </section>
                            <%}
                                %>
                    </form>

                </div>
                <!-- END PAGE CONTENT-->

            </div>
        </div>
        <!-- START SEARCH PANEL-->
        <form class="search-top-bar" action="search.html">
            <input class="form-control search-input" type="text" placeholder="Search...">
            <button class="reset input-search-icon"><i class="ti-search"></i></button>
            <button class="reset input-search-close" type="button"><i class="ti-close"></i></button>
        </form>
        <!-- END SEARCH PANEL-->
        <!-- BEGIN THEME CONFIG PANEL-->

        <!-- END THEME CONFIG PANEL-->
        <!-- BEGIN PAGA BACKDROPS-->
        <div class="sidenav-backdrop backdrop"></div>
        <div class="preloader-backdrop">
            <div class="page-preloader">Loading</div>
        </div>
        <!-- END PAGA BACKDROPS-->
        <!-- New question dialog-->

        <div id="modalpassengerdiv"></div>
        <!-- End New question dialog-->
        <!-- QUICK SIDEBAR-->

        <!-- END QUICK SIDEBAR-->
        <!-- CORE PLUGINS-->
        <script src="./assets/vendors/jquery/dist/jquery.min.js"></script>
        <script src="./assets/vendors/popper.js/dist/umd/popper.min.js"></script>
        <script src="./assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="./assets/vendors/metisMenu/dist/metisMenu.min.js"></script>
        <script src="./assets/vendors/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="./assets/vendors/jquery-idletimer/dist/idle-timer.min.js"></script>
        <script src="./assets/vendors/toastr/toastr.min.js"></script>
        <script src="./assets/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="./assets/vendors/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <!-- PAGE LEVEL PLUGINS-->
        <script src="./assets/vendors/jquery.steps/build/jquery.steps.min.js"></script>

        <script src="./assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js"></script>
        <!-- CORE SCRIPTS-->
        <script src="assets/js/app.min.js"></script>
        <!-- PAGE LEVEL SCRIPTS-->
        <script>
            $(function () {

                $('body').on('change', '#name', function (e) {

                    var n = $(this).val();
                    //alert(s);
                    //} 

                    e.stopPropagation();
                    return false;

                });
                $('#form-wizard').steps({
                    headerTag: "h6",
                    bodyTag: "section",
                    titleTemplate: '<span class="step-number">#index#</span> #title#',
                    onStepChanging: function (event, currentIndex, newIndex) {
                        var form = $(this);
                        // Always allow going backward even if the current step contains invalid fields!
                        if (currentIndex > newIndex) {
                            return true;
                        }

                        /*if (currentIndex == 0) {
                         var staffID = $('#staffIDX').val();
                         var name = $('#name').val();
                         $.ajax({
                         url: "intro_form_1.jsp?staffIDX=" + staffID + "&name=" + name,
                         success: function (result) {
                         $('#form_1').empty().html(result).hide().fadeIn(300);
                         }
                         });
                         //return false;
                         }
                         
                         if (currentIndex == 1) {
                         var staffID = $('#staffID').val();
                         $.ajax({
                         url: "intro_form_2.jsp?staffID=" + staffID,
                         success: function (result) {
                         $('#form_2').empty().html(result).hide().fadeIn(300);
                         }
                         });
                         //return false;
                         }
                         if (currentIndex == 2) {
                         var staffID = $('#staffID').val();
                         $.ajax({
                         url: "intro_form_3.jsp?staffID=" + staffID,
                         success: function (result) {
                         $('#form_3').empty().html(result).hide().fadeIn(300);
                         }
                         });
                         //return false;
                         }*/
                        // Clean up if user went backward before
                        if (currentIndex < newIndex) {
                            // To remove error styles
                            $(".body:eq(" + newIndex + ") label.error", form).remove();
                            $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                        }

                        // Disable validation on fields that are disabled or hidden.
                        form.validate().settings.ignore = ":disabled,:hidden";

                        // Start validation; Prevent going forward if false
                        return form.valid();
                    },
                    onFinishing: function (event, currentIndex) {
                        var form = $(this);
                        form.validate().settings.ignore = ":disabled";
                        return form.valid();
                    },
                    onFinished: function (event, currentIndex) {
                        //toastr.success('Submitted!');


                        var a = $("#form-wizard :input").serialize();
                        $.ajax({
                            async: true,
                            data: a,
                            type: 'POST',
                            url: "PathController?process=savecheckform",
                            success: function (result) {
                                swal({
                                    title: "Terima Kasih",
                                    text: "Sila ke proses saringan seterusnya!",
                                    type: "success"
                                }, function () {
                                    window.location = "Login";
                                });



                            }
                        });
                        //} 

                    }
                }).validate({
                    errorPlacement: function errorPlacement(error, element) {
                        error.insertAfter(element);
                    },
                    rules: {
                        question2: "required",
                        question3: "required",
                        temp: "required",
                    },
                    errorClass: "help-block error",
                    highlight: function (e) {
                        $(e).closest(".form-group").addClass("has-error")
                    },
                    unhighlight: function (e) {
                        $(e).closest(".form-group").removeClass("has-error")
                    },
                });

                $('#list_staff').on('click', '.viewmodalpassenger', function (e) {
                    e.preventDefault();

                    var id = $(this).attr('id');
                    var sessiondid = $('#sessionid').val();
                    $.ajax({
                        async: false,
                        url: "PathController?process=viewstafflist&sessionid=" + sessiondid + "&id=" + id,
                        success: function (result) {
                            $('#modalpassengerdiv').empty().html(result).hide().fadeIn(300);
                        }
                    });


                    $('#modalpassenger').modal('toggle');
                    return false;
                });

            })
        </script>
    </body>

</html>