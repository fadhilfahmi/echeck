<%-- 
    Document   : admin_shift_time
    Created on : May 31, 2020, 10:11:32 AM
    Author     : fadhilfahmi
--%>


<%@page import="com.lcsb.echeck.model.Checkworktime"%>
<%@page import="com.lcsb.echeck.model.InOutSummary"%>
<%@page import="com.lcsb.echeck.model.Check"%>
<%@page import="com.lcsb.echeck.model.Leave"%>
<%@page import="com.lcsb.echeck.dao.LeaveDAO"%>
<%@page import="com.lcsb.echeck.model.CheckReport"%>
<%-- 
    Document   : admin_summary
    Created on : May 15, 2020, 8:50:44 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.echeck.dao.AccountingPeriod"%>
<%@page import="com.lcsb.echeck.dao.CheckDAO"%>
<%@page import="com.lcsb.echeck.model.Checkmaster"%>
<%@page import="com.lcsb.echeck.dao.StaffDAO"%>
<%@page import="com.lcsb.echeck.model.CoStaff"%>
<%@page import="com.lcsb.echeck.model.LoginProfile"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

    if (log == null) {
%><script type="text/javascript">
    window.location.href = "index.jsp?linkTo=adminreport";
</script>
<%
    }

%>

<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        function printReport(data)
        {
            var h = $(window).height();
            var w = $(window).width();
            var mywindow = window.open('', 'Print', 'height=' + h + ',width=' + w + '');
            mywindow.document.write('<html><head><title>Print from eDaftar</title>');
            mywindow.document.write('<link href="./assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/line-awesome/css/line-awesome.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/themify-icons/css/themify-icons.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/animate.css/animate.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/toastr/toastr.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />');

            mywindow.document.write('<link href="assets/css/main.css" rel="stylesheet" />');


            mywindow.document.write('<link href="./assets/vendors/dataTables/datatables.min.css" rel="stylesheet" />');

            mywindow.document.write('<link href="./assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />');

            mywindow.document.write(' <link href="./assets/vendors/bootstrap-sweetalert/dist/sweetalert.css" rel="stylesheet" />');


            mywindow.document.write('<link href="./assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.css" rel="stylesheet" />');

            mywindow.document.write('<link href="./assets/vendors/multiselect/css/multi-select.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/select2/dist/css/select2.min.css" rel="stylesheet" />');



            mywindow.document.write('<link href="./assets/vendors/ion.rangeSlider/css/ion.rangeSlider.css" rel="stylesheet" />');
            mywindow.document.write(' <link href="./assets/vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/jquery-minicolors/jquery.minicolors.css" rel="stylesheet" />');

            mywindow.document.write('<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />');
            mywindow.document.write('<link href="./assets/css/pages/timeline.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/fullcalendar/dist/fullcalendar.print.min.css" rel="stylesheet" media="print" />');

            mywindow.document.write('<link href="./assets/vendors/alertifyjs/dist/css/alertify.css" rel="stylesheet" />');


            mywindow.document.write('</head><body>');
            mywindow.document.write(data);
            mywindow.document.write('</body></html>');
            //mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10
            setTimeout(function () {
                mywindow.print();
                mywindow.close();
            }, 1000);
            return true;
        }

        $('#print').click(function (e) {
            printReport($("#viewprint").html());
        });

        $('#report').click(function (e) {

            e.preventDefault();
            $(location).attr('href', 'admin_report_daily.jsp');



            return false;
        });

        $('#attendance').click(function (e) {

            e.preventDefault();
            $(location).attr('href', 'admin_summary.jsp');



            return false;
        });

        $('#datatable tbody').on('click', 'tr', function (e) {
            e.preventDefault();

            var id = $(this).attr('id');
            $.ajax({
                async: false,
                url: "PathController?process=addmodal&carID=" + id,
                success: function (result) {
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });


            $('#myModal').modal('toggle')
            return false;
        });


        $('.update-modal').click(function (e) {
            e.preventDefault();

            var id = $(this).attr('id');
            var sid = $(this).attr('type');
            $.ajax({
                async: false,
                url: "PathController?process=updatetempmodalrecord&staffid=" + id + "&id=" + sid,
                success: function (result) {
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });


            $('#myModal').modal('toggle')
            return false;
        });


        $('.activerowy').click(function (e) {
            e.preventDefault();
            //alert(b);
            var b = $(this).attr('id');
            $("#togglerow_st" + b).toggle();
            //$(this).toggleClass('info');
            // $("#togglerow_st" + b).toggleClass('warning');
        });

        $('.view-modal').click(function (e) {
       // $('.tab-content').on('click', '.view-modal', function (e) {
            e.preventDefault();

            var checkid = $(this).attr('id');
            var id = $(this).attr('href');
            $.ajax({
                async: false,
                url: "view_email_modal.jsp?id="+checkid,
                success: function (result) {
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });


            $('#myModal').modal('toggle')
            return false;
        });
        
        

    });

</script>
<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        


        <div class="content-wrapper">

            <!-- START PAGE CONTENT-->

            <div class="page-content fade-in-up">
                <div class="ibox ibox-fullheight">
                    <div class="ibox-head">
                        <div class="ibox-title">
                            <!--<button class="btn btn-primary btn-rounded" id="report">Tambah</button>-->
                        </div>
                        <div class="ibox-tools pull-right">
                            <button class="btn btn-outline-primary btn-rounded ml-2 mr-1" id="print"><i class="fa fa-print mr-2" aria-hidden="true"></i>Print</button>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12" id="viewprint">
                        <div class="ibox">
                            <div class="ibox-body" >
                                <h5 class="font-strong mb-4">Waktu Bekerja</h5>

                                <!--<div class="flexbox mb-4">
        
                                    <div class="input-group-icon input-group-icon-left mr-3">
                                        <span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
                                        <input class="form-control form-control-rounded form-control-solid" id="key-search" type="text" placeholder="Search ...">
                                    </div>
                                </div>-->
                                <div class="table-responsive row">
                                    <table class="table table-condensed">
                                        <thead>
                                            <tr>
                                                <th width="30%">Syif ID</th>
                                                <th width="5%">Keterangan</th>
                                                <th width="5%">Mula</th>
                                                <th width="5%">Akhir</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%List<Checkworktime> listAll = (List<Checkworktime>) CheckDAO.getListWorkTime(log);
                                                int i = 0;

                                                for (Checkworktime j : listAll) {
                                                    i++;

                                            %>
                                            <tr class="activerowy" id="<%=i%>">

                                                <td><%= j.getId() %></td>
                                                <td><%= j.getDescp() %></td>
                                                <td><%= j.getStart() %></td>
                                                <td><%= j.getEnd()%></td>
                                            </tr>
                                            

                                            <% }//}%>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div id="modalhere"></div>
            <jsp:include page='layout/bottom.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
            </body>
            <script>
                $(function () {
                    $('#datatable1').DataTable({
                        pageLength: 10,
                        fixedHeader: true,
                        responsive: true,
                        "sDom": 'rtip',
                        "order": [[0, "desc"]],
                        columnDefs: [{
                                targets: 'no-sort',
                                orderable: false
                            }]
                    });

                    var table = $('#datatable1').DataTable();
                    $('#key-search').on('keyup', function () {
                        table.search(this.value).draw();
                    });
                    $('#type-filter').on('change', function () {
                        table.column(4).search($(this).val()).draw();
                    });
                });
            </script>
            </html>
