<%-- 
    Document   : check_type
    Created on : May 17, 2020, 4:58:23 PM
    Author     : fadhilfahmi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $('.clickcard').click(function (e) {
            e.preventDefault();

            var id = $(this).attr('id');
            
            if(id == 'staff'){
                 $(location).attr('href', 'index.jsp?linkTo=scaninterdept&deptID=<%= request.getParameter("deptID") %>');
                
            }else if(id == 'visitor'){
                 $(location).attr('href', 'visitor_form_dept.jsp?deptID=<%= request.getParameter("deptID") %>');
            }
            
           

            return false;
        });


        $(".delete-leave").click(function (e) {

            var id = $(this).attr('id');
            swal({
                title: "Anda pasti untuk padam?",
                text: "Anda tidak akan dapat mengembalikannya semula!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: 'Ya, padam!',
                closeOnConfirm: false,
            }, function () {

                $.ajax({
                    async: false,
                    url: "PathController?process=deleteleave&id=" + id,
                    success: function (result) {
                        //swal("Deleted!", "Car has been deleted.", "success");
                        $(location).attr('href', 'leave_list.jsp');
                    }
                });

            });

            //} 

            e.stopPropagation();
            return false;
        });



        $('#section-refresh').on('click', '#gotoquestion', function (e) {
            e.preventDefault();

            var leaveid = $(this).attr('id');
            var id = $(this).attr('href');
            $(location).attr('href', 'question.jsp?process=viewleavetab&tabid=' + id);
            return false;
        });



    });

</script>
<body>
    <div class="page-wrapper">
        <header class="header  align-middle">
            <!-- START TOP-LEFT TOOLBAR-->


            <!-- END TOP-LEFT TOOLBAR-->
            <!--LOGO-->
            <a class="page-brand align-middle" href="Login"><img src="./assets/img/logolcsbwithtitle.png" width="85%"></a>

            <!-- END TOP-RIGHT TOOLBAR-->
        </header>

        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-content fade-in-up">
                <div class="col-lg-6 col-sm-12">
                    <h4 class="mb-0 text-center">Pilih Jenis Pelawat</h4>
                    <div class="container py-4">
                        <div class="card-deck-wrapper">
                            <div class="card-deck">
                                <div class="card p-2 mb-4 text-center">
                                    <a class="card-block stretched-link text-decoration-none clickcard" id="staff">
                                        <i class="fa fa-address-card text-center" style="font-size:150px" aria-hidden="true"></i>
                                        <h4 class="card-title">Kakitangan LCSB</h4>

                                    </a>
                                </div>
                                <div class="card p-2  text-center">
                                    <a class="card-block stretched-link text-decoration-none clickcard"  id="visitor">
                                        <i class="fa fa-address-card-o text-center"  style="font-size:150px" aria-hidden="true"></i>
                                        <h4 class="card-title">Pelawat Luar</h4>
                                        <!--<p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
                                        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>-->
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
        </div>
    </div>

    <jsp:include page='layout/bottom.jsp'>
        <jsp:param name="page" value="home"/>
    </jsp:include>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
        });
    </script>
</body>
</html>
