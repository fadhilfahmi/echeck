<%-- 
    Document   : admin_summary
    Created on : May 15, 2020, 8:50:44 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.echeck.dao.EstateDAO"%>
<%@page import="com.lcsb.echeck.model.Checkoutstation"%>
<%@page import="com.lcsb.echeck.model.CoStaffDepartment"%>
<%@page import="com.lcsb.echeck.dao.AccountingPeriod"%>
<%@page import="com.lcsb.echeck.dao.CheckDAO"%>
<%@page import="com.lcsb.echeck.model.Checkmaster"%>
<%@page import="com.lcsb.echeck.dao.StaffDAO"%>
<%@page import="com.lcsb.echeck.model.CoStaff"%>
<%@page import="com.lcsb.echeck.model.LoginProfile"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

    if (log == null) {
        //out.println("lalal");
        String site = new String("index.jsp?linkTo=adminsummary");
        response.setStatus(response.SC_MOVED_TEMPORARILY);
        response.setHeader("Location", site);

    } else {


%>

<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>



<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        function printReport(data)
        {
            var h = $(window).height();
            var w = $(window).width();
            var mywindow = window.open('', 'Print', 'height=' + h + ',width=' + w + '');
            mywindow.document.write('<html><head><title>Print from eDaftar</title>');
            mywindow.document.write('<link href="./assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/line-awesome/css/line-awesome.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/themify-icons/css/themify-icons.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/animate.css/animate.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/toastr/toastr.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />');

            mywindow.document.write('<link href="assets/css/main.css" rel="stylesheet" />');


            mywindow.document.write('<link href="./assets/vendors/dataTables/datatables.min.css" rel="stylesheet" />');

            mywindow.document.write('<link href="./assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />');

            mywindow.document.write(' <link href="./assets/vendors/bootstrap-sweetalert/dist/sweetalert.css" rel="stylesheet" />');


            mywindow.document.write('<link href="./assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.css" rel="stylesheet" />');

            mywindow.document.write('<link href="./assets/vendors/multiselect/css/multi-select.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/select2/dist/css/select2.min.css" rel="stylesheet" />');



            mywindow.document.write('<link href="./assets/vendors/ion.rangeSlider/css/ion.rangeSlider.css" rel="stylesheet" />');
            mywindow.document.write(' <link href="./assets/vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/jquery-minicolors/jquery.minicolors.css" rel="stylesheet" />');

            mywindow.document.write('<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />');
            mywindow.document.write('<link href="./assets/css/pages/timeline.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/fullcalendar/dist/fullcalendar.print.min.css" rel="stylesheet" media="print" />');

            mywindow.document.write('<link href="./assets/vendors/alertifyjs/dist/css/alertify.css" rel="stylesheet" />');


            mywindow.document.write('</head><body>');
            mywindow.document.write(data);
            mywindow.document.write('</body></html>');
            //mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10
            setTimeout(function () {
                mywindow.print();
                mywindow.close();
            }, 1000);
            return true;
        }

        $('#print').click(function (e) {
            printReport($("#viewprint").html());
        });
        /*var audioElement = document.createElement('audio');
         audioElement.setAttribute('src', 'http://www.uscis.gov/files/nativedocuments/Track%2093.mp3');
         audioElement.setAttribute('autoplay', 'autoplay');
         //audioElement.load()
         $.get();
         audioElement.addEventListener("load", function() {
         audioElement.play();
         }, true);
         
         
         
         
         $('.play').click(function() {
         audioElement.play();
         });
         
         
         $('.pause').click(function() {
         audioElement.pause();
         });*/





        $('#report').click(function (e) {

            e.preventDefault();
            $(location).attr('href', 'admin_report_daily.jsp');
            return false;
        });
        $('#attendance').click(function (e) {

            e.preventDefault();
            $(location).attr('href', 'admin_summary.jsp');
            return false;
        });
        $('#datatable tbody').on('click', 'tr', function (e) {
            e.preventDefault();
            var id = $(this).attr('id');
            $.ajax({
                async: false,
                url: "PathController?process=addmodal&carID=" + id,
                success: function (result) {
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });
            $('#myModal').modal('toggle')
            return false;
        });
        $('.update-modal').click(function (e) {
            e.preventDefault();
            var id = $(this).attr('id');
            var sid = $(this).attr('type');
            $.ajax({
                async: false,
                url: "PathController?process=updatetempmodalrecord&staffid=" + id + "&id=" + sid,
                success: function (result) {
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });
            $('#myModal').modal('toggle')
            return false;
        });
        $('#change-view').change(function (e) {
            e.preventDefault();
            var id = $(this).val();
            var viewdept = $('#change-dept').val();
            var inout = $('input[name="inout"]:checked').val();
            $(location).attr('href', 'admin_summary.jsp?filter=' + id + '&dept=' + viewdept + '&inout=' + inout);

            return false;
        });

        $('#change-dept').change(function (e) {
            e.preventDefault();
            var id = $(this).val();
            var viewdate = $('#change-view').val();
            var inout = $('input[name="inout"]:checked').val();
            $(location).attr('href', 'admin_summary.jsp?filter=' + viewdate + '&dept=' + id + '&inout=' + inout);

            return false;
        });

        $('.change-status').change(function (e) {
            e.preventDefault();

            //var v = $("input[name='inout']:checked").val();
            var v = $('input[name="inout"]:checked').val();

            var dept = $('#change-dept').val();
            var viewdate = $('#change-view').val();
            $(location).attr('href', 'admin_summary.jsp?filter=' + viewdate + '&dept=' + dept + '&inout=' + v);

            return false;
        });


    });

</script>
<style>
    @media print
    {    
        .no-print, .no-print *
        {
            display: none !important;
        }

        body{
            background-color: #000 !important;
            font-size: 10px !important;
        }
    }
</style>
<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>

        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>



        <div class="content-wrapper">

            <!-- <div class="row">
                 <div class="col-lg-6">
                     <div class="row"></div>
                      <div class="row"></div>
                 </div>
                 <div class="col-lg-6"></div>
             </div>-->

            <!-- START PAGE CONTENT-->

            <div class="page-content fade-in-up">
                <!-- <div class="play">Play</div>
 
 <div class="pause">Stop</div>-->
                <div class="ibox ibox-fullheight">
                    <div class="ibox-head">
                        <div class="ibox-title">
                            <button class="btn btn-primary btn-rounded mr-2" id="attendance">Kehadiran</button>
                            <button class="btn btn-outline-primary btn-rounded" id="report">Laporan</button>
                        </div>
                        <div class="ibox-tools pull-right">
                            <button class="btn btn-outline-primary btn-rounded ml-2 mr-1" id="print"><i class="fa fa-print mr-2" aria-hidden="true"></i>Print</button>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12" id="viewprint">
                        <div class="ibox ibox-fullheight">
                            <div class="ibox-head">
                                <div class="ibox-title">Senarai Kehadiran
                                    <%                                        String active1 = "";
                                        String active2 = "";
                                        String active3 = "";

                                        String check1 = "";
                                        String check2 = "";
                                        String check3 = "";
                                        if (request.getParameter("inout") != null) {
                                            if (request.getParameter("inout").equals("all")) {
                                                active1 = "active";
                                                check1 = "checked";
                                            } else if (request.getParameter("inout").equals("in")) {
                                                active2 = "active";
                                                check2 = "checked";
                                            } else if (request.getParameter("inout").equals("out")) {
                                                active3 = "active";
                                                check3 = "checked";
                                            }
                                        } else {
                                            active1 = "active";
                                            check1 = "checked";
                                        }

                                    %>
                                    <div class="btn-group no-print" data-toggle="buttons">
                                        <label class="btn btn-outline-primary btn-rounded ml-2 mr-2 <%=active1%> change-status">
                                            <input type="radio" name="inout" id="option1" value="all" <%=check1%>> All
                                        </label>
                                        <label class="btn btn-outline-blue btn-rounded mr-2 <%=active2%> change-status">
                                            <input type="radio" name="inout" id="option2" value="in" <%=check2%>> In
                                        </label>
                                        <label class="btn btn-outline-secondary btn-rounded mr-2 <%=active3%> change-status">
                                            <input type="radio" name="inout" id="option3" value="out" <%=check3%>> Out
                                        </label>
                                    </div>


                                </div>
                                <div class="ibox-tools">

                                    <div class="row  no-print">
                                        <div class="form-group mt-3 mr-2">
                                            <select class="form-control" id="change-dept">
                                                <%
                                                    String dpt = request.getParameter("dept");

                                                    if (!StaffDAO.isStaffHeadID(log) || log.getUserID().equals("P0256") || log.getUserID().equals("P0004") || log.getUserID().equals("P0702")) {

                                                %>
                                                <option value="Semua">Semua Bahagian</option>

                                                <%                                                    } else {
                                                        dpt = StaffDAO.getInfoDepartmentByHeadID(log).getId();
                                                    }

                                                    if (dpt == null) {
                                                        dpt = "Semua";
                                                    }

                                                    List<CoStaffDepartment> listAlldept = (List<CoStaffDepartment>) StaffDAO.getAllDepartmentWithoutGMMD(log);
                                                    //int l = 0;

                                                    for (CoStaffDepartment k : listAlldept) {
                                                        String selected = "";

                                                        if (k.getId().equals(request.getParameter("dept"))) {
                                                            selected = "selected";
                                                        }
                                                %>
                                                <option value="<%= k.getId()%>" <%= selected%>><%= k.getDescp()%></option>
                                                <%}%>
                                            </select>
                                        </div>
                                        <div class="form-group mt-3">
                                            <%
                                                String dtx = request.getParameter("filter");

                                                if (dtx == null) {
                                                    dtx = AccountingPeriod.getCurrentTimeStamp();
                                                }
                                            %>
                                            <select class="form-control" id="change-view">
                                                <!--<option value="Semua">Semua</option>-->
                                                <%
                                                    List<String> dt = (List<String>) CheckDAO.getTodate(log);
                                                    int l = 0;

                                                    boolean isDateExist = false;

                                                    for (String k : dt) {
                                                        String selected = "";

                                                        if (k.equals(dtx)) {
                                                            selected = "selected";
                                                        }

                                                        if (k.equals(AccountingPeriod.getCurrentTimeStamp())) {
                                                            isDateExist = true;
                                                        }
                                                %>
                                                <option value="<%= k%>" <%= selected%>><%= AccountingPeriod.fullDateMonth(k)%></option>
                                                <%}
                                                    if (!isDateExist) {

                                                        String slct = "";

                                                        if (dtx.equals(AccountingPeriod.getCurrentTimeStamp())) {
                                                            slct = "selected";
                                                        }
                                                %>
                                                <option value="<%= AccountingPeriod.getCurrentTimeStamp()%>" <%=slct%>><%= AccountingPeriod.fullDateMonth(AccountingPeriod.getCurrentTimeStamp())%></option>

                                                <%
                                                    }
                                                %>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="flexbox mt-4 ml-4">

                                <!--<div class="flexbox mb-4">
    
    
                                    <button id="addnew" class="btn btn-primary btn-air mr-4">Add Staff</button>
                                    <label class="mb-0 mr-2">Type:</label>
                                    <select class="selectpicker show-tick form-control" id="type-filter" title="Please select" data-style="btn-solid" data-width="150px">
                                        <option value="">All</option>
                                        <option>Shipped</option>
                                        <option>Completed</option>
                                        <option>Pending</option>
                                        <option>Canceled</option>
                                    </select>
                                </div>-->
                                <div class="input-group-icon input-group-icon-left mr-3 no-print">
                                    <span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
                                    <input class="form-control form-control-rounded form-control-solid" id="key-search" type="text" placeholder="Search ...">
                                </div>
                            </div>
                            <div class="ibox-body">

                                <div class="col-lg-6 col-sm-12 ">
                                    <div class="flexbox mb-4">
                                        <div class="flexbox">
                                            <span class="flexbox mr-3">
                                                <span class="mr-2 text-muted">Kehadiran</span>
                                                <span class="h5 mb-0 text-primary font-strong"><%= CheckDAO.getKehadiran(log, dtx, dpt)%></span>
                                            </span>
                                            <span class="flexbox mr-3">
                                                <span class="mr-2 text-muted">Lulus</span>
                                                <span class="h5 mb-0 text-success font-strong"><%= CheckDAO.getSumByStatus(log, dtx, "Lulus", dpt)%></span>
                                            </span>
                                            <span class="flexbox mr-3">
                                                <span class="mr-2 text-muted">Gagal</span>
                                                <span class="h5 mb-0 text-danger font-strong"><%= CheckDAO.getSumByStatus(log, dtx, "Gagal", dpt)%></span>
                                            </span>
                                            <span class="flexbox mr-3">
                                                <span class="mr-2 text-muted">Menunggu</span>
                                                <span class="h5 mb-0 text-warning font-strong"><%= CheckDAO.getSumByStatus(log, dtx, "Pending", dpt)%></span>
                                            </span>
                                        </div>
                                        <!--<a class="flexbox" href="ecommerce_orders_list.html" target="_blank">VIEW ALL<i class="ti-arrow-circle-right ml-2 font-18"></i></a>-->
                                    </div>
                                </div>
                                <div class="ibox-fullwidth-block">
                                    <table class="table table-hover"  id="datatable1">
                                        <thead class="thead-default thead-lg">
                                            <tr>
                                                <th style="display:none">ID</th>
                                                <th>ID</th>
                                                <th>Nama</th>
                                                    <%
                                                        if (dtx.equals("Semua")) {


                                                    %>
                                                <th>Tarikh</th>
                                                    <% }%>
                                                <th>Masa</th>
                                                <th>Suhu</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%
                                                List<Checkmaster> listAll = (List<Checkmaster>) CheckDAO.getListAttd(log, dtx, dpt, request.getParameter("inout"));
                                                int i = 0;

                                                for (Checkmaster j : listAll) {
                                                    i++;

                                                    CoStaff co = (CoStaff) StaffDAO.getInfo(log, j.getStaffID());
                                                    String stat = "IN";
                                                    String badgestat = "blue";

                                                    if (!j.getTimeout().equals("00:00:00")) {

                                                        stat = "OUT";
                                                        badgestat = "default";
                                                    }

                                                    Checkoutstation os = (Checkoutstation) CheckDAO.getCheckoutstation(log, j.getStaffID(), j.getId());
                                                    
                                                    if (os != null) {
                                                        stat = "OS";
                                                        badgestat = "pink";
                                                    }

                                            %>
                                            <tr>
                                                <td class="pl-4"  style="display:none">
                                                    <%= j.getId()%>
                                                </td>
                                                <td>
                                                    <%= co.getStaffid()%>
                                                </td>
                                                <td><span class="badge badge-<%=badgestat%> mr-2"><%=stat%></span><%= co.getName()%><p class="text-muted mb-0"><small><%= StaffDAO.getInfoDepartment(log, co.getDepartmentID()).getAbb()%> - <%= co.getPosition()%> </small></p>
                                                    <%
                                                        if (os != null) {
                                                        %>
                                                    <p class="text-pink mb-0"><small><%= EstateDAO.getEstateInfo(log, os.getLocationID(), "estatecode").getEstatedescp() %></small></p>
                                                <%}%>
                                                </td>
                                                        <%
                                                            if (dtx.equals("Semua")) {


                                                        %>
                                                <td><%= AccountingPeriod.fullDateMonth(j.getDate())%></td>
                                                <% }%>

                                                <td>
                                                    <p class="mb-0"><small class="text-muted ">Imbas</small> : <small><%= j.getTimescan()%></small></p>
                                                    <p class="mb-0"><small class="text-muted ">Saring</small> : <small><%= j.getTimevalid()%></small></p>
                                                    <p class="mb-0"><small class="text-muted ">Keluar</small> : <small><%= j.getTimeout()%></small></p>

                                                </td>
                                                <td><%= j.getTemperature()%>&#8451;</td>
                                                <td>
                                                    <span class="badge badge-<%= CheckDAO.getBadgeColor(j.getStatus())%> badge-pill"><%= j.getStatus()%></span>
                                                    <p><%= CheckDAO.getCheckForm(log, j.getId()).getQuestion1()%></p>

                                                </td>
                                            </tr>

                                            <%}%>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div id="modalhere"></div>
                <!-- END PAGE CONTENT-->
                <jsp:include page='layout/footer.jsp'>
                    <jsp:param name="page" value="home"/>
                </jsp:include>

            </div>

        </div>

        <jsp:include page='layout/bottom.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
</body>
<script>
    $(function () {
        $('#datatable1').DataTable({
            paging: false,
            fixedHeader: true,
            responsive: true,
            "sDom": 'rtip',
            "order": [[0, "desc"]],
            columnDefs: [{
                    "targets": [0],
                    "visible": false,
                    "searchable": false
                }]
        });
        var table = $('#datatable1').DataTable();
        $('#key-search').on('keyup', function () {
            table.search(this.value).draw();
        });
        $('#type-filter').on('change', function () {
            table.column(4).search($(this).val()).draw();
        });
    });
</script>
</html>

<%}%>
