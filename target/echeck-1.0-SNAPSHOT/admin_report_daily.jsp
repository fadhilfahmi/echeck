<%-- 
    Document   : admin_report_daily
    Created on : May 15, 2020, 9:32:18 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.echeck.model.InOutSummary"%>
<%@page import="com.lcsb.echeck.model.Check"%>
<%@page import="com.lcsb.echeck.model.Leave"%>
<%@page import="com.lcsb.echeck.dao.LeaveDAO"%>
<%@page import="com.lcsb.echeck.model.CheckReport"%>

<%@page import="com.lcsb.echeck.dao.AccountingPeriod"%>
<%@page import="com.lcsb.echeck.dao.CheckDAO"%>
<%@page import="com.lcsb.echeck.model.Checkmaster"%>
<%@page import="com.lcsb.echeck.model.Checkoutstation"%>
<%@page import="com.lcsb.echeck.dao.StaffDAO"%>
<%@page import="com.lcsb.echeck.model.CoStaff"%>
<%@page import="com.lcsb.echeck.model.LoginProfile"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

    if (log == null) {
%><script type="text/javascript">
    window.location.href = "index.jsp?linkTo=adminreport";
</script>
<%
    }

    String datetext = "";
    
if (request.getParameter("startdate") == null || request.getParameter("startdate").equals(null) || request.getParameter("startdate").equals("null")) {

}else{
    datetext = "Dari "+AccountingPeriod.fullDateMonth(request.getParameter("startdate"))+" - " +AccountingPeriod.fullDateMonth(request.getParameter("enddate"));
}

%>

<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        function printReport(data)
        {
            var h = $(window).height();
            var w = $(window).width();
            var mywindow = window.open('', 'Print', 'height=' + h + ',width=' + w + '');
            mywindow.document.write('<html><head><title>Print from eDaftar</title>');
            mywindow.document.write('<link href="./assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/line-awesome/css/line-awesome.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/themify-icons/css/themify-icons.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/animate.css/animate.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/toastr/toastr.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />');

            mywindow.document.write('<link href="assets/css/main.css" rel="stylesheet" />');


            mywindow.document.write('<link href="./assets/vendors/dataTables/datatables.min.css" rel="stylesheet" />');

            mywindow.document.write('<link href="./assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />');

            mywindow.document.write(' <link href="./assets/vendors/bootstrap-sweetalert/dist/sweetalert.css" rel="stylesheet" />');


            mywindow.document.write('<link href="./assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.css" rel="stylesheet" />');

            mywindow.document.write('<link href="./assets/vendors/multiselect/css/multi-select.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/select2/dist/css/select2.min.css" rel="stylesheet" />');



            mywindow.document.write('<link href="./assets/vendors/ion.rangeSlider/css/ion.rangeSlider.css" rel="stylesheet" />');
            mywindow.document.write(' <link href="./assets/vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/jquery-minicolors/jquery.minicolors.css" rel="stylesheet" />');

            mywindow.document.write('<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />');
            mywindow.document.write('<link href="./assets/css/pages/timeline.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/fullcalendar/dist/fullcalendar.print.min.css" rel="stylesheet" media="print" />');

            mywindow.document.write('<link href="./assets/vendors/alertifyjs/dist/css/alertify.css" rel="stylesheet" />');


            mywindow.document.write('</head><body>');
            mywindow.document.write(data);
            mywindow.document.write('</body></html>');
            //mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10
            setTimeout(function () {
                mywindow.print();
                mywindow.close();
            }, 1000);
            return true;
        }

        $('#print').click(function (e) {
            printReport($("#viewprint").html());
        });

        $('#report').click(function (e) {

            e.preventDefault();
            $(location).attr('href', 'admin_report_daily.jsp');



            return false;
        });

        $('#attendance').click(function (e) {

            e.preventDefault();
            $(location).attr('href', 'admin_summary.jsp');



            return false;
        });

        $('#datatable tbody').on('click', 'tr', function (e) {
            e.preventDefault();

            var id = $(this).attr('id');
            $.ajax({
                async: false,
                url: "PathController?process=addmodal&carID=" + id,
                success: function (result) {
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });


            $('#myModal').modal('toggle')
            return false;
        });


        $('.update-modal').click(function (e) {
            e.preventDefault();

            var id = $(this).attr('id');
            var sid = $(this).attr('type');
            $.ajax({
                async: false,
                url: "PathController?process=updatetempmodalrecord&staffid=" + id + "&id=" + sid,
                success: function (result) {
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });


            $('#myModal').modal('toggle')
            return false;
        });


        $('.activerowy').click(function (e) {
            e.preventDefault();
            //alert(b);
            var b = $(this).attr('id');
            $("#togglerow_st" + b).toggle();
            //$(this).toggleClass('info');
            // $("#togglerow_st" + b).toggleClass('warning');
        });

        $('.view-modal').click(function (e) {
            // $('.tab-content').on('click', '.view-modal', function (e) {
            e.preventDefault();

            var checkid = $(this).attr('id');
            var id = $(this).attr('href');
            $.ajax({
                async: false,
                url: "view_email_modal.jsp?id=" + checkid,
                success: function (result) {
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });


            $('#myModal').modal('toggle')
            return false;
        });

        $('#daterange_3').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true
        });
        
         $('.generate').click(function (e) {
            e.preventDefault();
            var s = $('#datestart').val();
            var e = $('#dateend').val();
            
            $(location).attr('href', 'admin_report_daily.jsp?startdate='+s+'&enddate='+e);
            
        });

    });

</script>
<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>



        <div class="content-wrapper">

            <!-- START PAGE CONTENT-->

            <div class="page-content fade-in-up">
                <div class="ibox ibox-fullheight">
                    <div class="ibox-head">
                        <div class="ibox-tools">

                            <div class="row  no-print">
                                <div class="form-group  mt-3  ml-4">
                                    <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%;"><i class="fa fa-calendar mr-2"></i>
                                        <span id="rangedate"></span><b class="caret"></b>
                                    
                                    </div>
                                    
                                    <input type="hidden" class="predefinerange" id="datestart" name="datestart" value="">
                                    <input type="hidden" class="predefinerange" id="dateend" name="dateend" value="">
                                </div>
                                <div class="form-group mt-3  ml-4">
                                    <button class="btn btn-primary btn-sm generate">Generate</button>
                                </div>
                            </div>
                        </div>
                        <div class="ibox-tools pull-right">
                            <div class="row  no-print">
                                <div class="form-group  mt-3 ">
                                    <button class="btn btn-outline-primary btn-rounded ml-2 mr-1" id="print"><i class="fa fa-print mr-2" aria-hidden="true"></i>Print</button>
                                </div>
                            </div>


                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12" id="viewprint">
                        <div class="ibox">
                            <div class="ibox-body" >
                                <h5 class="font-strong mb-4">Laporan Kehadiran & Saringan <span class="text-muted "><%=datetext%></span></h5>

                                <!--<div class="flexbox mb-4">
        
                                    <div class="input-group-icon input-group-icon-left mr-3">
                                        <span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
                                        <input class="form-control form-control-rounded form-control-solid" id="key-search" type="text" placeholder="Search ...">
                                    </div>
                                </div>-->
                                <div class="table-responsive row">
                                    <table class="table table-condensed">
                                        <thead>
                                            <tr>
                                                <th width="30%">Tarikh</th>
                                                <th width="5%">Jumlah</th>
                                                <th width="5%">Cuti</th>
                                                <th width="5%">Hadir</th>
                                                <th width="5%">Kerja Luar</th>
                                                <th width="5%">T. Hadir</th>
                                                <th width="5%">Lulus</th>
                                                <th width="5%">Gagal</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%List<CheckReport> listAll = (List<CheckReport>) CheckDAO.getReport(log, request.getParameter("startdate"), request.getParameter("enddate"));
                                                int i = 0;

                                                String year = AccountingPeriod.getCurYearByCurrentDate();
                                                
                                                

                                                int allstaff = StaffDAO.getAllStaffFilteredBy(log, "location", "IBU PEJABAT").size();

                                                for (CheckReport j : listAll) {
                                                    i++;
                                                    //int totOu = CheckDAO.getCountOutStationByDate(log, date)
                                                    int outStation = CheckDAO.getCountOutStationByDate(log, j.getDate());
                                                    int onLeave = LeaveDAO.getLeaveCountByStatusAndDateIncludeYou(log, "Approved", j.getDate());
                                                    int absent = allstaff - onLeave - j.getSumattendance() - outStation;

                                                    //LeaveInfo li = (LeaveInfo) LeaveDAO.getEligibleLeaveDetail(log, j.getStaffid());
                                                    //if(li != null){
                                                    //int total = li.getEligibleleave() +  li.getBf();
                                                    //int use = LeaveDAO.getTotalLeaveOfTheYear(log, j.getStaffid(), year);
                                                    //int bal = total - use;
                                                    //int totCS = LeaveDAO.getTotalSickLeaveUseOfTheYear(log, j.getStaffid(), year);
                                                    //CoStaff st = (CoStaff) StaffDAO.getInfo(log, j.getStaffID());

                                            %>
                                            <tr class="activerowy" id="<%=i%>">

                                                <td><%= AccountingPeriod.fullDateMonth(j.getDate())%></td>
                                                <td><%= allstaff%></td>
                                                <td><%= onLeave%></td>
                                                <td><%= j.getSumattendance()%></td>
                                                <td>
                                                    <%= outStation%>
                                                </td>
                                                <td><span class="text-danger"><%= absent%></span></td>
                                                <td>
                                                    <%= j.getLulus()%>
                                                </td>
                                                <td>
                                                    <%= j.getGagal()%>
                                                </td>
                                            </tr>
                                            <tr id="togglerow_st<%= i%>" style="display: none">
                                                <td colspan="8">
                                                    <div class="col-lg-12">
                                                        <div class="ibox">
                                                            <div class="ibox-head">
                                                                <!--<div class="ibox-title">Line Tabs</div>-->
                                                                <ul class="nav nav-tabs tabs-line  tabs-line-warning">
                                                                    <li class="nav-item">
                                                                        <%
                                                                            int totLeave = LeaveDAO.getAllLeaveToday(log, j.getDate()).size();
                                                                        %>
                                                                        <a class="nav-link active" href="#tab-2-1-<%= i%>" data-toggle="tab">Cuti  <%if (totLeave > 0) {%><span class="badge badge-primary badge-circle ml-1"><%= totLeave%></span><%}%></a>
                                                                    </li>

                                                                    <li class="nav-item">
                                                                        <a class="nav-link" href="#tab-2-2-<%= i%>" data-toggle="tab">Kerja Luar</a>
                                                                    </li>
                                                                    <li class="nav-item">
                                                                        <%
                                                                            int totAbsent = CheckDAO.getAllStaffAbsent(log, "location", "IBU PEJABAT", j.getDate()).size();
                                                                        %>

                                                                        <a class="nav-link" href="#tab-2-3-<%= i%>" data-toggle="tab">Tidak hadir<%if (totAbsent > 0) {%><span class="badge badge-primary badge-circle ml-1"><%= totAbsent%></span><%}%></a>
                                                                    </li>

                                                                    <li class="nav-item">
                                                                        <a class="nav-link" href="#tab-2-4-<%= i%>" data-toggle="tab">LI/EO</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="ibox-body">
                                                                <div class="tab-content">
                                                                    <div class="tab-pane fade show active" id="tab-2-1-<%= i%>">
                                                                        <ul class="media-list media-list-divider ml-4 mr-2" >




                                                                            <%
                                                                                List<Leave> listdeptleave = (List<Leave>) LeaveDAO.getAllLeaveToday(log, j.getDate());
                                                                                int t = 0;
                                                                                if (listdeptleave.isEmpty()) {

                                                                            %>
                                                                            <li class="media align-items-center">
                                                                                <span class="text-danger ">Tiada rekod cuti</span>

                                                                            </li>
                                                                            <%                                            }

                                                                                for (Leave r : listdeptleave) {
                                                                                    t++;

                                                                                    CoStaff st = (CoStaff) StaffDAO.getInfo(log, r.getStaffID());


                                                                            %> <li class="media align-items-center">
                                                                                <a class="media-img" href="javascript:;">
                                                                                    <img class="img-circle" src="<%= st.getImageURL()%>" alt="image" width="54" />
                                                                                </a>
                                                                                <div class="media-body d-flex align-items-center">
                                                                                    <div class="flex-1">
                                                                                        <div class="media-heading"><%= st.getName()%></div><small class="text-muted"><%= StaffDAO.getInfoDepartment(log, st.getDepartmentID()).getAbb()%> - <%= st.getPosition()%></small></div>

                                                                                </div>
                                                                            </li>



                                                                            <%}%>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="tab-pane fade" id="tab-2-2-<%= i%>">
                                                                        <ul class="media-list media-list-divider ml-4 mr-2" >




                                                                            <%
                                                                                List<Checkoutstation> listoutstation = (List<Checkoutstation>) CheckDAO.getAllOutStationByDate(log, j.getDate());
                                                                                int z = 0;
                                                                                if (listoutstation.isEmpty()) {

                                                                            %>
                                                                            <li class="media align-items-center">
                                                                                <span class="text-danger ">Tiada rekod kerja luar</span>

                                                                            </li>
                                                                            <%                                            }

                                                                                for (Checkoutstation r : listoutstation) {
                                                                                    t++;

                                                                                    CoStaff st = (CoStaff) StaffDAO.getInfo(log, r.getStaffID());


                                                                            %> <li class="media align-items-center">
                                                                                <a class="media-img" href="javascript:;">
                                                                                    <img class="img-circle" src="<%= st.getImageURL()%>" alt="image" width="54" />
                                                                                </a>
                                                                                <div class="media-body d-flex align-items-center">
                                                                                    <div class="flex-1">
                                                                                        <div class="media-heading"><%= st.getName()%></div><small class="text-muted"><%= StaffDAO.getInfoDepartment(log, st.getDepartmentID()).getAbb()%> - <%= st.getPosition()%></small></div>

                                                                                </div>
                                                                            </li>



                                                                            <%}%>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="tab-pane fade" id="tab-2-3-<%= i%>">
                                                                        <ul class="media-list media-list-divider ml-4 mr-2" >




                                                                            <%
                                                                                List<CoStaff> liststaffabsent = (List<CoStaff>) CheckDAO.getAllStaffAbsent(log, "location", "IBU PEJABAT", j.getDate());
                                                                                int v = 0;
                                                                                if (liststaffabsent.isEmpty()) {

                                                                            %>
                                                                            <li class="media align-items-center">
                                                                                <span class="text-danger ">Tiada rekod tidak hadir</span>

                                                                            </li>
                                                                            <%                                            }

                                                                                for (CoStaff d : liststaffabsent) {
                                                                                    v++;

                                                                                    //CoStaff st = (CoStaff) StaffDAO.getInfo(log, d.getStaffID());

                                                                            %> <li class="media align-items-center">
                                                                                <a class="media-img" href="javascript:;">
                                                                                    <img class="img-circle" src="<%= d.getImageURL()%>" alt="image" width="54" />
                                                                                </a>
                                                                                <div class="media-body d-flex align-items-center">
                                                                                    <div class="flex-1">
                                                                                        <div class="media-heading"><%= d.getName()%></div><small class="text-muted"><%= StaffDAO.getInfoDepartment(log, d.getDepartmentID()).getAbb()%> - <%= d.getPosition()%></small></div>

                                                                                </div>
                                                                            </li>



                                                                            <%}%>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="tab-pane fade" id="tab-2-4-<%= i%>">
                                                                        <ul class="media-list media-list-divider ml-2 mr-2" >




                                                                            <%
                                                                                List<InOutSummary> liststaffLateIn = (List<InOutSummary>) CheckDAO.getAllLateInEarlyOut(log, j.getDate());
                                                                                int w = 0;
                                                                                if (liststaffLateIn.isEmpty()) {

                                                                            %>
                                                                            <li class="media align-items-center">
                                                                                <span class="text-danger ">Tiada rekod LI/EO</span>

                                                                            </li>
                                                                            <%                                            }

                                                                                for (InOutSummary d : liststaffLateIn) {
                                                                                    t++;

                                                                                    Checkmaster cm = d.getCm();
                                                                                    CoStaff st = (CoStaff) StaffDAO.getInfo(log, cm.getStaffID());

                                                                                    //InOutSummary io = (InOutSummary) CheckDAO.getInOutSummary(log, cm);

                                                                            %> <li class="media align-items-center">
                                                                                <a class="media-img" href="javascript:;">
                                                                                    <img class="img-circle" src="<%= st.getImageURL()%>" alt="image" width="54" />
                                                                                </a>
                                                                                <div class="media-body align-items-center">
                                                                                    <div class="row">
                                                                                        <div class="col-lg-4 col-md-4 col-xs-12">
                                                                                            <div class="media-heading"><%= st.getName()%></div><small class="text-muted"><%= StaffDAO.getInfoDepartment(log, st.getDepartmentID()).getAbb()%> - <%= st.getPosition()%></small></div>
                                                                                        <div class="col-lg-2 col-md-2 col-xs-12" style="border-right: 1px solid rgba(0,0,0,.1);">
                                                                                            <div class="text-muted">MASUK</div>
                                                                                            <h4 class="text-<%= d.getColorLI()%> mt-1"><%= cm.getTimevalid()%></h4>
                                                                                        </div>
                                                                                        <div class="col-lg-2 col-md-2 col-xs-12" style="border-right: 1px solid rgba(0,0,0,.1);">
                                                                                            <div class="text-muted">KELUAR</div>
                                                                                            <h4 class="text-<%= d.getColorEO()%> mt-1"><%= cm.getTimeout()%></h4>
                                                                                        </div>
                                                                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                                                                            <div class="text-muted">TEMPOH</div>
                                                                                            <h4 class="text-<%= d.getColor()%> mt-1"><%= d.getWorkinghour()%></h4>
                                                                                        </div>
                                                                                        <div class="col-lg-1 col-md-1 col-xs-12">
                                                                                            <button class="btn btn-blue btn-sm view-modal" id="<%= cm.getId()%>"><i class="fa fa-envelope-o" aria-hidden="true"></i></button>

                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </li>



                                                                            <%}%>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>

                                            <% }//}%>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div id="modalhere"></div>
            <jsp:include page='layout/bottom.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
            </body>
            <script src="./assets/vendors/moment/min/moment.min.js"></script>
            <script src="./assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
            <script src="./assets/vendors/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
            <script src="./assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
            <script>
    $(function () {
        $('#datatable1').DataTable({
            pageLength: 10,
            fixedHeader: true,
            responsive: true,
            "sDom": 'rtip',
            "order": [[0, "desc"]],
            columnDefs: [{
                    targets: 'no-sort',
                    orderable: false
                }]
        });

        var table = $('#datatable1').DataTable();
        $('#key-search').on('keyup', function () {
            table.search(this.value).draw();
        });
        $('#type-filter').on('change', function () {
            table.column(4).search($(this).val()).draw();
        });

        var start = moment().subtract(29, 'days');
        var end = moment();
        function cb(start, end) {
            $('#reportrange span').html(start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY'));
            $('#datestart').val(start.format('YYYY-MM-DD'));
            $('#dateend').val(end.format('YYYY-MM-DD'));
            //$('.predefinerange').trigger("change");
        }
        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Hari Ini': [moment(), moment()],
                'Semalam': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '7 Hari Lepas': [moment().subtract(6, 'days'), moment()],
                '30 hari Lepas': [moment().subtract(29, 'days'), moment()],
                'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
                'Bulan Lepas': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);
        cb(start, end);
    });


            </script>
            </html>
