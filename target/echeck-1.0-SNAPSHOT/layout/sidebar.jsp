<%-- 
    Document   : sidebar
    Created on : Sep 29, 2019, 10:06:03 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.echeck.dao.CheckDAO"%>
<%@page import="com.lcsb.echeck.dao.StaffDAO"%>
<%@page import="com.lcsb.echeck.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    String sessionid = request.getParameter("sessionid");

%>
<!DOCTYPE html>
<nav class="page-sidebar">
    <ul class="side-menu metismenu scroller">
        <li class="active">
            <a href="Login"><i class="sidebar-item-icon ti-home"></i>
                <span class="nav-label">Paparan Utama</span></a>

        </li>
        <%
            if(CheckDAO.isMenuAccess(log, 1) || CheckDAO.isMenuAccess(log, 0)){
            %>
        
        <li>
            <a href="javascript:;"><i class="sidebar-item-icon ti-briefcase"></i>
                <span class="nav-label">Tetapan</span><i class="fa fa-angle-left arrow"></i></a>
            <ul class="nav-2-level collapse">
                <li>
                    <a href="admin_shift_time.jsp">Waktu Kerja</a>
                </li>
                <li>
                    <a href="admin_shift_staff.jsp">Pekerja Syif</a>
                </li>
                
            </ul>
        </li>
        
        <%
            }
            if(CheckDAO.isMenuAccess(log, 2) ||CheckDAO.isMenuAccess(log, 1) || CheckDAO.isMenuAccess(log, 0)){
            %>
        
        <li>
            <a href="javascript:;"><i class="sidebar-item-icon ti-briefcase"></i>
                <span class="nav-label">Ringkasan</span><i class="fa fa-angle-left arrow"></i></a>
            <ul class="nav-2-level collapse">
                <%
                if(!log.getUserID().equals("P0832")){
                %>
                <li>
                    <a href="admin_summary.jsp">Senarai Kehadiran</a>
                </li>
                <%
                    }
                %>
                <li>
                    <a href="admin_report_daily.jsp">Laporan Kehadiran</a>
                </li>
                 <%
                if(!log.getUserID().equals("P0832")){
                %>
                <li>
                    <a href="admin_report_staff.jsp">Laporan Individu</a>
                </li>
                <%
                    }
                %>
                
            </ul>
        </li>
        
        <%
            }
            if(CheckDAO.isMenuAccess(log, 0) || CheckDAO.isMenuAccess(log, 1)){
            %>
        
        <li>
            <a href="javascript:;"><i class="sidebar-item-icon ti-briefcase"></i>
                <span class="nav-label">Saringan</span><i class="fa fa-angle-left arrow"></i></a>
            <ul class="nav-2-level collapse">
                <li>
                    <a href="admin_list.jsp">Senarai Saringan</a>
                </li>
                
            </ul>
        </li>
        
        <%
            }
            %>

            <li>
            <a href="javascript:;"><i class="sidebar-item-icon ti-briefcase"></i>
                <span class="nav-label">Daftar</span><i class="fa fa-angle-left arrow"></i></a>
            <ul class="nav-2-level collapse">
                <li>
                    <a href="staff_pass.jsp">Kursus</a>
                </li>
                <li>
                    <a href="staff_shift.jsp">Kerja Luar</a>
                </li>
                
            </ul>
        </li>
        <li>
            <a href="javascript:;"><i class="sidebar-item-icon ti-briefcase"></i>
                <span class="nav-label">Peribadi</span><i class="fa fa-angle-left arrow"></i></a>
            <ul class="nav-2-level collapse">
                <li>
                    <a href="staff_pass.jsp">Pas Pekerja</a>
                </li>
                 <li>
                    <a href="staff_timeline.jsp">Hari Ini</a>
                </li>
                
            </ul>
        </li>
    </ul>
</nav>