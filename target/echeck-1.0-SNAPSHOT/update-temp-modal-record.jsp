<%-- 
    Document   : update-leave-modal-record
    Created on : Feb 27, 2020, 10:38:40 AM
    Author     : fadhilfahmi
--%>

<%-- 
    Document   : update_leave_modal
    Created on : Jan 23, 2020, 12:12:49 PM
    Author     : fadhilfahmi
--%>


<%@page import="com.lcsb.echeck.dao.AccountingPeriod"%>
<%@page import="com.lcsb.echeck.model.CoStaff"%>
<%@page import="com.lcsb.echeck.dao.StaffDAO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.lcsb.echeck.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%

    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

   
    CoStaff st = (CoStaff) StaffDAO.getInfo(log, request.getParameter("staffid"));

    

%>
<!-- PLUGINS STYLES-->
<link href="./assets/vendors/ion.rangeSlider/css/ion.rangeSlider.css" rel="stylesheet" />
<link href="./assets/vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet" />

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

       

        $(".actionto").unbind('click').bind('click', function (e) {
            sendMessage();
            
            var id = $(this).attr('id');
             //var id = $(this).attr('type');
            var a = $("#saveleaveinfo :input").serialize();
            $.ajax({
                async: true,
                data: a,
                type: 'POST',
                url: "ProcessController?process=updatetempinforecord&id=" + id,
                success: function (result) {
                    //if (result > 0) {

                    //}
                    console.log(result);
                    var ttl = "";
                    var txt = "";
                    var typ = "";
                    if (result > 0) {
                        ttl = "Gagal";
                        txt = "Tidak melepasi syarat saringan";
                        typ = "error";
                        
                    } else {
                        ttl = "Lulus";
                        txt = "Saringan telah lulus";
                        typ = "success";
                    }
                    swal({
                        title: ttl,
                        text: txt,
                        type: typ
                    }, function () {
                        $(location).attr('href', 'admin_list.jsp');
                    });

                }
            });
            //} 

            e.stopPropagation();
            return false;
        });


    });

</script>
<div class="modal fade" id="myModal">
    <div class="modal-dialog" role="document">
        <form class="modal-content" id="saveleaveinfo">
            <input type="hidden" name="destID" id="destID" value="<%//= destID%>">
            <input type="hidden" name="bookID" id="bookID" value="<%//= l.getLeaveID()%>">
            <div class="modal-header p-4">
                <h5 class="modal-title">Maklumat Saringan</h5><span class="badge badge-<%//= LeaveDAO.getBadgeColor(l.getStatus())%>"><%//= l.getStatus()%></span>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <div class="ibox">
                    <div class="ibox-body">
                        <div class="flexbox-b mb-4">
                            <img class="img-circle" src="<%= st.getImageURL()%>" alt="image" width="40" />
                            <div class="flex-1">

                                <div class="font-strong font-14">&nbsp;&nbsp;<%= st.getName()%><small class="text-muted float-right"><%//= l.getDateapply()%></small></div>
                                <div class="text-muted">&nbsp;&nbsp;<%= st.getPosition()%></div>
                                <div class="text-muted">&nbsp;&nbsp;<%= st.getLocation()%></div><br>


                            </div>



                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                
                                <div class="form-group mb-4 mt-4">
                                        <input class="form-control form-control-lg form-control-rounded form-control-air" name="temp" type="text" value="0.00" onClick="this.select();" >
                                    </div>
                            </div>

                        </div>

                    </div>
                </div>




            </div>
            <div class="modal-footer justify-content-between bg-primary-50">
                <div>
                   
                    <button class="btn btn-primary btn-rounded mr-1 actionto" title="<%//= titleValid %>" id="<%= request.getParameter("id") %>">Kemaskini</button>
                   
                    
                   

                </div>
            </div>
        </form>
    </div>
</div>


<script src="js/socket.io-2.2.0.js"></script>
<script src="js/moment-2.24.0.min.js"></script>

<script>
    var userName = 'user' + Math.floor((Math.random() * 1000) + 1);
    var socket = io('http://192.168.1.102:9092/chat?token=abc123', {
        transports: ['polling', 'websocket']
    });
    socket.on('connect', function () {
        output('<span class="connect-msg">The client has connected with the server. Username: ' + userName + '</span>');
    });
    socket.on('chat', function (data) {
        console.log('Received message', data);
        io.to(socketId).emit('hey', 'I just met you');
        output('<span class="username-msg">' + data.userName + ':</span> ' + data.message);
    });
    socket.on('disconnect', function () {
        output('<span class="disconnect-msg">The client has disconnected!</span>');
    });
    socket.on('reconnect_attempt', (attempts) => {
        console.log('Try to reconnect at ' + attempts + ' attempt(s).');
    });

    function sendDisconnect() {
        socket.disconnect();
    }

    function sendMessage() {
        var $msg = $('#msg');
        var message = $msg.val();
        $msg.val('');
        var jsonObject = {userName: userName, message: message, actionTime: new Date()};
        socket.emit('chat', jsonObject);
    }
    
    function updateStatus(){
        var $msg = $('#msg');
        var message = $msg.val();
        $msg.val('');
        var jsonObject = {userName: userName, message: message, actionTime: new Date()};
        socket.emit('chat', jsonObject);
  
    }

    function output(message) {
        var currentTime = "<span class='time'>" + moment().format('HH:mm:ss.SSS') + "</span>";
        var element = $("<div>" + currentTime + " " + message + "</div>");
        $('#console').prepend(element);
    }

    /*$(document).keydown(function (e) {
        if (e.keyCode == 13) {
            $('#send').click();
        }
    });*/
</script>
<!-- CORE SCRIPTS-->
<!-- PAGE LEVEL SCRIPTS-->