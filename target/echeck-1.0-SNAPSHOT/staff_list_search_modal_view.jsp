<%-- 
    Document   : staff_list_search_modal_view
    Created on : Jan 24, 2020, 10:03:04 AM
    Author     : fadhilfahmi
--%>


<%@page import="com.lcsb.echeck.dao.StaffDAO"%>
<%@page import="com.lcsb.echeck.model.CoStaff"%>
<%@page import="com.lcsb.echeck.model.LoginProfile"%>
<%@page import="com.lcsb.echeck.dao.MemberDAO"%>
<%@page import="com.lcsb.echeck.model.Members"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
%>
<!DOCTYPE html>
<ul class="media-list media-list-divider scroller mr-2 pre-scrollable" data-height="470px">

    <%
        List<CoStaff> listAll = (List<CoStaff>) StaffDAO.getAllStaffSearch(log, request.getParameter("keyword"));

        for (CoStaff j : listAll) {

    %>
    <li class="media thisresult-select" id="<%= j.getStaffid()%>" title="<%= j.getName()%>" href="<%= j.getImageURL() %>">
        <div class="media-body d-flex">
            <div class="flex-1">
                <div class="d-flex align-items-center font-13">
                    
                    <a class="mr-2 text--bold-shadow" href="javascript:;"><%= j.getName() %></a>
                    <!--<span class="text-muted">1 hrs ago</span>-->
                </div>
            </div>
        </div>
    </li>

    <%}
    %>
</ul>