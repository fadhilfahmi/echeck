<%-- 
    Document   : section-check
    Created on : May 12, 2020, 5:38:58 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.echeck.dao.StaffDAO"%>
<%@page import="com.lcsb.echeck.model.CoStaff"%>
<%@page import="com.lcsb.echeck.model.Checkform"%>
<%@page import="com.lcsb.echeck.model.Checkmaster"%>
<%@page import="com.lcsb.echeck.model.Check"%>
<%@page import="com.lcsb.echeck.model.LoginProfile"%>
<%@page import="com.lcsb.echeck.dao.AccountingPeriod"%>
<%@page import="com.lcsb.echeck.dao.CheckDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    CoStaff co = StaffDAO.getInfoByEmail(log, log.getEmail());

%>
<style>
    .pricing-item {
        text-align: center;
        background-color: #fff;
        margin-bottom: 26px;
    }

    .pricing-header {
        position: relative;
        padding-bottom: 30px;
    }

    .pricing-header:after {
        content: '';
        border-color: transparent;
        border-style: solid;
        border-width: 10px 10px 0 10px;
        position: absolute;
        top: 100%;
        left: 50%;
        margin-left: -10px;
    }

    .pricing-item-1 .pricing-header:after {
        border-top-color: #5c6bc0;
    }

    .pricing-item-2 .pricing-header:after {
        border-top-color: #18c5a9;
    }

    .pricing-item-3 .pricing-header:after {
        border-top-color: #ff4081;
    }

    .pricing-title {
        padding: 15px;
        background-color: rgba(0, 0, 0, .1);
        color: #fff;
    }

    .pricing-item .price {
        color: #fff;
    }

    .pricing-item .price .price-number {
        font-size: 45px;
    }

    .pricing-item .price sup {
        margin-right: 3px;
        font-size: 35px;
        top: -.5em;
    }

    .pricing-features {
        display: flex;
        flex-wrap: wrap;
        border-right: 1px solid rgba(0, 0, 0, .1);
    }

    .pricing-features .feature-item {
        flex: 50%;
        height: 140px;
        border-bottom: 1px solid rgba(0, 0, 0, .1);
        border-left: 1px solid rgba(0, 0, 0, .1);
    }

    .feature-item .feature-icon {
        display: block;
        font-size: 44px;
        padding: 20px;
    }

    .pricing-item-btn {
        background-color: transparent;
        color: #fff;
        padding: .75rem;
        width: 170px;
        border: 2px solid #fff;
        border-radius: 50px;
        box-shadow: none !important;
    }

    .pricing-item-btn:hover {
        background-color: #fff;
        color: inherit;
    }
</style>   

<%        if (!CheckDAO.isCheckingExisted(log, request.getParameter("staffID"))) {%>
<div class="text-center mb-3 mt-0"><span class="badge badge-primary text-center"><%= CheckDAO.getLocation(log, request.getParameter("deptID")) %></span></div>
<h4 class="text-center">Hi, <%= log.getFullname()%></h4>
<h6 class="text-center text-muted"><%= AccountingPeriod.getFullCurrentDate()%>, <%= AccountingPeriod.getCurrentTimeWithFormat()%></h6>



<p class="text-primary text-center"> <strong>LANGKAH-LANGKAH PENCEGAHAN COVID-19 DI TEMPAT KERJA</strong></p>

<ul class="list-group list-group-bordered mb-4">

    <li class="list-group-item flexbox border-danger">
        <span class="flexbox"><i class="fa fa-info-circle fa-2x mr-3 text-danger"></i>Lakukan pemeriksaan kendiri tahap kesihatan anda dan laporkan dengan jujur dan bertanggungjawab</span>
    </li>
    <li class="list-group-item flexbox border-danger">
        <span class="flexbox"><i class="fa fa-info-circle fa-2x mr-3 text-danger"></i>Sila jawab beberapa soalan ini sebelum masuk ke tempat kerja</span>
    </li>



</ul>


<p class="text-center mb-4"><button id="gotoquestion" type="<%= request.getParameter("deptID") %>" class="text-center btn btn-danger btn-lg btn-rounded">Mula</button></p>
<p class="text-center"></p>

<%} else {

    Check c = (Check) CheckDAO.getCheckInfoByStaffID(log, request.getParameter("staffID"));

    Checkmaster cm = c.getCm();
    Checkform cf = c.getCi();

    String q1 = "";
    String q2 = "";
    String q3 = "";

    String i1 = "";
    String i2 = "";
    String i3 = "";

    if (cf.getQuestion1().equals("")) {
        q1 = "Tidak bergejala";
        i1 = "ti-check text-success";
    } else {
        q1 = cf.getQuestion1();
        i1 = "ti-close text-danger";
    }

    if (cf.getQuestion2().equals("Tidak")) {
        q2 = "Tiada kontak rapat";
        i2 = "ti-check text-success";
    } else {
        q2 = "Mempunyai kontak rapat";;
        i2 = "ti-close text-danger";
    }

    if (cf.getQuestion3().equals("Tidak")) {
        q3 = "Tiada rentas negara";
        i3 = "ti-check text-success";
    } else {
        q3 = "Ada rentas negara";
        i3 = "ti-close text-danger";
    }

    String i4 = "ti-alert text-warning";
    if (cm.getTemperature() > 0.0) {
        if (cm.getTemperature() > 37.4) {
            i4 = "ti-close text-danger";
        } else {
            i4 = "ti-check text-success";
        }

    }
%>


<h4 class="text-center">Hi, <%= log.getFullname()%></h4>
<h6 class="text-center text-muted"><%= AccountingPeriod.getFullCurrentDate()%>, <%= AccountingPeriod.getCurrentTimeWithFormat()%></h6>



<p></p>
<% if (cm.getStatus().equals("Pending")) {%>

<div class="row text-center">
    <div class="col-lg-12 col-sm-12 text-center">
        <div class="card overflow-visible mt-0 pricing-item pricing-item-2" >

            <div class="pricing-header bg-warning">

                <h4 class="pricing-title">PAS PEKERJA</h4>
                <div class="price">
                    <span class="price-number">MENUNGGU</span>
                </div>
                <p class="mb-4 text-white">Sila ke kaunter saringan untuk lengkapkan proses.</p>
                <button class="btn pricing-item-btn">Hasil Saringan</button>
            </div>
            <div class="pricing-features">
                <span class="feature-item">
                    <a>
                        <span class="<%= i1%> feature-icon"></span>
                        <span><%= q1%></span></a>
                </span>
                <span class="feature-item">
                    <a>
                        <span class="<%= i2%> feature-icon"></span>
                        <span><%= q2%></span>
                    </a>
                </span>
                <span class="feature-item">
                    <a>
                        <span class="<%= i3%> feature-icon"></span>
                        <span><%= q3%></span>
                    </a>
                </span>
                <span class="feature-item">
                    <a>
                        <span class="<%= i4%> feature-icon"></span>
                        <span>Suhu <%= cm.getTemperature()%> &#8451;</span>
                    </a>
                </span>
            </div>
        </div>       

    </div>
</div>
<%
} else if (cm.getStatus().equals("Lulus")) {%>
<div class="row text-center">
    <div class="col-lg-12 col-sm-12 text-center">
        <div class="card overflow-visible mt-0 pricing-item pricing-item-2" >
            <div class="pricing-header bg-success">

                <h4 class="pricing-title">PAS PEKERJA</h4>
                <div class="price">
                    <span class="price-number">LULUS</span>
                </div>
                <p class="mb-4 text-white">Anda melepasi saringan kesihatan dan lulus untuk bekerja.</p>
                <button class="btn pricing-item-btn">Hasil Saringan</button>
            </div>
            <div class="pricing-features">
                <span class="feature-item">
                    <a>
                        <span class="<%= i1%> feature-icon"></span>
                        <span><%= q1%></span></a>
                </span>
                <span class="feature-item">
                    <a>
                        <span class="<%= i2%> feature-icon"></span>
                        <span><%= q2%></span>
                    </a>
                </span>
                <span class="feature-item">
                    <a>
                        <span class="<%= i3%> feature-icon"></span>
                        <span><%= q3%></span>
                    </a>
                </span>
                <span class="feature-item">
                    <a>
                        <span class="<%= i4%> feature-icon"></span>
                        <span>Suhu <%= cm.getTemperature()%> &#8451;</span>
                    </a>
                </span>
            </div>

        </div>
    </div>
</div>
<%} else if (cm.getStatus().equals("Gagal")) {%>
<div class="row text-center">
    <div class="col-lg-12 col-sm-12 text-center">
        <div class="card overflow-visible mt-4 pricing-item pricing-item-2" >


            <div class="pricing-header bg-danger">

                <h4 class="pricing-title">PAS PEKERJA P0702</h4>
                <div class="price">
                    <span class="price-number">GAGAL</span>
                </div>
                <p class="mb-4 text-white">Anda tidak melepasi saringan kesihatan dan tidak lulus untuk bekerja.</p>
                <button class="btn pricing-item-btn">Hasil Saringan</button>
            </div>
            <div class="pricing-features">
                <span class="feature-item">
                    <a>
                        <span class="<%= i1%> feature-icon"></span>
                        <span><%= q1%></span></a>
                </span>
                <span class="feature-item">
                    <a>
                        <span class="<%= i2%> feature-icon"></span>
                        <span><%= q2%></span>
                    </a>
                </span>
                <span class="feature-item">
                    <a>
                        <span class="<%= i3%> feature-icon"></span>
                        <span><%= q3%></span>
                    </a>
                </span>
                <span class="feature-item">
                    <a>
                        <span class="<%= i4%> feature-icon"></span>
                        <span>Suhu <%= cm.getTemperature()%> &#8451;</span>
                    </a>
                </span>
            </div>

        </div>      

    </div>
</div>
<%}%>

<%}%>
