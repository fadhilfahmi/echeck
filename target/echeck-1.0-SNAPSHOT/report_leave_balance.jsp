<%-- 
    Document   : report_leave_balance
    Created on : Jan 31, 2020, 10:12:54 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.eleave.dao.AccountingPeriod"%>
<%@page import="com.lcsb.eleave.model.LeaveInfo"%>
<%@page import="com.lcsb.eleave.dao.LeaveDAO"%>
<%@page import="com.lcsb.eleave.model.CoStaffLocation"%>
<%@page import="com.lcsb.eleave.dao.StaffDAO"%>
<%@page import="com.lcsb.eleave.model.CoStaff"%>
<%@page import="com.lcsb.eleave.model.LoginProfile"%>
<%@page import="com.lcsb.eleave.model.Car"%>
<%@page import="com.lcsb.eleave.dao.CarDAO"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");


%>

<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {


        $('#addnew').click(function (e) {
            e.preventDefault();
            $(location).attr('href', 'conf_car_add.jsp');

            return false;
        });

        $('#viewcarmodal').click(function (e) {

            e.preventDefault();
            $.ajax({
                async: false,
                url: "PathController?process=addmodal",
                success: function (result) {
                    //alert(result);
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });


            $('#myModal').modal('toggle')

            return false;
        });

        $('#datatable tbody').on('click', 'tr', function (e) {
            e.preventDefault();

            var id = $(this).attr('id');
            $.ajax({
                async: false,
                url: "PathController?process=addmodal&carID=" + id,
                success: function (result) {
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });


            $('#myModal').modal('toggle')
            return false;
        });

        $('#modalhere').on('click', '#editCarButton', function (e) {
            e.preventDefault();

            var id = $(this).attr('title');
            $(location).attr('href', 'conf_car_edit.jsp?id=' + id);

            return false;
        });

        $('#modalhere').on('click', '#deleteCarButton', function (e) {
            e.preventDefault();

            var id = $(this).attr('title');

            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: 'Yes, delete it!',
                closeOnConfirm: false,
            }, function () {

                $.ajax({
                    async: false,
                    url: "PathController?process=deletecar&carID=" + id,
                    success: function (result) {
                       //swal("Deleted!", "Car has been deleted.", "success");
                        $(location).attr('href', 'conf_car.jsp');
                    }
                });
                
            });


            return false;
        });


    });

</script>
<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>


        <div class="content-wrapper">

            <!-- START PAGE CONTENT-->
            
            <div class="page-content fade-in-up">
                <div class="ibox">
                    <div class="ibox-body">
                        <h5 class="font-strong mb-4">Maklumat Cuti</h5>

                        <div class="flexbox mb-4">

                            <div class="flexbox mb-4">


                                <label class="mb-0 mr-2">Location:</label>
                                <select class="selectpicker show-tick form-control" id="type-filter" title="Please select" data-style="btn-solid" data-width="150px">
                                    <option value="ALL" selected="">ALL</option>
                                    <%List<CoStaffLocation> listLoc = (List<CoStaffLocation>) StaffDAO.getAllLocation(log);
                                        int m = 0;

                                        for (CoStaffLocation j : listLoc) {
                                            m++;

                                    %>
                                    <option value="<%= j.getDescp() %>"><%= j.getDescp() %></option>
                                    <% }%>
                                </select>
                            </div>
                            <div class="input-group-icon input-group-icon-left mr-3">
                                <span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
                                <input class="form-control form-control-rounded form-control-solid" id="key-search" type="text" placeholder="Search ...">
                            </div>
                        </div>
                        <div class="table-responsive row">
                            <table class="table table-condensed">
                                <thead>
                                    <tr>
                                        <th width="5%" rowspan="2">ID</th>
                                        <th width="30%" rowspan="2">Nama</th>
                                        <th class="table-success" width="5%" colspan="5">Cuti Layak</th>
                                        <th class="table-warning" width="5%" colspan="2">Cuti Sakit</th>
                                    </tr>
                                    <tr>
                                        
                                        <th class="table-success" width="5%">Layak</th>
                                        <th class="table-success" width="5%">Tahun Lalu</th>
                                        <th class="table-success" width="5%">Jumlah</th>
                                        <th class="table-success" width="5%">Diguna</th>
                                        <th class="table-success" width="5%">Baki</th>
                                        <th class="table-warning" width="5%">Jumlah</th>
                                        <th class="table-warning" width="5%">Diguna</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%List<CoStaff> listAll = (List<CoStaff>) StaffDAO.getAllStaffFilteredBy(log, "location", "IBU PEJABAT");
                                        int i = 0;
                                        
                                        String year = AccountingPeriod.getCurYearByCurrentDate();

                                        for (CoStaff j : listAll) {
                                            i++;
                                            
                                            LeaveInfo li = (LeaveInfo) LeaveDAO.getEligibleLeaveDetail(log, j.getStaffid());
                                            
                                            if(li != null){
                                                
                                            int total = li.getEligibleleave() +  li.getBf();
                                            int use = LeaveDAO.getTotalLeaveOfTheYear(log, j.getStaffid(), year);
                                            int bal = total - use;
                                            int totCS = LeaveDAO.getTotalSickLeaveUseOfTheYear(log, j.getStaffid(), year);
                                                

                                    %>
                                    <tr id="<%= j.getStaffid()%>">
                                        <td>
                                            <a href="javascript:;"><%= j.getStaffid()%></a>
                                        </td>
                                        <td><%= j.getName()%></td>
                                        <td><%= li.getEligibleleave() %></td>
                                        <td>
                                            <%= li.getBf()%></span>
                                        </td>
                                        <td>
                                            <%= total %>
                                        </td>
                                        <td>
                                            <%= use %></span>
                                        </td>
                                        <td>
                                            <%= bal %></span>
                                        </td>
                                        <td>
                                            <%= li.getMc() %>
                                        </td>
                                        <td>
                                            <%= totCS %>
                                        </td>
                                    </tr>

                                    <% }}%>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div id="modalhere"></div>
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>

        </div>



        <jsp:include page='layout/bottom.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
</body>
<script>
    $(function () {
        $('#datatable').DataTable({
            pageLength: 10,
            fixedHeader: true,
            responsive: true,
            "sDom": 'rtip',
            columnDefs: [{
                    targets: 'no-sort',
                    orderable: false
                }]
        });

        var table = $('#datatable').DataTable();
        $('#key-search').on('keyup', function () {
            table.search(this.value).draw();
        });
        $('#type-filter').on('change', function () {
            table.column(2).search($(this).val()).draw();
        });
    });
</script>
</html>
