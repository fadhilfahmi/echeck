<%-- 
    Document   : check_record_list
    Created on : May 12, 2020, 3:17:49 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.echeck.dao.CheckDAO"%>
<%@page import="com.lcsb.echeck.model.Checkmaster"%>
<%-- 
    Document   : leave_record_list
    Created on : Feb 27, 2020, 8:59:25 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.echeck.dao.GeneralTerm"%>
<%-- 
    Document   : user_page
    Created on : Jan 22, 2020, 8:24:46 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.echeck.dao.AccountingPeriod"%>
<%@page import="com.lcsb.echeck.dao.StaffDAO"%>
<%@page import="com.lcsb.echeck.model.CoStaff"%>
<%@page import="com.lcsb.echeck.model.LoginProfile"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

    CoStaff st = (CoStaff) StaffDAO.getInfo(log, request.getParameter("id"));
    //Leave l = (Leave) LeaveDAO.getLeaveInfoDetail(log, request.getParameter("leaveID"));

    String yearApply = AccountingPeriod.getCurYearByDate(AccountingPeriod.getCurrentTimeStamp());
    String monthApply = AccountingPeriod.getCurPeriodByDate(AccountingPeriod.getCurrentTimeStamp());

    //int totLeaveUse = LeaveDAO.getTotalLeaveOfTheYear(log, st.getStaffid(), yearApply);
    //int totLeaveUseMonth = LeaveDAO.getTotalLeaveOfTheMonth(log, st.getStaffid(), yearApply, monthApply);
    //double totLeaveYear = LeaveDAO.getEligibleLeaveOfTheYear(log, st.getStaffid(), yearApply);
    //double percentYear = LeaveDAO.getPercentLeaveUseYear(log, st.getStaffid(), yearApply);
    //double percentMonth = LeaveDAO.getPercentLeaveUseMonth(log, st.getStaffid(), yearApply, monthApply, AccountingPeriod.getCurrentTimeStamp());


%>

<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {


        $('#gotochecklist').click(function (e) {
            e.preventDefault();
            $(location).attr('href', 'leave_list_approval.jsp');

            return false;
        });

        $('.addleaverecord').click(function (e) {
            e.preventDefault();
            var id = $(this).attr('id');
            $(location).attr('href', 'leave_record.jsp?id=' + id);

            return false;
        });

        $('.leave-view').click(function (e) {

            var id = $(this).attr('id');

            $(location).attr('href', 'PathController?process=viewleavedetail&id=' + id);

            return false;
        });

        $('#datatable tbody').on('click', 'tr', function (e) {
            e.preventDefault();

            var id = $(this).attr('id');
            $.ajax({
                async: false,
                url: "PathController?process=addmodal&carID=" + id,
                success: function (result) {
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });


            $('#myModal').modal('toggle')
            return false;
        });

        $('#modalhere').on('click', '#editCarButton', function (e) {
            e.preventDefault();

            var id = $(this).attr('title');
            $(location).attr('href', 'conf_car_edit.jsp?id=' + id);

            return false;
        });

        $('#modalhere').on('click', '#deleteCarButton', function (e) {
            e.preventDefault();

            var id = $(this).attr('title');

            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: 'Yes, delete it!',
                closeOnConfirm: false,
            }, function () {

                $.ajax({
                    async: false,
                    url: "PathController?process=deletecar&carID=" + id,
                    success: function (result) {
                        //swal("Deleted!", "Car has been deleted.", "success");
                        $(location).attr('href', 'conf_car.jsp');
                    }
                });

            });


            return false;
        });
        
         $('.update-modal').click(function (e) {
            e.preventDefault();

            var id = $(this).attr('id');
            $.ajax({
                async: false,
                url: "PathController?process=updateleavemodalrecord&id=" + id,
                success: function (result) {
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });


            $('#myModal').modal('toggle')
            return false;
        });
        
        $('.edit-leave').click(function (e) {
            e.preventDefault();

            var id = $(this).attr('id');
            $(location).attr('href', 'leave_record_edit.jsp?leaveID=' + id);

            return false;
        });


        $(".delete-leave").click(function (e) {

            var id = $(this).attr('id');
            var staffid = $(this).attr('href');
            swal({
                title: "Anda pasti untuk padam?",
                text: "Anda tidak akan dapat mengembalikannya semula!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: 'Ya, padam!',
                closeOnConfirm: false,
            }, function () {

                $.ajax({
                    async: false,
                    url: "PathController?process=deleteleave&id=" + id,
                    success: function (result) {
                        //swal("Deleted!", "Car has been deleted.", "success");
                        $(location).attr('href', 'leave_record_list.jsp?id='+staffid);
                    }
                });

            });

            //} 

            e.stopPropagation();
            return false;
        });
        


    });

</script>
<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>


        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-header">
                <div class="ibox flex-1">
                    <div class="ibox-body">
                        <div class="flexbox">
                            <div class="flexbox-b">
                                <div class="ml-5 mr-5">
                                    <img class="img-circle" src="<%= st.getImageURL()%>" alt="image" width="110" />
                                </div>
                                <div>
                                    <h4><%= st.getName()%></h4>
                                    <div class="text-muted font-13 mb-3">
                                        <span class="mr-3"><i class="ti-cup mr-2"></i><%= st.getEmail()%></span>
                                        <span><i class="ti-calendar mr-2"></i>12.04.2018</span>
                                    </div>
                                    <div>
                                        <span class="mr-3">
                                            <span class="badge badge-primary badge-circle mr-2 font-14" style="height:30px;width:30px;"><i class="ti-briefcase"></i></span><%= st.getLocation()%></span>
                                        <span>
                                            <span class="badge badge-pink badge-circle mr-2 font-14" style="height:30px;width:30px;"><i class="ti-cup"></i></span><%= st.getPosition()%></span>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>

                </div>
            </div>
            <div class="page-content fade-in-up">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Senarai Saringan</div>
                                
                              
                            </div>
                                <div class="ibox-body">
                                <table class="table table-condensed">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Tarikh</th>
                                            <th>Masa Saring</th>
                                            <th>Masa Pengesahan</th>
                                            <th>Status</th>
                                        </tr>
                                        
                                        <!--<tr>
                                            <th colspan="6"><span class="text-success">Cuti Tahunan</span></th>
                                        </tr>-->
                                    </thead>
                                    <tbody>
                                        <%List<Checkmaster> listAll = (List<Checkmaster>) CheckDAO.getAllForTodayByStaffID(log, st.getStaffid());
                                            int k = 0;

                                            for (Checkmaster j : listAll) {
                                                k++;

                                        %>
                                        <tr>
                                            <td><%= k%></td>
                                            <td><%=  AccountingPeriod.fullDateMonth(j.getDate())%></td>
                                            <td><%= j.getTimescan() %></td>
                                            <td><%= j.getTimevalid()%></td>
                                            <td> <span class="badge badge-<%= CheckDAO.getBadgeColor(j.getStatus())%> badge-pill"><%= j.getStatus()%></span></td>
                                            <!--<td><i class="ti-check text-success"></i></td>-->
                                            <td><a class="text-warning font-16 edit-leave" id="<%= j.getId()%>" href="javascript:;"><i class="ti-pencil"></i></a>&nbsp;&nbsp;&nbsp;<a class="text-danger font-16 delete-leave" id="<%= j.getId() %>" href="<%= j.getStaffID()%>"><i class="ti-trash"></i></a></td>
                                           
                                        </tr>
                                        <%
                                            }
                                        %>
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>




                        <div class="ibox">

                        </div>
                    </div>

                </div>
            </div>
            <!-- END PAGE CONTENT-->
            <div id="modalhere"></div>
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
        </div>



        <jsp:include page='layout/bottom.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>

        <script>
            $(function () {
                $("[name='timeline1-option']").change(function () {
                    +this.value ? $('.timeline-1').addClass('center-orientation') : $('.timeline-1').removeClass('center-orientation');
                });
                $("[name='timeline2-option']").change(function () {
                    +this.value ? $('.timeline-2').addClass('center-orientation') : $('.timeline-2').removeClass('center-orientation');
                });
            })
        </script>
</body>
<script>
    $(function () {
        $('#datatable').DataTable({
            pageLength: 10,
            fixedHeader: true,
            responsive: true,
            "sDom": 'rtip',
            columnDefs: [{
                    targets: 'no-sort',
                    orderable: false
                }]
        });

        var table = $('#datatable').DataTable();
        $('#key-search').on('keyup', function () {
            table.search(this.value).draw();
        });
        $('#type-filter').on('change', function () {
            table.column(4).search($(this).val()).draw();
        });
    });
</script>
</html>







