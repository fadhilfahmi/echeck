<%-- 
    Document   : intro_form_2
    Created on : Jan 23, 2020, 9:47:25 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.eleave.model.CoStaffDepartment"%>
<%@page import="com.lcsb.eleave.model.CoStaffLocation"%>
<%@page import="com.lcsb.eleave.model.CoStaffPosition"%>
<%@page import="com.lcsb.eleave.model.LoginProfile"%>
<%@page import="com.lcsb.eleave.dao.StaffDAO"%>
<%@page import="com.lcsb.eleave.model.CoStaff"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
%>
<!DOCTYPE html>


<div class="form-group">
    <label class="form-control-label">Nama</label>
    <select class="selectpicker form-control" id="staffID" data-live-search="true" name="position">
        <%List<CoStaffPosition> listAll = (List<CoStaffPosition>) StaffDAO.getAllPosition(log);
            int i = 0;

            for (CoStaffPosition j : listAll) {
                i++;

        %>
        <option value="<%= j.getDescp()%>"><%= j.getDescp()%></option>
        <%
            }
        %>

    </select>
</div>

<div class="form-group">
    <label class="form-control-label">Lokasi</label>
    <select class="selectpicker form-control" id="staffID" data-live-search="true" name="location">
        <%List<CoStaffLocation> listAll1 = (List<CoStaffLocation>) StaffDAO.getAllLocation(log);
            int k = 0;

            for (CoStaffLocation j : listAll1) {
                k++;

        %>
        <option value="<%= j.getDescp()%>"><%= j.getDescp()%></option>
        <%
            }
        %>

    </select>
</div>
        
        <div class="form-group">
    <label class="form-control-label">Bahagian</label>
    <select class="selectpicker form-control" id="staffID" data-live-search="true" name="department">
        <%List<CoStaffDepartment> listAll2 = (List<CoStaffDepartment>) StaffDAO.getAllDepartment(log);
            int l = 0;

            for (CoStaffDepartment j : listAll2) {
                l++;

        %>
        <option value="<%= j.getId()%>"><%= j.getDescp()%></option>
        <%
            }
        %>

    </select>
</div>


<script>
    $(function () {

        $('.selectpicker').selectpicker('render');
    })
</script>
