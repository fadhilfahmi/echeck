<%-- 
    Document   : home
    Created on : Jun 11, 2020, 8:31:38 PM
    Author     : fadhilfahmi
--%>


<%@page import="com.lcsb.echeck.model.Checkform"%>
<%@page import="com.lcsb.echeck.model.Checkmaster"%>
<%@page import="com.lcsb.echeck.model.Check"%>
<%@page import="com.lcsb.echeck.dao.CheckDAO"%>
<%@page import="com.lcsb.echeck.dao.StaffDAO"%>
<%@page import="com.lcsb.echeck.model.CoStaff"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.echeck.dao.AccountingPeriod"%>
<%@page import="com.lcsb.echeck.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

    //String balLeave = "";
    double balLeave = 0;

    String curYear = AccountingPeriod.getCurYearByCurrentDate();
    String curMonth = AccountingPeriod.getCurPeriodByCurrentDate();
    String curDate = AccountingPeriod.getCurrentTimeStamp();

    String staffID = StaffDAO.getInfoByEmail(log, log.getEmail()).getStaffid();


%>
<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {



        $(".delete-leave").click(function (e) {

            var id = $(this).attr('id');
            swal({
                title: "Anda pasti untuk padam?",
                text: "Anda tidak akan dapat mengembalikannya semula!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: 'Ya, padam!',
                closeOnConfirm: false,
            }, function () {

                $.ajax({
                    async: false,
                    url: "PathController?process=deleteleave&id=" + id,
                    success: function (result) {
                        //swal("Deleted!", "Car has been deleted.", "success");
                        $(location).attr('href', 'leave_list.jsp');
                    }
                });

            });

            //} 

            e.stopPropagation();
            return false;
        });





    });

</script>
<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>


        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-content fade-in-up">
                <div class="col-lg-6 col-sm-12" id="">
                    <div class="btn-group btn-group-lg">
                        <button class="btn btn-gradient-purple">
                            <span class="btn-icon"><i class="ti-music"></i>Music</span>
                        </button>
                        <span class="btn-label-out btn-label-out-right btn-label-out-gradient-purple pointing">145</span>
                    </div>
                </div>

            </div>
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
        </div>
    </div>

    <jsp:include page='layout/bottom.jsp'>
        <jsp:param name="page" value="home"/>
    </jsp:include>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
        });
    </script>
</body>
</html>
