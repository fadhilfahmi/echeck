<%-- 
    Document   : admin_report_staff
    Created on : Jun 2, 2020, 11:21:59 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.echeck.model.InOutSummary"%>
<%@page import="com.lcsb.echeck.model.CoStaffDepartment"%>
<%@page import="com.lcsb.echeck.dao.AccountingPeriod"%>
<%@page import="com.lcsb.echeck.dao.CheckDAO"%>
<%@page import="com.lcsb.echeck.model.Checkmaster"%>
<%@page import="com.lcsb.echeck.dao.StaffDAO"%>
<%@page import="com.lcsb.echeck.model.CoStaff"%>
<%@page import="com.lcsb.echeck.model.LoginProfile"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

    if (log == null) {
        //out.println("lalal");
        String site = new String("index.jsp?linkTo=adminsummary");
        response.setStatus(response.SC_MOVED_TEMPORARILY);
        response.setHeader("Location", site);

    } else {


%>

<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>



<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        function printReport(data)
        {
            var h = $(window).height();
            var w = $(window).width();
            var mywindow = window.open('', 'Print', 'height=' + h + ',width=' + w + '');
            mywindow.document.write('<html><head><title>Print from eDaftar</title>');
            mywindow.document.write('<link href="./assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/line-awesome/css/line-awesome.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/themify-icons/css/themify-icons.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/animate.css/animate.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/toastr/toastr.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />');

            mywindow.document.write('<link href="assets/css/main.css" rel="stylesheet" />');


            mywindow.document.write('<link href="./assets/vendors/dataTables/datatables.min.css" rel="stylesheet" />');

            mywindow.document.write('<link href="./assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />');

            mywindow.document.write(' <link href="./assets/vendors/bootstrap-sweetalert/dist/sweetalert.css" rel="stylesheet" />');


            mywindow.document.write('<link href="./assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.css" rel="stylesheet" />');

            mywindow.document.write('<link href="./assets/vendors/multiselect/css/multi-select.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/select2/dist/css/select2.min.css" rel="stylesheet" />');



            mywindow.document.write('<link href="./assets/vendors/ion.rangeSlider/css/ion.rangeSlider.css" rel="stylesheet" />');
            mywindow.document.write(' <link href="./assets/vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/jquery-minicolors/jquery.minicolors.css" rel="stylesheet" />');

            mywindow.document.write('<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />');
            mywindow.document.write('<link href="./assets/css/pages/timeline.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet" />');
            mywindow.document.write('<link href="./assets/vendors/fullcalendar/dist/fullcalendar.print.min.css" rel="stylesheet" media="print" />');

            mywindow.document.write('<link href="./assets/vendors/alertifyjs/dist/css/alertify.css" rel="stylesheet" />');


            mywindow.document.write('</head><body>');
            mywindow.document.write(data);
            mywindow.document.write('</body></html>');
            //mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10
            setTimeout(function () {
                mywindow.print();
                mywindow.close();
            }, 1000);
            return true;
        }

        $('#print').click(function (e) {
            printReport($("#viewprint").html());
        });
        /*var audioElement = document.createElement('audio');
         audioElement.setAttribute('src', 'http://www.uscis.gov/files/nativedocuments/Track%2093.mp3');
         audioElement.setAttribute('autoplay', 'autoplay');
         //audioElement.load()
         $.get();
         audioElement.addEventListener("load", function() {
         audioElement.play();
         }, true);
         
         
         
         
         $('.play').click(function() {
         audioElement.play();
         });
         
         
         $('.pause').click(function() {
         audioElement.pause();
         });*/





        $('#report').click(function (e) {

            e.preventDefault();
            $(location).attr('href', 'admin_report_daily.jsp');
            return false;
        });
        $('#attendance').click(function (e) {

            e.preventDefault();
            $(location).attr('href', 'admin_report_staff.jsp');
            return false;
        });
        $('#datatable tbody').on('click', 'tr', function (e) {
            e.preventDefault();
            var id = $(this).attr('id');
            $.ajax({
                async: false,
                url: "PathController?process=addmodal&carID=" + id,
                success: function (result) {
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });
            $('#myModal').modal('toggle')
            return false;
        });
        $('.update-modal').click(function (e) {
            e.preventDefault();
            var id = $(this).attr('id');
            var sid = $(this).attr('type');
            $.ajax({
                async: false,
                url: "PathController?process=updatetempmodalrecord&staffid=" + id + "&id=" + sid,
                success: function (result) {
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });
            $('#myModal').modal('toggle')
            return false;
        });
        $('#change-view').change(function (e) {
            e.preventDefault();
            var id = $(this).val();
            var viewdept = $('#change-dept').val();
            var inout = $('input[name="inout"]:checked').val();
            $(location).attr('href', 'admin_report_staff.jsp?filter=' + id + '&dept=' + viewdept + '&inout=' + inout);

            return false;
        });

        $('#change-dept').change(function (e) {
            e.preventDefault();
            var id = $(this).val();
            var viewdate = $('#change-view').val();
            var inout = $('input[name="inout"]:checked').val();
            $(location).attr('href', 'admin_report_staff.jsp?filter=' + viewdate + '&dept=' + id + '&inout=' + inout);

            return false;
        });

        $('#change-staff').change(function (e) {
            e.preventDefault();
            var id = $(this).val();
            var viewdate = $('#change-view').val();
            var viewdept = $('#change-dept').val();
            var inout = $('input[name="inout"]:checked').val();
            $(location).attr('href', 'admin_report_staff.jsp?filter=' + viewdate + '&dept=' + viewdept + '&staffID=' + id);

            return false;
        });

        $('.change-status').change(function (e) {
            e.preventDefault();

            //var v = $("input[name='inout']:checked").val();
            var v = $('input[name="inout"]:checked').val();

            var dept = $('#change-dept').val();
            var viewdate = $('#change-view').val();
            $(location).attr('href', 'admin_report_staff.jsp?filter=' + viewdate + '&dept=' + dept + '&inout=' + v);

            return false;
        });


    });

</script>
<style>
    @media print
    {    
        .no-print, .no-print *
        {
            display: none !important;
        }

        body{
            background-color: #000 !important;
            font-size: 10px !important;
        }
    }
</style>
<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>

        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>



        <div class="content-wrapper">

            <!-- <div class="row">
                 <div class="col-lg-6">
                     <div class="row"></div>
                      <div class="row"></div>
                 </div>
                 <div class="col-lg-6"></div>
             </div>-->

            <!-- START PAGE CONTENT-->

            <div class="page-content fade-in-up">

                <div class="row">
                    <div class="col-lg-12" id="viewprint">
                        <div class="ibox ibox-fullheight">
                            <div class="ibox-head">
                                <div class="ibox-title">
                                    <div class="ibox-tools">

                                        <div class="row  no-print">
                                            <!--<div class="form-group mt-3 mr-2">
                                            <%                                                String dtx = request.getParameter("filter");

                                                if (dtx == null) {
                                                    dtx = AccountingPeriod.getCurrentTimeStamp();
                                                }
                                            %>
                                            
                                            <%
                                                List<String> dt = (List<String>) CheckDAO.getTodate(log);
                                                int l = 0;

                                                boolean isDateExist = false;

                                                for (String k : dt) {
                                                    String selected = "";

                                                    if (k.equals(dtx)) {
                                                        selected = "selected";
                                                    }

                                                    if (k.equals(AccountingPeriod.getCurrentTimeStamp())) {
                                                        isDateExist = true;
                                                    }
                                            %>
                                            <option value="<%= k%>" <%= selected%>><%= AccountingPeriod.fullDateMonth(k)%></option>
                                            <%}
                                                if (!isDateExist) {

                                                    String slct = "";

                                                    if (dtx.equals(AccountingPeriod.getCurrentTimeStamp())) {
                                                        slct = "selected";
                                                    }
                                            %>
                                            <option value="<%= AccountingPeriod.getCurrentTimeStamp()%>" <%=slct%>><%= AccountingPeriod.fullDateMonth(AccountingPeriod.getCurrentTimeStamp())%></option>

                                            <%
                                                }
                                            %>
                                        </select>
                                    </div>-->
                                            <div class="form-group mt-3 mr-2">
                                                <select class="form-control" id="change-dept">
                                                    <%
                                                        String dpt = request.getParameter("dept");

                                                        if (!StaffDAO.isStaffHeadID(log) || log.getUserID().equals("P0256") || log.getUserID().equals("P0004") || log.getUserID().equals("P0702")) {

                                                    %>
                                                    <option value="Semua">Semua Bahagian</option>

                                                    <%                                                    } else {
                                                            dpt = StaffDAO.getInfoDepartmentByHeadID(log).getId();
                                                        }

                                                        if (dpt == null) {
                                                            dpt = "Semua";
                                                        }

                                                        List<CoStaffDepartment> listAlldept = (List<CoStaffDepartment>) StaffDAO.getAllDepartmentWithoutGMMD(log);
                                                        //int l = 0;

                                                        for (CoStaffDepartment k : listAlldept) {
                                                            String selected = "";

                                                            if (k.getId().equals(request.getParameter("dept"))) {
                                                                selected = "selected";
                                                            }
                                                    %>
                                                    <option value="<%= k.getId()%>" <%= selected%>><%= k.getDescp()%></option>
                                                    <%}%>
                                                </select>
                                            </div>
                                            <div class="form-group mt-3 mr-2">
                                                <select class="form-control" id="change-staff">

                                                    <option value="Semua">Pilih</option>
                                                    <%

                                                        List<CoStaff> listBydept = (List<CoStaff>) StaffDAO.getAllStaffByDept(log, dpt);
                                                        //int l = 0;

                                                        for (CoStaff k : listBydept) {
                                                            String selected = "";

                                                            if (k.getStaffid().equals(request.getParameter("staffID"))) {
                                                                selected = "selected";
                                                            }
                                                    %>
                                                    <option value="<%= k.getStaffid()%>" <%= selected%>><%= k.getName()%></option>
                                                    <%}%>
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="ibox-tools pull-right">
                                    <button class="btn btn-outline-primary btn-rounded ml-2 mr-1" id="print"><i class="fa fa-print mr-2" aria-hidden="true"></i>Print</button>
                                </div>

                            </div>

                            <div class="ibox-body">

                                <div class="ibox-fullwidth-block">
                                    <table class="table table-hover">
                                        <thead class="thead-default thead-lg">
                                            <tr>
                                                <th style="display:none">ID</th>
                                                <th>Tarikh</th>
                                                <th>Masuk</th>
                                                <th>Keluar</th>
                                                <th>Suhu</th>
                                                <th>Saringan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%
                                                List<Checkmaster> listAll = (List<Checkmaster>) CheckDAO.getAllForByStaffAndDateRange(log, request.getParameter("staffID"));
                                                int i = 0;

                                                for (Checkmaster j : listAll) {
                                                    i++;

                                                    

                                                    InOutSummary io = (InOutSummary) CheckDAO.getAllLateInEarlyOutByStaff(log, j.getDate(), j.getStaffID());


                                            %>
                                            <tr>
                                                <td class="pl-4"  style="display:none">
                                                    <%= j.getId()%>
                                                </td>

                                                <td><%= AccountingPeriod.fullDateMonth(j.getDate())%></td>


                                                <td><span class="text-<%= io.getColorLI() %>"><%= j.getTimevalid()%></span></td>
                                                <td><span class="text-<%= io.getColorEO()%>"><%= j.getTimeout()%></span></td>
                                                <td><%= j.getTemperature()%>&#8451;</td>
                                                <td>
                                                    <span class="badge badge-<%= CheckDAO.getBadgeColor(j.getStatus())%> badge-pill"><%= j.getStatus()%></span>
                                                </td>
                                            </tr>

                                            <%}%>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div id="modalhere"></div>
                <!-- END PAGE CONTENT-->
                <jsp:include page='layout/footer.jsp'>
                    <jsp:param name="page" value="home"/>
                </jsp:include>

            </div>

        </div>

        <jsp:include page='layout/bottom.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
</body>
<script>
    $(function () {
        $('#datatable1').DataTable({
            paging: false,
            fixedHeader: true,
            responsive: true,
            "sDom": 'rtip',
            "order": [[0, "desc"]],
            columnDefs: [{
                    "targets": [0],
                    "visible": false,
                    "searchable": false
                }]
        });
        var table = $('#datatable1').DataTable();
        $('#key-search').on('keyup', function () {
            table.search(this.value).draw();
        });
        $('#type-filter').on('change', function () {
            table.column(4).search($(this).val()).draw();
        });
    });
</script>
</html>

<%}%>
