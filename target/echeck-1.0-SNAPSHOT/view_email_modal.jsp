<%-- 
    Document   : view_email_modal
    Created on : May 28, 2020, 11:24:04 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.echeck.model.CoStaff"%>
<%@page import="com.lcsb.echeck.dao.StaffDAO"%>
<%@page import="com.lcsb.echeck.model.Checkmaster"%>
<%@page import="com.lcsb.echeck.dao.CheckDAO"%>
<%@page import="com.lcsb.echeck.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

    Checkmaster cm = (Checkmaster) CheckDAO.getInfoByCheckID(log, request.getParameter("id"));
    CoStaff st = (CoStaff) StaffDAO.getInfo(log, cm.getStaffID());
%>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $("#savebutton").unbind('click').bind('click', function (e) {
//var a = $("form").serialize();
            $(this).html('<i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i> Menghantar ... ');

            var path = $(this).attr('title');
            var process = $(this).attr('name');
            var a = $("#saveform :input").serialize();
            $('#maincontainer').empty().html('<div style="width:100px; margin:0 auto; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>').hide().fadeIn(300);
            //checkField();
            //var b = checkField();
            //if(b==false){
            $.ajax({
                async: true,
                data: a,
                type: 'POST',
                url: "PathController?process=sendemail",
                success: function (result) {
                    swal({
                        title: "Email telah dihantar",
                        text: "Sila semak e-mail untuk maklumat lanjut",
                        type: "success"
                    }, function () {
                        $('#myModal').modal('toggle')
                    });
                }
            });
            //} 

            e.stopPropagation();
            return false;
        });

    });

</script>
<div class="modal fade" id="myModal">
    <div class="modal-dialog" role="document">
        <form class="modal-content" id="saveform">
            <div class="modal-header p-4">
                <h5 class="modal-title">MESEJ BARU</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <div class="flexbox mb-4">
                    <span class="btn-icon-only btn-circle bg-primary-50 text-primary mr-3"><i class="ti-support"></i></span>
                    <div class="flex-1 d-flex">
                        <div class="flex-1">
                            <span class="text-muted mr-2">Dari:</span>
                            <div><%= log.getEmail()%><input type="hidden" name="sender" value="<%= log.getEmail()%>"></div>
                        </div>
                        <!--<div class="dropdown">
                            <a class="dropdown-toggle font-16" data-toggle="dropdown"><i class="ti-more-alt"></i></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item"><i class="ti-support"></i>support@example.com</a>
                                <a class="dropdown-item"><i class="ti-shopping-cart"></i>shopping@example.com</a>
                                <a class="dropdown-item"><i class="ti-help"></i>help@example.com</a>
                            </div>
                        </div>-->
                    </div>
                </div>
                <div class="form-group mb-4">
                    <input type="hidden" name="checkid" value="<%=request.getParameter("id")%>">
                    <input class="form-control form-control-line" type="text" name="email" placeholder="Recipient" value="<%= st.getEmail()%>">
                </div>
                <div class="form-group mb-4">
                    <input class="form-control form-control-line" type="text" name="subject" placeholder="Enter Subject" value="Makluman Rekod Waktu Keluar Masuk">
                </div>
                <div class="form-group mb-4">
                    <textarea class="form-control form-control-line" rows="4" name="body" placeholder="Masukkan mesej anda"></textarea>
                </div>
            </div>
            <div class="modal-footer justify-content-between bg-primary-50">
                <div>
                    <button class="btn btn-primary btn-rounded mr-3" id="savebutton">Hantar</button>

                </div>

            </div>
        </form>
    </div>
</div>