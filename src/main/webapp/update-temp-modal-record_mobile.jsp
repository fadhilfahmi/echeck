<%-- 
    Document   : update-leave-modal-record
    Created on : Feb 27, 2020, 10:38:40 AM
    Author     : fadhilfahmi
--%>

<%-- 
    Document   : update_leave_modal
    Created on : Jan 23, 2020, 12:12:49 PM
    Author     : fadhilfahmi
--%>


<%@page import="com.lcsb.echeck.dao.AccountingPeriod"%>
<%@page import="com.lcsb.echeck.model.CoStaff"%>
<%@page import="com.lcsb.echeck.dao.StaffDAO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.lcsb.echeck.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%

    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

   
    CoStaff st = (CoStaff) StaffDAO.getInfo(log, request.getParameter("staffid"));

    

%>
<!-- PLUGINS STYLES-->
<link href="./assets/vendors/ion.rangeSlider/css/ion.rangeSlider.css" rel="stylesheet" />
<link href="./assets/vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet" />
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $(".dial").knob();

        $(".actionto").unbind('click').bind('click', function (e) {
            
            var id = $(this).attr('id');
             //var id = $(this).attr('type');
            var a = $("#saveleaveinfo :input").serialize();
            $.ajax({
                async: true,
                data: a,
                type: 'POST',
                url: "ProcessController?process=updatetempinforecord&id=" + id,
                success: function (result) {
                    //if (result > 0) {

                    //}
                    console.log(result);
                    var ttl = "";
                    var txt = "";
                    var typ = "";
                    if (result > 0) {
                        ttl = "Gagal";
                        txt = "Tidak melepasi syarat saringan";
                        typ = "error";
                        
                    } else {
                        ttl = "Lulus";
                        txt = "Saringan telah lulus";
                        typ = "success";
                    }
                    swal({
                        title: ttl,
                        text: txt,
                        type: typ
                    }, function () {
                        $(location).attr('href', 'admin_list_mobile.jsp');
                    });

                }
            });
            //} 

            e.stopPropagation();
            return false;
        });


    });

</script>
<div class="modal fade" id="myModal">
    <div class="modal-dialog" role="document">
        <form class="modal-content" id="saveleaveinfo">
            <input type="hidden" name="destID" id="destID" value="<%//= destID%>">
            <input type="hidden" name="bookID" id="bookID" value="<%//= l.getLeaveID()%>">
            <div class="modal-header p-4">
                <h5 class="modal-title">Maklumat Saringan</h5><span class="badge badge-<%//= LeaveDAO.getBadgeColor(l.getStatus())%>"><%//= l.getStatus()%></span>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <div class="ibox">
                    <div class="ibox-body">
                        <div class="flexbox-b mb-4">
                            <img class="img-circle" src="<%= st.getImageURL()%>" alt="image" width="40" />
                            <div class="flex-1">

                                <div class="font-strong font-14">&nbsp;&nbsp;<%= st.getName()%><small class="text-muted float-right"><%//= l.getDateapply()%></small></div>
                                <div class="text-muted">&nbsp;&nbsp;<%= st.getPosition()%></div>
                                <div class="text-muted">&nbsp;&nbsp;<%= st.getLocation()%></div><br>


                            </div>



                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                
                                <div class="form-group mb-4 mt-4">
                                        <input class="form-control form-control-lg form-control-rounded form-control-air" name="temp" type="text" value="0.00" onClick="this.select();" >
                                    </div>
                            </div>

                        </div>

                    </div>
                </div>




            </div>
            <div class="modal-footer justify-content-between bg-primary-50">
                <div>
                   
                    <button class="btn btn-primary btn-rounded mr-1 actionto" title="<%//= titleValid %>" id="<%= request.getParameter("id") %>">Kemaskini</button>
                   
                    
                   

                </div>
            </div>
        </form>
    </div>
</div>

<!-- PAGE LEVEL PLUGINS-->
<script src="./assets/vendors/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- CORE SCRIPTS-->
<!-- PAGE LEVEL SCRIPTS-->