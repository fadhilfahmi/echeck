<%-- 
    Document   : user_page
    Created on : Jan 22, 2020, 8:24:46 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.eleave.dao.GeneralTerm"%>
<%@page import="com.lcsb.eleave.dao.AccountingPeriod"%>
<%@page import="com.lcsb.eleave.dao.LeaveDAO"%>
<%@page import="com.lcsb.eleave.model.Leave"%>
<%@page import="com.lcsb.eleave.dao.StaffDAO"%>
<%@page import="com.lcsb.eleave.model.CoStaff"%>
<%@page import="com.lcsb.eleave.model.LoginProfile"%>
<%@page import="com.lcsb.eleave.model.Car"%>
<%@page import="com.lcsb.eleave.dao.CarDAO"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

    CoStaff st = (CoStaff) StaffDAO.getInfo(log, request.getParameter("id"));
    //Leave l = (Leave) LeaveDAO.getLeaveInfoDetail(log, request.getParameter("leaveID"));

    String yearApply = AccountingPeriod.getCurYearByDate(AccountingPeriod.getCurrentTimeStamp());
    String monthApply = AccountingPeriod.getCurPeriodByDate(AccountingPeriod.getCurrentTimeStamp());

    int totLeaveUse = LeaveDAO.getTotalLeaveOfTheYear(log, st.getStaffid(), yearApply);
    int totLeaveUseMonth = LeaveDAO.getTotalLeaveOfTheMonth(log, st.getStaffid(), yearApply, monthApply);
    double totLeaveYear = LeaveDAO.getEligibleLeaveOfTheYear(log, st.getStaffid(), yearApply);
    double percentYear = LeaveDAO.getPercentLeaveUseYear(log, st.getStaffid(), yearApply);
    double percentMonth = LeaveDAO.getPercentLeaveUseMonth(log, st.getStaffid(), yearApply, monthApply, AccountingPeriod.getCurrentTimeStamp());
    double percentYearMedical = LeaveDAO.getPercentMedicalLeaveUseYear(log, st.getStaffid(), yearApply);

    double bakiCutiLayak = LeaveDAO.getEligibleLeaveForTheMonth(log, st.getStaffid(), AccountingPeriod.getCurYearByCurrentDate(), AccountingPeriod.getCurrentTimeStamp());



%>

<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {


        $('#gotochecklist').click(function (e) {
            e.preventDefault();
            $(location).attr('href', 'leave_list_approval.jsp');

            return false;
        });

        $('.leave-view').click(function (e) {

            var id = $(this).attr('id');

            $(location).attr('href', 'PathController?process=viewleavedetail&id=' + id);

            return false;
        });

        $('#datatable tbody').on('click', 'tr', function (e) {
            e.preventDefault();

            var id = $(this).attr('id');
            $.ajax({
                async: false,
                url: "PathController?process=addmodal&carID=" + id,
                success: function (result) {
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });


            $('#myModal').modal('toggle')
            return false;
        });

        $('#modalhere').on('click', '#editCarButton', function (e) {
            e.preventDefault();

            var id = $(this).attr('title');
            $(location).attr('href', 'conf_car_edit.jsp?id=' + id);

            return false;
        });

        $('#modalhere').on('click', '#deleteCarButton', function (e) {
            e.preventDefault();

            var id = $(this).attr('title');

            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: 'Yes, delete it!',
                closeOnConfirm: false,
            }, function () {

                $.ajax({
                    async: false,
                    url: "PathController?process=deletecar&carID=" + id,
                    success: function (result) {
                        //swal("Deleted!", "Car has been deleted.", "success");
                        $(location).attr('href', 'conf_car.jsp');
                    }
                });

            });


            return false;
        });


    });

</script>
<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>


        <div class="content-wrapper"> 

            <div class="page-content fade-in-up">
                <!--<div class="alert alert-danger alert-dismissable fade show alert-outline has-icon"><i class="la la-info-circle alert-icon"></i>

                    <div class="d-flex align-items-center justify-content-between">
                        <div><strong>Perhatian</strong><br>Ada beberapa permohonan yang perlu diambil tindakan.</div>
                <!--<div>
                    <button class="btn btn-sm btn-danger btn-rounded" id="gotochecklist"><%//= LeaveDAO.getPreparedLeave(log)%> Permohonan</button>
                </div>
            </div>
        </div>-->
                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title"><i class="fa fa-info-circle" aria-hidden="true"></i> Profil Pengguna</div>
                    </div>
                    <div class="ibox-body">
                        <ul class="nav nav-tabs tabs-line tabs-line-2x nav-fill">
                            <li class="nav-item">
                                <a class="nav-link active" href="#tab-11-1" data-toggle="tab"><i class="fa fa-id-card-o" aria-hidden="true"></i><span class="button-text-approve">  Anda</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#tab-11-2" data-toggle="tab"><i class="fa fa-calendar" aria-hidden="true"></i><span class="button-text-approve"> Cuti</span></a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="#tab-11-3" data-toggle="tab"><i class="fa fa-users" aria-hidden="true"></i><span class="button-text-approve"> Seliaan</span> 

                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade show active text-left" id="tab-11-1">

                                <div class="card text-center has-cup card-air centered mb-4" style="max-width:320px;">
                                    <div class="card-cup bg-primary"></div>
                                    <div class="card-body">
                                        <div class="card-avatar mb-4">
                                            <img class="img-circle" src="<%= log.getImageURL()%>" alt="image" />
                                        </div>
                                        <h4 class="card-title text-primary mb-1"><%= GeneralTerm.capitalizeFirstLetter(st.getName())%></h4>
                                        <div class="text-muted"><%= st.getPosition()%></div>
                                        <div class="text-muted"><%= st.getDepartment()%></div>
                                        <p class="mt-4"><span class="mr-3"><i class="ti-email mr-2"></i><%= st.getEmail()%></span></p>
                                        <p class="mb-5"><span class="mr-3"><i class="ti-id-badge mr-2"></i><%= st.getStaffid()%></span></p>
                                        <!--<div class="d-flex justify-content-around align-items-center font-18">
                                            <a class="text-primary" href="javascript:;"><i class="fa fa-facebook"></i></a>
                                            <a class="text-primary" href="javascript:;"><i class="fa fa-twitter"></i></a>
                                            <a class="text-primary" href="javascript:;"><i class="fa fa-pinterest-p"></i></a>
                                            <a class="text-primary" href="javascript:;"><i class="fa fa-instagram"></i></a>
                                        </div>-->
                                    </div>
                                </div>

                            </div>

                            <div class="tab-pane fade text-left" id="tab-11-2">

                                <div class="row">
                                    <div class="col-lg-4 col-md-6 mb-4">
                                        <div class="card bg-success">
                                            <div class="card-body">
                                                <h2 class="text-white"><%= GeneralTerm.PrecisionDoubleWithoutDecimal(LeaveDAO.getEligibleLeaveOfTheYear(log, st.getStaffid(), yearApply))%> <i class="fa fa-briefcase float-right" aria-hidden="true"></i></h2>
                                                <div><span class="text-white">CUTI TAHUNAN</span></div>
                                                <div class="text-white mt-1">
                                                    <small> <span class="badge mr-1 widget-dark-badge"><strong><%= GeneralTerm.PrecisionDoubleWithoutDecimal(LeaveDAO.getEligibleLeaveWithoutCF(log, st.getStaffid(), yearApply))%></strong></span> Layak</small>
                                                    <small> <span class="badge mr-1 widget-dark-badge"><strong><%= GeneralTerm.PrecisionDoubleWithoutDecimal(LeaveDAO.getPreviousLeaveCF(log, st.getStaffid(), yearApply))%></strong></span> Dibawa</small>
                                                    <small> <span class="badge mr-1 widget-dark-badge"><strong><%= GeneralTerm.PrecisionDoubleWithoutDecimal(LeaveDAO.getBalanceEligibleLeave(log, st.getStaffid(), yearApply))%></strong></span> Baki</small>
                                                </div>
                                            </div>
                                            <div class="progress mb-2 widget-dark-progress">
                                                <div class="progress-bar" role="progressbar" style="width:<%= percentYear%>%; height:5px;" aria-valuenow="<%= percentYear%>" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-6 mb-4">
                                        <div class="card bg-danger">
                                            <div class="card-body">
                                                <h2 class="text-white"><%= GeneralTerm.PrecisionDoubleWithoutDecimal(LeaveDAO.getMedicalLeaveOfTheYear(log, st.getStaffid(), yearApply))%> <i class="fa fa-medkit float-right" aria-hidden="true"></i></h2>
                                                <div><span class="text-white">CUTI SAKIT</span></div>
                                                <div class="text-white mt-1">
                                                    <span class="badge mr-1 widget-dark-badge"><%= LeaveDAO.getBalanceTotalSickLeaveOfTheYear(log, st.getStaffid(), yearApply)%></span><small>hari baki Cuti Sakit.</small></div>
                                            </div>
                                            <div class="progress mb-2 widget-dark-progress">
                                                <div class="progress-bar" role="progressbar" style="width:<%=percentYearMedical%>%; height:5px;" aria-valuenow="<%=percentYearMedical%>" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6 mb-4">
                                        <div class="card bg-primary">
                                            <div class="card-body">
                                                <h2 class="text-white"><%= GeneralTerm.PrecisionDoubleWithoutDecimal(bakiCutiLayak)%> <i class="fa fa-calendar-check-o float-right" aria-hidden="true"></i></i></h2>
                                                <div><span class="text-white">BAKI LAYAK BULAN</span></div>
                                                <div class="text-white mt-1"><small> <span class="badge mr-1 widget-dark-badge"><strong><%= GeneralTerm.PrecisionDoubleWithoutDecimal(LeaveDAO.getEligibleLeaveForTheMonthExcludeApprovedThisMonth(log, st.getStaffid(), yearApply, AccountingPeriod.getCurrentTimeStamp()))%></strong></span> hari jumlah layak bulan ini.</small></div>
                                            </div>
                                            <div class="progress mb-2 widget-dark-progress">
                                                <div class="progress-bar" role="progressbar" style="width:<%=percentMonth%>%; height:5px;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                            </div>

                            <div class="tab-pane fade text-left" id="tab-11-3">

                                <%
                                    CoStaff hd = (CoStaff) StaffDAO.getInfoHead(log, st.getStaffid());
                                    CoStaff sv = (CoStaff) StaffDAO.getInfoSupervisor(log, st.getStaffid());

                                %>

                                <div class="col-sm-12 col-lg-12">

                                    <div class="card overflow-visible mt-5 bg-primary" >

                                        <div class="card-body text-center mt-4">

                                            <img class="img-circle img-bordered card-abs-top-center" src="<%= hd.getImageURL()%>" alt="image" width="60" />
                                            <h6 class="mb-1">
                                                <a class="text-white"><%= hd.getName()%></a>
                                            </h6><small class="text-white"><%= hd.getPosition()%></small><br>
                                            <small class="text-white"><%= hd.getDepartment()%></small>
                                            <p class="mt-2"><span class="badge badge-default">Pelulus</span></p>

                                        </div>
                                    </div>
                                </div>
                                <%
                                    if (!StaffDAO.isSupervisorAsHeadDept(log, st.getStaffid())) {
                                %>
                                <div class="col-sm-12 col-lg-12">
                                    <div class="card overflow-visible mt-5 bg-primary" >

                                        <div class="card-body text-center mt-4">

                                            <img class="img-circle img-bordered card-abs-top-center" src="<%= sv.getImageURL()%>" alt="image" width="60" />
                                            <h6 class="mb-1">
                                                <a class="text-white"><%= sv.getName()%></a>
                                            </h6><small class="text-white"><%= sv.getPosition()%></small><br>
                                            <small class="text-white"><%= sv.getDepartment()%></small>
                                            <p class="mt-2"><span class="badge badge-default">Penyokong</span></p>

                                        </div>
                                    </div>
                                </div>

                                <%
                                    }
                                %>
                                <div class="col-sm-12 col-lg-12">
                                    <div class="card overflow-visible mt-5 bg-primary" >

                                        <div class="card-body text-center mt-4">

                                            <img class="img-circle img-bordered card-abs-top-center" src="<%= log.getImageURL()%>" alt="image" width="60" />
                                            <h6 class="mb-1">
                                                <a class="text-white"><%= st.getName()%></a>
                                            </h6><small class="text-white"><%= st.getPosition()%></small><br>
                                            <small class="text-white"><%= st.getDepartment()%></small>
                                            <p class="mt-2"><span class="badge badge-default">Anda</span></p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!-- END PAGE CONTENT-->
            <div id="modalhere"></div>
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
        </div>



        <jsp:include page='layout/bottom.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>

        <script>
            $(function () {
                $("[name='timeline1-option']").change(function () {
                    +this.value ? $('.timeline-1').addClass('center-orientation') : $('.timeline-1').removeClass('center-orientation');
                });
                $("[name='timeline2-option']").change(function () {
                    +this.value ? $('.timeline-2').addClass('center-orientation') : $('.timeline-2').removeClass('center-orientation');
                });
            })
        </script>
</body>
<script>
    $(function () {
        $('#datatable').DataTable({
            pageLength: 10,
            fixedHeader: true,
            responsive: true,
            "sDom": 'rtip',
            columnDefs: [{
                    targets: 'no-sort',
                    orderable: false
                }]
        });

        var table = $('#datatable').DataTable();
        $('#key-search').on('keyup', function () {
            table.search(this.value).draw();
        });
        $('#type-filter').on('change', function () {
            table.column(4).search($(this).val()).draw();
        });
    });
</script>
</html>







