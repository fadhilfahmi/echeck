<%-- 
    Document   : summary_check
    Created on : May 13, 2020, 1:14:42 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.echeck.dao.AccountingPeriod"%>
<%@page import="com.lcsb.echeck.model.Checkform"%>
<%@page import="com.lcsb.echeck.model.Checkmaster"%>
<%@page import="com.lcsb.echeck.dao.CheckDAO"%>
<%@page import="com.lcsb.echeck.model.LoginProfile"%>
<%@page import="com.lcsb.echeck.dao.StaffDAO"%>
<%@page import="com.lcsb.echeck.model.CoStaff"%>
<%@page import="java.util.List"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width initial-scale=1.0">
        <title>eDaftar</title>
        <!-- GLOBAL MAINLY STYLES-->
        <link href="./assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
        <link href="./assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
        <link href="./assets/vendors/line-awesome/css/line-awesome.min.css" rel="stylesheet" />
        <link href="./assets/vendors/themify-icons/css/themify-icons.css" rel="stylesheet" />
        <link href="./assets/vendors/animate.css/animate.min.css" rel="stylesheet" />
        <link href="./assets/vendors/toastr/toastr.min.css" rel="stylesheet" />
        <link href="./assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
        <link href="./assets/vendors/bootstrap-sweetalert/dist/sweetalert.css" rel="stylesheet" />
        <!-- PLUGINS STYLES-->
        <!-- THEME STYLES-->
        <link href="assets/css/main.css" rel="stylesheet" />


        <!-- PAGE LEVEL STYLES-->
    </head>

    <body>
        <div class="page-wrapper">

            <!-- END HEADER-->
            <!-- START SIDEBAR-->

            <!-- END SIDEBAR-->
            <div class="content-wrapper">
                <!-- START PAGE CONTENT-->

                <div class="page-content fade-in-up col-lg-12 col-sm-12 centered">

                    <div class="ibox">
                        <div class="ibox-head">
                            <div class="ibox-title">Senarai Anggota Pekerja pada <%= AccountingPeriod.getFullCurrentDate()%></div>
                        </div>
                        <div class="ibox-body">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>ID Pekerja</th>
                                        <th>Nama</th>
                                        <th>Masa Imbas</th>
                                        <th>Suhu</th>
                                        <th>Saringan</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <%List<Checkmaster> listAll = (List<Checkmaster>) CheckDAO.getAllForToday(log);
                                        int i = 0;

                                        for (Checkmaster j : listAll) {
                                            i++;

                                            CoStaff co = (CoStaff) StaffDAO.getInfo(log, j.getStaffID());
                                            Checkform c = (Checkform) CheckDAO.getCheckForm(log, j.getId());

                                    %>

                                    <tr id="<%= j.getId()%>">
                                        <td>
                                            <a href="javascript:;"><%= co.getStaffid()%></a>
                                        </td>
                                        <td><%= co.getName()%></td>

                                        <td><%= j.getTimevalid() %></td>
                                        <td><%= j.getTemperature() %></td>
                                        <td>
                                            <span class="badge badge-<%= CheckDAO.getBadgeColor(j.getStatus())%> badge-pill"><%= j.getStatus()%></span>
                                        </td>
                                    </tr>

                                    <% }%>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT-->

            </div>
        </div>
        <!-- START SEARCH PANEL-->
        <form class="search-top-bar" action="search.html">
            <input class="form-control search-input" type="text" placeholder="Search...">
            <button class="reset input-search-icon"><i class="ti-search"></i></button>
            <button class="reset input-search-close" type="button"><i class="ti-close"></i></button>
        </form>
        <!-- END SEARCH PANEL-->
        <!-- BEGIN THEME CONFIG PANEL-->

        <!-- END THEME CONFIG PANEL-->
        <!-- BEGIN PAGA BACKDROPS-->
        <div class="sidenav-backdrop backdrop"></div>
        <div class="preloader-backdrop">
            <div class="page-preloader">Loading</div>
        </div>
        <!-- END PAGA BACKDROPS-->
        <!-- New question dialog-->

        <div id="modalpassengerdiv"></div>
        <!-- End New question dialog-->
        <!-- QUICK SIDEBAR-->

        <!-- END QUICK SIDEBAR-->
        <!-- CORE PLUGINS-->
        <script src="./assets/vendors/jquery/dist/jquery.min.js"></script>
        <script src="./assets/vendors/popper.js/dist/umd/popper.min.js"></script>
        <script src="./assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="./assets/vendors/metisMenu/dist/metisMenu.min.js"></script>
        <script src="./assets/vendors/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="./assets/vendors/jquery-idletimer/dist/idle-timer.min.js"></script>
        <script src="./assets/vendors/toastr/toastr.min.js"></script>
        <script src="./assets/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="./assets/vendors/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <!-- PAGE LEVEL PLUGINS-->
        <script src="./assets/vendors/jquery.steps/build/jquery.steps.min.js"></script>

        <script src="./assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js"></script>
        <!-- CORE SCRIPTS-->
        <script src="assets/js/app.min.js"></script>
        <!-- PAGE LEVEL SCRIPTS-->
        <script>
            $(function () {

                $('body').on('change', '#name', function (e) {

                    var n = $(this).val();
                    //alert(s);
                    //} 

                    e.stopPropagation();
                    return false;

                });
                $('#form-wizard').steps({
                    headerTag: "h6",
                    bodyTag: "section",
                    titleTemplate: '<span class="step-number">#index#</span> #title#',
                    onStepChanging: function (event, currentIndex, newIndex) {
                        var form = $(this);
                        // Always allow going backward even if the current step contains invalid fields!
                        if (currentIndex > newIndex) {
                            return true;
                        }

                        /*if (currentIndex == 0) {
                         var staffID = $('#staffIDX').val();
                         var name = $('#name').val();
                         $.ajax({
                         url: "intro_form_1.jsp?staffIDX=" + staffID + "&name=" + name,
                         success: function (result) {
                         $('#form_1').empty().html(result).hide().fadeIn(300);
                         }
                         });
                         //return false;
                         }
                         
                         if (currentIndex == 1) {
                         var staffID = $('#staffID').val();
                         $.ajax({
                         url: "intro_form_2.jsp?staffID=" + staffID,
                         success: function (result) {
                         $('#form_2').empty().html(result).hide().fadeIn(300);
                         }
                         });
                         //return false;
                         }
                         if (currentIndex == 2) {
                         var staffID = $('#staffID').val();
                         $.ajax({
                         url: "intro_form_3.jsp?staffID=" + staffID,
                         success: function (result) {
                         $('#form_3').empty().html(result).hide().fadeIn(300);
                         }
                         });
                         //return false;
                         }*/
                        // Clean up if user went backward before
                        if (currentIndex < newIndex) {
                            // To remove error styles
                            $(".body:eq(" + newIndex + ") label.error", form).remove();
                            $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                        }

                        // Disable validation on fields that are disabled or hidden.
                        form.validate().settings.ignore = ":disabled,:hidden";

                        // Start validation; Prevent going forward if false
                        return form.valid();
                    },
                    onFinishing: function (event, currentIndex) {
                        var form = $(this);
                        form.validate().settings.ignore = ":disabled";
                        return form.valid();
                    },
                    onFinished: function (event, currentIndex) {
                        //toastr.success('Submitted!');


                        var a = $("#form-wizard :input").serialize();
                        $.ajax({
                            async: true,
                            data: a,
                            type: 'POST',
                            url: "PathController?process=savecheckform",
                            success: function (result) {
                                swal({
                                    title: "Terima Kasih",
                                    text: "Sila ke proses saringan seterusnya!",
                                    type: "success"
                                }, function () {
                                    window.location = "Login";
                                });



                            }
                        });
                        //} 

                    }
                }).validate({
                    errorPlacement: function errorPlacement(error, element) {
                        error.insertAfter(element);
                    },
                    rules: {
                        question2: "required",
                        question3: "required",
                    },
                    errorClass: "help-block error",
                    highlight: function (e) {
                        $(e).closest(".form-group").addClass("has-error")
                    },
                    unhighlight: function (e) {
                        $(e).closest(".form-group").removeClass("has-error")
                    },
                });

                $('#list_staff').on('click', '.viewmodalpassenger', function (e) {
                    e.preventDefault();

                    var id = $(this).attr('id');
                    var sessiondid = $('#sessionid').val();
                    $.ajax({
                        async: false,
                        url: "PathController?process=viewstafflist&sessionid=" + sessiondid + "&id=" + id,
                        success: function (result) {
                            $('#modalpassengerdiv').empty().html(result).hide().fadeIn(300);
                        }
                    });


                    $('#modalpassenger').modal('toggle');
                    return false;
                });

            })
        </script>
    </body>

</html>