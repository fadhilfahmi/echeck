<%-- 
    Document   : checkout
    Created on : May 13, 2020, 6:17:51 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.echeck.model.Checkform"%>
<%@page import="com.lcsb.echeck.model.Checkmaster"%>
<%@page import="com.lcsb.echeck.model.Check"%>
<%@page import="com.lcsb.echeck.dao.CheckDAO"%>
<%@page import="com.lcsb.echeck.dao.StaffDAO"%>
<%@page import="com.lcsb.echeck.model.CoStaff"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.echeck.dao.AccountingPeriod"%>
<%@page import="com.lcsb.echeck.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    CoStaff co = StaffDAO.getInfoByEmail(log, log.getEmail());
    
%>
<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>
<style>
        .pricing-title {
            margin: 0;
            padding: 15px;
        }

        .price {
            padding: 15px;
            background-color: #e3e6e7;
        }

        .price sup {
            margin-right: 3px;
            font-size: 35px;
            top: -.5em;
        }

        .price-number {
            font-size: 60px
        }

        .price-arrow {
            box-sizing: content-box;
            width: 50%;
            height: 0;
            padding-left: 50%;
            padding-top: 5%;
            overflow: hidden;
        }

        .price-arrow div {
            width: 0;
            height: 0;
            margin-left: -1000px;
            margin-top: -100px;
            border-left: 1000px solid transparent;
            border-right: 1000px solid transparent;
            border-top: 100px solid #e3e6e7;
        }

        .pricing-item-active .pricing-title {
            background-color: #18c5a9;
            color: #fff;
        }

        .pricing-item-active .price {
            background-color: #16B198;
            color: #fff;
        }

        .pricing-item-active .price-arrow div {
            border-top-color: #16B198;
        }
    </style>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {


        $(".delete-leave").click(function (e) {

            var id = $(this).attr('id');
            swal({
                title: "Anda pasti untuk padam?",
                text: "Anda tidak akan dapat mengembalikannya semula!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: 'Ya, padam!',
                closeOnConfirm: false,
            }, function () {

                $.ajax({
                    async: false,
                    url: "PathController?process=deleteleave&id=" + id,
                    success: function (result) {
                        //swal("Deleted!", "Car has been deleted.", "success");
                        $(location).attr('href', 'leave_list.jsp');
                    }
                });

            });

            //} 

            e.stopPropagation();
            return false;
        });



        $('#section-refresh').on('click', '#gotoquestion', function (e) {
            e.preventDefault();

            var leaveid = $(this).attr('id');
            var id = $(this).attr('href');
            $(location).attr('href', 'question.jsp?process=viewleavetab&tabid=' + id);
            return false;
        });



    });

</script>
<body>
    <div class="page-wrapper">
           <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        

        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-content fade-in-up">
                <% if (CheckDAO.isCheckingExisted(log, co.getStaffid())) {
                             Checkmaster cm = (Checkmaster) CheckDAO.getInfoByStaffID(log, co.getStaffid());
                             %>
                <div class="col-md-4 pricing-item-active">
                        <div class="ibox shadow-wide text-center">
                            <h4 class="pricing-title">DAFTAR KELUAR</h4>
                            <div class="price"><i class="fa fa-check-circle fa-5x" aria-hidden="true"></i>
                            </div>
                            <div class="price-arrow">
                                <div></div>
                            </div>
                            <div class="ibox-body">
                                <div class="pt-4 pb-5">
                                    
                             <p class="mt-4 mb-4">Anda telah mendaftar keluar!</p>
                            <div class="d-flex align-items-center justify-content-between mb-5">
                                <div class="text-center">
                                    <h5 class="text-primary m-0">MASUK</h5>
                                    <div class="text-muted"><%= cm.getTimevalid() %></div>
                                </div>
                                <div class="text-center">
                                    <h5 class="text-primary m-0">KELUAR</h5>
                                    <div class="text-muted"><%= cm.getTimeout()%></div>
                                </div>
                                <div class="text-center">
                                    <h5 class="text-primary m-0">SARINGAN</h5>
                                    <div class="text-muted"><%= cm.getStatus() %></div>
                                </div>
                            </div>
                            <!--<div class="d-flex justify-content-around align-items-center">
                                <button class="btn btn-primary btn-rounded mr-2">
                                    <span class="btn-icon"><i class="la la-heart-o"></i>Follow</span>
                                </button>
                                <button class="btn btn-secondary btn-rounded">
                                    <span class="btn-icon"><i class="la la-envelope"></i>Message</span>
                                </button>
                            </div>-->
                               
                                </div>
                                <button class="btn btn-success btn-air" style="width:170px;">HARI INI</button>
                            </div>
                        </div>
                    </div>
                                 <%}else{%>
                               <div class="col-md-4 ">
                        <div class="ibox shadow-wide text-center">
                            <h4 class="pricing-title">DAFTAR KELUAR</h4>
                            <div class="price"><i class="fa fa-exclamation-circle fa-5x text-danger" aria-hidden="true"></i>
                            </div>
                            <div class="price-arrow">
                                <div></div>
                            </div>
                            <div class="ibox-body">
                                <div class="pt-4 pb-5">
                                    
                             <h4 class="mt-4 mb-4">Tiada rekod kemasukan pada hari ini!</h4>
                            
                            <!--<div class="d-flex justify-content-around align-items-center">
                                <button class="btn btn-primary btn-rounded mr-2">
                                    <span class="btn-icon"><i class="la la-heart-o"></i>Follow</span>
                                </button>
                                <button class="btn btn-secondary btn-rounded">
                                    <span class="btn-icon"><i class="la la-envelope"></i>Message</span>
                                </button>
                            </div>-->
                               
                                </div>
                                
                            </div>
                        </div>
                    </div>
                                <%}%>
                <!--<div class="card card-air text-center centered mb-4" style="max-width:320px;">
                        <div class="card-body">
                        
                    </div>
                

            </div>-->
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
        </div>
    </div>

    <jsp:include page='layout/bottom.jsp'>
        <jsp:param name="page" value="home"/>
    </jsp:include>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
        });
    </script>
</body>
</html>

