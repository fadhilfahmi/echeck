<%-- 
    Document   : main
    Created on : Sep 29, 2019, 9:59:49 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.echeck.model.Checkform"%>
<%@page import="com.lcsb.echeck.model.Checkmaster"%>
<%@page import="com.lcsb.echeck.model.Check"%>
<%@page import="com.lcsb.echeck.dao.CheckDAO"%>
<%@page import="com.lcsb.echeck.dao.StaffDAO"%>
<%@page import="com.lcsb.echeck.model.CoStaff"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.echeck.dao.AccountingPeriod"%>
<%@page import="com.lcsb.echeck.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

    //String balLeave = "";
    double balLeave = 0;

    String curYear = AccountingPeriod.getCurYearByCurrentDate();
    String curMonth = AccountingPeriod.getCurPeriodByCurrentDate();
    String curDate = AccountingPeriod.getCurrentTimeStamp();

    String staffID = StaffDAO.getInfoByEmail(log, log.getEmail()).getStaffid();


%>
<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        var idleState = false;
        var idleTimer = null;
        $('*').bind('mousemove click mouseup mousedown keydown keypress keyup submit change mouseenter scroll resize dblclick', function () {
            //console.log('breakidle');
            clearTimeout(idleTimer);
            if (idleState == true) {
                console.log('idle1:' + idleTimer);
                $.ajax({
                    url: "section-check.jsp?staffID=<%= staffID%>",
                    success: function (result) {
                        $('#section-refresh').empty().html(result).hide().fadeIn(300);
                    }
                });

            }
            idleState = false;
            idleTimer = setTimeout(function () {

                idleState = true;
                console.log('idle2:' + idleState);
            }, 5000);
        });
        $("body").trigger("mousemove");

        $.ajax({
            url: "section-check.jsp?staffID=<%= staffID%>",
            success: function (result) {
                $('#section-refresh').empty().html(result).hide().fadeIn(300);
            }
        });

        $(".delete-leave").click(function (e) {

            var id = $(this).attr('id');
            swal({
                title: "Anda pasti untuk padam?",
                text: "Anda tidak akan dapat mengembalikannya semula!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: 'Ya, padam!',
                closeOnConfirm: false,
            }, function () {

                $.ajax({
                    async: false,
                    url: "PathController?process=deleteleave&id=" + id,
                    success: function (result) {
                        //swal("Deleted!", "Car has been deleted.", "success");
                        $(location).attr('href', 'leave_list.jsp');
                    }
                });

            });

            //} 

            e.stopPropagation();
            return false;
        });



        $('#section-refresh').on('click', '#gotoquestion', function (e) {
            e.preventDefault();

            var leaveid = $(this).attr('id');
            var id = $(this).attr('href');
            $(location).attr('href', 'question.jsp?process=viewleavetab&tabid=' + id);
            return false;
        });



    });

</script>
<style>
    body {
        //background: radial-gradient(circle, $white , lighten($dark-grey, 40));
    }

    .container {
        max-width: 350px;
        max-height: 630px;
        overflow: hidden;
        margin: 30px auto 0;
        box-shadow: 0 0 40px lighten(#202020, 50);
        font-family: 'Open Sans', sans-serif;
    }

    .navbar {
        background: #B97CFC;
        color: #fff;
        padding: 1em 0.5em;

        a {
            color: #fff;
            text-decoration: none;
            font-size: 1.3em;
            float: left;
        }

        span {
            font-size: 1.1em;
            font-weight: 300;
            display: block;
            text-align: center;
        }
    }

    .profile-pic {
        width: 30px;
        height: 30px;
        display: inline-block;
        float: right;
        position: relative;

        img {
            width: 100%;
            border-radius: 50%;
        }
    }

    .notification {
        position: absolute;
        width: 5px;
        height: 5px;
        border-radius: 50%;
        top: 2px;
        right: 2px;
        background: #F93B69;
    }

    .headers {
        background: url(https://unsplash.it/1080/720?image=1044);
        background-size: cover;
        color: #fff;
        position: relative;
    }

    .color-overlay {
        padding: 3em 2em;
        box-sizing: border-box;
        background: rgba(123, 94, 155, 0.5) ;
    }

    .actionbutton {
        position: absolute;
        background: #F93B69;
        width: 50px;
        height: 50px;
        font-size: 3em;
        font-weight: 300;
        border-radius: 50%;
        display: flex;
        justify-content: center;
        align-items: center;
        bottom: -25px;
        right: 20px;
        box-shadow: 0 0 8px #202020;
    }

    .day-number {
        font-size: 4em;
        display: inline-block;
        margin-right: 15px;
    }

    .date-right {
        display: inline-block;
    }

    .day-name {
        font-size: 1.6em;
    }

    .month {
        text-transform: uppercase;
        font-weight: 300;
        font-size: 0.6em;
        letter-spacing: 2px;
        margin-top: 2px;
    }

    .timeline {

        ul {
            padding: 1em 0 0 2em;
            margin: 0;
            list-style: none;
            position: relative;

            &::before {
                content: ' ';
                height: 100%;
                width: 1px;
                background-color: #d9d9d9;
                position: absolute;
                top: 0;
                left: 2.5em;
                z-index: -1;
            }
        }

        li div{
            display: inline-block;
            margin: 1em 0;
            vertical-align: top;
        }

        .bullet {
            width: 1em;
            height: 1em;
            box-sizing: border-box;
            border-radius: 50%;
            background: #fff;
            z-index: 1;
            margin-right: 1em;

            &.pink {
                border: 2px solid #F93B69;
            }

            &.green {
                border: 2px solid #B0E8E2;
            }

            &.orange {
                border: 2px solid #EB8B6E;
            }
        }

        .time {
            width: 20%;
            font-size: 0.75em;
            padding-top: 0.25em;
        }

        .desc {
            width: 50%;
        }

        h3 {
            font-size: 0.9em;
            font-weight: 400;
            margin: 0;
        }

        h4 {
            margin: 0;
            font-size: 0.7em;
            font-weight: 400;
            color: #808080;
        }

        .people img{
            width: 30px;
            height: 30px;
            border-radius: 50%;
        }
    }

    .credits, .video{
        position: absolute;
        bottom:10px;
        color: #808080;
        font-size: 100%;
        text-decoration: underline;
    }

    .credits {
        left: 10px;
    }

    .video{
        right: 10px;
    }

</style>
<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>


        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-content fade-in-up mr-0">
                 <div class="cd-timeline center-orientation timeline-1">
                     
                     <%
                     Checkmaster cm = (Checkmaster) CheckDAO.getInfoByStaffIDAndDate(log, log.getUserID(), AccountingPeriod.getCurrentTimeStamp());
                     %>
                    <div class="cd-timeline-block">
                        <div class="cd-timeline-icon bg-success text-white"><i class="fa fa-file-text"></i></div>
                        <div class="cd-timeline-content">
                            <h4>Send documents</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut.</p>
                            <a class="btn btn-success btn-sm"
                                href="javascript:;">More ...</a>
                            <span class="cd-date">Today 14:25</span>
                        </div>
                    </div>
                    <div class="cd-timeline-block">
                        <div class="cd-timeline-icon bg-primary text-white"><i class="fa fa-briefcase"></i></div>
                        <div class="cd-timeline-content">
                            <h4>Meeting</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut.</p>
                            <a class="btn btn-success btn-sm"
                                href="javascript:;">More ...</a>
                            <span class="cd-date">2 Days ago</span>
                        </div>
                    </div>
                    <div class="cd-timeline-block">
                        <div class="cd-timeline-icon bg-warning text-white"><i class="fa fa-shopping-basket"></i></div>
                        <div class="cd-timeline-content">
                            <h4>Shopping with Olivia</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut.</p>
                            <a class="btn btn-success btn-sm"
                                href="javascript:;">More ...</a>
                            <span class="cd-date">Dec 15</span>
                        </div>
                    </div>
                    <div class="cd-timeline-block">
                        <div class="cd-timeline-icon bg-blue text-white"><i class="fa fa-birthday-cake"></i></div>
                        <div class="cd-timeline-content">
                            <h4>Emma's birthday</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut.</p>
                            <a class="btn btn-success btn-sm"
                                href="javascript:;">More ...</a>
                            <span class="cd-date">Dec 17</span>
                        </div>
                    </div>
                </div>

                    <!--<div class="card mb-4 mr-4" style="width:346px;">
                        <div class="rel">
                            <img class="card-img-top" src="./assets/img/blog/09.jpeg" alt="image" />
                            <div class="card-img-overlay text-white">
                                <div class="text-right">
                                    <a><i class="la la-heart-o font-20"></i></a>
                                </div>
                                <div class="overlay-panel overlay-panel-bottom color-white mb-2">
                                    <h4 class="card-title mb-2">
                                        <a>Selasa</a>
                                    </h4>
                                    <div><%= AccountingPeriod.getFullCurrentDate()%></div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body d-flex">
                            <div>
                                <div>
                                    <span class="text-primary pb-1 d-inline-block" style="border-bottom:2px solid;">About</span>
                                </div>
                                <p class="text-light mt-3">Some quick example text to build on the card title and make up the bulk.</p>
                                <div class="mb-4 mt-4">
                                    <a class="text-muted mr-3"><i class="fa fa-dribbble"></i></a>
                                    <a class="text-muted mr-3"><i class="fa fa-twitter"></i></a>
                                    <a class="text-muted mr-3"><i class="fa fa-pinterest-p"></i></a>
                                    <a class="text-muted"><i class="fa fa-instagram"></i></a>
                                </div>
                                <div>
                                    <button class="btn btn-primary btn-rounded btn-air mr-2">Follow</button>
                                    <button class="btn btn-outline-primary btn-rounded mr-2">Message</button>
                                </div>
                            </div>
                            <div class="text-right ml-4 mt-3">
                                <div class="mb-3">
                                    <div class="h2 m-0 font-light">124</div><small class="text-muted font-11">PROJECTS</small></div>
                                <div class="mb-3">
                                    <div class="h2 m-0 font-light">3.2k</div><small class="text-muted font-11">FOLLOWERS</small></div>
                                <div>
                                    <div class="h2 m-0 font-light">25k</div><small class="text-muted font-11">VIEWS</small></div>
                            </div>
                        </div>
                    </div>-->
            </div>
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
        </div>
    </div>

    <jsp:include page='layout/bottom.jsp'>
        <jsp:param name="page" value="home"/>
    </jsp:include>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
        });
    </script>
</body>
</html>
