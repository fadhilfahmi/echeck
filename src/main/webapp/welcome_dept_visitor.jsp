<%-- 
    Document   : welcome_dept_visitor
    Created on : May 17, 2020, 9:13:27 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.echeck.model.Checkdept"%>
<%@page import="com.lcsb.echeck.model.Checkform"%>
<%@page import="com.lcsb.echeck.model.Checkmaster"%>
<%@page import="com.lcsb.echeck.model.Check"%>
<%@page import="com.lcsb.echeck.dao.CheckDAO"%>
<%@page import="com.lcsb.echeck.dao.StaffDAO"%>
<%@page import="com.lcsb.echeck.model.CoStaff"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.echeck.dao.AccountingPeriod"%>
<%@page import="com.lcsb.echeck.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {


        $(".delete-leave").click(function (e) {

            var id = $(this).attr('id');
            swal({
                title: "Anda pasti untuk padam?",
                text: "Anda tidak akan dapat mengembalikannya semula!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: 'Ya, padam!',
                closeOnConfirm: false,
            }, function () {

                $.ajax({
                    async: false,
                    url: "PathController?process=deleteleave&id=" + id,
                    success: function (result) {
                        //swal("Deleted!", "Car has been deleted.", "success");
                        $(location).attr('href', 'leave_list.jsp');
                    }
                });

            });

            //} 

            e.stopPropagation();
            return false;
        });



        $('#section-refresh').on('click', '#gotoquestion', function (e) {
            e.preventDefault();

            var leaveid = $(this).attr('id');
            var id = $(this).attr('href');
            $(location).attr('href', 'question.jsp?process=viewleavetab&tabid=' + id);
            return false;
        });



    });

</script>
<body>
    <div class="page-wrapper">
        <header class="header  align-middle">
            <!-- START TOP-LEFT TOOLBAR-->


            <!-- END TOP-LEFT TOOLBAR-->
            <!--LOGO-->
            <a class="page-brand align-middle" href="Login"><img src="./assets/img/logolcsbwithtitle.png" width="85%"></a>

            <!-- END TOP-RIGHT TOOLBAR-->
        </header>

        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-content fade-in-up">
                <div class="card card-air text-center centered mb-4" style="max-width:320px;">
                        <div class="card-body">
                           
                            
                           
                            <h5 class="mt-2">Selamat Datang <%= request.getParameter("name") %></h5>
                            <p class="mt-2 mb-2">ke</p>
                             <p class="mt-0 mb-4"><%= request.getParameter("dept") %></p>
                            <p class="text-primary text-center"> <i class="fa fa-sign-in fa-5x" style="color:green" aria-hidden="true"></i></p>
                            <div class="d-flex align-items-center  mb-5 ">
                                <div class="text-center ml-5">
                                    <h5 class="text-primary">MASUK</h5>
                                    <div class="text-muted"><%= AccountingPeriod.getFullCurrentDate() %></div>
                                    <div class="text-muted"><%= AccountingPeriod.getCurrentTimeWithFormat()%></div>
                                </div>
                                
                            </div>
                           
                        </div>
                    </div>
                

            </div>
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
        </div>
    </div>

    <jsp:include page='layout/bottom.jsp'>
        <jsp:param name="page" value="home"/>
    </jsp:include>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
        });
    </script>
</body>
</html>

