<%-- 
    Document   : admin_add_manual
    Created on : May 31, 2020, 7:20:12 AM
    Author     : fadhilfahmi
--%>
<%@page import="com.lcsb.echeck.dao.AccountingPeriod"%>
<%@page import="com.lcsb.echeck.model.LoginProfile"%>
<%@page import="com.lcsb.echeck.dao.StaffDAO"%>
<%@page import="com.lcsb.echeck.model.CoStaff"%>
<%@page import="java.util.List"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width initial-scale=1.0">
        <title>Sistem Permohonan Cuti LCSB</title>
        <!-- GLOBAL MAINLY STYLES-->

        <script src="./assets/vendors/jquery/dist/jquery.min.js"></script>
        <link href="./assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
        <link href="./assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
        <link href="./assets/vendors/line-awesome/css/line-awesome.min.css" rel="stylesheet" />
        <link href="./assets/vendors/themify-icons/css/themify-icons.css" rel="stylesheet" />
        <link href="./assets/vendors/animate.css/animate.min.css" rel="stylesheet" />
        <link href="./assets/vendors/toastr/toastr.min.css" rel="stylesheet" />
        <link href="./assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
        <!-- PLUGINS STYLES-->
        <link href="./assets/vendors/jvectormap/jquery-jvectormap-2.0.3.css" rel="stylesheet" />
        <!-- THEME STYLES-->
        <link href="assets/css/main.css" rel="stylesheet" />


        <link href="./assets/vendors/dataTables/datatables.min.css" rel="stylesheet" />

        <link href="./assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
        <!-- PLUGINS STYLES-->
        <link href="./assets/vendors/bootstrap-sweetalert/dist/sweetalert.css" rel="stylesheet" />


        <link href="./assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
        <link href="./assets/vendors/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
        <link href="./assets/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
        <link href="./assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.css" rel="stylesheet" />

        <link href="./assets/vendors/multiselect/css/multi-select.css" rel="stylesheet" />
        <link href="./assets/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
        <link href="./assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
        <link href="./assets/vendors/select2/dist/css/select2.min.css" rel="stylesheet" />



        <link href="./assets/vendors/ion.rangeSlider/css/ion.rangeSlider.css" rel="stylesheet" />
        <link href="./assets/vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet" />
        <link href="./assets/vendors/jquery-minicolors/jquery.minicolors.css" rel="stylesheet" />

        <link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
        <link href="./assets/css/pages/timeline.css" rel="stylesheet" />
        <link href="./assets/vendors/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet" />
        <link href="./assets/vendors/fullcalendar/dist/fullcalendar.print.min.css" rel="stylesheet" media="print" />

        <link href="./assets/vendors/alertifyjs/dist/css/alertify.css" rel="stylesheet" />
        <!-- PLUGINS STYLES-->
        <!-- THEME STYLES-->
        <link href="assets/css/main.css" rel="stylesheet" />

        <!-- PAGE LEVEL STYLES-->
        
    </head>

    <body>
        <div class="page-wrapper">
            <!-- START HEADER-->

            <!-- END HEADER-->
            <!-- START SIDEBAR-->

            <!-- END SIDEBAR-->
            <div class="content-wrapper">
                <!-- START PAGE CONTENT-->

                <div class="page-content fade-in-up col-lg-6 col-sm-12 centered">

                    <form id="form-wizard" action="javascript:;" novalidate="novalidate" name="form_intro">

                        <h6>Nama</h6>
                        <section>

                            <div class="form-group">
                                <label class="form-control-label">Nama Anggota Perkhidmatan</label>
                                <div class="form-group" id="list_staff">
                                    <div class="form-group mb-4">
                                        <label>Jika tiada dalam senarai, tambah manual.</label>
                                        <div class="input-group">
                                            <input class="form-control viewmodalpassenger" type="text" placeholder="Carian nama..." id="name" value="" autocomplete="off">
                                            <input class="form-control" type="hidden"  id="staffIDX" name="staffIDX" value="">
                                            <span class="input-group-btn">
                                                <button class="btn btn-outline-secondary viewmodalpassenger">Cari</button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <h6>Masa</h6>
                        <section>
                            <label class="font-normal">Tarikh</label>

                            <div class="form-group" id="date_1">
                                <label class="font-normal"></label>
                                <div class="input-group date">
                                    <span class="input-group-addon bg-white"><i class="fa fa-calendar"></i></span>
                                    <input class="form-control" id="change-date" type="text" name="date" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">
                                </div>
                            </div>
                                <div class="col-sm-6 form-group mb-4">
                        <div class="form-group mb-4">
                            <label>Masuk</label>
                            <div class="input-group clockpicker" data-autoclose="true">
                                <input class="form-control" type="text" value="<%= AccountingPeriod.getCurrentTime() %>" name="timein" id="timein" >
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 form-group mb-4">
                        <div class="form-group mb-4">
                            <label>Keluar</label>
                            <div class="input-group clockpicker" data-autoclose="true">
                                <input class="form-control" type="text" value="00:00:00" name="timeout" id="timeout">
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                        </section>
                        <h6>Gejala</h6>
                        <section>
                            <h3>Adakah anda mempunyai gejala berikut :</h3>

                            <div class="form-group" id="list_staff">
                                <div class="ibox-body">
                                    <div class="form-group">
                                        <label class="checkbox">
                                            <input type="checkbox" name="gejala" value="Demam">
                                            <span class="input-span"></span>Demam</label>
                                    </div>
                                    <div class="form-group">
                                        <label class="checkbox">
                                            <input type="checkbox" name="gejala" value="Batuk Kering">
                                            <span class="input-span"></span>Batuk Kering</label>
                                    </div>
                                    <div class="form-group">
                                        <label class="checkbox">
                                            <input type="checkbox" name="gejala" value="Sakit Tekak">
                                            <span class="input-span"></span>Sakit Tekak</label>
                                    </div>
                                    <div class="form-group">
                                        <label class="checkbox">
                                            <input type="checkbox" name="gejala" value="Selsema">
                                            <span class="input-span"></span>Selsema</label>
                                    </div>
                                    <div class="form-group">
                                        <label class="checkbox">
                                            <input type="checkbox" name="gejala" value="Sesak Nafas">
                                            <span class="input-span"></span>Sesak Nafas</label>
                                    </div>
                                </div>
                                <small> <i class="fa fa-info-circle" aria-hidden="true"></i> Abaikan jika tiada dan tekan 'Seterusnya'</small>
                            </div>

                        </section>
                        <h6>Kontak</h6>
                        <section>
                            <p>Adakah anda pernah berhubung rapat dengan sebarang kluster COVID-19 yang dinyatakan oleh Kementerian
                                Kesihatan Malaysia atau Pesakit Di Bawah Siasatan (PUI) atau pesakit COVID-19 positif dalam masa 14 hari lalu?</p>
                            <div class="form-group">
                                <label class="radio radio-danger">
                                    <input type="radio" name="question2" value="Ya">
                                    <span class="input-span"></span>Ya</label>
                            </div>
                            <div class="form-group">
                                <label class="radio radio-success">
                                    <input type="radio" name="question2" value="Tidak">
                                    <span class="input-span"></span>Tidak</label>
                            </div>


                            <!--<button class="btn btn-danger btn-rounded mr-2">Ya</button><button class="btn btn-success btn-rounded">Tidak</button> -->
                            <div id="form_1">

                            </div>
                        </section>
                        <h6>Perjalanan</h6>
                        <section>
                            <p>Pernahkah anda ke Negara atau kawasan yang terjejas COVID-19 dalam masa 14 hari lalu?</p>
                            <div class="form-group">
                                <label class="radio radio-danger">
                                    <input type="radio" name="question3" value="Ya">
                                    <span class="input-span"></span>Ya</label>
                            </div>
                            <div class="form-group">
                                <label class="radio radio-success">
                                    <input type="radio" name="question3" value="Tidak">
                                    <span class="input-span"></span>Tidak</label>
                            </div>
                            <div id="form_2">
                            </div>
                        </section>
                        <h6>Suhu</h6>
                        <section>
                            <div class="form-group mb-4 mt-4">
                                <input class="form-control form-control-lg form-control-rounded form-control-air" type="text" name="temp" placeholder="Suhu">
                            </div>
                        </section>
                    </form>

                </div>
                <!-- END PAGE CONTENT-->

            </div>
        </div>
        <!-- START SEARCH PANEL-->
        <form class="search-top-bar" action="search.html">
            <input class="form-control search-input" type="text" placeholder="Search...">
            <button class="reset input-search-icon"><i class="ti-search"></i></button>
            <button class="reset input-search-close" type="button"><i class="ti-close"></i></button>
        </form>
        <!-- END SEARCH PANEL-->
        <!-- BEGIN THEME CONFIG PANEL-->

        <!-- END THEME CONFIG PANEL-->
        <!-- BEGIN PAGA BACKDROPS-->
        <div class="sidenav-backdrop backdrop"></div>
        <div class="preloader-backdrop">
            <div class="page-preloader">Loading</div>
        </div>
        <!-- END PAGA BACKDROPS-->
        <!-- New question dialog-->

        <div id="modalpassengerdiv"></div>
        <!-- End New question dialog-->
        <!-- QUICK SIDEBAR-->

        <!-- END QUICK SIDEBAR-->
        <!-- CORE PLUGINS-->
        <script src="./assets/vendors/jquery/dist/jquery.min.js"></script>
        <script src="./assets/vendors/popper.js/dist/umd/popper.min.js"></script>
        <script src="./assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="./assets/vendors/metisMenu/dist/metisMenu.min.js"></script>
        <script src="./assets/vendors/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="./assets/vendors/jquery-idletimer/dist/idle-timer.min.js"></script>
        <script src="./assets/vendors/toastr/toastr.min.js"></script>
        <script src="./assets/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="./assets/vendors/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <!-- PAGE LEVEL PLUGINS-->
        <script src="./assets/vendors/chart.js/dist/Chart.min.js"></script>
        <script src="./assets/vendors/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
        <script src="./assets/vendors/jvectormap/jquery-jvectormap-2.0.3.min.js"></script>
        <script src="./assets/vendors/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <!-- CORE SCRIPTS-->
        <script src="assets/js/app.min.js"></script>
        <!-- PAGE LEVEL SCRIPTS-->
        <script src="./assets/js/scripts/dashboard_visitors.js"></script>
        <script src="./assets/vendors/dataTables/datatables.min.js"></script>
        <script src="./assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js"></script>


        <script src="./assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <script src="./assets/vendors/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
        <script src="./assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="./assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.js"></script>

        <script src="./assets/vendors/multiselect/js/jquery.multi-select.js"></script>
        <script src="./assets/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
        <script src="./assets/vendors/bootstrap-maxlength/src/bootstrap-maxlength.js"></script>
        <script src="./assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
        <script src="./assets/vendors/select2/dist/js/select2.full.min.js"></script>
        <script src="./assets/vendors/jquery.steps/build/jquery.steps.min.js"></script>

        <script src="./assets/vendors/moment/min/moment.min.js"></script>
        <script src="./assets/vendors/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="./assets/vendors/jquery-ui/jquery-ui.min.js"></script>
        <script src="./assets/vendors/alertifyjs/dist/js/alertify.js"></script>

        <script src="./assets/js/scripts/calendar-demo.js"></script>
        <script src="./assets/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.js"></script>
        <!-- CORE SCRIPTS-->
        <script src="assets/js/app.min.js"></script>


        <script src="./assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <script src="./assets/vendors/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
        <script src="./assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
        <!-- PAGE LEVEL SCRIPTS-->
        <script>
            $(function () {


                $('body').on('change', '#name', function (e) {

                    var n = $(this).val();
                    //alert(s);
                    //} 

                    e.stopPropagation();
                    return false;

                });
                $('#form-wizard').steps({
                    headerTag: "h6",
                    bodyTag: "section",
                    titleTemplate: '<span class="step-number">#index#</span> #title#',
                    onStepChanging: function (event, currentIndex, newIndex) {
                        var form = $(this);
                        // Always allow going backward even if the current step contains invalid fields!
                        if (currentIndex > newIndex) {
                            return true;
                        }

                        /*if (currentIndex == 0) {
                         var staffID = $('#staffIDX').val();
                         var name = $('#name').val();
                         $.ajax({
                         url: "intro_form_1.jsp?staffIDX=" + staffID + "&name=" + name,
                         success: function (result) {
                         $('#form_1').empty().html(result).hide().fadeIn(300);
                         }
                         });
                         //return false;
                         }
                         
                         if (currentIndex == 1) {
                         var staffID = $('#staffID').val();
                         $.ajax({
                         url: "intro_form_2.jsp?staffID=" + staffID,
                         success: function (result) {
                         $('#form_2').empty().html(result).hide().fadeIn(300);
                         }
                         });
                         //return false;
                         }
                         if (currentIndex == 2) {
                         var staffID = $('#staffID').val();
                         $.ajax({
                         url: "intro_form_3.jsp?staffID=" + staffID,
                         success: function (result) {
                         $('#form_3').empty().html(result).hide().fadeIn(300);
                         }
                         });
                         //return false;
                         }*/
                        // Clean up if user went backward before
                        if (currentIndex < newIndex) {
                            // To remove error styles
                            $(".body:eq(" + newIndex + ") label.error", form).remove();
                            $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                        }

                        // Disable validation on fields that are disabled or hidden.
                        form.validate().settings.ignore = ":disabled,:hidden";

                        // Start validation; Prevent going forward if false
                        return form.valid();
                    },
                    onFinishing: function (event, currentIndex) {
                        var form = $(this);
                        form.validate().settings.ignore = ":disabled";
                        return form.valid();
                    },
                    onFinished: function (event, currentIndex) {
                        //toastr.success('Submitted!');


                        var a = $("#form-wizard :input").serialize();
                        $.ajax({
                            async: true,
                            data: a,
                            type: 'POST',
                            url: "PathController?process=savecheckformmanual",
                            success: function (result) {
                                swal({
                                    title: "Selesai",
                                    text: "Maklumat kedatangan telah ditambah",
                                    type: "success"
                                }, function () {
                                    window.location = "admin_list.jsp";
                                });



                            }
                        });
                        //} 

                    }
                }).validate({
                    errorPlacement: function errorPlacement(error, element) {
                        error.insertAfter(element);
                    },
                    rules: {
                        question2: "required",
                        question3: "required",
                    },
                    errorClass: "help-block error",
                    highlight: function (e) {
                        $(e).closest(".form-group").addClass("has-error")
                    },
                    unhighlight: function (e) {
                        $(e).closest(".form-group").removeClass("has-error")
                    },
                });



                $('#list_staff').on('click', '.viewmodalpassenger', function (e) {
                    e.preventDefault();

                    var id = $(this).attr('id');
                    var sessiondid = $('#sessionid').val();
                    $.ajax({
                        async: false,
                        url: "PathController?process=viewstafflist&sessionid=" + sessiondid + "&id=" + id,
                        success: function (result) {
                            $('#modalpassengerdiv').empty().html(result).hide().fadeIn(300);
                        }
                    });


                    $('#modalpassenger').modal('toggle');
                    return false;
                });

            })
        </script>
<script type="text/javascript" charset="utf-8">
            $(document).ready(function () {




                $('#date_1 .input-group.date').datepicker({
                    keyboardNavigation: false,
                    forceParse: false,
                    autoclose: true,
                    format: 'yyyy-mm-dd',
                    orientation: "bottom"
                });

 $('.clockpicker').clockpicker({
     
        format: "HH:ii:SS",
 });




            });

        </script>
    </body>

</html>