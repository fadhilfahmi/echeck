<%-- 
    Document   : checkout_work
    Created on : Jun 14, 2020, 10:29:18 PM
    Author     : fadhilfahmi
--%>


<%@page import="com.lcsb.echeck.model.Checkform"%>
<%@page import="com.lcsb.echeck.model.Checkmaster"%>
<%@page import="com.lcsb.echeck.model.Check"%>
<%@page import="com.lcsb.echeck.dao.CheckDAO"%>
<%@page import="com.lcsb.echeck.dao.StaffDAO"%>
<%@page import="com.lcsb.echeck.model.CoStaff"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.echeck.dao.AccountingPeriod"%>
<%@page import="com.lcsb.echeck.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    CoStaff co = StaffDAO.getInfoByEmail(log, log.getEmail());
    
%>
<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>
<style>
        .pricing-title {
            margin: 0;
            padding: 15px;
        }

        .price {
            padding: 15px;
            background-color: #e3e6e7;
        }

        .price sup {
            margin-right: 3px;
            font-size: 35px;
            top: -.5em;
        }

        .price-number {
            font-size: 60px
        }

        .price-arrow {
            box-sizing: content-box;
            width: 50%;
            height: 0;
            padding-left: 50%;
            padding-top: 5%;
            overflow: hidden;
        }

        .price-arrow div {
            width: 0;
            height: 0;
            margin-left: -1000px;
            margin-top: -100px;
            border-left: 1000px solid transparent;
            border-right: 1000px solid transparent;
            border-top: 100px solid #e3e6e7;
        }

        .pricing-item-active .pricing-title {
            background-color: #18c5a9;
            color: #fff;
        }

        .pricing-item-active .price {
            background-color: #16B198;
            color: #fff;
        }

        .pricing-item-active .price-arrow div {
            border-top-color: #16B198;
        }
    </style>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {


        $(".delete-leave").click(function (e) {

            var id = $(this).attr('id');
            swal({
                title: "Anda pasti untuk padam?",
                text: "Anda tidak akan dapat mengembalikannya semula!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: 'Ya, padam!',
                closeOnConfirm: false,
            }, function () {

                $.ajax({
                    async: false,
                    url: "PathController?process=deleteleave&id=" + id,
                    success: function (result) {
                        //swal("Deleted!", "Car has been deleted.", "success");
                        $(location).attr('href', 'leave_list.jsp');
                    }
                });

            });

            //} 

            e.stopPropagation();
            return false;
        });



        $('#section-refresh').on('click', '#gotoquestion', function (e) {
            e.preventDefault();

            var leaveid = $(this).attr('id');
            var id = $(this).attr('href');
            $(location).attr('href', 'question.jsp?process=viewleavetab&tabid=' + id);
            return false;
        });



    });

</script>
<body>
    <div class="page-wrapper">
           <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        

        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-content fade-in-up">
                <h5 class="text-center">Sila Pilih</h5>
                <button class="btn btn-primary btn-lg btn-block">Makan</button>
                <button class="btn btn-primary btn-lg btn-block">Urusan Rasmi</button>
                <button class="btn btn-primary btn-lg btn-block">Urusan Tidak Rasmi</button>
                <button class="btn btn-primary btn-lg btn-block">Pulang Awal</button>
                
                <!--<div class="card card-air text-center centered mb-4" style="max-width:320px;">
                        <div class="card-body">
                        
                    </div>
                

            </div>-->
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
        </div>
    </div>

    <jsp:include page='layout/bottom.jsp'>
        <jsp:param name="page" value="home"/>
    </jsp:include>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
        });
    </script>
</body>
</html>

