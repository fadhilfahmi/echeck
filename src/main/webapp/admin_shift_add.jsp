<%-- 
    Document   : admin_shift_add
    Created on : May 31, 2020, 11:06:26 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.echeck.model.Checkworktime"%>
<%@page import="com.lcsb.echeck.model.Checkshift"%>
<%@page import="com.lcsb.echeck.dao.CheckDAO"%>
<%@page import="com.lcsb.echeck.model.Checkmaster"%>
<%@page import="com.lcsb.echeck.dao.StaffDAO"%>
<%@page import="com.lcsb.echeck.model.CoStaff"%>
<%@page import="com.lcsb.echeck.model.LoginProfile"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

    if (log == null) {
%><script type="text/javascript">
    window.location.href = "index.jsp?linkTo=admin";
</script>
<%
    }

%>

<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {


        $('#addnew').click(function (e) {
            e.preventDefault();
            $(location).attr('href', 'conf_car_add.jsp');

            return false;
        });

        $('.updatepage').click(function (e) {
            e.preventDefault();

            var id = $(this).attr('id');
            $(location).attr('href', 'check_record_list.jsp?id=' + id);

            return false;
        });

        $('.add-manual').click(function (e) {
            e.preventDefault();

            var id = $(this).attr('id');
            $(location).attr('href', 'admin_add_manual.jsp?');

            return false;
        });


        $('#datatable tbody').on('click', 'tr', function (e) {
            e.preventDefault();

            var id = $(this).attr('id');
            $.ajax({
                async: false,
                url: "PathController?process=addmodal&carID=" + id,
                success: function (result) {
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });


            $('#myModal').modal('toggle')
            return false;
        });

        $('#modalhere').on('click', '#editCarButton', function (e) {
            e.preventDefault();

            var id = $(this).attr('title');
            $(location).attr('href', 'conf_car_edit.jsp?id=' + id);

            return false;
        });

        $('#modalhere').on('click', '#deleteCarButton', function (e) {
            e.preventDefault();

            var id = $(this).attr('title');

            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: 'Yes, delete it!',
                closeOnConfirm: false,
            }, function () {

                $.ajax({
                    async: false,
                    url: "PathController?process=deletecar&carID=" + id,
                    success: function (result) {
                        //swal("Deleted!", "Car has been deleted.", "success");
                        $(location).attr('href', 'conf_car.jsp');
                    }
                });

            });


            return false;
        });

        $('.update-modal').click(function (e) {
            e.preventDefault();

            var id = $(this).attr('id');
            var sid = $(this).attr('type');
            $.ajax({
                async: false,
                url: "PathController?process=updatetempmodalrecord&staffid=" + id + "&id=" + sid,
                success: function (result) {
                    $('#modalhere').empty().html(result).hide().fadeIn(300);
                }
            });


            $('#myModal').modal('toggle')
            return false;
        });

        $(".delete-leave").click(function (e) {

            var id = $(this).attr('id');
            var staffid = $(this).attr('href');
            swal({
                title: "Anda pasti untuk padam?",
                text: "Anda tidak akan dapat mengembalikannya semula!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: 'Ya, padam!',
                closeOnConfirm: false,
            }, function () {

                $.ajax({
                    async: false,
                    url: "PathController?process=deletecheck&id=" + id,
                    success: function (result) {
                        //swal("Deleted!", "Car has been deleted.", "success");
                        $(location).attr('href', 'admin_list.jsp');
                    }
                });

            });

            //} 

            e.stopPropagation();
            return false;
        });


    });

</script>
<body>
    <div class="page-wrapper">
        <jsp:include page='layout/top.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
        <jsp:include page='layout/sidebar.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>


        <div class="content-wrapper">


            <div class="page-content fade-in-up">
                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">
                            <button class="btn btn-outline-primary btn-rounded mr-2 add-manual" id="attendance">Tambah</button>
                        </div>
                       <!-- <div class="ibox-tools pull-right">
                            <button class="btn btn-outline-primary btn-rounded ml-2 mr-1" id="print"><i class="fa fa-print mr-2" aria-hidden="true"></i>Print</button>
                        </div>-->

                    </div>
                    <div class="ibox-body">
                        <h5 class="font-strong mb-4">Senarai Anggota Perkhidmatan</h5>

                        <div class="flexbox mb-4">

                            <!--<div class="flexbox mb-4">


                                <button id="addnew" class="btn btn-primary btn-air mr-4">Add Staff</button>
                                <label class="mb-0 mr-2">Type:</label>
                                <select class="selectpicker show-tick form-control" id="type-filter" title="Please select" data-style="btn-solid" data-width="150px">
                                    <option value="">All</option>
                                    <option>Shipped</option>
                                    <option>Completed</option>
                                    <option>Pending</option>
                                    <option>Canceled</option>
                                </select>
                            </div>-->
                            <div class="input-group-icon input-group-icon-left mr-3">
                                <span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
                                <input class="form-control form-control-rounded form-control-solid" id="key-search" type="text" placeholder="Search ...">
                            </div>
                        </div>
                        <div class="table-responsive row">
                            <table class="table table-bordered table-hover" id="datatable1">
                                <thead class="thead-default thead-lg">
                                    <tr>
                                        <th>#</th>
                                        <th>ID Pekerja</th>
                                        <th>Nama</th>
                                        <th>Syif ID</th>
                                        <th>Active</th>
                                        <th>Status</th>
                                        <th class="no-sort"></th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <%List<Checkshift> listAll = (List<Checkshift>) CheckDAO.getListShiftStaff(log);
                                        int i = 0;

                                        for (Checkshift j : listAll) {
                                            i++;

                                            CoStaff co = (CoStaff) StaffDAO.getInfo(log, j.getStaffID());
                                            Checkworktime wt = (Checkworktime) CheckDAO.getCheckWorkTime(log, j.getSyifID());

                                    %>

                                    <tr id="<%= j.getId()%>">

                                        <td><%= j.getId()%></td>
                                        <td>
                                            <a href="javascript:;"><%= co.getStaffid()%></a>
                                        </td>
                                        <td><%= co.getName()%><p class="text-muted mb-0"><small><%= co.getPosition()%></small></p></td>

                                        <td>
                                            <p class="mb-0"><small class="text-muted ">Syif ID</small> : <small><%= j.getSyifID()%></small></p>
                                            <p class="mb-0"><small class="text-muted ">Syif</small> : <small><%= wt.getDescp() %></small></p>

                                        </td>
                                        <td><%= j.getActive()%></td>
                                        <td>
                                            <%= j.getApprove() %>
                                        </td>
                                        <td>
                                            <button class="btn btn-circle btn-sm btn-info update-modal" type="<%= j.getId()%>" id="<%= co.getStaffid()%>">Suhu</button>
                                            <!--<button class="btn btn-circle btn-sm btn-warning updatepage" id="<%= co.getStaffid()%>">Kemaskini</button>-->
                                            <!--<a class="text-danger font-16 delete-leave" id="<%= j.getId()%>" href="<%= j.getStaffID()%>"><i class="ti-trash"></i></a>-->
                                        </td>
                                    </tr>

                                    <% }%>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div id="modalhere"></div>
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>

        </div>



        <jsp:include page='layout/bottom.jsp'>
            <jsp:param name="page" value="home"/>
        </jsp:include>
</body>
<script>
    $(function () {
        $('#datatable1').DataTable({
            pageLength: 10,
            fixedHeader: true,
            responsive: true,
            "sDom": 'rtip',
            "order": [[0, "desc"]],
            columnDefs: [{
                    targets: 'no-sort',
                    orderable: false
                }]
        });

        var table = $('#datatable1').DataTable();
        $('#key-search').on('keyup', function () {
            table.search(this.value).draw();
        });
        $('#type-filter').on('change', function () {
            table.column(4).search($(this).val()).draw();
        });
    });
</script>
</html>
