<%-- 
    Document   : visitor_form
    Created on : May 17, 2020, 7:45:07 PM
    Author     : fadhilfahmi
--%>


<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $(".delete-leave").click(function (e) {

            var id = $(this).attr('id');
            swal({
                title: "Anda pasti untuk padam?",
                text: "Anda tidak akan dapat mengembalikannya semula!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: 'Ya, padam!',
                closeOnConfirm: false,
            }, function () {

                $.ajax({
                    async: false,
                    url: "PathController?process=deleteleave&id=" + id,
                    success: function (result) {
                        //swal("Deleted!", "Car has been deleted.", "success");
                        $(location).attr('href', 'leave_list.jsp');
                    }
                });

            });

            //} 

            e.stopPropagation();
            return false;
        });



        $('#section-refresh').on('click', '#gotoquestion', function (e) {
            e.preventDefault();

            var leaveid = $(this).attr('id');
            var id = $(this).attr('href');
            $(location).attr('href', 'question.jsp?process=viewleavetab&tabid=' + id);
            return false;
        });



    });

</script>
<body>
    <div class="page-wrapper">
        <header class="header  align-middle">
            <!-- START TOP-LEFT TOOLBAR-->


            <!-- END TOP-LEFT TOOLBAR-->
            <!--LOGO-->
            <a class="page-brand align-middle" href="Login"><img src="./assets/img/logolcsbwithtitle.png" width="85%"></a>

            <!-- END TOP-RIGHT TOOLBAR-->
        </header>

        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-content fade-in-up">
                <div class="col-lg-6 col-sm-12">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Maklumat Pelawat</div>
                            </div>
                            <div class="ibox-body">
                                 <form class="form-horizontal" id="form-sample-1" method="post" novalidate="novalidate" action="VisitorController?process=savedeptform">
                                     <input type="hidden" name="deptID" value="<%= request.getParameter("deptID") %>">
                                    <div class="form-group mb-4 mt-4">
                                        <input class="form-control form-control-lg form-control-rounded form-control-air" type="text" placeholder="Nama"  name="name">
                                    </div>
                                    <div class="form-group mb-4">
                                        <input class="form-control form-control-lg form-control-rounded form-control-air" type="text" placeholder="No. Telefon"  name="notel">
                                    </div>
                                    <div class="text-center mb-4">
                                       
                                        <button class="btn btn-lg btn-blue btn-rounded btn-air" type="submit" style="width:200px;">Daftar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                </div>

            </div>
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
        </div>
    </div>

    <jsp:include page='layout/bottom.jsp'>
        <jsp:param name="page" value="home"/>
    </jsp:include>
   <script src="./assets/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="./assets/vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <!-- PAGE LEVEL PLUGINS-->
    <!-- CORE SCRIPTS-->
    <!-- PAGE LEVEL SCRIPTS-->
    <script>
        $("#form-sample-1").validate({
            rules: {
                name: {
                    minlength: 2,
                    required: !0
                },
                notel: {
                    required: !0,
                    notel: !0
                }
            },
            errorClass: "help-block error",
            highlight: function(e) {
                $(e).closest(".form-group.row").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group.row").removeClass("has-error")
            },
        });
    </script>
</body>
</html>
