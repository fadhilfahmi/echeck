<%-- 
    Document   : visitor
    Created on : May 17, 2020, 12:14:35 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.echeck.model.Checkform"%>
<%@page import="com.lcsb.echeck.model.Checkmaster"%>
<%@page import="com.lcsb.echeck.model.Check"%>
<%@page import="com.lcsb.echeck.dao.CheckDAO"%>
<%@page import="com.lcsb.echeck.dao.StaffDAO"%>
<%@page import="com.lcsb.echeck.model.CoStaff"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.echeck.dao.AccountingPeriod"%>
<%@page import="com.lcsb.echeck.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

    //String balLeave = "";
    double balLeave = 0;

    String curYear = AccountingPeriod.getCurYearByCurrentDate();
    String curMonth = AccountingPeriod.getCurPeriodByCurrentDate();
    String curDate = AccountingPeriod.getCurrentTimeStamp();

    String staffID = StaffDAO.getInfoByEmail(log, log.getEmail()).getStaffid();


%>
<!DOCTYPE html>
<jsp:include page='layout/header.jsp'>
    <jsp:param name="page" value="home"/>
</jsp:include>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        var idleState = false;
        var idleTimer = null;
        $('*').bind('mousemove click mouseup mousedown keydown keypress keyup submit change mouseenter scroll resize dblclick', function () {
            //console.log('breakidle');
            clearTimeout(idleTimer);
            if (idleState == true) {
                console.log('idle1:' + idleTimer);
                $.ajax({
                    url: "section-check.jsp?staffID=<%= staffID%>",
                    success: function (result) {
                        $('#section-refresh').empty().html(result).hide().fadeIn(300);
                    }
                });

            }
            idleState = false;
            idleTimer = setTimeout(function () {

                idleState = true;
                console.log('idle2:' + idleState);
            }, 5000);
        });
        $("body").trigger("mousemove");

        $.ajax({
            url: "section-check.jsp?staffID=<%= staffID%>",
            success: function (result) {
                $('#section-refresh').empty().html(result).hide().fadeIn(300);
            }
        });

        $(".delete-leave").click(function (e) {

            var id = $(this).attr('id');
            swal({
                title: "Anda pasti untuk padam?",
                text: "Anda tidak akan dapat mengembalikannya semula!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: 'Ya, padam!',
                closeOnConfirm: false,
            }, function () {

                $.ajax({
                    async: false,
                    url: "PathController?process=deleteleave&id=" + id,
                    success: function (result) {
                        //swal("Deleted!", "Car has been deleted.", "success");
                        $(location).attr('href', 'leave_list.jsp');
                    }
                });

            });

            //} 

            e.stopPropagation();
            return false;
        });



        $('#section-refresh').on('click', '#gotoquestion', function (e) {
            e.preventDefault();

            var leaveid = $(this).attr('id');
            var id = $(this).attr('href');
            $(location).attr('href', 'question.jsp?process=viewleavetab&tabid=' + id);
            return false;
        });



    });

</script>
<body>
    <div class="page-wrapper">
        <header class="header  align-middle">
            <!-- START TOP-LEFT TOOLBAR-->


            <!-- END TOP-LEFT TOOLBAR-->
            <!--LOGO-->
            <a class="page-brand align-middle" href="Login"><img src="./assets/img/logolcsbwithtitle.png" width="85%"></a>

            <!-- END TOP-RIGHT TOOLBAR-->
        </header>

        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-content fade-in-up">
                <div class="col-lg-6 col-sm-12" id="section-refresh">
                </div>

            </div>
            <!-- END PAGE CONTENT-->
            <jsp:include page='layout/footer.jsp'>
                <jsp:param name="page" value="home"/>
            </jsp:include>
        </div>
    </div>

    <jsp:include page='layout/bottom.jsp'>
        <jsp:param name="page" value="home"/>
    </jsp:include>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
        });
    </script>
</body>
</html>
