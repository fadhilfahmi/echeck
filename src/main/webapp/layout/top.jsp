<%-- 
    Document   : top
    Created on : Sep 29, 2019, 10:02:16 PM
    Author     : fadhilfahmi
--%>

<%@page import="java.util.List"%>
<%@page import="com.lcsb.echeck.dao.StaffDAO"%>
<%@page import="com.lcsb.echeck.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");


%>
<!DOCTYPE html>


<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $(this).scrollTop(0);

        


        $('#applyleave').click(function (e) {
            e.preventDefault();
            $(location).attr('href', 'PathController?process=applyleavestep');

            return false;
        });



    });

</script>
<header class="header">
    <ul class="nav navbar-toolbar">
        <li>
            <a class="nav-link sidebar-toggler js-sidebar-toggler" href="javascript:;">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
        </li>
        
        
        <a class="page-brand align-middle" href="Login"><img src="./assets/img/logolcsbwithtitle.png" width="85%"></a>
        <li>
            <a class="nav-link">
                <span>eDaftar</span>
            </a>
        </li>
    </ul>
    
    <ul class="nav navbar-toolbar">
        <li class="dropdown dropdown-user mr-0">
            <a class="nav-link dropdown-toggle link mr-0 ml-2">
                 <span><%= log.getFullname()%></span>
                <div class="admin-avatar ml-2 mr-0">
                    <img class="img-circle" src="<%= log.getImageURL()%>" alt="image" />
                </div>
            </a>
            
        </li>

        <!--<li>
            <a class="nav-link toolbar-icon" href="user_page.jsp?id=<%= log.getUserID() %>"><i class="fa fa-user" aria-hidden="true"></i></a>
        </li>-->
        <li class="timeout-toggler ml-0">
            <a class="nav-link toolbar-icon ml-0" data-toggle="modal" data-target="#session-dialog" href="javascript:;"><i class="fa fa-sign-out" aria-hidden="true"><span class="notify-signal"></span></i></a>
        </li>
    </ul>

    <!-- END TOP-LEFT TOOLBAR-->
    <!--LOGO-->

    <!-- START TOP-RIGHT TOOLBAR-->
    
    <!-- END TOP-RIGHT TOOLBAR-->
</header>