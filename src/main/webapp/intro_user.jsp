<%-- 
    Document   : intro_user
    Created on : Jan 22, 2020, 10:57:48 PM
    Author     : fadhilfahmi
--%>
<%@page import="com.lcsb.eleave.model.LoginProfile"%>
<%@page import="com.lcsb.eleave.dao.StaffDAO"%>
<%@page import="com.lcsb.eleave.model.CoStaff"%>
<%@page import="com.lcsb.eleave.model.CoStaff"%>
<%@page import="java.util.List"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width initial-scale=1.0">
        <title>Sistem Permohonan Cuti LCSB</title>
        <!-- GLOBAL MAINLY STYLES-->
        <link href="./assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
        <link href="./assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
        <link href="./assets/vendors/line-awesome/css/line-awesome.min.css" rel="stylesheet" />
        <link href="./assets/vendors/themify-icons/css/themify-icons.css" rel="stylesheet" />
        <link href="./assets/vendors/animate.css/animate.min.css" rel="stylesheet" />
        <link href="./assets/vendors/toastr/toastr.min.css" rel="stylesheet" />
        <link href="./assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
        <link href="./assets/vendors/bootstrap-sweetalert/dist/sweetalert.css" rel="stylesheet" />
        <!-- PLUGINS STYLES-->
        <!-- THEME STYLES-->
        <link href="assets/css/main.min.css" rel="stylesheet" />


        <!-- PAGE LEVEL STYLES-->
    </head>

    <body>
        <div class="page-wrapper">
            <!-- START HEADER-->
            <header class="header  align-middle">
                <!-- START TOP-LEFT TOOLBAR-->
                

                <!-- END TOP-LEFT TOOLBAR-->
                <!--LOGO-->
                <a class="page-brand align-middle" href="Login"><img src="./assets/img/logolcsb3.png" width="85%"></a>

                <!-- END TOP-RIGHT TOOLBAR-->
            </header>
            <!-- END HEADER-->
            <!-- START SIDEBAR-->

            <!-- END SIDEBAR-->
            <div class="content-wrapper">
                <!-- START PAGE CONTENT-->

                <div class="page-content fade-in-up col-lg-6 col-sm-12 centered">
                    <div class="alert alert-primary alert-bordered">
                        <h4>Assalamualaikum,</h4>
                        <p>Nampaknya ini adalah kali pertama anda daftar masuk. Sila lengkapkan maklumat anda untuk mengguna Sistem Permohonan Cuti ini.</p>



                    </div>
                    <div class="ibox">

                        <div class="ibox-body">
                            <form id="form-wizard" action="javascript:;" novalidate="novalidate" name="form_intro">
                                <h6>Step 1</h6>
                                <section>
                                    <h3>Nama</h3>
                                    <div class="form-group" id="list_staff">
                                        <div class="form-group mb-4">
                                            <label>Jika tiada dalam senarai, tambah manual.</label>
                                            <div class="input-group">
                                                <input class="form-control viewmodalpassenger" type="text" placeholder="Carian nama..." id="name" value="" autocomplete="off">
                                                <input class="form-control" type="hidden"  id="staffIDX" name="staffIDX" value="">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-outline-secondary viewmodalpassenger">Cari</button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                </section>
                                <h6>Step 2</h6>
                                <section>
                                    <h3>Maklumat Diri</h3>
                                    <div id="form_1">

                                    </div>
                                </section>
                                <h6>Step 3</h6>
                                <section>
                                    <h3>Perjawatan</h3>
                                    <div id="form_2">
                                    </div>
                                </section>
                                <h6>Step 4</h6>
                                <section>
                                    <h3>Penyeliaan</h3>
                                    <div id="form_3"></div>
                                </section>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT-->

            </div>
        </div>
        <!-- START SEARCH PANEL-->
        <form class="search-top-bar" action="search.html">
            <input class="form-control search-input" type="text" placeholder="Search...">
            <button class="reset input-search-icon"><i class="ti-search"></i></button>
            <button class="reset input-search-close" type="button"><i class="ti-close"></i></button>
        </form>
        <!-- END SEARCH PANEL-->
        <!-- BEGIN THEME CONFIG PANEL-->
        
        <!-- END THEME CONFIG PANEL-->
        <!-- BEGIN PAGA BACKDROPS-->
        <div class="sidenav-backdrop backdrop"></div>
        <div class="preloader-backdrop">
            <div class="page-preloader">Loading</div>
        </div>
        <!-- END PAGA BACKDROPS-->
        <!-- New question dialog-->
        <div class="modal fade" id="session-dialog">
            <div class="modal-dialog" style="width:400px;" role="document">
                <div class="modal-content timeout-modal">
                    <div class="modal-body">
                        <button class="close" data-dismiss="modal" aria-label="Close"></button>
                        <div class="text-center mt-3 mb-4"><i class="ti-lock timeout-icon"></i></div>
                        <div class="text-center h4 mb-3">Set Auto Logout</div>
                        <p class="text-center mb-4">You are about to be signed out due to inactivity.<br>Select after how many minutes of inactivity you log out of the system.</p>
                        <div id="timeout-reset-box" style="display:none;">
                            <div class="form-group text-center">
                                <button class="btn btn-danger btn-fix btn-air" id="timeout-reset">Deactivate</button>
                            </div>
                        </div>
                        <div id="timeout-activate-box">
                            <form id="timeout-form" action="javascript:;">
                                <div class="form-group pl-3 pr-3 mb-4">
                                    <input class="form-control form-control-line" type="text" name="timeout_count" placeholder="Minutes" id="timeout-count">
                                </div>
                                <div class="form-group text-center">
                                    <button class="btn btn-primary btn-fix btn-air" id="timeout-activate">Activate</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalpassengerdiv"></div>
        <!-- End New question dialog-->
        <!-- QUICK SIDEBAR-->

        <!-- END QUICK SIDEBAR-->
        <!-- CORE PLUGINS-->
        <script src="./assets/vendors/jquery/dist/jquery.min.js"></script>
        <script src="./assets/vendors/popper.js/dist/umd/popper.min.js"></script>
        <script src="./assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="./assets/vendors/metisMenu/dist/metisMenu.min.js"></script>
        <script src="./assets/vendors/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="./assets/vendors/jquery-idletimer/dist/idle-timer.min.js"></script>
        <script src="./assets/vendors/toastr/toastr.min.js"></script>
        <script src="./assets/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="./assets/vendors/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <!-- PAGE LEVEL PLUGINS-->
        <script src="./assets/vendors/jquery.steps/build/jquery.steps.min.js"></script>

        <script src="./assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js"></script>
        <!-- CORE SCRIPTS-->
        <script src="assets/js/app.min.js"></script>
        <!-- PAGE LEVEL SCRIPTS-->
        <script>
            $(function () {

                $('body').on('change', '#name', function (e) {

                    var n = $(this).val();
                    //alert(s);
                    //} 

                    e.stopPropagation();
                    return false;

                });
                $('#form-wizard').steps({
                    headerTag: "h6",
                    bodyTag: "section",
                    titleTemplate: '<span class="step-number">#index#</span> #title#',
                    onStepChanging: function (event, currentIndex, newIndex) {
                        var form = $(this);
                        // Always allow going backward even if the current step contains invalid fields!
                        if (currentIndex > newIndex) {
                            return true;
                        }

                        if (currentIndex == 0) {
                            var staffID = $('#staffIDX').val();
                            var name = $('#name').val();
                            $.ajax({
                                url: "intro_form_1.jsp?staffIDX=" + staffID + "&name=" + name,
                                success: function (result) {
                                    $('#form_1').empty().html(result).hide().fadeIn(300);
                                }
                            });
                            //return false;
                        }

                        if (currentIndex == 1) {
                            var staffID = $('#staffID').val();
                            $.ajax({
                                url: "intro_form_2.jsp?staffID=" + staffID,
                                success: function (result) {
                                    $('#form_2').empty().html(result).hide().fadeIn(300);
                                }
                            });
                            //return false;
                        }
                        if (currentIndex == 2) {
                            var staffID = $('#staffID').val();
                            $.ajax({
                                url: "intro_form_3.jsp?staffID=" + staffID,
                                success: function (result) {
                                    $('#form_3').empty().html(result).hide().fadeIn(300);
                                }
                            });
                            //return false;
                        }
                        // Clean up if user went backward before
                        if (currentIndex < newIndex) {
                            // To remove error styles
                            $(".body:eq(" + newIndex + ") label.error", form).remove();
                            $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                        }

                        // Disable validation on fields that are disabled or hidden.
                        form.validate().settings.ignore = ":disabled,:hidden";

                        // Start validation; Prevent going forward if false
                        return form.valid();
                    },
                    onFinishing: function (event, currentIndex) {
                        var form = $(this);
                        form.validate().settings.ignore = ":disabled";
                        return form.valid();
                    },
                    onFinished: function (event, currentIndex) {
                        //toastr.success('Submitted!');


                        var a = $("#form-wizard :input").serialize();
                        $.ajax({
                            async: true,
                            data: a,
                            type: 'POST',
                            url: "PathController?process=saveintroform",
                            success: function (result) {
                                swal({
                                    title: "Terima Kasih",
                                    text: "Maklumat anda telah dikemaskini!",
                                    type: "success"
                                }, function () {
                                    window.location = "Login";
                                });



                            }
                        });
                        //} 

                    }
                }).validate({
                    errorPlacement: function errorPlacement(error, element) {
                        error.insertAfter(element);
                    },
                    rules: {
                        confirm: {
                            equalTo: "#password"
                        }
                    },
                    errorClass: "help-block error",
                    highlight: function (e) {
                        $(e).closest(".form-group").addClass("has-error")
                    },
                    unhighlight: function (e) {
                        $(e).closest(".form-group").removeClass("has-error")
                    },
                });

                $('#list_staff').on('click', '.viewmodalpassenger', function (e) {
                    e.preventDefault();

                    var id = $(this).attr('id');
                    var sessiondid = $('#sessionid').val();
                    $.ajax({
                        async: false,
                        url: "PathController?process=viewstafflist&sessionid=" + sessiondid + "&id=" + id,
                        success: function (result) {
                            $('#modalpassengerdiv').empty().html(result).hide().fadeIn(300);
                        }
                    });


                    $('#modalpassenger').modal('toggle');
                    return false;
                });

            })
        </script>
    </body>

</html>