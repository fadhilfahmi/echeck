<%-- 
    Document   : intro_form_3
    Created on : Jan 23, 2020, 10:39:54 AM
    Author     : fadhilfahmi
--%>



<%@page import="com.lcsb.echeck.model.LoginProfile"%>
<%@page import="com.lcsb.echeck.dao.StaffDAO"%>
<%@page import="com.lcsb.echeck.model.CoStaff"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
%>
<!DOCTYPE html>


<div class="form-group">
    <label class="form-control-label">Di bawah seliaan</label>
    <select class="selectpicker form-control" id="supervisor" data-live-search="true" name="supervisor">
        <option value="Tiada">Tiada</option>
        <%List<CoStaff> listAll = (List<CoStaff>) StaffDAO.getAllStaff(log);
            int i = 0;

            for (CoStaff j : listAll) {
                i++;

        %>
        <option value="<%= j.getStaffid()%>"><%= j.getName()%></option>
        <%
            }
        %>

    </select>
</div>


        


<script>
    $(function () {

        $('.selectpicker').selectpicker('render');
    })
</script>
