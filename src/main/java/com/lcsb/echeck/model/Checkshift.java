/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.echeck.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "checkshift")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Checkshift.findAll", query = "SELECT c FROM Checkshift c"),
    @NamedQuery(name = "Checkshift.findById", query = "SELECT c FROM Checkshift c WHERE c.id = :id"),
    @NamedQuery(name = "Checkshift.findByStaffID", query = "SELECT c FROM Checkshift c WHERE c.staffID = :staffID"),
    @NamedQuery(name = "Checkshift.findBySyifID", query = "SELECT c FROM Checkshift c WHERE c.syifID = :syifID"),
    @NamedQuery(name = "Checkshift.findByActive", query = "SELECT c FROM Checkshift c WHERE c.active = :active"),
    @NamedQuery(name = "Checkshift.findByApprove", query = "SELECT c FROM Checkshift c WHERE c.approve = :approve"),
    @NamedQuery(name = "Checkshift.findByApprovedbyID", query = "SELECT c FROM Checkshift c WHERE c.approvedbyID = :approvedbyID"),
    @NamedQuery(name = "Checkshift.findByApprovedbyDate", query = "SELECT c FROM Checkshift c WHERE c.approvedbyDate = :approvedbyDate")})
public class Checkshift implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "staffID")
    private String staffID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "syifID")
    private String syifID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "active")
    private boolean active;
    @Basic(optional = false)
    @NotNull
    @Column(name = "approve")
    private boolean approve;
    @Size(max = 10)
    @Column(name = "approvedbyID")
    private String approvedbyID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "approvedbyDate")
    private String approvedbyDate;

    public Checkshift() {
    }

    public Checkshift(Integer id) {
        this.id = id;
    }

    public Checkshift(Integer id, String staffID, String syifID, boolean active, boolean approve, String approvedbyDate) {
        this.id = id;
        this.staffID = staffID;
        this.syifID = syifID;
        this.active = active;
        this.approve = approve;
        this.approvedbyDate = approvedbyDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStaffID() {
        return staffID;
    }

    public void setStaffID(String staffID) {
        this.staffID = staffID;
    }

    public String getSyifID() {
        return syifID;
    }

    public void setSyifID(String syifID) {
        this.syifID = syifID;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean getApprove() {
        return approve;
    }

    public void setApprove(boolean approve) {
        this.approve = approve;
    }

    public String getApprovedbyID() {
        return approvedbyID;
    }

    public void setApprovedbyID(String approvedbyID) {
        this.approvedbyID = approvedbyID;
    }

    public String getApprovedbyDate() {
        return approvedbyDate;
    }

    public void setApprovedbyDate(String approvedbyDate) {
        this.approvedbyDate = approvedbyDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Checkshift)) {
            return false;
        }
        Checkshift other = (Checkshift) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.echeck.model.Checkshift[ id=" + id + " ]";
    }
    
}
