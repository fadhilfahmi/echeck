/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.echeck.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "leave_request")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LeaveRequest.findAll", query = "SELECT l FROM LeaveRequest l"),
    @NamedQuery(name = "LeaveRequest.findById", query = "SELECT l FROM LeaveRequest l WHERE l.id = :id"),
    @NamedQuery(name = "LeaveRequest.findByLeaveID", query = "SELECT l FROM LeaveRequest l WHERE l.leaveID = :leaveID"),
    @NamedQuery(name = "LeaveRequest.findByRequestto", query = "SELECT l FROM LeaveRequest l WHERE l.requestto = :requestto"),
    @NamedQuery(name = "LeaveRequest.findByStatus", query = "SELECT l FROM LeaveRequest l WHERE l.status = :status"),
    @NamedQuery(name = "LeaveRequest.findByDatetochange", query = "SELECT l FROM LeaveRequest l WHERE l.datetochange = :datetochange"),
    @NamedQuery(name = "LeaveRequest.findByDaterequest", query = "SELECT l FROM LeaveRequest l WHERE l.daterequest = :daterequest"),
    @NamedQuery(name = "LeaveRequest.findByTimerequest", query = "SELECT l FROM LeaveRequest l WHERE l.timerequest = :timerequest")})
public class LeaveRequest implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 11)
    @Column(name = "leaveID")
    private String leaveID;
    @Size(max = 20)
    @Column(name = "requestto")
    private String requestto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "status")
    private boolean status;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "datetochange")
    private String datetochange;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "daterequest")
    private String daterequest;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "timerequest")
    private String timerequest;

    public LeaveRequest() {
    }

    public LeaveRequest(Integer id) {
        this.id = id;
    }

    public LeaveRequest(Integer id, String leaveID, boolean status, String datetochange, String daterequest, String timerequest) {
        this.id = id;
        this.leaveID = leaveID;
        this.status = status;
        this.datetochange = datetochange;
        this.daterequest = daterequest;
        this.timerequest = timerequest;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLeaveID() {
        return leaveID;
    }

    public void setLeaveID(String leaveID) {
        this.leaveID = leaveID;
    }

    public String getRequestto() {
        return requestto;
    }

    public void setRequestto(String requestto) {
        this.requestto = requestto;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getDatetochange() {
        return datetochange;
    }

    public void setDatetochange(String datetochange) {
        this.datetochange = datetochange;
    }

    public String getDaterequest() {
        return daterequest;
    }

    public void setDaterequest(String daterequest) {
        this.daterequest = daterequest;
    }

    public String getTimerequest() {
        return timerequest;
    }

    public void setTimerequest(String timerequest) {
        this.timerequest = timerequest;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LeaveRequest)) {
            return false;
        }
        LeaveRequest other = (LeaveRequest) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.eleave.model.LeaveRequest[ id=" + id + " ]";
    }
    
}
