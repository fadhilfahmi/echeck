/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.echeck.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "checkform")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Checkform.findAll", query = "SELECT c FROM Checkform c"),
    @NamedQuery(name = "Checkform.findById", query = "SELECT c FROM Checkform c WHERE c.id = :id"),
    @NamedQuery(name = "Checkform.findByCheckID", query = "SELECT c FROM Checkform c WHERE c.checkID = :checkID"),
    @NamedQuery(name = "Checkform.findByQuestion1", query = "SELECT c FROM Checkform c WHERE c.question1 = :question1"),
    @NamedQuery(name = "Checkform.findByQuestion2", query = "SELECT c FROM Checkform c WHERE c.question2 = :question2"),
    @NamedQuery(name = "Checkform.findByQuestion3", query = "SELECT c FROM Checkform c WHERE c.question3 = :question3")})
public class Checkform implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 11)
    @Column(name = "checkID")
    private String checkID;
    @Size(max = 200)
    @Column(name = "question1")
    private String question1;
    @Size(max = 10)
    @Column(name = "question2")
    private String question2;
    @Size(max = 10)
    @Column(name = "question3")
    private String question3;

    public Checkform() {
    }

    public Checkform(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCheckID() {
        return checkID;
    }

    public void setCheckID(String checkID) {
        this.checkID = checkID;
    }

    public String getQuestion1() {
        return question1;
    }

    public void setQuestion1(String question1) {
        this.question1 = question1;
    }

    public String getQuestion2() {
        return question2;
    }

    public void setQuestion2(String question2) {
        this.question2 = question2;
    }

    public String getQuestion3() {
        return question3;
    }

    public void setQuestion3(String question3) {
        this.question3 = question3;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Checkform)) {
            return false;
        }
        Checkform other = (Checkform) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.echeck.model.Checkform[ id=" + id + " ]";
    }
    
}
