/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.echeck.model;

/**
 *
 * @author fadhilfahmi
 */
public class Check {
    
    private Checkmaster cm;
    private Checkform ci;

    public Checkmaster getCm() {
        return cm;
    }

    public void setCm(Checkmaster cm) {
        this.cm = cm;
    }

    public Checkform getCi() {
        return ci;
    }

    public void setCi(Checkform ci) {
        this.ci = ci;
    }
    
    
    
}
