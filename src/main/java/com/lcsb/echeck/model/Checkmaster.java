/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.echeck.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "checkmaster")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Checkmaster.findAll", query = "SELECT c FROM Checkmaster c"),
    @NamedQuery(name = "Checkmaster.findById", query = "SELECT c FROM Checkmaster c WHERE c.id = :id"),
    @NamedQuery(name = "Checkmaster.findByStaffID", query = "SELECT c FROM Checkmaster c WHERE c.staffID = :staffID"),
    @NamedQuery(name = "Checkmaster.findByDate", query = "SELECT c FROM Checkmaster c WHERE c.date = :date"),
    @NamedQuery(name = "Checkmaster.findByTimescan", query = "SELECT c FROM Checkmaster c WHERE c.timescan = :timescan"),
    @NamedQuery(name = "Checkmaster.findByTimevalid", query = "SELECT c FROM Checkmaster c WHERE c.timevalid = :timevalid"),
    @NamedQuery(name = "Checkmaster.findByTemperature", query = "SELECT c FROM Checkmaster c WHERE c.temperature = :temperature"),
    @NamedQuery(name = "Checkmaster.findByStatus", query = "SELECT c FROM Checkmaster c WHERE c.status = :status"),
    @NamedQuery(name = "Checkmaster.findByTimeout", query = "SELECT c FROM Checkmaster c WHERE c.timeout = :timeout")})
public class Checkmaster implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 11)
    @Column(name = "id")
    private String id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "staffID")
    private String staffID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "date")
    private String date;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "timescan")
    private String timescan;
    @Size(max = 8)
    @Column(name = "timevalid")
    private String timevalid;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "temperature")
    private Double temperature;
    @Size(max = 20)
    @Column(name = "status")
    private String status;
    @Size(max = 8)
    @Column(name = "timeout")
    private String timeout;

    public Checkmaster() {
    }

    public Checkmaster(String id) {
        this.id = id;
    }

    public Checkmaster(String id, String staffID, String date, String timescan) {
        this.id = id;
        this.staffID = staffID;
        this.date = date;
        this.timescan = timescan;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStaffID() {
        return staffID;
    }

    public void setStaffID(String staffID) {
        this.staffID = staffID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTimescan() {
        return timescan;
    }

    public void setTimescan(String timescan) {
        this.timescan = timescan;
    }

    public String getTimevalid() {
        return timevalid;
    }

    public void setTimevalid(String timevalid) {
        this.timevalid = timevalid;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Checkmaster)) {
            return false;
        }
        Checkmaster other = (Checkmaster) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.echeck.model.Checkmaster[ id=" + id + " ]";
    }
    
}
