/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.echeck.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "leave_info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LeaveInfo.findAll", query = "SELECT l FROM LeaveInfo l"),
    @NamedQuery(name = "LeaveInfo.findById", query = "SELECT l FROM LeaveInfo l WHERE l.id = :id"),
    @NamedQuery(name = "LeaveInfo.findByStaffID", query = "SELECT l FROM LeaveInfo l WHERE l.staffID = :staffID"),
    @NamedQuery(name = "LeaveInfo.findByEligibleleave", query = "SELECT l FROM LeaveInfo l WHERE l.eligibleleave = :eligibleleave"),
    @NamedQuery(name = "LeaveInfo.findByYear", query = "SELECT l FROM LeaveInfo l WHERE l.year = :year"),
    @NamedQuery(name = "LeaveInfo.findByBf", query = "SELECT l FROM LeaveInfo l WHERE l.bf = :bf"),
    @NamedQuery(name = "LeaveInfo.findByMc", query = "SELECT l FROM LeaveInfo l WHERE l.mc = :mc")})
public class LeaveInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 30)
    @Column(name = "staffID")
    private String staffID;
    @Column(name = "eligibleleave")
    private Integer eligibleleave;
    @Size(max = 4)
    @Column(name = "year")
    private String year;
    @Column(name = "bf")
    private Integer bf;
    @Column(name = "mc")
    private Integer mc;

    public LeaveInfo() {
    }

    public LeaveInfo(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStaffID() {
        return staffID;
    }

    public void setStaffID(String staffID) {
        this.staffID = staffID;
    }

    public Integer getEligibleleave() {
        return eligibleleave;
    }

    public void setEligibleleave(Integer eligibleleave) {
        this.eligibleleave = eligibleleave;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Integer getBf() {
        return bf;
    }

    public void setBf(Integer bf) {
        this.bf = bf;
    }

    public Integer getMc() {
        return mc;
    }

    public void setMc(Integer mc) {
        this.mc = mc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LeaveInfo)) {
            return false;
        }
        LeaveInfo other = (LeaveInfo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.eleave.model.LeaveInfo[ id=" + id + " ]";
    }
    
}
