/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.echeck.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "leave_comment")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LeaveComment.findAll", query = "SELECT l FROM LeaveComment l"),
    @NamedQuery(name = "LeaveComment.findById", query = "SELECT l FROM LeaveComment l WHERE l.id = :id"),
    @NamedQuery(name = "LeaveComment.findByLeaveID", query = "SELECT l FROM LeaveComment l WHERE l.leaveID = :leaveID"),
    @NamedQuery(name = "LeaveComment.findByComment", query = "SELECT l FROM LeaveComment l WHERE l.comment = :comment"),
    @NamedQuery(name = "LeaveComment.findByDate", query = "SELECT l FROM LeaveComment l WHERE l.date = :date"),
    @NamedQuery(name = "LeaveComment.findByTime", query = "SELECT l FROM LeaveComment l WHERE l.time = :time"),
    @NamedQuery(name = "LeaveComment.findByStaffID", query = "SELECT l FROM LeaveComment l WHERE l.staffID = :staffID")})
public class LeaveComment implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 4)
    @Column(name = "leaveID")
    private String leaveID;
    @Size(max = 300)
    @Column(name = "comment")
    private String comment;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "date")
    private String date;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "time")
    private String time;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "staffID")
    private String staffID;

    public LeaveComment() {
    }

    public LeaveComment(Integer id) {
        this.id = id;
    }

    public LeaveComment(Integer id, String date, String time, String staffID) {
        this.id = id;
        this.date = date;
        this.time = time;
        this.staffID = staffID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLeaveID() {
        return leaveID;
    }

    public void setLeaveID(String leaveID) {
        this.leaveID = leaveID;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStaffID() {
        return staffID;
    }

    public void setStaffID(String staffID) {
        this.staffID = staffID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LeaveComment)) {
            return false;
        }
        LeaveComment other = (LeaveComment) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.eleave.model.LeaveComment[ id=" + id + " ]";
    }
    
}
