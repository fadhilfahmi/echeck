/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.echeck.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "leave_reason_template")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LeaveReasonTemplate.findAll", query = "SELECT l FROM LeaveReasonTemplate l"),
    @NamedQuery(name = "LeaveReasonTemplate.findById", query = "SELECT l FROM LeaveReasonTemplate l WHERE l.id = :id"),
    @NamedQuery(name = "LeaveReasonTemplate.findByReason", query = "SELECT l FROM LeaveReasonTemplate l WHERE l.reason = :reason"),
    @NamedQuery(name = "LeaveReasonTemplate.findByActive", query = "SELECT l FROM LeaveReasonTemplate l WHERE l.active = :active"),
    @NamedQuery(name = "LeaveReasonTemplate.findByDorder", query = "SELECT l FROM LeaveReasonTemplate l WHERE l.dorder = :dorder")})
public class LeaveReasonTemplate implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "reason")
    private String reason;
    @Basic(optional = false)
    @NotNull
    @Column(name = "active")
    private boolean active;
    @Column(name = "dorder")
    private Integer dorder;

    public LeaveReasonTemplate() {
    }

    public LeaveReasonTemplate(Integer id) {
        this.id = id;
    }

    public LeaveReasonTemplate(Integer id, String reason, boolean active) {
        this.id = id;
        this.reason = reason;
        this.active = active;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Integer getDorder() {
        return dorder;
    }

    public void setDorder(Integer dorder) {
        this.dorder = dorder;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LeaveReasonTemplate)) {
            return false;
        }
        LeaveReasonTemplate other = (LeaveReasonTemplate) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.eleave.model.LeaveReasonTemplate[ id=" + id + " ]";
    }
    
}
