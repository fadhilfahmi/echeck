/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.echeck.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "estateinfo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EstateInfo.findAll", query = "SELECT e FROM EstateInfo e"),
    @NamedQuery(name = "EstateInfo.findByEstatecode", query = "SELECT e FROM EstateInfo e WHERE e.estatecode = :estatecode"),
    @NamedQuery(name = "EstateInfo.findByEstatedescp", query = "SELECT e FROM EstateInfo e WHERE e.estatedescp = :estatedescp"),
    @NamedQuery(name = "EstateInfo.findByPostcode", query = "SELECT e FROM EstateInfo e WHERE e.postcode = :postcode"),
    @NamedQuery(name = "EstateInfo.findByCity", query = "SELECT e FROM EstateInfo e WHERE e.city = :city"),
    @NamedQuery(name = "EstateInfo.findByDistrict", query = "SELECT e FROM EstateInfo e WHERE e.district = :district"),
    @NamedQuery(name = "EstateInfo.findByState", query = "SELECT e FROM EstateInfo e WHERE e.state = :state"),
    @NamedQuery(name = "EstateInfo.findByPhone", query = "SELECT e FROM EstateInfo e WHERE e.phone = :phone"),
    @NamedQuery(name = "EstateInfo.findByFax", query = "SELECT e FROM EstateInfo e WHERE e.fax = :fax")})
public class EstateInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatecode")
    private String estatecode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatedescp")
    private String estatedescp;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "address")
    private String address;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "postcode")
    private String postcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "city")
    private String city;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "district")
    private String district;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "state")
    private String state;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "phone")
    private String phone;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "fax")
    private String fax;

    public EstateInfo() {
    }

    public EstateInfo(String estatecode) {
        this.estatecode = estatecode;
    }

    public EstateInfo(String estatecode, String estatedescp, String address, String postcode, String city, String district, String state, String phone, String fax) {
        this.estatecode = estatecode;
        this.estatedescp = estatedescp;
        this.address = address;
        this.postcode = postcode;
        this.city = city;
        this.district = district;
        this.state = state;
        this.phone = phone;
        this.fax = fax;
    }

    public String getEstatecode() {
        return estatecode;
    }

    public void setEstatecode(String estatecode) {
        this.estatecode = estatecode;
    }

    public String getEstatedescp() {
        return estatedescp;
    }

    public void setEstatedescp(String estatedescp) {
        this.estatedescp = estatedescp;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (estatecode != null ? estatecode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstateInfo)) {
            return false;
        }
        EstateInfo other = (EstateInfo) object;
        if ((this.estatecode == null && other.estatecode != null) || (this.estatecode != null && !this.estatecode.equals(other.estatecode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.smartbooking.model.EstateInfo[ estatecode=" + estatecode + " ]";
    }
    
}
