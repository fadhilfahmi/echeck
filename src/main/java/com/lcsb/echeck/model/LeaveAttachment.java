/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.echeck.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "leave_attachment")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LeaveAttachment.findAll", query = "SELECT l FROM LeaveAttachment l"),
    @NamedQuery(name = "LeaveAttachment.findById", query = "SELECT l FROM LeaveAttachment l WHERE l.id = :id"),
    @NamedQuery(name = "LeaveAttachment.findByLeaveID", query = "SELECT l FROM LeaveAttachment l WHERE l.leaveID = :leaveID"),
    @NamedQuery(name = "LeaveAttachment.findByFilename", query = "SELECT l FROM LeaveAttachment l WHERE l.filename = :filename"),
    @NamedQuery(name = "LeaveAttachment.findByStaffID", query = "SELECT l FROM LeaveAttachment l WHERE l.staffID = :staffID"),
    @NamedQuery(name = "LeaveAttachment.findBySessionID", query = "SELECT l FROM LeaveAttachment l WHERE l.sessionID = :sessionID")})
public class LeaveAttachment implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 10)
    @Column(name = "leaveID")
    private String leaveID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "filename")
    private String filename;
    @Size(max = 10)
    @Column(name = "staffID")
    private String staffID;
    @Size(max = 200)
    @Column(name = "sessionID")
    private String sessionID;

    public LeaveAttachment() {
    }

    public LeaveAttachment(Integer id) {
        this.id = id;
    }

    public LeaveAttachment(Integer id, String filename) {
        this.id = id;
        this.filename = filename;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLeaveID() {
        return leaveID;
    }

    public void setLeaveID(String leaveID) {
        this.leaveID = leaveID;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getStaffID() {
        return staffID;
    }

    public void setStaffID(String staffID) {
        this.staffID = staffID;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LeaveAttachment)) {
            return false;
        }
        LeaveAttachment other = (LeaveAttachment) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.eleave.model.LeaveAttachment[ id=" + id + " ]";
    }
    
}
