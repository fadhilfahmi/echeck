/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.echeck.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "checkdept")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Checkdept.findAll", query = "SELECT c FROM Checkdept c"),
    @NamedQuery(name = "Checkdept.findById", query = "SELECT c FROM Checkdept c WHERE c.id = :id"),
    @NamedQuery(name = "Checkdept.findByStaffID", query = "SELECT c FROM Checkdept c WHERE c.staffID = :staffID"),
    @NamedQuery(name = "Checkdept.findByDate", query = "SELECT c FROM Checkdept c WHERE c.date = :date"),
    @NamedQuery(name = "Checkdept.findByTimescan", query = "SELECT c FROM Checkdept c WHERE c.timescan = :timescan"),
    @NamedQuery(name = "Checkdept.findByStatus", query = "SELECT c FROM Checkdept c WHERE c.status = :status"),
    @NamedQuery(name = "Checkdept.findByTimeout", query = "SELECT c FROM Checkdept c WHERE c.timeout = :timeout"),
    @NamedQuery(name = "Checkdept.findByDeptID", query = "SELECT c FROM Checkdept c WHERE c.deptID = :deptID"),
    @NamedQuery(name = "Checkdept.findByNotel", query = "SELECT c FROM Checkdept c WHERE c.notel = :notel"),
    @NamedQuery(name = "Checkdept.findByName", query = "SELECT c FROM Checkdept c WHERE c.name = :name")})
public class Checkdept implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 11)
    @Column(name = "id")
    private String id;
    @Size(max = 10)
    @Column(name = "staffID")
    private String staffID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "date")
    private String date;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "timescan")
    private String timescan;
    @Size(max = 20)
    @Column(name = "status")
    private String status;
    @Size(max = 8)
    @Column(name = "timeout")
    private String timeout;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "deptID")
    private String deptID;
    @Size(max = 20)
    @Column(name = "notel")
    private String notel;
    @Size(max = 100)
    @Column(name = "name")
    private String name;

    public Checkdept() {
    }

    public Checkdept(String id) {
        this.id = id;
    }

    public Checkdept(String id, String date, String timescan, String deptID) {
        this.id = id;
        this.date = date;
        this.timescan = timescan;
        this.deptID = deptID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStaffID() {
        return staffID;
    }

    public void setStaffID(String staffID) {
        this.staffID = staffID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTimescan() {
        return timescan;
    }

    public void setTimescan(String timescan) {
        this.timescan = timescan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    public String getDeptID() {
        return deptID;
    }

    public void setDeptID(String deptID) {
        this.deptID = deptID;
    }

    public String getNotel() {
        return notel;
    }

    public void setNotel(String notel) {
        this.notel = notel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Checkdept)) {
            return false;
        }
        Checkdept other = (Checkdept) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.echeck.model.Checkdept[ id=" + id + " ]";
    }
    
}
