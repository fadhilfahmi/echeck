/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.echeck.model;

/**
 *
 * @author fadhilfahmi
 */
public class CheckReport {
    
    private String date;
    private int sumattendance;
    private int lulus;
    private int gagal;
    private int pending;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getSumattendance() {
        return sumattendance;
    }

    public void setSumattendance(int sumattendance) {
        this.sumattendance = sumattendance;
    }

    public int getLulus() {
        return lulus;
    }

    public void setLulus(int lulus) {
        this.lulus = lulus;
    }

    public int getGagal() {
        return gagal;
    }

    public void setGagal(int gagal) {
        this.gagal = gagal;
    }

    public int getPending() {
        return pending;
    }

    public void setPending(int pending) {
        this.pending = pending;
    }
    
    
    
}
