/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.echeck.model;

/**
 *
 * @author fadhilfahmi
 */
public class InOutSummary {
    
    private String timevalid;
    private String timeout;
    private String workinghour;
    private boolean isCompleteWorkingHour;
    private String color;
    private String colorLI;
    private String colorEO;
    private String syif;
    private Checkmaster cm;

    public Checkmaster getCm() {
        return cm;
    }

    public void setCm(Checkmaster cm) {
        this.cm = cm;
    }

    
    public String getColorLI() {
        return colorLI;
    }

    public void setColorLI(String colorLI) {
        this.colorLI = colorLI;
    }

    public String getColorEO() {
        return colorEO;
    }

    public void setColorEO(String colorEO) {
        this.colorEO = colorEO;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isIsCompleteWorkingHour() {
        return isCompleteWorkingHour;
    }

    public void setIsCompleteWorkingHour(boolean isCompleteWorkingHour) {
        this.isCompleteWorkingHour = isCompleteWorkingHour;
    }

    public String getTimevalid() {
        return timevalid;
    }

    public void setTimevalid(String timevalid) {
        this.timevalid = timevalid;
    }

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    public String getWorkinghour() {
        return workinghour;
    }

    public void setWorkinghour(String workinghour) {
        this.workinghour = workinghour;
    }

    public String getSyif() {
        return syif;
    }

    public void setSyif(String syif) {
        this.syif = syif;
    }
    
    
    
}
