/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.echeck.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "checkaccessmenu")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Checkaccessmenu.findAll", query = "SELECT c FROM Checkaccessmenu c"),
    @NamedQuery(name = "Checkaccessmenu.findById", query = "SELECT c FROM Checkaccessmenu c WHERE c.id = :id"),
    @NamedQuery(name = "Checkaccessmenu.findByStaffID", query = "SELECT c FROM Checkaccessmenu c WHERE c.staffID = :staffID"),
    @NamedQuery(name = "Checkaccessmenu.findByMenulevel", query = "SELECT c FROM Checkaccessmenu c WHERE c.menulevel = :menulevel")})
public class Checkaccessmenu implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "staffID")
    private String staffID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "menulevel")
    private int menulevel;

    public Checkaccessmenu() {
    }

    public Checkaccessmenu(Integer id) {
        this.id = id;
    }

    public Checkaccessmenu(Integer id, String staffID, int menulevel) {
        this.id = id;
        this.staffID = staffID;
        this.menulevel = menulevel;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStaffID() {
        return staffID;
    }

    public void setStaffID(String staffID) {
        this.staffID = staffID;
    }

    public int getMenulevel() {
        return menulevel;
    }

    public void setMenulevel(int menulevel) {
        this.menulevel = menulevel;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Checkaccessmenu)) {
            return false;
        }
        Checkaccessmenu other = (Checkaccessmenu) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.echeck.model.Checkaccessmenu[ id=" + id + " ]";
    }
    
}
