/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.echeck.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "co_staff")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CoStaff.findAll", query = "SELECT c FROM CoStaff c"),
    @NamedQuery(name = "CoStaff.findById", query = "SELECT c FROM CoStaff c WHERE c.id = :id"),
    @NamedQuery(name = "CoStaff.findByStaffid", query = "SELECT c FROM CoStaff c WHERE c.staffid = :staffid"),
    @NamedQuery(name = "CoStaff.findByTitle", query = "SELECT c FROM CoStaff c WHERE c.title = :title"),
    @NamedQuery(name = "CoStaff.findByName", query = "SELECT c FROM CoStaff c WHERE c.name = :name"),
    @NamedQuery(name = "CoStaff.findByStatus", query = "SELECT c FROM CoStaff c WHERE c.status = :status"),
    @NamedQuery(name = "CoStaff.findByIc", query = "SELECT c FROM CoStaff c WHERE c.ic = :ic"),
    @NamedQuery(name = "CoStaff.findByBirth", query = "SELECT c FROM CoStaff c WHERE c.birth = :birth"),
    @NamedQuery(name = "CoStaff.findBySex", query = "SELECT c FROM CoStaff c WHERE c.sex = :sex"),
    @NamedQuery(name = "CoStaff.findByMarital", query = "SELECT c FROM CoStaff c WHERE c.marital = :marital"),
    @NamedQuery(name = "CoStaff.findByReligion", query = "SELECT c FROM CoStaff c WHERE c.religion = :religion"),
    @NamedQuery(name = "CoStaff.findByCitizen", query = "SELECT c FROM CoStaff c WHERE c.citizen = :citizen"),
    @NamedQuery(name = "CoStaff.findByAddress", query = "SELECT c FROM CoStaff c WHERE c.address = :address"),
    @NamedQuery(name = "CoStaff.findByCity", query = "SELECT c FROM CoStaff c WHERE c.city = :city"),
    @NamedQuery(name = "CoStaff.findByState", query = "SELECT c FROM CoStaff c WHERE c.state = :state"),
    @NamedQuery(name = "CoStaff.findByPostcode", query = "SELECT c FROM CoStaff c WHERE c.postcode = :postcode"),
    @NamedQuery(name = "CoStaff.findByPhone", query = "SELECT c FROM CoStaff c WHERE c.phone = :phone"),
    @NamedQuery(name = "CoStaff.findByHp", query = "SELECT c FROM CoStaff c WHERE c.hp = :hp"),
    @NamedQuery(name = "CoStaff.findByFax", query = "SELECT c FROM CoStaff c WHERE c.fax = :fax"),
    @NamedQuery(name = "CoStaff.findByEmail", query = "SELECT c FROM CoStaff c WHERE c.email = :email"),
    @NamedQuery(name = "CoStaff.findByPposition", query = "SELECT c FROM CoStaff c WHERE c.pposition = :pposition"),
    @NamedQuery(name = "CoStaff.findByRemarks", query = "SELECT c FROM CoStaff c WHERE c.remarks = :remarks"),
    @NamedQuery(name = "CoStaff.findByRace", query = "SELECT c FROM CoStaff c WHERE c.race = :race"),
    @NamedQuery(name = "CoStaff.findByFlag", query = "SELECT c FROM CoStaff c WHERE c.flag = :flag"),
    @NamedQuery(name = "CoStaff.findByCategory", query = "SELECT c FROM CoStaff c WHERE c.category = :category"),
    @NamedQuery(name = "CoStaff.findByWorkertype", query = "SELECT c FROM CoStaff c WHERE c.workertype = :workertype"),
    @NamedQuery(name = "CoStaff.findByOldic", query = "SELECT c FROM CoStaff c WHERE c.oldic = :oldic"),
    @NamedQuery(name = "CoStaff.findByDatejoin", query = "SELECT c FROM CoStaff c WHERE c.datejoin = :datejoin"),
    @NamedQuery(name = "CoStaff.findByPobirth", query = "SELECT c FROM CoStaff c WHERE c.pobirth = :pobirth"),
    @NamedQuery(name = "CoStaff.findByDepartment", query = "SELECT c FROM CoStaff c WHERE c.department = :department"),
    @NamedQuery(name = "CoStaff.findByPosition", query = "SELECT c FROM CoStaff c WHERE c.position = :position"),
    @NamedQuery(name = "CoStaff.findByLocation", query = "SELECT c FROM CoStaff c WHERE c.location = :location"),
    @NamedQuery(name = "CoStaff.findByImageURL", query = "SELECT c FROM CoStaff c WHERE c.imageURL = :imageURL"),
    @NamedQuery(name = "CoStaff.findByDepartmentID", query = "SELECT c FROM CoStaff c WHERE c.departmentID = :departmentID")})
public class CoStaff implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 11)
    @Column(name = "staffid")
    private String staffid;
    @Size(max = 11)
    @Column(name = "title")
    private String title;
    @Size(max = 200)
    @Column(name = "name")
    private String name;
    @Size(max = 100)
    @Column(name = "status")
    private String status;
    @Size(max = 100)
    @Column(name = "ic")
    private String ic;
    @Size(max = 10)
    @Column(name = "birth")
    private String birth;
    @Size(max = 100)
    @Column(name = "sex")
    private String sex;
    @Size(max = 100)
    @Column(name = "marital")
    private String marital;
    @Size(max = 100)
    @Column(name = "religion")
    private String religion;
    @Size(max = 100)
    @Column(name = "citizen")
    private String citizen;
    @Size(max = 100)
    @Column(name = "address")
    private String address;
    @Size(max = 100)
    @Column(name = "city")
    private String city;
    @Size(max = 100)
    @Column(name = "state")
    private String state;
    @Size(max = 100)
    @Column(name = "postcode")
    private String postcode;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 100)
    @Column(name = "phone")
    private String phone;
    @Size(max = 100)
    @Column(name = "hp")
    private String hp;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 100)
    @Column(name = "fax")
    private String fax;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 100)
    @Column(name = "email")
    private String email;
    @Size(max = 100)
    @Column(name = "pposition")
    private String pposition;
    @Size(max = 1000)
    @Column(name = "remarks")
    private String remarks;
    @Size(max = 100)
    @Column(name = "race")
    private String race;
    @Size(max = 100)
    @Column(name = "flag")
    private String flag;
    @Size(max = 100)
    @Column(name = "category")
    private String category;
    @Size(max = 100)
    @Column(name = "workertype")
    private String workertype;
    @Size(max = 10)
    @Column(name = "oldic")
    private String oldic;
    @Size(max = 10)
    @Column(name = "datejoin")
    private String datejoin;
    @Size(max = 100)
    @Column(name = "pobirth")
    private String pobirth;
    @Size(max = 200)
    @Column(name = "department")
    private String department;
    @Size(max = 100)
    @Column(name = "position")
    private String position;
    @Size(max = 100)
    @Column(name = "location")
    private String location;
    @Size(max = 300)
    @Column(name = "imageURL")
    private String imageURL;
    @Size(max = 10)
    @Column(name = "departmentID")
    private String departmentID;

    public CoStaff() {
    }

    public CoStaff(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStaffid() {
        return staffid;
    }

    public void setStaffid(String staffid) {
        this.staffid = staffid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIc() {
        return ic;
    }

    public void setIc(String ic) {
        this.ic = ic;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getMarital() {
        return marital;
    }

    public void setMarital(String marital) {
        this.marital = marital;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getCitizen() {
        return citizen;
    }

    public void setCitizen(String citizen) {
        this.citizen = citizen;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getHp() {
        return hp;
    }

    public void setHp(String hp) {
        this.hp = hp;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPposition() {
        return pposition;
    }

    public void setPposition(String pposition) {
        this.pposition = pposition;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getWorkertype() {
        return workertype;
    }

    public void setWorkertype(String workertype) {
        this.workertype = workertype;
    }

    public String getOldic() {
        return oldic;
    }

    public void setOldic(String oldic) {
        this.oldic = oldic;
    }

    public String getDatejoin() {
        return datejoin;
    }

    public void setDatejoin(String datejoin) {
        this.datejoin = datejoin;
    }

    public String getPobirth() {
        return pobirth;
    }

    public void setPobirth(String pobirth) {
        this.pobirth = pobirth;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getDepartmentID() {
        return departmentID;
    }

    public void setDepartmentID(String departmentID) {
        this.departmentID = departmentID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CoStaff)) {
            return false;
        }
        CoStaff other = (CoStaff) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.eleave.model.CoStaff[ id=" + id + " ]";
    }
    
}
