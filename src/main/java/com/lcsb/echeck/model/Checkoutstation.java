/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.echeck.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "checkoutstation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Checkoutstation.findAll", query = "SELECT c FROM Checkoutstation c"),
    @NamedQuery(name = "Checkoutstation.findById", query = "SELECT c FROM Checkoutstation c WHERE c.id = :id"),
    @NamedQuery(name = "Checkoutstation.findByStaffID", query = "SELECT c FROM Checkoutstation c WHERE c.staffID = :staffID"),
    @NamedQuery(name = "Checkoutstation.findByCheckID", query = "SELECT c FROM Checkoutstation c WHERE c.checkID = :checkID"),
    @NamedQuery(name = "Checkoutstation.findByLocationID", query = "SELECT c FROM Checkoutstation c WHERE c.locationID = :locationID"),
    @NamedQuery(name = "Checkoutstation.findByDate", query = "SELECT c FROM Checkoutstation c WHERE c.date = :date"),
    @NamedQuery(name = "Checkoutstation.findByTimein", query = "SELECT c FROM Checkoutstation c WHERE c.timein = :timein"),
    @NamedQuery(name = "Checkoutstation.findByTimeout", query = "SELECT c FROM Checkoutstation c WHERE c.timeout = :timeout"),
    @NamedQuery(name = "Checkoutstation.findByReason", query = "SELECT c FROM Checkoutstation c WHERE c.reason = :reason"),
    @NamedQuery(name = "Checkoutstation.findByApproveID", query = "SELECT c FROM Checkoutstation c WHERE c.approveID = :approveID"),
    @NamedQuery(name = "Checkoutstation.findByApprovedate", query = "SELECT c FROM Checkoutstation c WHERE c.approvedate = :approvedate"),
    @NamedQuery(name = "Checkoutstation.findByApprovetime", query = "SELECT c FROM Checkoutstation c WHERE c.approvetime = :approvetime"),
    @NamedQuery(name = "Checkoutstation.findByStatus", query = "SELECT c FROM Checkoutstation c WHERE c.status = :status")})
public class Checkoutstation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 11)
    @Column(name = "id")
    private String id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "staffID")
    private String staffID;
    @Size(max = 11)
    @Column(name = "checkID")
    private String checkID;
    @Size(max = 10)
    @Column(name = "locationID")
    private String locationID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "date")
    private String date;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "timein")
    private String timein;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "timeout")
    private String timeout;
    @Size(max = 200)
    @Column(name = "reason")
    private String reason;
    @Size(max = 10)
    @Column(name = "approveID")
    private String approveID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "approvedate")
    private String approvedate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "approvetime")
    private String approvetime;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "status")
    private String status;

    public Checkoutstation() {
    }

    public Checkoutstation(String id) {
        this.id = id;
    }

    public Checkoutstation(String id, String staffID, String date, String timein, String timeout, String approvedate, String approvetime, String status) {
        this.id = id;
        this.staffID = staffID;
        this.date = date;
        this.timein = timein;
        this.timeout = timeout;
        this.approvedate = approvedate;
        this.approvetime = approvetime;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStaffID() {
        return staffID;
    }

    public void setStaffID(String staffID) {
        this.staffID = staffID;
    }

    public String getCheckID() {
        return checkID;
    }

    public void setCheckID(String checkID) {
        this.checkID = checkID;
    }

    public String getLocationID() {
        return locationID;
    }

    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTimein() {
        return timein;
    }

    public void setTimein(String timein) {
        this.timein = timein;
    }

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getApproveID() {
        return approveID;
    }

    public void setApproveID(String approveID) {
        this.approveID = approveID;
    }

    public String getApprovedate() {
        return approvedate;
    }

    public void setApprovedate(String approvedate) {
        this.approvedate = approvedate;
    }

    public String getApprovetime() {
        return approvetime;
    }

    public void setApprovetime(String approvetime) {
        this.approvetime = approvetime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Checkoutstation)) {
            return false;
        }
        Checkoutstation other = (Checkoutstation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.echeck.model.Checkoutstation[ id=" + id + " ]";
    }
    
}
