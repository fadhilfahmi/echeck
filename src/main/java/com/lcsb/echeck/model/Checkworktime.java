/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.echeck.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "checkworktime")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Checkworktime.findAll", query = "SELECT c FROM Checkworktime c"),
    @NamedQuery(name = "Checkworktime.findById", query = "SELECT c FROM Checkworktime c WHERE c.id = :id"),
    @NamedQuery(name = "Checkworktime.findByDescp", query = "SELECT c FROM Checkworktime c WHERE c.descp = :descp"),
    @NamedQuery(name = "Checkworktime.findByStart", query = "SELECT c FROM Checkworktime c WHERE c.start = :start"),
    @NamedQuery(name = "Checkworktime.findByEnd", query = "SELECT c FROM Checkworktime c WHERE c.end = :end")})
public class Checkworktime implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "id")
    private String id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "descp")
    private String descp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "start")
    private String start;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "end")
    private String end;

    public Checkworktime() {
    }

    public Checkworktime(String id) {
        this.id = id;
    }

    public Checkworktime(String id, String descp, String start, String end) {
        this.id = id;
        this.descp = descp;
        this.start = start;
        this.end = end;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescp() {
        return descp;
    }

    public void setDescp(String descp) {
        this.descp = descp;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Checkworktime)) {
            return false;
        }
        Checkworktime other = (Checkworktime) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.echeck.model.Checkworktime[ id=" + id + " ]";
    }
    
}
