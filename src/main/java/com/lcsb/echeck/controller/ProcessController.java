/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.echeck.controller;

import com.lcsb.echeck.dao.CheckDAO;
import com.lcsb.echeck.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.simple.JSONObject;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "ProcessController", urlPatterns = {"/ProcessController"})
public class ProcessController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String moduleid = request.getParameter("moduleid");
        String process = request.getParameter("process");
        HttpSession session = request.getSession();
        LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

        try {
            switch (process) {
                case "checkDCnote":
                    //out.println(DebitCreditNoteDAO.checkDCNoteExist(log, request.getParameter("referno")));
                    break;
                    case "updatetempinforecord": {


                    out.println(CheckDAO.updateTemperatureAndValidate(log, Double.parseDouble(request.getParameter("temp")), request.getParameter("id")));

                    break;
                }
//                case "savedestinationselected": {
//
//                    BookingDestinationTemp c = new BookingDestinationTemp();
//
//                    c.setDestcode(request.getParameter("destcode"));
//                    c.setDestdescp(request.getParameter("destdescp"));
//                    c.setDesttype(request.getParameter("desttype"));
//                    c.setUserID(log.getUserID());
//                    c.setSessionid(request.getParameter("sessionid"));
//
//                    int i = BookingDAO.insertSelectedDestination(log, c);
//
//                    out.println(i);
//
//                    //urlsend = "/conf_car.jsp";
//                    break;
//                }
//                case "savedatetime": {
//
//                    BookingDestinationTemp c = new BookingDestinationTemp();
//
//                    c.setEnddate(request.getParameter("enddate"));
//                    c.setEndtime(request.getParameter("endtime"));
//                    c.setStartdate(request.getParameter("startdate"));
//                    c.setStarttime(request.getParameter("starttime"));
//                    c.setId(Integer.parseInt(request.getParameter("id")));
//
//                    int i = BookingDAO.updateDateTimeforSelectedDestination(log, c);
//
//                    out.println(i);
//
//                    //urlsend = "/conf_car.jsp";
//                    break;
//                }
//                case "savepassenger": {
//
//                    Members c = new Members();
//
//                    c.setCompID(request.getParameter("compID"));
//
//                    int i = BookingDAO.insertSelectedPassenger(log, c, request.getParameter("sessionid"));
//
//                    out.println(i);
//
//                    //urlsend = "/conf_car.jsp";
//                    break;
//                }
//                case "deletedestinationselected": {
//
//                    int i = BookingDAO.deleteSelectedDestination(log, request.getParameter("id"));
//
//                    out.println(i);
//
//                    //urlsend = "/conf_car.jsp";
//                    break;
//                }
//                case "deletepassengerselected": {
//
//                    int i = BookingDAO.deleteSelectedPassenger(log, request.getParameter("id"));
//
//                    out.println(i);
//
//                    //urlsend = "/conf_car.jsp";
//                    break;
//                }
//                
//                case "assigncarfordestination":{
//                    
//                    BookingCar b = new BookingCar();
//                    
//                    b.setBookID(request.getParameter("bookID"));
//                    b.setCarID(request.getParameter("carID"));
//                    b.setDrivetype(request.getParameter("drivetype"));
//                    b.setDestID(Integer.parseInt(request.getParameter("destID")));
//                    //b.setDriverID(process);
//
//                    int i = BookingDAO.assignCarForDestination(log, b);
//
//                    out.println(i);
//
//                    //urlsend = "/conf_car.jsp";
//                    break;
//                }
//                case "confirmbook": {
//
//                    int i = BookingDAO.confirmBooking(log, request.getParameter("bookID"));
//
//                    out.println(i);
//
//                    //urlsend = "/conf_car.jsp";
//                    break;
//                }
//                
//                case "getLeaveForCalendar": {
//
//                    //JSONObject i = BookingDAO.confirmUndo();
//
//                    out.println(LeaveDAO.getAllLeaveForCalendar(log, request.getParameter("filter")));
//
//                    //urlsend = "/conf_car.jsp";
//                    break;
//                }
//                case "getAnalysis": {
//
//                    //JSONObject i = BookingDAO.confirmUndo();
//
//                    out.println(LeaveDAO.getAnalysisData(log, request.getParameter("type")));
//
//                    //urlsend = "/conf_car.jsp";
//                    break;
//                }
//                case "seennotify": {
//
//                    int i = NotifyDAO.updateNotify(log, request.getParameter("id"));
//
//                    out.println(i);
//
//                    //urlsend = "/conf_car.jsp";
//                    break;
//                }
                default:
                    break;
            }
        } finally {
            out.close();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ProcessController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ProcessController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
