/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.echeck.controller;

import com.lcsb.echeck.dao.AccountingPeriod;
import com.lcsb.echeck.dao.CheckDAO;
import com.lcsb.echeck.dao.ConnectionDAO;
import com.lcsb.echeck.dao.StaffDAO;
import com.lcsb.echeck.model.Checkform;
import com.lcsb.echeck.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "VisitorController", urlPatterns = {"/VisitorController"})
public class VisitorController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            String process = request.getParameter("process");

            String urlsend = "";

            switch (process) {
                case "savedeptform":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    Connection con = ConnectionDAO.getConnection();
                    LoginProfile log = new LoginProfile();
                    log.setCon(con);
                    
                    CheckDAO.checkInDeptVisitor(con, request.getParameter("deptID"), request.getParameter("name"), request.getParameter("notel"));
                    String dept = StaffDAO.getInfoDepartment(log, request.getParameter("deptID")).getDescp();
                    con.close();
                    
                    urlsend = "/welcome_dept_visitor.jsp?dept="+dept+"&name="+request.getParameter("name");
                    break;
                case "viewleaveapproval":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/view_leave_approval.jsp";
                    break;
                case "applyleavestep":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/apply_leave_step.jsp?sessionid=" + session.getId() + AccountingPeriod.getCurrentTimeStamp() + AccountingPeriod.getCurrentTime();
                    break;
                case "applysickleave":
                    //AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/apply_sick_leave.jsp?sessionid=" + session.getId() + AccountingPeriod.getCurrentTimeStamp().replaceAll("[-+.^:,]", "") + AccountingPeriod.getCurrentTime().replaceAll("[-+.^:,]", "");
                    break;
                default:

                    //urlsend = "/error.jsp?moduleid=" + moduleid + "&refer=" + request.getParameter("referno");
                    break;
            }

            Logger.getLogger(PathController.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(VisitorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(VisitorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
