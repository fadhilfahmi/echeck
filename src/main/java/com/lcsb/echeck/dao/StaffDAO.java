/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.echeck.dao;

import com.lcsb.echeck.model.CoStaff;
import com.lcsb.echeck.model.CoStaff;
import com.lcsb.echeck.model.CoStaffDepartment;
import com.lcsb.echeck.model.CoStaffDepartment;
import com.lcsb.echeck.model.CoStaffLocation;
import com.lcsb.echeck.model.CoStaffLocation;
import com.lcsb.echeck.model.CoStaffPosition;
import com.lcsb.echeck.model.CoStaffPosition;
import com.lcsb.echeck.model.LoginProfile;
import com.lcsb.echeck.model.LoginProfile;
import com.lcsb.echeck.model.UserAccess;
import com.lcsb.echeck.model.UserAccess;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class StaffDAO {

    public static CoStaff getInfo(LoginProfile log, String no) throws SQLException, Exception {
        CoStaff c = null;
        
        try{
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM co_staff WHERE staffid=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    public static CoStaff getInfoByEmail(LoginProfile log, String no) throws SQLException, Exception {
        CoStaff c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM co_staff WHERE email=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        return c;
    }

    public static CoStaff getInfo(ResultSet rs) throws SQLException {

        CoStaff g = new CoStaff();

        g.setAddress(rs.getString("address"));
        g.setBirth(rs.getString("birth"));
        g.setCitizen(rs.getString("citizen"));
        g.setCity(rs.getString("city"));
        g.setEmail(rs.getString("email"));
        g.setFax(rs.getString("fax"));
        g.setHp(rs.getString("hp"));
        g.setMarital(rs.getString("marital"));
        g.setName(rs.getString("name"));
        g.setIc(rs.getString("ic"));
        g.setPhone(rs.getString("phone"));
        g.setPostcode(rs.getString("postcode"));
        g.setRace(rs.getString("race"));
        g.setReligion(rs.getString("religion"));
        g.setSex(rs.getString("sex"));
        g.setState(rs.getString("state"));
        g.setStatus(rs.getString("status"));
        g.setStaffid(rs.getString("staffid"));
        g.setPobirth(rs.getString("pobirth"));
        g.setRemarks(rs.getString("remarks"));
        g.setOldic(rs.getString("oldic"));
        g.setTitle(rs.getString("title"));
        g.setStaffid(rs.getString("staffid"));
        g.setDepartment(rs.getString("department"));
        g.setPosition(rs.getString("position"));
        g.setImageURL(rs.getString("imageURL"));
        g.setLocation(rs.getString("location"));
        g.setDepartmentID(rs.getString("departmentID"));

        return g;
    }

    public static List<CoStaff> getAllStaff(LoginProfile log) {

//        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.lcsb_fms_mvn_war_1.0-SNAPSHOTPU");
//
//        EntityManager em = emf.createEntityManager();
//
//        Query query = em.createNamedQuery("Staff.findAll");
//
//        List<Staff> resultList = query.getResultList();
//        for (Staff c : resultList) {
//
//            resultList.add(c);
//
//        }
        ResultSet rs = null;
        List<CoStaff> CVi;
        CVi = new ArrayList();
        CoStaff c = new CoStaff();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff");
            //stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
    public static List<CoStaff> getAllStaffWithEmail(LoginProfile log) {

//        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.lcsb_fms_mvn_war_1.0-SNAPSHOTPU");
//
//        EntityManager em = emf.createEntityManager();
//
//        Query query = em.createNamedQuery("Staff.findAll");
//
//        List<Staff> resultList = query.getResultList();
//        for (Staff c : resultList) {
//
//            resultList.add(c);
//
//        }
        ResultSet rs = null;
        List<CoStaff> CVi;
        CVi = new ArrayList();
        CoStaff c = new CoStaff();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff where email <> ''");
            //stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
     public static List<CoStaff> getAllStaffByDept(LoginProfile log, String dept) {

        ResultSet rs = null;
        List<CoStaff> CVi;
        CVi = new ArrayList();
        CoStaff c = new CoStaff();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff where email <> '' AND departmentID = ?");
            stmt.setString(1, dept);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
    public static List<CoStaff> getAllStaffFilteredBy(LoginProfile log, String filter, String param_filter) throws Exception {

        String q = "";
        
        if(filter.equals("location")){
            
            if(param_filter.equals("IBU PEJABAT")){
                
                q = " AND ("+filter+" = 'IBU PEJABAT' OR "+filter+" = 'CAWANGAN INDERA MAHKOTA') AND position <> 'PENYELIA' ";
                
            }
            
        }
        
        if(filter.equals("departmentID")){
            
            if(!param_filter.equals("none")){
                
                q = " AND "+filter+" = '"+ param_filter + "'";
                
            }else{
                q = " AND email <> ''";
            }
            
        }
        
        if(log.getAccessLevel() == 0){
            q += " AND staffID = '" + log.getUserID() + "'";
        }else if(log.getAccessLevel() == 2 || log.getAccessLevel() == 3){
           // q += " AND staffID = '" + log.getUserID() + "' OR staffID IN " + LeaveDAO.getSuperviseeID(log);
        }
        
        String q1 = "";
        
        if(StaffDAO.getInfoDepartmentByHeadID(log) != null && !(CheckDAO.isMenuAccess(log, 1))) {
            q1 = "AND a.departmentID = '"+ StaffDAO.getInfoDepartmentByHeadID(log).getId() +"'";
        }
        
        if(CheckDAO.isMenuAccess(log, 0)){
             q1 = "";
        }
        
        
        

        ResultSet rs = null;
        List<CoStaff> CVi;
        CVi = new ArrayList();
        CoStaff c = new CoStaff();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff a, co_staff_position b WHERE a.position = b.descp " + q +" "+q1+" ORDER BY a.name ASC  ");
            //stmt.setString(1, staffid);
            Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "---cekcek-- " + stmt);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
    

    public static boolean isStaffInfoExisted(LoginProfile log, String id) throws SQLException, Exception {
        boolean c = false;

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM co_staff WHERE staffID=?");
        stmt.setString(1, id);
        Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "---adaketak-- " + stmt);

        rs = stmt.executeQuery();
        if (rs.next()) {
            c = true;
        }

        return c;
    }

    public static void saveStaff(LoginProfile log, CoStaff c, String supervisorID) throws Exception {

        try {

            if (isStaffInfoExisted(log, c.getStaffid())) {

                String q = ("UPDATE co_staff SET name = ?, imageURL = ?, email = ?, position = ?, department = ?,departmentID = ?, location = ? WHERE staffID = ?");
                PreparedStatement ps = log.getCon().prepareStatement(q);
                ps.setString(1, c.getName());
                ps.setString(2, log.getImageURL());
                ps.setString(3, log.getEmail());
                ps.setString(4, c.getPosition());
                ps.setString(5, StaffDAO.getInfoDepartment(log, c.getDepartment()).getDescp());
                ps.setString(6, c.getDepartment());
                ps.setString(7, c.getLocation());
                ps.setString(8, c.getStaffid());

                ps.executeUpdate();
                ps.close();
            }else{
                
                String q = ("INSERT INTO co_staff(name, imageURL, email, position, department, location, staffID, departmentID) VALUES (?,?,?,?,?,?,?,?)");
                PreparedStatement ps = log.getCon().prepareStatement(q);
                ps.setString(1, c.getName());
                ps.setString(2, log.getImageURL());
                ps.setString(3, log.getEmail());
                ps.setString(4, c.getPosition());
                ps.setString(5, StaffDAO.getInfoDepartment(log, c.getDepartment()).getDescp());
                ps.setString(6, c.getLocation());
                ps.setString(7, c.getStaffid());
                ps.setString(7, c.getDepartment());
                
                Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "---query-- " + ps);

                ps.executeUpdate();
                ps.close();
                
            }
            
            String q1 = ("INSERT into supervise_info(staffID, supervisorID, headID) values (?,?,?)");
            PreparedStatement ps1 = log.getCon().prepareStatement(q1);
            ps1.setString(1, c.getStaffid());
            ps1.setString(2, supervisorID);
            ps1.setString(3, StaffDAO.getHeadID(log, c));

            ps1.executeUpdate();
            ps1.close();

            int is = 0;
            if (isStaffSupervisorFirstLogin(log, c.getStaffid())) {
                is = 2;
            }
            Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "---supervisor-- " + is);

            if (isStaffHeadIDFirstLogin(log, c.getStaffid())) {
                is = 3;
            }

            String q2 = ("INSERT into user_access(staffID, level) values (?,?)");
            PreparedStatement ps2 = log.getCon().prepareStatement(q2);
            ps2.setString(1, c.getStaffid());
            ps2.setInt(2, is);

            ps2.executeUpdate();
            ps2.close();

            if (isSupervisorRegistered(log, supervisorID)) {
                StaffDAO.updateToSupervisor(log, supervisorID);
            }
            
            //dummy leave info - remove once released
            //LeaveDAO.saveDummyLeave(log, c.getStaffid());

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    
    public static String getHeadID(LoginProfile log, CoStaff st) throws SQLException, Exception {
        String c = "";
        
        
        c = StaffDAO.getInfoDepartment(log, st.getDepartment()).getHeadID();
        
        
        if(c.equals(st.getStaffid())){
            c = StaffDAO.getInfoDepartment(log, "0010").getHeadID();
        }

//        ResultSet rs = null;
//        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM supervise_info WHERE supervisorID=?");
//        stmt.setString(1, id);
//        Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "---supervisor-- " + stmt);
//
//        rs = stmt.executeQuery();
//        if (rs.next()) {
//            c = true;
//        }

        return c;
    }

    public static boolean isStaffSupervisorFirstLogin(LoginProfile log, String id) throws SQLException, Exception {
        boolean c = false;

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM supervise_info WHERE supervisorID=?");
        stmt.setString(1, id);
        Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "---supervisor-- " + stmt);

        rs = stmt.executeQuery();
        if (rs.next()) {
            c = true;
        }

        return c;
    }

    public static boolean isStaffHeadIDFirstLogin(LoginProfile log, String id) throws SQLException, Exception {
        boolean c = false;

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM co_staff_department WHERE headID=?");
        stmt.setString(1, id);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = true;
        }

        return c;
    }

    public static boolean isSupervisorRegistered(LoginProfile log, String id) throws SQLException, Exception {
        boolean c = false;

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM user_access WHERE staffID=?");
        stmt.setString(1, id);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = true;
        }

        return c;
    }

    public static void updateToSupervisor(LoginProfile log, String supervisorID) throws Exception {//kalau user bawah login update dia jadi penyelia

        try {

            if (StaffDAO.getUserAccess(log, supervisorID).getLevel() == 0 && !supervisorID.equals("Tiada")) {

                String q = ("UPDATE user_access SET level = ? WHERE staffID = ?");
                PreparedStatement ps = log.getCon().prepareStatement(q);
                ps.setInt(1, 2);
                ps.setString(2, supervisorID);

                ps.executeUpdate();
                ps.close();

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static List<CoStaff> getMatchNamefromRecord(LoginProfile log, String name) throws Exception {

        Statement stmt = null;
        List<CoStaff> Com;
        Com = new ArrayList();

        String q = "";
        ArrayList<String> totalCheckCode = new ArrayList<String>();
        StringTokenizer t = new StringTokenizer(name);
        String word = "";
        while (t.hasMoreTokens()) {
            word = t.nextToken();

        }

        String[] n = name.split("\\s+");

        word = n[0] + " " + n[1];

        //if (!word.contains("MOHD") || !word.contains("B.") || !word.contains("Mohd")) {
        //if(!(word.equalsIgnoreCase("mohd")) && !(word.equalsIgnoreCase("b.")) && !(word.equalsIgnoreCase("bt.")) && !(word.equalsIgnoreCase("mat"))){
        Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "--- " + word);
        if (name != null) {
            q = "where name like '%" + word + "%'";
            //Logger.getLogger(BuyerDAO.class.getName()).log(Level.INFO, ">>>>>>> " + q);
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select staffID,name from co_staff " + q + " order by name");
            Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, String.valueOf("select staffID,name from co_staff " + q + " order by name"));
            while (rs.next()) {

                if (totalCheckCode.size() == 0) {
                    //Logger.getLogger(DebitCreditNoteDAO.class.getName()).log(Level.INFO, "firstinsert=={0}", a.getCoacode());
                    totalCheckCode.add(rs.getString("staffID"));
                    CoStaff by = new CoStaff();
                    by.setStaffid(rs.getString("staffID"));
                    by.setName(rs.getString("name"));

                    Com.add(by);
                }

                int x = 0;
                for (int i = 0; i < totalCheckCode.size(); i++) {
                    if (totalCheckCode.get(i).equals(rs.getString("staffID"))) {
                        x++;

                    }
                }

                //Logger.getLogger(DebitCreditNoteDAO.class.getName()).log(Level.INFO, "2ndrow=={0}", x);
                if (x == 0) {
                    totalCheckCode.add(rs.getString("staffID"));
                    CoStaff by = new CoStaff();
                    by.setStaffid(rs.getString("staffID"));
                    by.setName(rs.getString("name"));

                    Com.add(by);
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //}

        for (int i = 0; i < totalCheckCode.size(); i++) {
            //Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "********* " + totalCheckCode.get(i));
        }

        return Com;
    }

    public static CoStaffPosition getInfoStaffPositionRS(ResultSet rs) throws SQLException {

        CoStaffPosition g = new CoStaffPosition();

        g.setDescp(rs.getString("descp"));
        g.setDorder(rs.getInt("dorder"));
        g.setId(rs.getInt("id"));
        g.setLevel(rs.getInt("level"));

        return g;
    }

    public static List<CoStaffPosition> getAllPosition(LoginProfile log) {

        ResultSet rs = null;
        List<CoStaffPosition> CVi;
        CVi = new ArrayList();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff_position order by dorder asc");
            //stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfoStaffPositionRS(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static CoStaffDepartment getInfoDepartmentRS(ResultSet rs) throws SQLException {

        CoStaffDepartment g = new CoStaffDepartment();

        g.setDescp(rs.getString("descp"));
        g.setId(rs.getString("id"));
        g.setHeadID(rs.getString("headID"));
        g.setAbb(rs.getString("abb"));

        return g;
    }

    public static List<CoStaffDepartment> getAllDepartment(LoginProfile log) {

        ResultSet rs = null;
        List<CoStaffDepartment> CVi;
        CVi = new ArrayList();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff_department order by id asc");
            //stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfoDepartmentRS(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
    public static List<CoStaffDepartment> getAllDepartmentWithoutGMMD(LoginProfile log) throws Exception {

        ResultSet rs = null;
        List<CoStaffDepartment> CVi;
        CVi = new ArrayList();
        
        String q = "";
        
        if(StaffDAO.getInfoDepartmentByHeadID(log) != null && !(log.getUserID().equals("P0256") || log.getUserID().equals("P0004") || log.getUserID().equals("P0702"))) {
            q = "AND headID = '"+ log.getUserID() +"'";
        }

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff_department WHERE id NOT IN ('0010','0011') "+q+" order by id asc");
            //stmt.setString(1, staffid);
            Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "----- " + stmt);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfoDepartmentRS(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
     public static List<CoStaffDepartment> getAllDepartmentStaff(LoginProfile log) {

        ResultSet rs = null;
        List<CoStaffDepartment> CVi;
        CVi = new ArrayList();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff_department WHERE id NOT IN ('0010','0011') order by id asc");
            //stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfoDepartmentRS(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static CoStaffLocation getInfoLocationRS(ResultSet rs) throws SQLException {

        CoStaffLocation g = new CoStaffLocation();

        g.setDescp(rs.getString("descp"));
        g.setId(rs.getInt("id"));

        return g;
    }

    public static List<CoStaffLocation> getAllLocation(LoginProfile log) {

        ResultSet rs = null;
        List<CoStaffLocation> CVi;
        CVi = new ArrayList();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff_location order by id asc");
            //stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfoLocationRS(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static CoStaffDepartment getInfoDepartment(LoginProfile log, String no) throws SQLException, Exception {
        CoStaffDepartment c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM co_staff_department WHERE id=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfoDepartmentRS(rs);
        }

        return c;
    }
    
    public static CoStaffDepartment getInfoDepartmentByHeadID(LoginProfile log) throws SQLException, Exception {
        CoStaffDepartment c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM co_staff_department WHERE headID=? AND id NOT IN ('0010','0011')");
        stmt.setString(1, log.getUserID());
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfoDepartmentRS(rs);
        }

        return c;
    }
    
    public static boolean isStaffHeadID(LoginProfile log) throws SQLException, Exception {
        boolean c = false;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM co_staff_department WHERE headID=?");
        stmt.setString(1, log.getUserID());
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = true;
        }

        return c;
    }

    public static UserAccess getUserAccessRS(ResultSet rs) throws SQLException {

        UserAccess g = new UserAccess();

        g.setId(rs.getInt("id"));
        g.setLevel(rs.getInt("level"));
        g.setStaffID(rs.getString("staffID"));

        return g;
    }

    public static UserAccess getUserAccess(LoginProfile log, String no) throws SQLException, Exception {
        UserAccess c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM user_access WHERE staffID=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getUserAccessRS(rs);
        }

        return c;
    }

    public static UserAccess getUserAccessByLevel(LoginProfile log, int no) throws SQLException, Exception {
        UserAccess c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM user_access WHERE level=?");
        stmt.setInt(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getUserAccessRS(rs);
        }

        return c;
    }

    public static CoStaff getInfoSupervisor(LoginProfile log, String no) throws SQLException, Exception {

        CoStaff c = null;
        String supervisorID = "";
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM supervise_info WHERE staffid=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            supervisorID = rs.getString("supervisorID");
        }

        c = (CoStaff) StaffDAO.getInfo(log, supervisorID);

        return c;
    }

    public static CoStaff getInfoHead(LoginProfile log, String no) throws SQLException, Exception {

        CoStaff c = null;
        String headID = "";
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM supervise_info WHERE staffid=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            headID = rs.getString("headID");
        }

        c = (CoStaff) StaffDAO.getInfo(log, headID);

        return c;
    }

    public static CoStaff getInfoHRHead(LoginProfile log) throws SQLException, Exception {

        CoStaff c = (CoStaff) StaffDAO.getInfo(log, getUserAccessByLevel(log, 4).getStaffID());

        return c;
    }

    public static List<CoStaff> getAllStaffSearch(LoginProfile log, String keyword) throws Exception {

        Statement stmt = null;
        List<CoStaff> Com;
        Com = new ArrayList();

        String q = "";

        if (keyword != null) {
            q = " WHERE name like '%" + keyword + "%' or staffID like '%" + keyword + "%'";
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM co_staff " + q);
            while (rs.next()) {
                Com.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }
    
    public static String getBadgeLevelColor(int lvl) {

        String color = "";

        if (lvl == 0) {
            color = "warning";
        }else if (lvl == 1) {
            color = "success";
        } else if (lvl == 2) {
            color = "blue";
        } else if (lvl == 3) {
            color = "info";
        } else if (lvl == 4) {
            color = "default";
        }

        return color;
    }
    
    public static String getDepartmentMemberID(LoginProfile log, String departmentID) throws SQLException, Exception {

        

        String s = "(";
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM co_staff WHERE departmentID = ?");
        //stmt.setString(1, no);
        stmt.setString(1, departmentID);
        rs = stmt.executeQuery();

        while (rs.next()) {

            if (rs.isLast()) {
                s += "'" + rs.getString("staffID") + "'";
            } else {
                s += "'" + rs.getString("staffID") + "'" + ",";
            }

        }

        s += ")";

        Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "------string" + String.valueOf(s));

        return s;
    }
    
     public static boolean isSupervisorAsHeadDept(LoginProfile log, String staffID) throws SQLException, Exception {
        boolean c = false;

        if(StaffDAO.getInfoSupervisor(log, staffID).getStaffid().equals(StaffDAO.getInfoHead(log, staffID).getStaffid())){
            c = true;
        }

        return c;
    }
     
     
    

}
