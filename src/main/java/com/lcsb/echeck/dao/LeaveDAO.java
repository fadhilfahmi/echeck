/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.echeck.dao;

import static com.lcsb.echeck.dao.StaffDAO.getInfo;
import com.lcsb.echeck.model.CoStaff;
import com.lcsb.echeck.model.CoStaffDepartment;
import com.lcsb.echeck.model.Leave;
import com.lcsb.echeck.model.LeaveAttachment;
import com.lcsb.echeck.model.LeaveComment;
import com.lcsb.echeck.model.LeaveInfo;
import com.lcsb.echeck.model.LeaveReasonTemplate;
import com.lcsb.echeck.model.LeaveRequest;
import com.lcsb.echeck.model.LoginProfile;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author fadhilfahmi
 */
public class LeaveDAO {

    public static double PrecisionDouble(Double value) {
        DecimalFormat df = new DecimalFormat("###########0.00");

        double number = Math.ceil(value * 100);

        double output = Double.parseDouble(df.format(number / 100));

        return output;

    }

    public static double twoDecimalDouble(Double value) {
        DecimalFormat df = new DecimalFormat("###########0.00");

        double output = Double.parseDouble(df.format(value));

        return output;

    }

    private static Leave getLeaveRS(ResultSet rs) throws SQLException, Exception {
        Leave cv = new Leave();

        cv.setCheckDate(rs.getString("check_date"));
        cv.setCheckID(rs.getString("checkID"));
        cv.setDateapply(rs.getString("dateapply"));
        cv.setDateend(rs.getString("dateend"));
        cv.setDatestart(rs.getString("datestart"));
        cv.setDays(rs.getInt("days"));
        cv.setHeadDate(rs.getString("head_date"));
        cv.setHeadID(rs.getString("headID"));
        cv.setHrDate(rs.getString("hr_date"));
        cv.setHrID(rs.getString("hrID"));
        cv.setLeaveID(rs.getString("leaveID"));
        cv.setReason(rs.getString("reason"));
        cv.setStaffID(rs.getString("staffID"));
        cv.setSupervisorDate(rs.getString("supervisor_date"));
        cv.setSupervisorID(rs.getString("supervisorID"));
        cv.setType(rs.getString("type"));
        cv.setStatus(rs.getString("status"));
        cv.setYear(rs.getString("year"));
        cv.setPeriod(rs.getString("period"));
        cv.setStafflevel(rs.getInt("stafflevel"));

        return cv;
    }

    public static String saveLeave(LoginProfile log, Leave item, String sessionid) throws Exception {

        String leaveID = AutoGenerate.get4digitNo(log, "leave_master", "leaveID");
        String reason = item.getReason();
        if (item.getType().equals("Cuti Sakit")) {
            reason = "Rujuk Lampiran";
        }
        try {
            String q = ("insert into leave_master(leaveID,dateapply,dateend,datestart,reason,type, staffID, days, year, period, status, stafflevel) values (?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, leaveID);
            ps.setString(2, AccountingPeriod.getCurrentTimeStamp());
            ps.setString(3, item.getDateend());
            ps.setString(4, item.getDatestart());
            ps.setString(5, reason);
            ps.setString(6, item.getType());
            ps.setString(7, log.getUserID());
            ps.setInt(8, item.getDays());
            ps.setString(9, AccountingPeriod.getCurYearByDate(item.getDatestart()));
            ps.setString(10, AccountingPeriod.getCurPeriodByDate(item.getDatestart()));
            ps.setString(11, "Preparing");
            ps.setInt(12, getUserLevel(log));

            ps.executeUpdate();
            ps.close();

            if (item.getType().equals("Cuti Sakit")) {
                String q_attach = ("UPDATE leave_attachment SET leaveID = ? WHERE sessionid = ?");
                PreparedStatement ps_attach = log.getCon().prepareStatement(q_attach);
                ps_attach.setString(1, leaveID);
                ps_attach.setString(2, sessionid);

                ps_attach.executeUpdate();
                ps_attach.close();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return leaveID;

    }

    public static String saveLeaveOldRecord(LoginProfile log, Leave item, String sessionid) throws Exception {

        String leaveID = AutoGenerate.get4digitNo(log, "leave_master", "leaveID");
        String reason = item.getReason();
        if (item.getType().equals("Cuti Sakit")) {
            reason = "Rujuk Lampiran";
        }
        try {
            String q = ("insert into leave_master(leaveID,dateapply,dateend,datestart,reason,type, staffID, days, year, period, status, stafflevel) values (?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, leaveID);
            ps.setString(2, item.getDateapply());
            ps.setString(3, item.getDateend());
            ps.setString(4, item.getDatestart());
            ps.setString(5, reason);
            ps.setString(6, item.getType());
            ps.setString(7, item.getStaffID());
            ps.setInt(8, item.getDays());
            ps.setString(9, AccountingPeriod.getCurYearByDate(item.getDatestart()));
            ps.setString(10, AccountingPeriod.getCurPeriodByDate(item.getDatestart()));
            ps.setString(11, "Approved");
            ps.setInt(12, 0);

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return leaveID;

    }

    public static void saveLeaveAttachment(LoginProfile log, String name, String sessionid) throws Exception {

        try {

            String q_attach = ("insert into leave_attachment(filename,sessionID, staffID) values (?,?,?)");
            PreparedStatement ps_attach = log.getCon().prepareStatement(q_attach);
            ps_attach.setString(1, name);
            ps_attach.setString(2, sessionid);
            ps_attach.setString(3, log.getUserID());

            ps_attach.executeUpdate();
            ps_attach.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static int getUserLevel(LoginProfile log) throws SQLException {

        int i = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM user_level WHERE staffID=?");
        stmt.setString(1, log.getUserID());
        rs = stmt.executeQuery();
        if (rs.next()) {
            i = rs.getInt("level");
        }

        return i;

    }

    public static List<Leave> getAllLeaveExcludeYou(LoginProfile log, String year, String period, String deptID) throws Exception {

        ResultSet rs = null;
        List<Leave> CVi;
        CVi = new ArrayList();
        Leave c = new Leave();

        String q = "";
        String q1 = "";

        if (!deptID.equals("None")) {
            q1 = "AND b.departmentID = '" + deptID + "'";
        }

        if (log.getAccessLevel() == 1) {

            CVi = LeaveDAO.getAllLeaveExcludeYouExecuteQuery(log, q, q1);

        } else if (log.getAccessLevel() == 2) {//level supervisor

            q = "AND a.staffID IN " + LeaveDAO.getSuperviseeID(log) + "  AND type <> 'Cuti Sakit' AND (a.status = 'Checked' OR a.status = 'Supported' OR a.status = 'Approved'  OR a.status = 'Verified') OR (a.staffID = b.staffID  AND a.staffID IN " + LeaveDAO.getSuperviseeID(log) + "  AND type <> 'Cuti Sakit' AND a.status = 'Rejected' AND supervisorID = '" + log.getUserID() + "' AND type <> 'Cuti Sakit')";

            CVi = LeaveDAO.getAllLeaveExcludeYouExecuteQuery(log, q, q1);

        } else if (log.getAccessLevel() == 3) {//level head dept

            //q = "AND a.staffID IN " + LeaveDAO.getSuperviseeIDByLevel(log, "headID") + " AND (a.status = 'Supported' OR a.status = 'Approved'  OR a.status = 'Verified' OR  (a.status = 'Checked' AND type = 'Cuti Sakit')  OR (a.status = 'Rejected' AND headID = '" + log.getUserID() + "'))";
            
            String ql1 = "";
            String ql2 = "";
            
            if(LeaveDAO.isSuperViseeExistForLevel3(log, "support")){
                ql1 = "OR a.staffID IN " + LeaveDAO.getSuperviseeIDByLevel(log, "supervisorID") + " AND (a.status = 'Checked' OR a.status = 'Supported' OR a.status = 'Approved' OR (a.status = 'Checked' AND type = 'Cuti Sakit') OR (a.status = 'Rejected' AND headID = '" + log.getUserID() + "'))";
            }else if(LeaveDAO.isSuperViseeExistForLevel3(log, "approve")){
                ql2 = "";
            }
            
            
            q = " AND (a.staffID IN " + LeaveDAO.getSuperviseeIDByLevel(log, "headID") + " AND (a.status = 'Supported' OR a.status = 'Approved' OR (a.status = 'Checked' AND type = 'Cuti Sakit') OR (a.status = 'Rejected' AND headID = '" + log.getUserID() + "')) "+ql1+")";

            //q = "AND a.staffID IN " + LeaveDAO.getSuperviseeID(log) + " AND (a.status = 'Checked' OR a.status = 'Supported' OR a.status = 'Approved'  OR a.status = 'Verified' OR (a.status = 'Checked' AND stafflevel = 2)  OR (a.status = 'Checked' AND stafflevel = 0) OR  (a.status = 'Checked' AND type = 'Cuti Sakit')  OR (a.status = 'Rejected' AND headID = '" + log.getUserID() + "')) OR (a.status = 'Checked' AND stafflevel = 3)";
            CVi.addAll(LeaveDAO.getAllLeaveExcludeYouExecuteQuery(log, q, q1));

        } else if (log.getAccessLevel() == 4) {//level HR dept

            q = "AND (a.status = 'Approved' OR a.status = 'Verified') OR (a.status = 'Rejected' AND hrID = '" + log.getUserID() + "') ";

            CVi = LeaveDAO.getAllLeaveExcludeYouExecuteQuery(log, q, q1);

        }

        return CVi;
    }
    
    public static List<CoStaff> getAllLeaveExcludeYouGroupByStaff(LoginProfile log, String year, String period) throws Exception {

        ResultSet rs = null;
        List<CoStaff> CVi;
        CVi = new ArrayList();
        CoStaff c = new CoStaff();

        String q = "";
        String q1 = "";

       

        if (log.getAccessLevel() == 1) {

           
        } else if (log.getAccessLevel() == 2) {//level supervisor

            q = "AND a.staffID IN " + LeaveDAO.getSuperviseeID(log) + "  AND type <> 'Cuti Sakit' AND (a.status = 'Checked' OR a.status = 'Supported' OR a.status = 'Approved'  OR a.status = 'Verified') OR (a.staffID = b.staffID  AND a.staffID IN " + LeaveDAO.getSuperviseeID(log) + "  AND type <> 'Cuti Sakit' AND a.status = 'Rejected' AND supervisorID = '" + log.getUserID() + "' AND type <> 'Cuti Sakit')";

            

        } else if (log.getAccessLevel() == 3) {//level head dept

            //q = "AND a.staffID IN " + LeaveDAO.getSuperviseeIDByLevel(log, "headID") + " AND (a.status = 'Supported' OR a.status = 'Approved'  OR a.status = 'Verified' OR  (a.status = 'Checked' AND type = 'Cuti Sakit')  OR (a.status = 'Rejected' AND headID = '" + log.getUserID() + "'))";
            
            String ql1 = "";
            String ql2 = "";
            
            if(LeaveDAO.isSuperViseeExistForLevel3(log, "support")){
                ql1 = "OR a.staffID IN " + LeaveDAO.getSuperviseeIDByLevel(log, "supervisorID") + " AND (a.status = 'Checked' OR a.status = 'Supported' OR a.status = 'Approved' OR (a.status = 'Checked' AND type = 'Cuti Sakit') OR (a.status = 'Rejected' AND headID = '" + log.getUserID() + "'))";
            }else if(LeaveDAO.isSuperViseeExistForLevel3(log, "approve")){
                ql2 = "";
            }
            
            
            q = " AND (a.staffID IN " + LeaveDAO.getSuperviseeIDByLevel(log, "headID") + " AND (a.status = 'Supported' OR a.status = 'Approved' OR (a.status = 'Checked' AND type = 'Cuti Sakit') OR (a.status = 'Rejected' AND headID = '" + log.getUserID() + "')) "+ql1+")";

           

        } else if (log.getAccessLevel() == 4) {//level HR dept

            q = "AND (a.status = 'Approved' OR a.status = 'Verified') OR (a.status = 'Rejected' AND hrID = '" + log.getUserID() + "') ";

        }
        
        String j =  "select * from leave_master a, co_staff b WHERE  a.staffID = b.staffID " + q1 + " AND a.staffID <> '" + log.getUserID() + "' " + q + "  AND ('" + AccountingPeriod.getCurrentTimeStamp() + "' BETWEEN datestart AND dateend) group by a.staffID ORDER BY FIELD(a.status, 'Preparing', 'Checked', 'Supported', 'Approved', 'Rejected'), dateapply desc";
        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "--" + String.valueOf(j));
       
       try {
            PreparedStatement stmt = log.getCon().prepareStatement(j);
            //stmt.setString(1, dept);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
    public static List<Leave> getAllLeaveExcludeYouByDept(LoginProfile log, String year, String period, String staffID) throws Exception {

        ResultSet rs = null;
        List<Leave> CVi;
        CVi = new ArrayList();
        Leave c = new Leave();

        String q = "";
        String q1 = "";

        if (!staffID.equals("None")) {
            q1 = "AND b.staffID = '" + staffID + "'";
        }

        if (log.getAccessLevel() == 1) {

            CVi = LeaveDAO.getAllLeaveExcludeYouExecuteQuery(log, q, q1);

        } else if (log.getAccessLevel() == 2) {//level supervisor

            q = "AND a.staffID IN " + LeaveDAO.getSuperviseeID(log) + "  AND type <> 'Cuti Sakit' AND (a.status = 'Checked' OR a.status = 'Supported' OR a.status = 'Approved'  OR a.status = 'Verified') OR (a.staffID = b.staffID  AND a.staffID IN " + LeaveDAO.getSuperviseeID(log) + "  AND type <> 'Cuti Sakit' AND a.status = 'Rejected' AND supervisorID = '" + log.getUserID() + "' AND type <> 'Cuti Sakit')";

            CVi = LeaveDAO.getAllLeaveExcludeYouExecuteQuery(log, q, q1);

        } else if (log.getAccessLevel() == 3) {//level head dept

            //q = "AND a.staffID IN " + LeaveDAO.getSuperviseeIDByLevel(log, "headID") + " AND (a.status = 'Supported' OR a.status = 'Approved'  OR a.status = 'Verified' OR  (a.status = 'Checked' AND type = 'Cuti Sakit')  OR (a.status = 'Rejected' AND headID = '" + log.getUserID() + "'))";
            
            String ql1 = "";
            String ql2 = "";
            
            if(LeaveDAO.isSuperViseeExistForLevel3(log, "support")){
                ql1 = "OR a.staffID IN " + LeaveDAO.getSuperviseeIDByLevel(log, "supervisorID") + " AND (a.status = 'Checked' OR a.status = 'Supported' OR a.status = 'Approved' OR (a.status = 'Checked' AND type = 'Cuti Sakit') OR (a.status = 'Rejected' AND headID = '" + log.getUserID() + "'))";
            }else if(LeaveDAO.isSuperViseeExistForLevel3(log, "approve")){
                ql2 = "";
            }
            
            
            q = " AND (a.staffID IN " + LeaveDAO.getSuperviseeIDByLevel(log, "headID") + " AND (a.status = 'Supported' OR a.status = 'Approved' OR (a.status = 'Checked' AND type = 'Cuti Sakit') OR (a.status = 'Rejected' AND headID = '" + log.getUserID() + "')) "+ql1+")";

            //q = "AND a.staffID IN " + LeaveDAO.getSuperviseeID(log) + " AND (a.status = 'Checked' OR a.status = 'Supported' OR a.status = 'Approved'  OR a.status = 'Verified' OR (a.status = 'Checked' AND stafflevel = 2)  OR (a.status = 'Checked' AND stafflevel = 0) OR  (a.status = 'Checked' AND type = 'Cuti Sakit')  OR (a.status = 'Rejected' AND headID = '" + log.getUserID() + "')) OR (a.status = 'Checked' AND stafflevel = 3)";
            CVi.addAll(LeaveDAO.getAllLeaveExcludeYouExecuteQuery(log, q, q1));

        } else if (log.getAccessLevel() == 4) {//level HR dept

            q = "AND (a.status = 'Approved' OR a.status = 'Verified') OR (a.status = 'Rejected' AND hrID = '" + log.getUserID() + "') ";

            CVi = LeaveDAO.getAllLeaveExcludeYouExecuteQuery(log, q, q1);

        }

        return CVi;
    }

    public static List<Leave> getAllLeaveExcludeYouExecuteQuery(LoginProfile log, String query, String query1) throws Exception {

        ResultSet rs = null;
        List<Leave> CVi;
        CVi = new ArrayList();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from leave_master a, co_staff b WHERE  a.staffID = b.staffID " + query1 + " AND a.staffID <> '" + log.getUserID() + "' " + query + " AND ('" + AccountingPeriod.getCurrentTimeStamp() + "' BETWEEN datestart AND dateend)  ORDER BY FIELD(a.status, 'Preparing', 'Checked', 'Supported', 'Approved', 'Rejected'), dateapply desc");
            //stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "--" + String.valueOf(stmt));
            while (rs.next()) {

                Leave cv = new Leave();

                cv.setCheckDate(rs.getString("check_date"));
                cv.setCheckID(rs.getString("checkID"));
                cv.setDateapply(rs.getString("dateapply"));
                cv.setDateend(rs.getString("dateend"));
                cv.setDatestart(rs.getString("datestart"));
                cv.setDays(rs.getInt("days"));
                cv.setHeadDate(rs.getString("head_date"));
                cv.setHeadID(rs.getString("headID"));
                cv.setHrDate(rs.getString("hr_date"));
                cv.setHrID(rs.getString("hrID"));
                cv.setLeaveID(rs.getString("leaveID"));
                cv.setReason(rs.getString("reason"));
                cv.setStaffID(rs.getString("a.staffID"));
                cv.setSupervisorDate(rs.getString("supervisor_date"));
                cv.setSupervisorID(rs.getString("supervisorID"));
                cv.setType(rs.getString("type"));
                cv.setStatus(rs.getString("status"));
                cv.setYear(rs.getString("year"));
                cv.setPeriod(rs.getString("period"));
                cv.setStafflevel(rs.getInt("stafflevel"));

                CVi.add(cv);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static List<Leave> getAllLeave(LoginProfile log) throws Exception {

        ResultSet rs = null;
        List<Leave> CVi;
        CVi = new ArrayList();
        Leave c = new Leave();

        String q = "";

        if (log.getAccessLevel() == 0) {
            q += "WHERE staffID = '" + log.getUserID() + "'";
        } else if (log.getAccessLevel() == 2 || log.getAccessLevel() == 3) {
            q += "WHERE staffID IN " + LeaveDAO.getSuperviseeID(log) + " OR staffID = '" + log.getUserID() + "'";
        }

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from leave_master " + q + " ORDER BY leaveID DESC");
            //stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getLeaveRS(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();//3
        }

        return CVi;
    }

    public static List<Leave> getAllLeaveOfYou(LoginProfile log) throws Exception {

        ResultSet rs = null;
        List<Leave> CVi;
        CVi = new ArrayList();
        Leave c = new Leave();

        String q = "";

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from leave_master WHERE staffID = '" + log.getUserID() + "' ORDER BY leaveID DESC");
            //stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getLeaveRS(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static Leave getLeaveInfoDetail(LoginProfile log, String no) throws SQLException, Exception {
        Leave c = null;
        try {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_master WHERE leaveID=?");
            stmt.setString(1, no);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getLeaveRS(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    private static LeaveInfo getLeaveInfoRS(ResultSet rs) throws SQLException, Exception {
        LeaveInfo cv = new LeaveInfo();

        cv.setBf(rs.getInt("bf"));
        cv.setEligibleleave(rs.getInt("eligibleleave"));
        cv.setId(rs.getInt("id"));
        cv.setStaffID(rs.getString("staffID"));
        cv.setYear(rs.getString("year"));
        cv.setMc(rs.getInt("mc"));

        return cv;
    }

    public static LeaveInfo getEligibleLeaveDetail(LoginProfile log, String no) throws SQLException, Exception {
        LeaveInfo c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_info WHERE staffID=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getLeaveInfoRS(rs);
        }

        return c;
    }

    public static int getTotalLeaveOfTheYear(LoginProfile log, String no, String year) throws SQLException, Exception {
        int bal = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT sum(days) as cnt FROM leave_master WHERE staffID=? and year = ? and status = ? and type <> ?");
        stmt.setString(1, no);
        stmt.setString(2, year);
        stmt.setString(3, "Approved");
        stmt.setString(4, "Cuti Sakit");
        rs = stmt.executeQuery();
        if (rs.next()) {
            bal = rs.getInt("cnt");
        }

        return bal;
    }

    public static int getTotalSickLeaveUseOfTheYear(LoginProfile log, String no, String year) throws SQLException, Exception {
        int bal = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT sum(days) as cnt FROM leave_master WHERE staffID=? and year = ? and status = ? and type = ?");
        stmt.setString(1, no);
        stmt.setString(2, year);
        stmt.setString(3, "Approved");
        stmt.setString(4, "Cuti Sakit");
        rs = stmt.executeQuery();
        if (rs.next()) {
            bal = rs.getInt("cnt");
        }

        return bal;
    }

    public static int getTotalSickLeaveOfTheYear(LoginProfile log, String no, String year) throws SQLException, Exception {
        int bal = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT mc as cnt FROM leave_info WHERE staffID=? and year = ?");
        stmt.setString(1, no);
        stmt.setString(2, year);
        rs = stmt.executeQuery();
        if (rs.next()) {
            bal = rs.getInt("cnt");
        }

        return bal;
    }

    public static int getBalanceTotalSickLeaveOfTheYear(LoginProfile log, String no, String year) throws SQLException, Exception {
        int bal = getTotalSickLeaveOfTheYear(log, no, year) - getTotalSickLeaveUseOfTheYear(log, no, year);

        return bal;
    }

    public static int getTotalLeaveOfTheMonth(LoginProfile log, String no, String year, String period) throws SQLException, Exception {
        int bal = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT sum(days) as cnt FROM leave_master WHERE staffID=? and year = ? and period = ? and status = ? and type <> ?");
        stmt.setString(1, no);
        stmt.setString(2, year);
        stmt.setString(3, period);
        stmt.setString(4, "Approved");
        stmt.setString(5, "Cuti Sakit");
        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------" + stmt);
        rs = stmt.executeQuery();
        if (rs.next()) {
            bal = rs.getInt("cnt");
            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "***" + rs.getInt("cnt"));
            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "***" + bal);
        }

        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "***" + bal);

        return bal;
    }

    public static double getEligibleLeaveOfTheYear(LoginProfile log, String no, String year) throws SQLException, Exception {
        double bal = 0;

        bal = LeaveDAO.getEligibleLeaveWithoutCF(log, no, year) + LeaveDAO.getPreviousLeaveCF(log, no, year);

        return bal;
    }

    public static double getEligibleLeaveWithoutCF(LoginProfile log, String no, String year) throws SQLException, Exception {
        double bal = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_info WHERE staffID=? AND year = ?");
        stmt.setString(1, no);
        stmt.setString(2, year);
        rs = stmt.executeQuery();
        if (rs.next()) {

            bal = rs.getInt("eligibleleave");
        }

        return bal;
    }

    public static double getMedicalLeaveOfTheYear(LoginProfile log, String no, String year) throws SQLException, Exception {
        double bal = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_info WHERE staffID=? AND year = ?");
        stmt.setString(1, no);
        stmt.setString(2, year);
        rs = stmt.executeQuery();
        if (rs.next()) {

            bal = rs.getInt("mc");
        }

        return bal;
    }

    public static double getPreviousLeaveCF(LoginProfile log, String no, String year) throws SQLException, Exception {
        double bal = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_info WHERE staffID=? AND year = ?");
        stmt.setString(1, no);
        stmt.setString(2, year);
        rs = stmt.executeQuery();
        if (rs.next()) {

            bal = rs.getInt("bf");
        }

        return bal;
    }

    public static double getBalanceEligibleLeave(LoginProfile log, String staffID, String year) throws Exception {

        double bal = getEligibleLeaveOfTheYear(log, staffID, year) - getTotalLeaveOfTheYear(log, staffID, year);

        return bal;

    }

    public static void updateLeave(LoginProfile log, Leave item) throws Exception {

        try {
            String q = ("UPDATE leave_master SET dateend = ?,datestart = ?,reason = ?,type = ?, days = ?, year = ?, period = ?  WHERE leaveID = ?");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setString(1, item.getDateend());
            ps.setString(2, item.getDatestart());
            ps.setString(3, item.getReason());
            ps.setString(4, item.getType());
            ps.setInt(5, item.getDays());
            ps.setString(6, AccountingPeriod.getCurYearByDate(item.getDatestart()));
            ps.setString(7, AccountingPeriod.getCurPeriodByDate(item.getDatestart()));
            ps.setString(8, item.getLeaveID());

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void deleteLeave(LoginProfile log, String id) throws Exception {

        try {
            String q = ("DELETE FROM leave_master WHERE leaveID = ?");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setString(1, id);

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static String getBadgeColor(String status) {

        String color = "dark";

        if (status.equals("Approved")) {
            color = "success";
        } else if (status.equals("Preparing")) {
            color = "blue";
        } else if (status.equals("Checked")) {
            color = "warning";
        } else if (status.equals("Supported")) {
            color = "pink";
        } else if (status.equals("Verified")) {
            color = "success";
        } else {
            color = "danger";
        }

        return color;
    }

    public static String getBadgeColorTypeLeave(String status) {

        String color = "dark";

        if (status.equals("Cuti Sakit")) {
            color = "warning";
        } else if (status.equals("Cuti Kecemasan")) {
            color = "danger";
        } else if (status.equals("Cuti Tahunan")) {
            color = "success";
        }

        return color;
    }

    public static double getEligibleLeaveForTheMonth(LoginProfile log, String staffID, String year, String date) throws Exception {

        double i = 0;

        double CL = LeaveDAO.getEligibleLeaveWithoutCF(log, staffID, year);
        double curMonth = Double.parseDouble(AccountingPeriod.getCurPeriodByDate(date));

        double beforeAddCF = CL / 12 * curMonth;
        double afterAddCF = beforeAddCF + LeaveDAO.getPreviousLeaveCF(log, staffID, year);
        double plusLeaveUse = afterAddCF - LeaveDAO.getTotalLeaveOfTheYear(log, staffID, year);
        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "date : " + date + "------leave : " + beforeAddCF + " , CL : " + CL + " , addCF  : " + plusLeaveUse);

//        double balLeave = LeaveDAO.getBalanceEligibleLeave(log, staffID, year);
//        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------balleave" + String.valueOf(balLeave));
//        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------balmonth" + String.valueOf(getBalanceMonthofYear(date)));
//
//        double e = balLeave / getBalanceMonthofYear(date);
//        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------leave--" + String.valueOf(e));
        //String number = String.valueOf(d);
        //number = number.substring(number.indexOf(".")).substring(1);
        return plusLeaveUse;

    }

    public static double getEligibleLeaveForTheMonthExcludeApprovedThisMonth(LoginProfile log, String staffID, String year, String date) throws Exception {

        double i = 0;

        double CL = LeaveDAO.getEligibleLeaveWithoutCF(log, staffID, year);
        double curMonth = Double.parseDouble(AccountingPeriod.getCurPeriodByDate(date));

        double beforeAddCF = CL / 12 * curMonth;
        double afterAddCF = beforeAddCF + LeaveDAO.getPreviousLeaveCF(log, staffID, year);
        double plusLeaveUse = afterAddCF - LeaveDAO.getTotalLeaveOfTheYear(log, staffID, year) + LeaveDAO.getTotalLeaveOfTheMonth(log, staffID, year, AccountingPeriod.getCurPeriodByDate(date));
        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------leave : " + beforeAddCF + " , CL : " + CL + " , addCF  : " + afterAddCF);

//        double balLeave = LeaveDAO.getBalanceEligibleLeave(log, staffID, year);
//        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------balleave" + String.valueOf(balLeave));
//        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------balmonth" + String.valueOf(getBalanceMonthofYear(date)));
//
//        double e = balLeave / getBalanceMonthofYear(date);
//        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------leave--" + String.valueOf(e));
        //String number = String.valueOf(d);
        //number = number.substring(number.indexOf(".")).substring(1);
        return LeaveDAO.PrecisionDouble(plusLeaveUse);

    }

    public static int getPreparedLeave(LoginProfile log) throws SQLException, Exception {
        int bal = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT count(*) as cnt FROM leave_master WHERE  status = ?");
        //stmt.setString(1, no);
        stmt.setString(1, "Preparing");
        rs = stmt.executeQuery();
        if (rs.next()) {

            bal = rs.getInt("cnt");
        }

        return bal;
    }

    public static int getLeaveInProcess(LoginProfile log) throws SQLException, Exception {
        int bal = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT count(*) as cnt FROM leave_master WHERE  status <> ? AND status <> ? and staffID = ?");
        //stmt.setString(1, no);
        stmt.setString(1, "Approved");
        stmt.setString(2, "Rejected");
        stmt.setString(3, log.getUserID());
        rs = stmt.executeQuery();
        if (rs.next()) {

            bal = rs.getInt("cnt");
        }

        return bal;
    }

    public static int getBalanceMonthofYear(String date) throws Exception {

        int b = 0;

        String currentMonth = AccountingPeriod.getCurPeriodByDate(date);

        int balMonth = 12 - Integer.parseInt(currentMonth) + 1;
        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------balMonth" + String.valueOf(balMonth));
        if (balMonth == 0) {
            balMonth = 1;
        }

        return balMonth;

    }

    public static double getPercentLeaveUseYear(LoginProfile log, String staffID, String year) throws Exception {

        double l = 0;

        double totLeaveUse = LeaveDAO.getTotalLeaveOfTheYear(log, staffID, year);
        double totLeaveYear = LeaveDAO.getEligibleLeaveOfTheYear(log, staffID, year);

        l = totLeaveUse * 100 / totLeaveYear;

        return l;
    }

    public static double getPercentMedicalLeaveUseYear(LoginProfile log, String staffID, String year) throws Exception {

        double l = 0;

        double totLeaveUse = LeaveDAO.getTotalSickLeaveUseOfTheYear(log, staffID, year);
        double totLeaveYear = LeaveDAO.getMedicalLeaveOfTheYear(log, staffID, year);

        l = totLeaveUse * 100 / totLeaveYear;

        return l;
    }

    public static double getPercentLeaveUseMonth(LoginProfile log, String staffID, String year, String period, String date) throws Exception {

        double l = 0;

        double totLeaveUse = LeaveDAO.getTotalLeaveOfTheMonth(log, staffID, year, period);
        double totLeaveYear = LeaveDAO.getEligibleLeaveForTheMonthExcludeApprovedThisMonth(log, staffID, year, date);

        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------getPercentLeaveUseMonth " + totLeaveYear);

        l = totLeaveUse * 100 / totLeaveYear;

        return l;
    }

    public static String getProgressBarColor(LoginProfile log, double percent) throws Exception {

        String c = "";

        if (percent < 31) {
            c = "success";
        } else if (percent > 30 && percent < 61) {
            c = "warning";
        } else if (percent > 60) {
            c = "danger";
        }

        return c;
    }

    public static void validateLeave(LoginProfile log, String leaveID, String action) throws Exception {

        String colID = "";
        String colDate = "";

        Leave l = (Leave) LeaveDAO.getLeaveInfoDetail(log, leaveID);

        if (log.getAccessLevel() == 1) {

            colID = "checkID";
            colDate = "check_date";

        } else if (log.getAccessLevel() == 2) {

            colID = "supervisorID";
            colDate = "supervisor_date";

        } else if (log.getAccessLevel() == 3) {

            if (LeaveDAO.getLevelThreeSupportOrApprove(log, l.getStaffID()).equals("support")) {
                colID = "supervisorID";
                colDate = "supervisor_date";
            } else if (LeaveDAO.getLevelThreeSupportOrApprove(log, l.getStaffID()).equals("approve")) {
                colID = "headID";
                colDate = "head_date";
            }

        } else if (log.getAccessLevel() == 4) {

            colID = "hrID";
            colDate = "hr_date";

        }

        try {
            String q = ("UPDATE leave_master SET status = ?, " + colID + " = ?, " + colDate + " = ?  WHERE leaveID = ?");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setString(1, action);
            ps.setString(2, log.getUserID());
            ps.setString(3, AccountingPeriod.getCurrentTimeStamp());
            ps.setString(4, leaveID);

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static int getLeaveCountForSupervisee(LoginProfile log) throws SQLException, Exception {

        String q = "";
        String status = "";

        if (log.getAccessLevel() == 1) {
            q = "AND status = 'Preparing'";
        } else if (log.getAccessLevel() == 2) {
            q = "AND staffiD IN " + getSuperviseeID(log) + " AND status = 'Checked'";
            //status = "status = 'Checked'";
        } else if (log.getAccessLevel() == 3) {
            q = "AND (staffiD IN " + LeaveDAO.getSuperviseeIDByLevel(log, "supervisorID")+" AND status = 'Checked') OR (staffiD IN " + LeaveDAO.getSuperviseeIDByLevel(log, "headID")+" AND status = 'Supported')";
            //status = "(status = 'Checked' OR status = 'Supported')";
        }

        int i = 0;

        //if (isSuperViseeExist(log)) {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT count(*) as cnt FROM leave_master WHERE staffID <> ?  " + q);
            //stmt.setString(1, status);
            stmt.setString(1, log.getUserID());
            rs = stmt.executeQuery();

            while (rs.next()) {
                i = rs.getInt("cnt");
            }
        //}

        return i;
    }
    
     public static boolean isSuperViseeExistForLevel3(LoginProfile log, String type) throws SQLException, Exception {

        boolean b = false;

        String colID = "";

        if (type.equals("support")) {
            colID = "supervisorID";
        } else if (type.equals("approve")) {
            colID = "headID";
        }

        if (log.getAccessLevel() != 1) {

            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM supervise_info WHERE " + colID + " = ?");
            //stmt.setString(1, no);
            stmt.setString(1, log.getUserID());
            rs = stmt.executeQuery();
            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------q " + stmt);
            if (rs.next()) {

                b = true;
            }

        }

        return b;

    }

    public static boolean isSuperViseeExist(LoginProfile log) throws SQLException, Exception {

        boolean b = false;

        String colID = "";

        if (log.getAccessLevel() == 2) {
            colID = "supervisorID";
        } else if (log.getAccessLevel() == 3) {
            colID = "headID";
        }

        if (log.getAccessLevel() != 1) {

            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM supervise_info WHERE " + colID + " = ?");
            //stmt.setString(1, no);
            stmt.setString(1, log.getUserID());
            rs = stmt.executeQuery();
            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------q " + stmt);
            if (rs.next()) {

                b = true;
            }

        }

        return b;

    }

    public static String getSuperviseeID(LoginProfile log) throws SQLException, Exception {

        String colID = "";

        if (log.getAccessLevel() == 2) {
            colID = "supervisorID";
        } else if (log.getAccessLevel() == 3) {
            colID = "headID";
        }

        String s = "(";
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM supervise_info WHERE " + colID + " = ?");
        //stmt.setString(1, no);
        stmt.setString(1, log.getUserID());
        rs = stmt.executeQuery();

        while (rs.next()) {

            if (rs.isLast()) {
                s += "'" + rs.getString("staffID") + "'";
            } else {
                s += "'" + rs.getString("staffID") + "'" + ",";
            }

        }

        s += ")";

        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------string" + String.valueOf(s));

        return s;
    }

    public static String getSuperviseeIDByLevel(LoginProfile log, String col) throws SQLException, Exception {

        String s = "(";
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM supervise_info WHERE " + col + " = ?");
        //stmt.setString(1, no);
        stmt.setString(1, log.getUserID());
        rs = stmt.executeQuery();

        while (rs.next()) {

            if (rs.isLast()) {
                s += "'" + rs.getString("staffID") + "'";
            } else {
                s += "'" + rs.getString("staffID") + "'" + ",";
            }

        }

        s += ")";

        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------string" + String.valueOf(s));

        return s;
    }

    public static boolean isButtonDisable(LoginProfile log, String leaveID) throws Exception {

        boolean b = false;

        Leave l = (Leave) LeaveDAO.getLeaveInfoDetail(log, leaveID);

        if (log.getAccessLevel() == 2 && (l.getStatus().equals("Supported") || l.getStatus().equals("Approved") || l.getStatus().equals("Rejected")) || (l.getStaffID().equals(log.getUserID()))) {
            b = true;
        } else if (log.getAccessLevel() == 1 && (l.getStatus().equals("Checked") || l.getStatus().equals("Supported") || l.getStatus().equals("Approved") || l.getStatus().equals("Rejected"))) {
            b = true;
        } else if (log.getAccessLevel() == 3 && (l.getStatus().equals("Supported") || l.getStatus().equals("Approved") || l.getStatus().equals("Rejected")) && !(l.getStaffID().equals(log.getUserID()))) {

            if (LeaveDAO.getLevelThreeSupportOrApprove(log, l.getStaffID()).equals("support") && l.getStatus().equals("Supported")) {
                b = true;
            } else if (LeaveDAO.getLevelThreeSupportOrApprove(log, l.getStaffID()).equals("approve") && l.getStatus().equals("Approved")) {
                b = true;
            }
            
        } else if (log.getAccessLevel() == 0) {
            b = true;
        }//else if(log.getAccessLevel() == 4 && (l.getStatus().equals("Verified"))){
        //    b = true;
        // }

        if (log.getAccessLevel() == 1 && l.getStatus().equals("Preparing")) {
            b = false;
        }

        return b;

    }

    public static boolean isLeaveInfoExist(LoginProfile log, String staffID, String date) throws SQLException, Exception {

        boolean b = false;

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_info WHERE  staffID = ? AND year = ?");
        //stmt.setString(1, no);
        stmt.setString(1, staffID);
        stmt.setString(2, AccountingPeriod.getCurYearByDate(date));
        rs = stmt.executeQuery();
        if (rs.next()) {

            b = true;
        }

        return b;

    }

    public static String getLevelThreeSupportOrApprove(LoginProfile log, String staffID) throws SQLException, Exception {

        String isa = "";

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM supervise_info WHERE  staffID = ?");
        //stmt.setString(1, no);
        stmt.setString(1, staffID);
        rs = stmt.executeQuery();
        if (rs.next()) {

            String i = rs.getString("supervisorID");
            String j = rs.getString("headID");

            if (i.equals(log.getUserID())) {
                isa = "support";
            }

            if (j.equals(log.getUserID())) {
                isa = "approve";
            }
        }

        return isa;

    }

    public static void insertLeave(LoginProfile log, LeaveInfo item) throws Exception {

        try {
            String q = ("INSERT INTO leave_info(staffID, year, bf, eligibleleave,mc) values (?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setString(1, item.getStaffID());
            ps.setString(2, item.getYear());
            ps.setInt(3, item.getBf());
            ps.setInt(4, item.getEligibleleave());
            ps.setInt(5, item.getMc());

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static JSONArray getAllLeaveForCalendar(LoginProfile log, String filter) throws Exception {

        JSONArray array = new JSONArray();

        List<Leave> bdt = LeaveDAO.getAllConfirmedLeave(log, filter);

        for (Leave j : bdt) {

            if (j.getDays() > 1) {
                j.setDateend(AccountingPeriod.addOneDay(j.getDateend()));
            }

            //List<BookingDestination> ls = BookingDAO.getListDestinationByBookID(log, j.getBookID());
            //for (BookingDestination i : ls) {
            String typeColor = "";

            if (j.getType().equals("Cuti Tahunan")) {
                typeColor = "success";
            } else {
                typeColor = "danger";
            }

            JSONObject jo = new JSONObject();
            jo.put("id", j.getLeaveID());
            //jo.put("title", "<img src = \""+MemberDAO.getMemberInfo(log, j.getStaffID()).getImageURL()+"\">-"+ i.getDestdescp());
            jo.put("title", StaffDAO.getInfo(log, j.getStaffID()).getName());
            jo.put("start", j.getDatestart());
            jo.put("end", j.getDateend());
            jo.put("className", "fc-event-" + typeColor);
            jo.put("imageurl", StaffDAO.getInfo(log, j.getStaffID()).getImageURL());
            array.add(jo);
            jo = null;

            //}
        }

        return array;

    }

    public static List<Leave> getAllConfirmedLeave(LoginProfile log, String filter) throws Exception, SQLException {

        Statement stmt = null;
        List<Leave> CV;
        CV = new ArrayList();

        String restrictByLevel = "";

        if (!filter.equals("none")) {

            if (log.getAccessLevel() == 0) {//normal user level : department view
                restrictByLevel = "  staffID IN " + StaffDAO.getDepartmentMemberID(log, filter) + " AND ";
            }

            if (log.getAccessLevel() == 1) {//level leave admin : all staff view
                restrictByLevel = "  staffID IN " + StaffDAO.getDepartmentMemberID(log, filter) + " AND ";
            }

            if (log.getAccessLevel() == 2) {//level supervisor :  : department view

                restrictByLevel = "  staffID IN " + StaffDAO.getDepartmentMemberID(log, filter) + " AND ";

            } else if (log.getAccessLevel() == 3) {//level head dept @ gm 

                restrictByLevel = "  staffID IN " + StaffDAO.getDepartmentMemberID(log, filter) + " AND ";

            } else if (log.getAccessLevel() == 4) {//level HR dept

                //q = "AND (status = 'Approved' OR status = 'Verified') OR (status = 'Rejected' AND hrID = '" + log.getUserID() + "') ";
            }

        } else if (filter.equals("none")) {

        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM leave_master WHERE " + restrictByLevel + "  status = 'Approved' order by leaveID asc ");
            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "SELECT * FROM leave_master WHERE " + restrictByLevel + "  status = 'Approved' order by leaveID asc ");

            while (rs.next()) {
                CV.add(LeaveDAO.getLeaveRS(rs));
            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    public static int getCountConfirmedLeave(LoginProfile log) throws Exception, SQLException {

        int cnt = 0;

        Statement stmt = null;

        String restrictByLevel = "";

        if (log.getAccessLevel() == 0) {//normal user level : department view
            restrictByLevel = "  staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " AND ";
        }

        if (log.getAccessLevel() == 1) {//level leave admin : all staff view

        }

        if (log.getAccessLevel() == 2) {//level supervisor :  : department view

            restrictByLevel = "  staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " AND ";

        } else if (log.getAccessLevel() == 3) {//level head dept @ gm 

            restrictByLevel = "  staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " AND ";

        } else if (log.getAccessLevel() == 4) {//level HR dept

            //q = "AND (status = 'Approved' OR status = 'Verified') OR (status = 'Rejected' AND hrID = '" + log.getUserID() + "') ";
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT count(*) as cnt FROM leave_master WHERE " + restrictByLevel + "  status = 'Approved'");
            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "-SELECT count(*) as cnt FROM leave_master WHERE " + restrictByLevel + "  status = 'Approved'");

            if (rs.next()) {
                cnt = rs.getInt("cnt");
            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return cnt;
    }

    public static int getAllLeaveForStaff(LoginProfile log) throws SQLException, Exception {
        int bal = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT count(*) as cnt FROM leave_master WHERE  staffID = ?");
        //stmt.setString(1, no);
        stmt.setString(1, log.getUserID());
        rs = stmt.executeQuery();
        if (rs.next()) {

            bal = rs.getInt("cnt");
        }

        return bal;
    }

    public static int getCountSuperviseeLeave(LoginProfile log) throws SQLException, Exception {
        int bal = 0;

        if (LeaveDAO.isSuperViseeExist(log)) {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT count(*) as cnt FROM leave_master WHERE  staffID IN " + LeaveDAO.getSuperviseeID(log));
            //stmt.setString(1, no);
            //stmt.setString(1, log.getUserID());
            rs = stmt.executeQuery();
            if (rs.next()) {

                bal = rs.getInt("cnt");
            }
            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------string" + String.valueOf(stmt));
        }

        return bal;
    }

    private static LeaveAttachment getLeaveAttachmentRS(ResultSet rs) throws SQLException, Exception {
        LeaveAttachment cv = new LeaveAttachment();

        cv.setFilename(rs.getString("filename"));
        cv.setId(rs.getInt("id"));
        cv.setLeaveID(rs.getString("leaveID"));
        cv.setSessionID(rs.getString("sessionID"));
        cv.setStaffID(rs.getString("staffID"));

        return cv;
    }

    public static LeaveAttachment getLeaveAttachment(LoginProfile log, String no) throws SQLException, Exception {
        LeaveAttachment c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_attachment WHERE leaveID=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getLeaveAttachmentRS(rs);
        }

        return c;
    }

    public static String getMalayWord(String word) {

        String newword = "";

        if (word.equals("Approved")) {
            newword = "Lulus";
        } else if (word.equals("Preparing")) {
            newword = "Baru";
        } else if (word.equals("Checked")) {
            newword = "Semak";
        } else if (word.equals("Supported")) {
            newword = "Sokong";
        } else if (word.equals("Verified")) {
            newword = "Sah";
        } else if (word.equals("Rejected")) {
            newword = "Batal";
        }

        return newword;
    }

    public static String getMalayWord2(String word) {

        String newword = "";

        if (word.equals("Approved")) {
            newword = "Lulus";
        } else if (word.equals("Preparing")) {
            newword = "Baru";
        } else if (word.equals("Checked")) {
            newword = "Disemak";
        } else if (word.equals("Supported")) {
            newword = "Disokong";
        } else if (word.equals("Verified")) {
            newword = "Sah";
        } else if (word.equals("Rejected")) {
            newword = "Batal";
        } else if (word.equals("cancel")) {
            newword = "Batal?";
        }

        return newword;
    }

    public static List<Leave> getAllConfirmedLeaveToday(LoginProfile log, String filter) throws Exception, SQLException {

        Statement stmt = null;
        List<Leave> CV;
        CV = new ArrayList();

        String restrictByLevel = "";

        if (log.getAccessLevel() == 0) {//normal user level : department view
            restrictByLevel = "  staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " AND ";
        }

        if (log.getAccessLevel() == 1) {//level leave admin : all staff view

        }

        if (log.getAccessLevel() == 2) {//level supervisor :  : department view

            restrictByLevel = "  staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " AND ";

        } else if (log.getAccessLevel() == 3) {//level head dept @ gm 

            restrictByLevel = "  staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " AND ";

        } else if (log.getAccessLevel() == 4) {//level HR dept

            //q = "AND (status = 'Approved' OR status = 'Verified') OR (status = 'Rejected' AND hrID = '" + log.getUserID() + "') ";
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM leave_master WHERE ('" + AccountingPeriod.getCurrentTimeStamp() + "' BETWEEN datestart AND dateend) AND " + restrictByLevel + "  status = 'Approved' order by leaveID asc ");

            while (rs.next()) {
                CV.add(LeaveDAO.getLeaveRS(rs));
            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }
    
    public static List<Leave> getAllLeaveTodayByDept(LoginProfile log, String date, String deptID) throws Exception, SQLException {

        Statement stmt = null;
        List<Leave> CV;
        CV = new ArrayList();

date  = date.substring(6, 10) + "-" + date.substring(3, 5) + "-" + date.substring(0, 2);
     

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM leave_master a, co_staff b WHERE a.staffID = b.staffID and ('" + date + "' BETWEEN datestart AND dateend)  and b.departmentID = '"+deptID+"' order by a.leaveID asc ");
            
            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------string" + String.valueOf("SELECT * FROM leave_master a, co_staff b WHERE a.staffID = b.staffID and ('" + date + "' BETWEEN datestart AND dateend)  and b.departmentID = '"+deptID+"' order by a.leaveID asc "));

            while (rs.next()) {
                CV.add(LeaveDAO.getLeaveRS(rs));
            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }
    
    public static List<Leave> getAllLeaveToday(LoginProfile log, String date) throws Exception, SQLException {

        Statement stmt = null;
        List<Leave> CV;
        CV = new ArrayList();

//date  = date.substring(6, 10) + "-" + date.substring(3, 5) + "-" + date.substring(0, 2);

        String q = "";
        
        if(StaffDAO.getInfoDepartmentByHeadID(log) != null && !(CheckDAO.isMenuAccess(log, 1))) {
            q = "AND b.departmentID = '"+ StaffDAO.getInfoDepartmentByHeadID(log).getId() +"'";
        }
        
        if(CheckDAO.isMenuAccess(log, 0)){
             q = "";
        }
     

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM leave_master a, co_staff b WHERE a.staffID = b.staffID and ('" + date + "' BETWEEN datestart AND dateend) AND a.status = 'Approved' AND (location = 'IBU PEJABAT' OR location = 'CAWANGAN INDERA MAHKOTA') AND position <> 'PENYELIA' "+q+"  order by a.leaveID asc ");
            
            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------string" + String.valueOf("SELECT * FROM leave_master a, co_staff b WHERE a.staffID = b.staffID and ('" + date + "' BETWEEN datestart AND dateend) AND a.status = 'Approved' AND (location = 'IBU PEJABAT' OR location = 'CAWANGAN INDERA MAHKOTA') AND position <> 'PENYELIA' "+q+"  order by a.leaveID asc "));

            while (rs.next()) {
                CV.add(LeaveDAO.getLeaveRS(rs));
            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    public static JSONArray getAnalysisData(LoginProfile log, String type) throws Exception {

        JSONArray array = new JSONArray();

        for (int i = 1; i <= 12; i++) {

            array.add(LeaveDAO.getCountConfirmedLeaveForChart(log, type, AccountingPeriod.getCurYearByCurrentDate(), String.valueOf(i)));
        }

        return array;

    }

    public static int getCountConfirmedLeaveForChart(LoginProfile log, String type, String year, String period) throws Exception, SQLException {

        int cnt = 0;

        Statement stmt = null;

        String restrictByLevel = "";

        //if (log.getAccessLevel() == 0) {//normal user level : department view//aaaaa
        restrictByLevel = "  staffID = '" + log.getUserID() + "' AND ";
        //}

//        if (log.getAccessLevel() == 1) {//level leave admin : all staff view
//
//           
//
//        }
//        
//        if (log.getAccessLevel() == 2) {//level supervisor :  : department view
//
//           restrictByLevel = "  staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " AND ";
//
//        } else if (log.getAccessLevel() == 3) {//level head dept @ gm 
//
//           restrictByLevel = "  staffID IN " + StaffDAO.getDepartmentMemberID(log, StaffDAO.getInfo(log, log.getUserID()).getDepartmentID()) + " AND ";
//
//        } else if (log.getAccessLevel() == 4) {//level HR dept
//
//            //q = "AND (status = 'Approved' OR status = 'Verified') OR (status = 'Rejected' AND hrID = '" + log.getUserID() + "') ";
//
//        }
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT sum(days) as cnt FROM leave_master WHERE " + restrictByLevel + "  status = 'Approved' AND year = '" + year + "' AND period = '" + period + "' AND type = '" + type + "'");
            //Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "SELECT sum(days) as cnt FROM leave_master WHERE " + restrictByLevel + "  status = 'Approved' AND year = '" + year + "' AND period = '" + period + "' AND type = '" + type + "'");

            if (rs.next()) {
                cnt = rs.getInt("cnt");
            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return cnt;
    }

    public static boolean isNewLeaveExceedEligibleMonth(LoginProfile log, String staffID, String year, String date, int newLeave, String leaveType) throws SQLException, Exception {

        boolean b = false;

        double elM = 0.0;

        if (leaveType.equals("Cuti Tahunan")) {
            elM = LeaveDAO.getEligibleLeaveForTheMonth(log, staffID, year, date);
        } else if (leaveType.equals("Cuti Sakit")) {
            elM = LeaveDAO.getMedicalLeaveOfTheYear(log, staffID, year) - LeaveDAO.getTotalSickLeaveUseOfTheYear(log, staffID, year);
            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "allmc : " + LeaveDAO.getMedicalLeaveOfTheYear(log, staffID, year) + " usemc :'" + LeaveDAO.getTotalSickLeaveUseOfTheYear(log, staffID, year));
        }

        if (newLeave > elM) {
            b = true;
        }

        return b;

    }

    public static void saveComment(LoginProfile log, LeaveComment lv) throws Exception {

        try {

            String q_attach = ("insert into leave_comment(comment, date, leaveID, staffID, time) values (?,?,?,?,?)");
            PreparedStatement ps_attach = log.getCon().prepareStatement(q_attach);
            ps_attach.setString(1, lv.getComment());
            ps_attach.setString(2, AccountingPeriod.getCurrentTimeStamp());
            ps_attach.setString(3, lv.getLeaveID());
            ps_attach.setString(4, log.getUserID());
            ps_attach.setString(5, AccountingPeriod.getCurrentTime());

            ps_attach.executeUpdate();
            ps_attach.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private static LeaveComment getLeaveCommentRS(ResultSet rs) throws SQLException, Exception {
        LeaveComment lv = new LeaveComment();

        lv.setComment(rs.getString("comment"));
        lv.setDate(rs.getString("date"));
        lv.setId(rs.getInt("id"));
        lv.setLeaveID(rs.getString("leaveID"));
        lv.setStaffID(rs.getString("staffID"));
        lv.setTime(rs.getString("time"));

        return lv;
    }

    public static LeaveComment getLeaveComment(LoginProfile log, String no) throws SQLException, Exception {
        LeaveComment c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_comment WHERE leaveID=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getLeaveCommentRS(rs);
        }

        return c;
    }

    public static List<LeaveComment> getAllLeaveComment(LoginProfile log, String leaveID) throws Exception {

        ResultSet rs = null;
        List<LeaveComment> CVi;
        CVi = new ArrayList();
        LeaveComment c = new LeaveComment();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from leave_comment WHERE leaveID = ? ORDER BY id DESC");
            stmt.setString(1, leaveID);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getLeaveCommentRS(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();//3
        }

        return CVi;
    }

    public static boolean hasComment(LoginProfile log, String no) throws SQLException, Exception {
        boolean c = false;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_comment WHERE leaveID=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = true;
        }

        return c;
    }

    public static List<CoStaff> getSuperviseeListInfo(LoginProfile log) throws SQLException, Exception {

        String colID = "";

        if (log.getAccessLevel() == 2) {
            colID = "supervisorID";
        } else if (log.getAccessLevel() == 3) {
            colID = "headID";
        }

        String s = "(";
        ResultSet rs = null;

        List<CoStaff> CVi;
        CVi = new ArrayList();
        CoStaff c = new CoStaff();

        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM supervise_info WHERE " + colID + " = ?");
        //stmt.setString(1, no);
        stmt.setString(1, log.getUserID());
        rs = stmt.executeQuery();

        while (rs.next()) {
            CVi.add(StaffDAO.getInfo(log, rs.getString("staffID")));
        }

        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------string" + String.valueOf(s));

        return CVi;

    }

    public static List<Leave> getAllLeaveByStaffID(LoginProfile log, String staffID, String type) throws Exception {

        ResultSet rs = null;
        List<Leave> CVi;
        CVi = new ArrayList();
        Leave c = new Leave();

        String q = "";

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from leave_master WHERE staffID = '" + staffID + "' AND type = '" + type + "' ORDER BY leaveID DESC");
            //stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getLeaveRS(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static void saveDummyLeave(LoginProfile log, String staffID) throws Exception {

        try {
            String q = ("INSERT INTO leave_info(staffID, year, bf, eligibleleave,mc) values (?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setString(1, staffID);
            ps.setString(2, AccountingPeriod.getCurYearByCurrentDate());
            ps.setInt(3, 10);
            ps.setInt(4, 30);
            ps.setInt(5, 20);

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static List<CoStaffDepartment> getAllLeaveGroupByDepartment(LoginProfile log, String year, String period) throws Exception {

        ResultSet rs = null;
        List<CoStaffDepartment> CVi;
        CVi = new ArrayList();

        try {
            //PreparedStatement stmt = log.getCon().prepareStatement("select * from leave_master a, co_staff b where a.staffID = b.staffID and a.year = ? AND a.period = ?  group by departmentID");
            PreparedStatement stmt = log.getCon().prepareStatement("select * from leave_master a, co_staff b where a.staffID = b.staffID  group by departmentID");
            //stmt.setString(1, year);
            //stmt.setString(2, period);

            rs = stmt.executeQuery();
            while (rs.next()) {

                CVi.add(StaffDAO.getInfoDepartment(log, rs.getString("b.departmentID")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static int getCountLeaveByDepartment(LoginProfile log) throws SQLException {

        int i = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_master WHERE staffID=?");
        stmt.setString(1, log.getUserID());
        rs = stmt.executeQuery();
        if (rs.next()) {
            i = rs.getInt("level");
        }

        return i;

    }

    public static int getLeaveCountForDepartmentByStatus(LoginProfile log, String deptID, String status) throws SQLException, Exception {

        int i = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from leave_master a, co_staff b WHERE  a.staffID = b.staffID AND b.departmentID = '" + deptID + "' AND a.status = '" + status + "' AND a.staffID <> '" + log.getUserID() + "' AND ('" + AccountingPeriod.getCurrentTimeStamp() + "' BETWEEN datestart AND dateend) ORDER BY FIELD(a.status, 'Preparing', 'Checked', 'Supported', 'Approved', 'Rejected')");
//        stmt.setString(1, deptID);
//        stmt.setString(2, log.getUserID());
        rs = stmt.executeQuery();

        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------string" + String.valueOf(stmt));

        while (rs.next()) {
            i = rs.getInt("cnt");
        }

        return i;
    }
    
    public static int getLeaveCountByStatusAndDate(LoginProfile log, String status, String date) throws SQLException, Exception {
        
        date  = date.substring(6, 10) + "-" + date.substring(3, 5) + "-" + date.substring(0, 2);
        int i = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from leave_master a, co_staff b WHERE  a.staffID = b.staffID AND a.status = '" + status + "' AND a.staffID <> '" + log.getUserID() + "' AND ('" + date + "' BETWEEN datestart AND dateend) ORDER BY FIELD(a.status, 'Preparing', 'Checked', 'Supported', 'Approved', 'Rejected')");
//        stmt.setString(1, deptID);
//        stmt.setString(2, log.getUserID());
        rs = stmt.executeQuery();

        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------string" + String.valueOf(stmt));

        while (rs.next()) {
            i = rs.getInt("cnt");
        }

        return i;
    }
    
    public static int getLeaveCountByStatusAndDateIncludeYou(LoginProfile log, String status, String date) throws SQLException, Exception {
        
        //date  = date.substring(6, 10) + "-" + date.substring(3, 5) + "-" + date.substring(0, 2);
        
        String q = "";
        
        if(StaffDAO.getInfoDepartmentByHeadID(log) != null && !(CheckDAO.isMenuAccess(log, 1))) {
            q = "AND b.departmentID = '"+ StaffDAO.getInfoDepartmentByHeadID(log).getId() +"'";
        }
        
        if(CheckDAO.isMenuAccess(log, 0)){
             q = "";
        }
        
        int i = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from leave_master a, co_staff b WHERE  a.staffID = b.staffID AND a.status = '" + status + "' AND ('" + date + "' BETWEEN datestart AND dateend) AND (location = 'IBU PEJABAT' OR location = 'CAWANGAN INDERA MAHKOTA') AND position <> 'PENYELIA' "+q+" ORDER BY FIELD(a.status, 'Preparing', 'Checked', 'Supported', 'Approved', 'Rejected')");
//        stmt.setString(1, deptID);
//        stmt.setString(2, log.getUserID());
        rs = stmt.executeQuery();

        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------string" + String.valueOf(stmt));

        while (rs.next()) {
            i = rs.getInt("cnt");
        }

        return i;
    }
    
    public static int getLeaveCountForDepartmentByStatusAndDate(LoginProfile log, String deptID, String status, String date) throws SQLException, Exception {

        date  = date.substring(6, 10) + "-" + date.substring(3, 5) + "-" + date.substring(0, 2);
        int i = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from leave_master a, co_staff b WHERE  a.staffID = b.staffID AND b.departmentID = '" + deptID + "' AND a.status = '" + status + "' AND a.staffID <> '" + log.getUserID() + "' AND ('" + date + "' BETWEEN datestart AND dateend) ORDER BY FIELD(a.status, 'Preparing', 'Checked', 'Supported', 'Approved', 'Rejected')");
//        stmt.setString(1, deptID);
//        stmt.setString(2, log.getUserID());
        rs = stmt.executeQuery();

        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------string" + String.valueOf(stmt));

        while (rs.next()) {
            i = rs.getInt("cnt");
        }

        return i;
    }
    
     public static int getLeaveCountForStaffByStatus(LoginProfile log, String staffID, String status) throws SQLException, Exception {

        int i = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from leave_master a, co_staff b WHERE  a.staffID = b.staffID AND b.staffID = '" + staffID + "' AND a.status = '" + status + "' AND a.staffID <> '" + log.getUserID() + "' AND ('" + AccountingPeriod.getCurrentTimeStamp() + "' BETWEEN datestart AND dateend) ORDER BY FIELD(a.status, 'Preparing', 'Checked', 'Supported', 'Approved', 'Rejected')");
//        stmt.setString(1, deptID);
//        stmt.setString(2, log.getUserID());
        rs = stmt.executeQuery();

        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------string" + String.valueOf(stmt));

        while (rs.next()) {
            i = rs.getInt("cnt");
        }

        return i;
    }
     
     public static int getLeaveCountForStaffByStatusByLevel(LoginProfile log, String staffID, String status) throws SQLException, Exception {

        int i = 0;
        ResultSet rs = null;
        
        String q = "";
        
        if (log.getAccessLevel() == 1) {

           
        } else if (log.getAccessLevel() == 2) {//level supervisor

            q = "AND a.staffID IN " + LeaveDAO.getSuperviseeID(log) + "  AND type <> 'Cuti Sakit' AND (a.status = 'Checked' OR a.status = 'Supported' OR a.status = 'Approved'  OR a.status = 'Verified') OR (a.staffID = b.staffID  AND a.staffID IN " + LeaveDAO.getSuperviseeID(log) + "  AND type <> 'Cuti Sakit' AND a.status = 'Rejected' AND supervisorID = '" + log.getUserID() + "' AND type <> 'Cuti Sakit')";

            

        } else if (log.getAccessLevel() == 3) {//level head dept

            //q = "AND a.staffID IN " + LeaveDAO.getSuperviseeIDByLevel(log, "headID") + " AND (a.status = 'Supported' OR a.status = 'Approved'  OR a.status = 'Verified' OR  (a.status = 'Checked' AND type = 'Cuti Sakit')  OR (a.status = 'Rejected' AND headID = '" + log.getUserID() + "'))";
            
            String ql1 = "";
            String ql2 = "";
            
            if(LeaveDAO.isSuperViseeExistForLevel3(log, "support")){
                ql1 = "OR a.staffID IN " + LeaveDAO.getSuperviseeIDByLevel(log, "supervisorID") + " AND (a.status = 'Checked' OR a.status = 'Supported' OR a.status = 'Approved' OR (a.status = 'Checked' AND type = 'Cuti Sakit') OR (a.status = 'Rejected' AND headID = '" + log.getUserID() + "'))";
            }else if(LeaveDAO.isSuperViseeExistForLevel3(log, "approve")){
                ql2 = "";
            }
            
            
            q = " AND (a.staffID IN " + LeaveDAO.getSuperviseeIDByLevel(log, "headID") + " AND (a.status = 'Supported' OR a.status = 'Approved' OR (a.status = 'Checked' AND type = 'Cuti Sakit') OR (a.status = 'Rejected' AND headID = '" + log.getUserID() + "')) "+ql1+")";

           

        } else if (log.getAccessLevel() == 4) {//level HR dept

            q = "AND (a.status = 'Approved' OR a.status = 'Verified') OR (a.status = 'Rejected' AND hrID = '" + log.getUserID() + "') ";

        }
        
        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from leave_master a, co_staff b WHERE  a.staffID = b.staffID AND b.staffID = '" + staffID + "' AND a.status = '" + status + "'  AND ('" + AccountingPeriod.getCurrentTimeStamp() + "' BETWEEN datestart AND dateend) AND  a.staffID <> '" + log.getUserID() + "' "+q);
//        stmt.setString(1, deptID);
//        stmt.setString(2, log.getUserID());
        rs = stmt.executeQuery();

        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------string" + String.valueOf(stmt));

        while (rs.next()) {
            i = rs.getInt("cnt");
        }

        return i;
    }

    public static void saveRequest(LoginProfile log, LeaveRequest lr) throws Exception {

        try {
            String q = ("INSERT INTO leave_request(datetochange, leaveID, requestto, status, daterequest, timerequest) values (?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setString(1, lr.getDatetochange());
            ps.setString(2, lr.getLeaveID());
            ps.setString(3, lr.getRequestto());
            ps.setBoolean(4, lr.getStatus());
            ps.setString(5, AccountingPeriod.getCurrentTimeStamp());
            ps.setString(6, AccountingPeriod.getCurrentTime());

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private static LeaveRequest getLeaveRequestRS(ResultSet rs) throws SQLException, Exception {
        LeaveRequest cv = new LeaveRequest();

        cv.setDaterequest(rs.getString("daterequest"));
        cv.setDatetochange(rs.getString("datetochange"));
        cv.setId(rs.getInt("id"));
        cv.setLeaveID(rs.getString("leaveID"));
        cv.setRequestto(rs.getString("requestto"));
        cv.setStatus(rs.getBoolean("status"));
        cv.setTimerequest(rs.getString("timerequest"));

        return cv;
    }

    public static LeaveRequest getLeaveRequest(LoginProfile log, String no) throws SQLException, Exception {
        LeaveRequest c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_request WHERE leaveID=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getLeaveRequestRS(rs);
        }

        return c;
    }

    public static boolean isLeaveRequestExist(LoginProfile log, String leaveID, String request) throws SQLException, Exception {

        boolean b = false;

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_request WHERE  leaveID = ? AND requestto = ?");
        //stmt.setString(1, no);
        stmt.setString(1, leaveID);
        stmt.setString(2, request);
        rs = stmt.executeQuery();
        if (rs.next()) {

            b = true;
        }

        return b;

    }

    private static LeaveReasonTemplate getLeaveReasonTemplateRS(ResultSet rs) throws SQLException, Exception {
        LeaveReasonTemplate cv = new LeaveReasonTemplate();

        cv.setId(rs.getInt("id"));
        cv.setDorder(rs.getInt("dorder"));
        cv.setActive(rs.getBoolean("active"));
        cv.setReason(rs.getString("reason"));

        return cv;
    }

    public static LeaveReasonTemplate getLeaveReasonTemplate(LoginProfile log, String no) throws SQLException, Exception {
        LeaveReasonTemplate c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM leave_reason_template WHERE id=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getLeaveReasonTemplateRS(rs);
        }

        return c;
    }

    public static List<LeaveReasonTemplate> getAllLeaveReasonTemplate(LoginProfile log) throws Exception {

        ResultSet rs = null;
        List<LeaveReasonTemplate> CVi;
        CVi = new ArrayList();
        LeaveReasonTemplate c = new LeaveReasonTemplate();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from leave_reason_template WHERE active = ? ORDER BY dorder");
            stmt.setBoolean(1, true);
            rs = stmt.executeQuery();
            Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "--" + String.valueOf(stmt));
            while (rs.next()) {
                CVi.add(getLeaveReasonTemplateRS(rs));
            }

        } catch (SQLException e) {
            e.printStackTrace();//3
        }

        return CVi;
    }

//    public static String getLastSync(LoginProfile log) throws SQLException, Exception {
//        String lastsync = "";
//        ResultSet rs = null;
//        try {
//
//            PreparedStatement stmt = log.getCon().prepareStatement("SELECT CONCAT(datesync,' ',timesync) as sync FROM ar_agree_lastsync WHERE id = (select max(id) from ar_agree_lastsync)");
//            rs = stmt.executeQuery();
//            if (rs.next()) {
//                lastsync = rs.getString("sync");
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//        return lastsync;
//    }
//    public static JSONArray getMonthActivity(LoginProfile log) throws Exception {
//
//        JSONArray array = new JSONArray();
//        
//
//        String year = AccountingPeriod.getCurrentTimeStamp().substring(0,4);
//        String period = AccountingPeriod.getCurrentTimeStamp().substring(5,7);
//
//        for (int i = 1; i <= AccountingPeriod.getDayPerMonth(Integer.parseInt(period), Integer.parseInt(year)); i++) {
//            JSONObject jo = new JSONObject();
//            String j = String.valueOf(i);
//
//            if (j.length() == 1) {
//                j = "0" + j;
//            }
//            String dt = year + "-" + period + "-" + j;
//            jo.put("year", year);
//            jo.put("month", period);
//            jo.put("total", getUserLoginPerday(log, dt));
//            jo.put("totaltrans", getTransactionPerday(log, dt));
//            array.add(jo);
//            jo = null;
//        }
//
//        return array;
//
//    }
}
