/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */
package com.lcsb.echeck.dao;

import com.lcsb.echeck.model.LoginProfile;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class AutoGenerate {
    
    public static String getReferenceNox(LoginProfile log, String refertitle, String table, String abb, String estatecode, String y, String p) throws Exception {
        Connection con = log.getCon();
        ResultSet rs = null;
        String no = "";
        String period = "period";
        String ecode = "estatecode";
        String year = "year";

        try {

            if (abb.equals("JVN") || abb.equals("JTX")) {
                period = "curperiod";
            }

            if (abb.equals("INV") || abb.equals("DNE") || abb.equals("DNR") || abb.equals("CNE") || abb.equals("CNR") || abb.equals("TGS") || abb.equals("CNP") || abb.equals("DNP")) {
                ecode = "estcode";
            }

            if (abb.equals("TXI") || abb.equals("TXO") || abb.equals("TXR")) {
                ecode = "loccode";
            }
            
            if (abb.equals("EMP") || abb.equals("BRN") || abb.equals("STP") || abb.equals("CON")) {
                ecode = "companycode";
            }
            
            Logger.getLogger(AutoGenerate.class.getName()).log(Level.INFO, "select ifnull(concat('" + abb + "','" + estatecode + "',substring('" + y + "',3,2),lpad('" + p + "',2,'0'),lpad((max(substring(" + refertitle + ",12,4))+1),4,'0')),concat('" + abb + "','" + estatecode + "',substring('" + y + "',3,2),lpad('" + p + "',2,'0'),'0001')) as refer from " + table + " where " + year + " ='" + y + "' and " + period + "='" + p + "' and " + ecode + " like concat('" + estatecode + "','%') and substring("+refertitle+",1,3) = '"+abb+"'");

            PreparedStatement stmt = con.prepareStatement("select ifnull(concat('" + abb + "','" + estatecode + "',substring('" + y + "',3,2),lpad('" + p + "',2,'0'),lpad((max(substring(" + refertitle + ",12,4))+1),4,'0')),concat('" + abb + "','" + estatecode + "',substring('" + y + "',3,2),lpad('" + p + "',2,'0'),'0001')) as refer from " + table + " where " + year + " ='" + y + "' and " + period + "='" + p + "' and " + ecode + " like concat('" + estatecode + "','%') and substring("+refertitle+",1,3) = '"+abb+"'");

       
            rs = stmt.executeQuery();
            if (rs.next()) {
                no = rs.getString(1);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return no;
    }


    public static String getVoucherNo(LoginProfile log, String table, String voucherno) throws Exception {
        ResultSet rs = null;
        String no = "";

        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(concat(lpad(max(" + voucherno + ")+1,5,'0')), '00001') as new from " + table + "");

        rs = stmt.executeQuery();
        if (rs.next()) {
            no = rs.getString(1);
        }

        return no;
    }

    public static String get4digitNo(LoginProfile log, String table, String voucherno) throws Exception {
        ResultSet rs = null;
        String no = "";

        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(concat(lpad(max(" + voucherno + ")+1,4,'0')), '0001') as new from " + table + "");

        rs = stmt.executeQuery();
        if (rs.next()) {
            no = rs.getString(1);
        }

        return no;
    }
    
    public static String get10digitNo(LoginProfile log, String table, String voucherno) throws Exception {
        ResultSet rs = null;
        String no = "";

        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(concat(lpad(max(" + voucherno + ")+1,10,'0')), '0000000001') as new from " + table + "");

        rs = stmt.executeQuery();
        if (rs.next()) {
            no = rs.getString(1);
        }

        return no;
    }
    
    public static String get10digitNoVisitor(Connection con, String table, String voucherno) throws Exception {
        ResultSet rs = null;
        String no = "";

        PreparedStatement stmt = con.prepareStatement("select ifnull(concat(lpad(max(" + voucherno + ")+1,10,'0')), '0000000001') as new from " + table + "");

        rs = stmt.executeQuery();
        if (rs.next()) {
            no = rs.getString(1);
        }

        return no;
    }
    
    public static String get4digitNoWithSpecialChar(LoginProfile log, String table, String voucherno,String sChar) throws Exception {
        ResultSet rs = null;
        String no = "";

        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(concat('"+sChar+"',lpad((max(substring("+voucherno+",2,4))+1),4,'0')),concat('"+sChar+"','0001')) as refer from "+table);

        rs = stmt.executeQuery();
        if (rs.next()) {
            no = rs.getString(1);
        }

        return no;
    }
    
    public static String get4digitNoWithThreeSpecialChar(LoginProfile log, String table, String voucherno,String sChar) throws Exception {
        ResultSet rs = null;
        String no = "";

        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(concat('"+sChar+"',lpad((max(substring("+voucherno+",4,4))+1),4,'0')),concat('"+sChar+"','0001')) as refer from "+table);

        rs = stmt.executeQuery();
        if (rs.next()) {
            no = rs.getString(1);
        }

        return no;
    }
   
     public static String get5digitNoWithSpecialChar(LoginProfile log, String table, String voucherno,String sChar) throws Exception {
        ResultSet rs = null;
        String no = "";

        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(concat('"+sChar+"',lpad((max(substring("+voucherno+",4,5))+1),5,'0')),concat('"+sChar+"','00001')) as refer from "+table);

        rs = stmt.executeQuery();
        if (rs.next()) {
            no = rs.getString(1);
        }

        return no;
    }

    public static String get2digitNo(LoginProfile log, String table, String voucherno) throws Exception {
        ResultSet rs = null;
        String no = "";

        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(concat(lpad(max(" + voucherno + ")+1,2,'0')), '01') as new from " + table + "");

        rs = stmt.executeQuery();
        if (rs.next()) {
            no = rs.getString(1);
        }

        return no;
    }

    public static String getReferAddBack(LoginProfile log, String table, String voucherno, String refno, String columnfrom) throws Exception {
        ResultSet rs = null;
        String no = "";

        //select ifnull(concat(jvrefno,lpad((max(right(jvid,4))+1),4,'0')),concat('"+ request.getParameter("novoucher") +"','0001')) as jvidx  from gl_jv_item where jvrefno='"+ request.getParameter("novoucher") +"'");
        String query = "select ifnull(concat(" + columnfrom + ",lpad((max(right(" + voucherno + ",4))+1),4,'0')),concat('" + refno + "','0001')) as no  from " + table + " where   " + columnfrom + "='" + refno + "'";
        PreparedStatement stmt = log.getCon().prepareStatement(query);

        Logger.getLogger(AutoGenerate.class.getName()).log(Level.INFO, query);

        rs = stmt.executeQuery();
        if (rs.next()) {
            no = rs.getString(1);
        }

        return no;
    }

    public static String getBranchCode(LoginProfile log, String table, String branchcolumn, String origincolumn, String origincode) throws Exception {
        ResultSet rs = null;
        String no = "";
        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(lpad(max(" + branchcolumn + ")+1,4,'0'),concat('" + origincode + "','01')) as mcode from " + table + " where " + origincolumn + "='" + origincode + "'");

        rs = stmt.executeQuery();
        if (rs.next()) {
            no = rs.getString(1);
        }

        return no;
    }
    
    
    
    

}
