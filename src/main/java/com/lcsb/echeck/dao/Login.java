/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.echeck.dao;

import com.lcsb.echeck.dao.LoginDAO;
import com.lcsb.echeck.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Login", urlPatterns = {"/Login"})
public class Login extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();

            try {
                HttpSession session = request.getSession();
                SessionConnection sessionConnection = (SessionConnection) session.getAttribute("sessionconnection");

                Connection connection = null;
                if (sessionConnection != null) {
                    connection = sessionConnection.getConnection();
                    Logger.getLogger(Login.class.getName()).log(Level.INFO, "-------connection----" + connection);
                }
                String urlsend = "";

                LoginProfile login = (LoginProfile) LoginDAO.setLogin(connection);

                login.setEmail((String) session.getAttribute("sessionemail"));
                login.setFullname((String) session.getAttribute("sessionname"));
                login.setImageURL((String) session.getAttribute("sessionimageurl"));

                if (LoginDAO.checkUserUsingEmail((String) session.getAttribute("sessionemail"), connection)) {

                    //LoginDAO.updateMemberWithGoogleCredential(connection, (String) session.getAttribute("sessionemail"), (String) session.getAttribute("sessionuserid"), (String) session.getAttribute("sessionname"), (String) session.getAttribute("sessionimageurl"));
                    login.setUserID(StaffDAO.getInfoByEmail(login, login.getEmail()).getStaffid());
                    login.setAccessLevel(LoginDAO.getAccessLevel(connection, login.getUserID()));
                    session.setAttribute("login_detail", login);
                    session.removeAttribute("login_error");
                    LoginDAO.updateImageGoogle(connection, login.getEmail(), login.getImageURL());//

                    if (request.getParameter("linkTo") == null) {
                        urlsend = "/main.jsp";
                    } else if (request.getParameter("linkTo").equals("checkin")) {
                        if(request.getParameter("deptID") == null || request.getParameter("deptID").equals(null) || request.getParameter("deptID").equals("null")){
                             urlsend = "/main.jsp";
                        }else{
                             urlsend = "/checkin.jsp?deptID="+request.getParameter("deptID");
                        }
                       
                    } else if (request.getParameter("linkTo").equals("checkout")) {
                        if(AccountingPeriod.convertStringTimeToTime("14:00:00").after(AccountingPeriod.convertStringTimeToTime("17:00:00"))){
                            CheckDAO.checkOut(login);
                            urlsend = "/checkout.jsp";
                        }else{
                            urlsend = "/checkout_work.jsp";
                        }
                        
                        
                    } else if (request.getParameter("linkTo").equals("adminmobile")) {
                        urlsend = "/admin_list_mobile.jsp";
                    }else if (request.getParameter("linkTo").equals("admin")) {
                        urlsend = "/admin_list.jsp";
                    }else if (request.getParameter("linkTo").equals("adminsummary")) {
                        urlsend = "/admin_summary.jsp";
                    }else if (request.getParameter("linkTo").equals("adminreport")) {
                        urlsend = "/admin_report_daily.jsp";
                    }else if (request.getParameter("linkTo").equals("scaninterdept")) {
                        CheckDAO.checkInDept(login, request.getParameter("deptID"));
                        urlsend = "/welcome_dept.jsp?deptID="+request.getParameter("deptID");
                    }else if (request.getParameter("linkTo").equals("adminsummarydept")) {
                        urlsend = "/admin_summary_dept.jsp?deptID="+request.getParameter("deptID");
                    }

                } else {

                    session.setAttribute("login_detail", login);

//                    Members member = new Members();
//                    member.setCompID(login.getUserID());
//                    member.setEmail(login.getEmail());
//                    member.setImageURL(login.getImageURL());
//                    member.setLevel(0);
//                    member.setMemberName(login.getFullname());
//                    member.setPosition("n/a");
//
//                    LoginDAO.saveMemberInfo(login, member);
                    //StaffDAO.saveStaff(login);
                    urlsend = "/intro_user.jsp";
                }

//                    } else {
//
////                        LogError err = new LogError();
////                        err.setHaveError(true);
////                        err.setMessageid("0001");
////                        err.setMessage("Incorrect username or password!");
////                        err.setType("Danger");
////                        session.setAttribute("login_error", err);
//                        urlsend = "/login.jsp";
//
//                    }
//            
                RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
                dispatcher.forward(request, response);

            } catch (Exception ex) {

                String urlerror = "";

                if (request.getParameter("linkTo") == null) {
                    urlerror = "/errorPage.jsp";
                } else if (request.getParameter("linkTo").equals("viewleaveapproval")) {
                    urlerror = "/index.jsp?linkTo=viewleaveapproval&leaveID="+request.getParameter("leaveID");
                }
//                if(request.getParameter("linkTo").equals("ada")){
//                    urlerror = "/index.jsp";
//                }else if(request.getParameter("linkTo").equals("null")){//aaaaaaaa
//                     urlerror = "/errorPage.jsp";a
//                }

                RequestDispatcher dispatcher_error = getServletContext().getRequestDispatcher(urlerror);
                dispatcher_error.forward(request, response);
                Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (Exception ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
