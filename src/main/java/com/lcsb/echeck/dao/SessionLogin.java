/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.echeck.dao;

import com.lcsb.echeck.dao.ConnectionDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "SessionLogin", urlPatterns = {"/SessionLogin"})
public class SessionLogin extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SessionLogin</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SessionLogin at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        try {
            // load the driver
            //Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (ClassNotFoundException e) {
            throw new UnavailableException(
                    "Login init() ClassNotFoundException: " + e.getMessage());
        } catch (IllegalAccessException e) {
            throw new UnavailableException(
                    "Login init() IllegalAccessException: " + e.getMessage());
        } catch (InstantiationException e) {
            throw new UnavailableException(
                    "Login init() InstantiationException: " + e.getMessage());
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();
        SessionConnection sessionConnection = (SessionConnection) session.getAttribute("sessionconnection");
        Connection connection = null;
        if (sessionConnection != null) {
            //connection = sessionConnection.getConnection();
            session.removeAttribute("sessionconnection");
            session.removeAttribute("sessionuser");
            session.removeAttribute("sessionpassword");

        }

        Logger.getLogger(ConnectionDAO.class.getName()).log(Level.INFO, "==========" + connection);
        if (connection == null) {
            Connection initialCon = null;
            try {
                connection = ConnectionDAO.getConnection();
                Logger.getLogger(ConnectionDAO.class.getName()).log(Level.INFO, "==========++++" + connection);

            } catch (Exception ex) {
                Logger.getLogger(SessionLogin.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (connection != null) {
                // store the connection
                sessionConnection = new SessionConnection();
                sessionConnection.setConnection(connection);
                session.setAttribute("sessionconnection", sessionConnection);
                session.setAttribute("sessionuserid", request.getParameter("userID"));
                session.setAttribute("sessionemail", request.getParameter("email"));
                session.setAttribute("sessionname", request.getParameter("name"));
                session.setAttribute("sessionimageurl", request.getParameter("imageurl"));

                String linkTo = "";
                String deptID = "";
                if (request.getParameter("linkTo") == null || request.getParameter("linkTo").equals("null")  || request.getParameter("linkTo").equals("")) {
                    
                    response.sendRedirect("Login");
                }else{
                    linkTo = request.getParameter("linkTo");
                    deptID = request.getParameter("deptID");
                    //aa
                    
                    response.sendRedirect("Login?linkTo="+linkTo+"&deptID="+deptID);
                }
                
                return;

            }
        } else {
            String logout = request.getParameter("logout");
            if (logout == null) {

                response.sendRedirect("Login?user=" + request.getParameter("user") + "&password=" + request.getParameter("password"));
                return;
                // test the connection
//                Statement statement = null;
//                ResultSet resultSet = null;
//                String userName = null;
//                try {
//                    statement = connection.createStatement();
//                    resultSet = statement
//                            .executeQuery("select initcap(user) from sys.dual");
//                    if (resultSet.next()) {a
//                        userName = resultSet.getString(1);
//                    }
//                } catch (SQLException e) {
//                    out.println("Login doGet() SQLException: " + e.getMessage()
//                            + "<p>");
//                } finally {
//                    if (resultSet != null) {
//                        try {
//                            resultSet.close();
//                        } catch (SQLException ignore) {
//                        }
//                    }
//                    if (statement != null) {
//                        try {
//                            statement.close();
//                        } catch (SQLException ignore) {
//                        }
//                    }
//                }
//                out.println("Hello " + userName + "!<p>");
//                out.println("Your session ID is " + session.getId() + "<p>");
//                out
//                        .println("It was created on "
//                                + new java.util.Date(session.getCreationTime())
//                                + "<p>");
//                out.println("It was last accessed on "
//                        + new java.util.Date(session.getLastAccessedTime())
//                        + "<p>");
//                out.println("<form method=\"get\" action=\"SessionLogin\">");
//                out.println("<input type=\"submit\" name=\"logout\" "
//                        + "value=\"Logout\">");
//                out.println("</form>");
            } else {
                // close the connection and remove it from the session
                try {
                    connection.close();
                } catch (SQLException ignore) {
                }
                session.removeAttribute("sessionconnection");
                out.println("You have been logged out.");
            }
        }
        out.println("</body>");
        out.println("</html>");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        doGet(request, response);
    }
}

class SessionConnection implements HttpSessionBindingListener {

    Connection connection;

    public SessionConnection() {
        connection = null;
    }

    public SessionConnection(Connection connection) {
        this.connection = connection;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public void valueBound(HttpSessionBindingEvent event) {
        if (connection != null) {
            System.out.println("Binding a valid connection");
        } else {
            System.out.println("Binding a null connection");
        }
    }

    public void valueUnbound(HttpSessionBindingEvent event) {
        if (connection != null) {
            System.out
                    .println("Closing the bound connection as the session expires");
            try {
                connection.close();
            } catch (SQLException ignore) {
            }
        }
    }

}
