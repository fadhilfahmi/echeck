/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.echeck.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class ConnectionDAO {
    
    public static Connection getConnection() throws Exception {

        Connection newcon = null;
        String dbname = "eleave";


        try {

            Class.forName("com.mysql.jdbc.Driver");
//            String dbURL = "jdbc:mysql://103.8.24.34:3306/" + dbname + "?zeroDateTimeBehavior=convertToNull";
//            String dbuser = "izznabra";
//            String dbpass = "#kQl4x6#4AWX0j";
            String dbURL = "jdbc:mysql://localhost:3306/" + dbname + "?zeroDateTimeBehavior=convertToNull";
            String dbuser = "root";
            String dbpass = "adminlcsb";
            //String dbURL = "jdbc:mysql://mysql/"+dbname + "?zeroDateTimeBehavior=convertToNull";
            
            newcon = DriverManager.getConnection(dbURL, dbuser, dbpass);

        } catch (SQLException ex) {
            Logger.getLogger(ConnectionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }


        return newcon;
    }
    
    public static Connection getSystemConnection() throws Exception {

        Connection newcon = null;
        String dbname = "echeck";


        try {

            Class.forName("com.mysql.jdbc.Driver");
//            String dbURL = "jdbc:mysql://103.8.24.34:3306/" + dbname + "?zeroDateTimeBehavior=convertToNull";
//            String dbuser = "izznabra";
//            String dbpass = "#kQl4x6#4AWX0j";
            String dbURL = "jdbc:mysql://localhost:3306/" + dbname + "?zeroDateTimeBehavior=convertToNull";
            String dbuser = "root";
            String dbpass = "adminlcsb";
            //String dbURL = "jdbc:mysql://mysql/"+dbname + "?zeroDateTimeBehavior=convertToNull";
            
            newcon = DriverManager.getConnection(dbURL, dbuser, dbpass);

        } catch (SQLException ex) {
            Logger.getLogger(ConnectionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }


        return newcon;
    }
    
}
