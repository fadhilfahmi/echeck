/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.echeck.dao;

import static com.lcsb.echeck.dao.StaffDAO.getInfo;
import static com.lcsb.echeck.dao.StaffDAO.isStaffHeadIDFirstLogin;
import static com.lcsb.echeck.dao.StaffDAO.isStaffInfoExisted;
import static com.lcsb.echeck.dao.StaffDAO.isStaffSupervisorFirstLogin;
import static com.lcsb.echeck.dao.StaffDAO.isSupervisorRegistered;
import com.lcsb.echeck.model.Check;
import com.lcsb.echeck.model.CheckReport;
import com.lcsb.echeck.model.Checkaccessmenu;
import com.lcsb.echeck.model.Checkdept;
import com.lcsb.echeck.model.Checkform;
import com.lcsb.echeck.model.Checkmaster;
import com.lcsb.echeck.model.Checkoutstation;
import com.lcsb.echeck.model.Checkshift;
import com.lcsb.echeck.model.Checkworktime;
import com.lcsb.echeck.model.CoStaff;
import com.lcsb.echeck.model.InOutSummary;
import com.lcsb.echeck.model.LoginProfile;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class CheckDAO {

    public static void saveCheck(LoginProfile log, Checkform c, String locationID, String reasonOutStation, double temp) throws Exception {

        try {

            CoStaff co = (CoStaff) StaffDAO.getInfoByEmail(log, log.getEmail());

            String id = AutoGenerate.get10digitNo(log, "checkmaster", "id");

            if (!isCheckingExisted(log, co.getStaffid())) {

                String q = ("INSERT INTO checkmaster(staffID, date, timescan, status, id) VALUES (?,?,?,?,?)");
                PreparedStatement ps = log.getCon().prepareStatement(q);
                ps.setString(1, co.getStaffid());
                ps.setString(2, AccountingPeriod.getCurrentTimeStamp());
                ps.setString(3, AccountingPeriod.getCurrentTime());
                ps.setString(4, "Pending");
                ps.setString(5, id);

                Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "---query-- " + ps);

                ps.executeUpdate();
                ps.close();

                String q1 = ("INSERT into checkform(checkID, question1, question2, question3) values (?,?,?,?)");
                PreparedStatement ps1 = log.getCon().prepareStatement(q1);
                ps1.setString(1, id);
                ps1.setString(2, c.getQuestion1());
                ps1.setString(3, c.getQuestion2());
                ps1.setString(4, c.getQuestion3());

                ps1.executeUpdate();
                ps1.close();

            }

            if (CheckDAO.isStaffCheckInOutsideTheirBase(log, co.getStaffid(), locationID)) {

                Checkoutstation os = new Checkoutstation();
                os.setCheckID(id);
                os.setDate(AccountingPeriod.getCurrentTimeStamp());
                os.setLocationID(locationID);
                os.setReason(reasonOutStation);
                os.setStaffID(log.getUserID());
                os.setTimein(AccountingPeriod.getCurrentTime());
                os.setStatus("Pending");
                CheckDAO.saveCheckOutStation(log, os, locationID);
                
                CheckDAO.updateTemperatureAndValidate(log, temp, id);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void saveCheckManual(LoginProfile log, Checkform c, String staffID, double temperature, String date, String timein, String timeout) throws Exception {

        try {

            if (timein.length() == 5) {
                timein += ":00";
            }

            if (timeout.length() == 5) {
                timeout += ":00";
            }

            CoStaff co = (CoStaff) StaffDAO.getInfoByEmail(log, log.getEmail());

            //if (!isCheckingExisted(log, co.getStaffid())) {
            String id = AutoGenerate.get10digitNo(log, "checkmaster", "id");

            String q = ("INSERT INTO checkmaster(staffID, date, timevalid, timescan, status, id, timeout) VALUES (?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, staffID);
            ps.setString(2, date);
            ps.setString(3, timein);
            ps.setString(4, timein);
            ps.setString(5, "Pending");
            ps.setString(6, id);
            ps.setString(7, timeout);

            Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "---query-- " + ps);

            ps.executeUpdate();
            ps.close();

            String q1 = ("INSERT into checkform(checkID, question1, question2, question3) values (?,?,?,?)");
            PreparedStatement ps1 = log.getCon().prepareStatement(q1);
            ps1.setString(1, id);
            ps1.setString(2, c.getQuestion1());
            ps1.setString(3, c.getQuestion2());
            ps1.setString(4, c.getQuestion3());

            ps1.executeUpdate();
            ps1.close();

            CheckDAO.updateTemperatureAndValidate(log, temperature, id);

            //}
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static boolean isCheckingExisted(LoginProfile log, String id) throws SQLException, Exception {
        boolean c = false;

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM checkmaster WHERE staffID=? and date=?");
        stmt.setString(1, id);
        stmt.setString(2, AccountingPeriod.getCurrentTimeStamp());
        Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "---adaketak-- " + stmt);

        rs = stmt.executeQuery();
        if (rs.next()) {
            c = true;
        }

        return c;
    }

    public static Check getCheckInfoByStaffID(LoginProfile log, String no) throws SQLException, Exception {
        Check c = new Check();

        Checkmaster cm = (Checkmaster) getInfoByStaffID(log, no);
        c.setCm(cm);
        c.setCi(getCheckForm(log, cm.getId()));

        return c;
    }

    public static Check getCheckInfoByStaffIDAndDate(LoginProfile log, String no, String date) throws SQLException, Exception {
        Check c = new Check();

        Checkmaster cm = (Checkmaster) getInfoByStaffIDAndDate(log, no, date);
        c.setCm(cm);
        c.setCi(getCheckForm(log, cm.getId()));

        return c;
    }

    public static Checkmaster getInfoByStaffIDAndDate(LoginProfile log, String no, String date) throws SQLException, Exception {
        Checkmaster c = null;

        try {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM checkmaster WHERE  staffID=? and date=?");
            stmt.setString(1, no);
            stmt.setString(2, date);
            rs = stmt.executeQuery();
            Logger.getLogger(CheckDAO.class.getName()).log(Level.INFO, "-------------------------asdsad----------" + String.valueOf(stmt));
            if (rs.next()) {
                c = getInfo(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    public static Checkmaster getInfoByCheckID(LoginProfile log, String no) throws SQLException, Exception {
        Checkmaster c = null;

        try {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM checkmaster WHERE  id=?");
            stmt.setString(1, no);
            rs = stmt.executeQuery();
            Logger.getLogger(CheckDAO.class.getName()).log(Level.INFO, "-------------------------asdsad----------" + String.valueOf(stmt));
            if (rs.next()) {
                c = getInfo(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    public static Checkmaster getInfoByStaffID(LoginProfile log, String no) throws SQLException, Exception {
        Checkmaster c = null;

        try {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM checkmaster WHERE  staffID=? and date=?");
            stmt.setString(1, no);
            stmt.setString(2, AccountingPeriod.getCurrentTimeStamp());
            rs = stmt.executeQuery();
            Logger.getLogger(CheckDAO.class.getName()).log(Level.INFO, "-------------------------asdsad----------" + String.valueOf(stmt));
            if (rs.next()) {
                c = getInfo(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    public static Checkaccessmenu getAccessByStaffID(LoginProfile log, int lvl) throws SQLException, Exception {
        Checkaccessmenu c = null;

        try {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM checkaccessmenu WHERE  staffID=? and menulevel = ?");
            stmt.setString(1, log.getUserID());
            stmt.setInt(2, lvl);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getInfoAccess(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    public static Checkaccessmenu getAccessByStaffIDOnly(LoginProfile log, int lvl) throws SQLException, Exception {
        Checkaccessmenu c = null;

        try {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM checkaccessmenu WHERE  staffID=?");
            stmt.setString(1, log.getUserID());
            stmt.setInt(2, lvl);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getInfoAccess(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    public static Checkmaster getInfo(ResultSet rs) throws SQLException {

        Checkmaster g = new Checkmaster();

        g.setDate(rs.getString("date"));
        g.setId(rs.getString("id"));
        g.setStaffID(rs.getString("staffID"));
        g.setStatus(rs.getString("status"));
        g.setTemperature(rs.getDouble("temperature"));
        g.setTimescan(rs.getString("timescan"));
        g.setTimevalid(rs.getString("timevalid"));
        g.setTimeout(rs.getString("timeout"));

        return g;
    }

    public static Checkaccessmenu getInfoAccess(ResultSet rs) throws SQLException {

        Checkaccessmenu g = new Checkaccessmenu();

        g.setId(rs.getInt("id"));
        g.setMenulevel(rs.getInt("menulevel"));
        g.setStaffID(rs.getString("staffID"));

        return g;
    }

    public static Checkworktime getRSWorktime(ResultSet rs) throws SQLException {

        Checkworktime g = new Checkworktime();

        g.setId(rs.getString("id"));
        g.setDescp(rs.getString("descp"));
        g.setEnd(rs.getString("end"));
        g.setStart(rs.getString("start"));

        return g;
    }

    public static Checkform getCheckForm(LoginProfile log, String no) throws SQLException, Exception {
        Checkform c = null;

        try {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM checkform WHERE checkID=?");
            stmt.setString(1, no);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getInfoCheckform(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    public static Checkform getInfoCheckform(ResultSet rs) throws SQLException {

        Checkform g = new Checkform();

        g.setCheckID(rs.getString("checkID"));
        g.setId(rs.getInt("id"));
        g.setQuestion1(rs.getString("question1"));
        g.setQuestion2(rs.getString("question2"));
        g.setQuestion3(rs.getString("question3"));

        return g;
    }

    public static Checkshift getRSCheckshift(ResultSet rs) throws SQLException {

        Checkshift g = new Checkshift();

        g.setActive(rs.getBoolean("active"));
        g.setApprove(rs.getBoolean("approve"));
        g.setApprovedbyDate(rs.getString("approvedbyDate"));
        g.setApprovedbyID(rs.getString("approvedbyID"));
        g.setId(rs.getInt("id"));
        g.setStaffID(rs.getString("staffID"));
        g.setSyifID(rs.getString("syifID"));

        return g;
    }

    public static Checkdept getInfoCheckdept(ResultSet rs) throws SQLException {

        Checkdept g = new Checkdept();

        g.setDate(rs.getString("date"));
        g.setId(rs.getString("id"));
        g.setStaffID(rs.getString("staffID"));
        g.setStatus(rs.getString("status"));
        g.setTimescan(rs.getString("timescan"));
        g.setTimeout(rs.getString("timeout"));
        g.setDeptID(rs.getString("deptID"));
        g.setName(rs.getString("name"));
        g.setNotel(rs.getString("notel"));

        return g;
    }

    public static Checkdept getCheckDept(LoginProfile log, String no) throws SQLException, Exception {
        Checkdept c = null;

        try {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM checkdept WHERE date=? and staffID = ? and deptID = ?");
            stmt.setString(1, AccountingPeriod.getCurrentTimeStamp());
            stmt.setString(2, StaffDAO.getInfoByEmail(log, log.getEmail()).getStaffid());
            stmt.setString(3, no);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getInfoCheckdept(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    public static Checkworktime getCheckWorkTime(LoginProfile log, String no) throws SQLException, Exception {
        Checkworktime c = null;

        try {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM checkworktime WHERE id=?");
            stmt.setString(1, no);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getRSWorktime(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    public static List<Checkworktime> getListWorkTime(LoginProfile log) {

        ResultSet rs = null;
        List<Checkworktime> CVi;
        CVi = new ArrayList();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM checkworktime order by id asc");
            //stmt.setString(1, date);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getRSWorktime(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static List<Checkshift> getListShiftStaff(LoginProfile log) {

        ResultSet rs = null;
        List<Checkshift> CVi;
        CVi = new ArrayList();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM checkshift order by id asc");
            //stmt.setString(1, date);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getRSCheckshift(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static List<Checkmaster> getListAttd(LoginProfile log, String date, String dept, String inout) throws Exception {

        String q = "";
        String q1 = "";

        if (inout == null || inout.equals("all")) {

        } else if (inout.equals("in")) {
            q1 = " and timeout = '00:00:00'";
        } else if (inout.equals("out")) {
            q1 = " and timeout <> '00:00:00'";
        }

        if (date.equals("Semua") && dept.equals("Semua")) {
            if (inout == null || inout.equals("all")) {
                q = "select * from checkmaster order by date, timescan desc";
            } else if (inout.equals("in")) {
                q = "select * from checkmaster where timeout = '00:00:00' order by date, timescan desc";
            } else if (inout.equals("out")) {
                q = "select * from checkmaster where timeout <> '00:00:00' order by date, timescan desc";
            }
        } else if (!date.equals("Semua") && dept.equals("Semua")) {
            q = "select * from checkmaster  where date = '" + date + "' " + q1 + " order by date, timescan desc";
        } else if (!dept.equals("Semua") && !date.equals("Semua")) {
            q = "select * from checkmaster a, co_staff b where a.staffID = b.staffID and b.departmentID = '" + dept + "' and a.date = '" + date + "' " + q1 + " order by date, timescan desc";
        } else if (!dept.equals("Semua") && date.equals("Semua")) {
            q = "select * from checkmaster a, co_staff b where a.staffID = b.staffID and b.departmentID = '" + dept + "' " + q1 + " order by date, timescan desc";
        }

        // if(StaffDAO.isStaffHeadID(log) && CheckDAO.isMenuAccess(log, CheckDAO.get)){
        //}
        ResultSet rs = null;
        List<Checkmaster> CVi;
        CVi = new ArrayList();
        CoStaff c = new CoStaff();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement(q);
            //stmt.setString(1, date);
            rs = stmt.executeQuery();
            Logger.getLogger(CheckDAO.class.getName()).log(Level.INFO, "-------------------------dada----------" + String.valueOf(stmt));
            while (rs.next()) {
                CVi.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static List<Checkdept> getListDepartmentVisitor(LoginProfile log, String date, String dept) {

        String q = "";
        String q1 = "";

        if (date.equals("Semua")) {
            q = "select * from checkdept where deptID = '" + dept + "' order by date, timescan desc";

        } else if (!date.equals("Semua")) {
            q = "select * from checkdept  where date = '" + date + "' AND deptID = '" + dept + "' order by date, timescan desc";
        }

        ResultSet rs = null;
        List<Checkdept> CVi;
        CVi = new ArrayList();
        CoStaff c = new CoStaff();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement(q);
            //stmt.setString(1, date);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfoCheckdept(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static List<Checkmaster> getAllForToday(LoginProfile log) {

//        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.lcsb_fms_mvn_war_1.0-SNAPSHOTPU");
//
//        EntityManager em = emf.createEntityManager();
//
//        Query query = em.createNamedQuery("Staff.findAll");
//
//        List<Staff> resultList = query.getResultList();
//        for (Staff c : resultList) {
//
//            resultList.add(c);
//
//        }
        ResultSet rs = null;
        List<Checkmaster> CVi;
        CVi = new ArrayList();
        CoStaff c = new CoStaff();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from checkmaster where date = ? order by date, timescan desc");
            stmt.setString(1, AccountingPeriod.getCurrentTimeStamp());
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static List<Checkmaster> getAllForByStaffAndDateRange(LoginProfile log, String staffID) {

        ResultSet rs = null;
        List<Checkmaster> CVi;
        CVi = new ArrayList();
        CoStaff c = new CoStaff();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from checkmaster where staffID = ? order by date, timescan desc");
            stmt.setString(1, staffID);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static List<Checkmaster> getAllForTodayByStaffID(LoginProfile log, String id) {

//        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.lcsb_fms_mvn_war_1.0-SNAPSHOTPU");
//
//        EntityManager em = emf.createEntityManager();
//
//        Query query = em.createNamedQuery("Staff.findAll");
//
//        List<Staff> resultList = query.getResultList();
//        for (Staff c : resultList) {
//
//            resultList.add(c);
//
//        }
        ResultSet rs = null;
        List<Checkmaster> CVi;
        CVi = new ArrayList();
        CoStaff c = new CoStaff();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from checkmaster where date = ? and staffID = ? order by date, timescan desc");
            stmt.setString(1, AccountingPeriod.getCurrentTimeStamp());
            stmt.setString(2, id);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static List<CheckReport> getReport(LoginProfile log, String startdate, String enddate) throws Exception {

        String q1 = "";

        if (StaffDAO.getInfoDepartmentByHeadID(log) != null && !(CheckDAO.isMenuAccess(log, 1))) {
            q1 = "AND b.departmentID = '" + StaffDAO.getInfoDepartmentByHeadID(log).getId() + "'";
        }

        if (CheckDAO.isMenuAccess(log, 0)) {
            q1 = "";
        }

        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");

        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setFirstDayOfWeek(Calendar.MONDAY);
        c.setTime(date);
        int i = c.get(Calendar.DAY_OF_WEEK) - c.getFirstDayOfWeek();
        c.add(Calendar.DATE, -i - 7);
        Date start = c.getTime();
        c.add(Calendar.DATE, 6);
        Date end = c.getTime();

        Logger.getLogger(CheckDAO.class.getName()).log(Level.INFO, "-------------------------week----------" + String.valueOf(dateFormat1.format(start)) + " - " + dateFormat1.format(end));

        String sdate = "";
        String edate = "";

        if (startdate == null || startdate.equals(null) || startdate.equals("null")) {
            sdate = dateFormat1.format(start);
            edate = dateFormat1.format(end);
        }else{
            sdate = startdate;
            edate = enddate;
        }

        ResultSet rs = null;
        List<CheckReport> CVi;
        CVi = new ArrayList();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from checkmaster a, co_staff b WHERE a.staffID = b.staffID and date BETWEEN '" + sdate + "' AND '" + edate + "' " + q1 + " group by date order by date desc");
            //stmt.setString(1, AccountingPeriod.getCurrentTimeStamp());
            //stmt.setString(2, id);
            rs = stmt.executeQuery();
            Logger.getLogger(CheckDAO.class.getName()).log(Level.INFO, "-------------------------stmt----------" + String.valueOf(stmt));
            while (rs.next()) {
                CheckReport cr = new CheckReport();

                cr.setDate(rs.getString("date"));
                cr.setSumattendance(CheckDAO.getKehadiran(log, rs.getString("date"), "Semua"));
                cr.setGagal(CheckDAO.getSumByStatus(log, rs.getString("date"), "Gagal", "Semua"));
                cr.setLulus(CheckDAO.getSumByStatus(log, rs.getString("date"), "Lulus", "Semua"));
                cr.setPending(CheckDAO.getSumByStatus(log, rs.getString("date"), "Pending", "Semua"));
                CVi.add(cr);
            }
        } catch (SQLException e) {//
            e.printStackTrace();//
        }

        return CVi;//
    }

    public static List<String> getTodate(LoginProfile log) throws Exception {

        ResultSet rs = null;
        List<String> CVi;
        CVi = new ArrayList();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from checkmaster group by date order by date asc");
            //stmt.setString(1, AccountingPeriod.getCurrentTimeStamp());
            //stmt.setString(2, id);
            rs = stmt.executeQuery();
            while (rs.next()) {

                CVi.add(rs.getString("date"));
            }
        } catch (SQLException e) {//
            e.printStackTrace();//
        }

        return CVi;
    }

    public static String getBadgeColor(String status) {

        String color = "dark";

        if (status.equals("Lulus")) {
            color = "success";
        } else if (status.equals("Gagal")) {
            color = "danger";
        } else if (status.equals("Pending")) {
            color = "warning";
        }

        return color;
    }

    public static void updateTemperature(LoginProfile log, double temp, String id) throws Exception {

        try {
            String q = ("UPDATE checkmaster SET temperature = ?,timevalid = ?  WHERE id = ?");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setDouble(1, temp);
            ps.setString(2, AccountingPeriod.getCurrentTime());
            ps.setString(3, id);

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static int updateTemperatureAndValidate(LoginProfile log, double temp, String id) throws Exception {

        int c = 0;
        updateTemperature(log, temp, id);

        Checkform cf = (Checkform) CheckDAO.getCheckForm(log, id);

        c = getCounter(log, temp, id);

        if (c > 0) {
            updateStatus(log, id, "Gagal");
        } else {
            updateStatus(log, id, "Lulus");
        }

        return c;

    }

    public static int getCounter(LoginProfile log, double temp, String id) throws Exception {

        int c = 0;

        Checkform cf = (Checkform) CheckDAO.getCheckForm(log, id);

        if (!cf.getQuestion1().equals("")) {
            c += 1;
        }

        if (cf.getQuestion2().equals("Ya")) {
            c += 1;
        }

        if (cf.getQuestion3().equals("Ya")) {
            c += 1;
        }

        if (temp > 37.4) {
            c += 1;
        }

        return c;

    }

    public static void updateStatus(LoginProfile log, String id, String status) throws Exception {

        try {
            String q = ("UPDATE checkmaster SET status = ?  WHERE id = ?");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setString(1, status);
            ps.setString(2, id);

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void deleteCheck(LoginProfile log, String id) throws Exception {

        try {
            String q = ("DELETE FROM checkmaster WHERE id = ?");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setString(1, id);

            ps.executeUpdate();
            ps.close();

            String q1 = ("DELETE FROM checkform WHERE checkID = ?");
            PreparedStatement ps1 = log.getCon().prepareStatement(q1);

            ps1.setString(1, id);

            ps1.executeUpdate();
            ps1.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void checkOut(LoginProfile log) throws Exception {

        try {
            String q = ("UPDATE checkmaster SET timeout = ? WHERE date = ? AND staffID = ?");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setString(1, AccountingPeriod.getCurrentTime());
            ps.setString(2, AccountingPeriod.getCurrentTimeStamp());
            ps.setString(3, StaffDAO.getInfoByEmail(log, log.getEmail()).getStaffid());
            Logger.getLogger(CheckDAO.class.getName()).log(Level.INFO, "-------------------------asdsad----------" + String.valueOf(ps));
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void checkInDept(LoginProfile log, String deptID) throws Exception {

        try {
            String id = AutoGenerate.get10digitNo(log, "checkdept", "id");
            String q = ("INSERT INTO checkdept(staffID, date, timescan, id, deptID) VALUES (?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setString(1, StaffDAO.getInfoByEmail(log, log.getEmail()).getStaffid());
            ps.setString(2, AccountingPeriod.getCurrentTimeStamp());
            ps.setString(3, AccountingPeriod.getCurrentTime());
            ps.setString(4, id);
            ps.setString(5, deptID);
            Logger.getLogger(CheckDAO.class.getName()).log(Level.INFO, "-------------------------asdsad----------" + String.valueOf(ps));
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();////////
        }

    }

    public static void checkInDeptVisitor(Connection con, String deptID, String name, String notel) throws Exception {

        try {
            String id = AutoGenerate.get10digitNoVisitor(con, "checkdept", "id");

            String q = ("INSERT INTO checkdept(notel, date, timescan, id, deptID, name) VALUES (?,?,?,?,?,?)");
            PreparedStatement ps = con.prepareStatement(q);

            ps.setString(1, notel);
            ps.setString(2, AccountingPeriod.getCurrentTimeStamp());
            ps.setString(3, AccountingPeriod.getCurrentTime());
            ps.setString(4, id);
            ps.setString(5, deptID);
            ps.setString(6, name);
            Logger.getLogger(CheckDAO.class.getName()).log(Level.INFO, "-------------------------asdsad----------" + String.valueOf(ps));
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void checkOutDept(LoginProfile log) throws Exception {

        try {
            String q = ("UPDATE checkdept SET timeout = ? WHERE date = ? AND staffID = ?");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setString(1, AccountingPeriod.getCurrentTime());
            ps.setString(2, AccountingPeriod.getCurrentTimeStamp());
            ps.setString(3, StaffDAO.getInfoByEmail(log, log.getEmail()).getStaffid());
            Logger.getLogger(CheckDAO.class.getName()).log(Level.INFO, "-------------------------asdsad----------" + String.valueOf(ps));
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static InOutSummary getAllLateInEarlyOutByStaff(LoginProfile log, String date, String staffID) throws Exception {

        ResultSet rs = null;
        InOutSummary io = new InOutSummary();

        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat formatD = new SimpleDateFormat("yyyy-MM-dd");

        Date timeInMax = null;
        Date timeInMaxRange = null;

        Date timeInMax1 = null;
        Date timeInMaxRange1 = null;
        Date timeInMax2 = null;

        Date timeOutMin1 = null;
        Date timeOutMin2 = null;

        Date lastFasting = formatD.parse("2020-05-24");
        Date normalWorkHour = formatD.parse("2020-06-10");
        Date thisDate = formatD.parse(date);

        if (thisDate.compareTo(lastFasting) > 0) {

            timeInMax = format.parse("08:31:00");
            timeInMaxRange = format.parse("10:00:00");

            timeInMax1 = format.parse("11:30:00");
            timeInMaxRange1 = format.parse("14:00:00");
            timeInMax2 = format.parse("13:01:00");

            timeOutMin1 = format.parse("12:30:00");
            timeOutMin2 = format.parse("17:00:00");

        }

        if (thisDate.compareTo(lastFasting) < 0) {

            timeInMax = format.parse("08:16:00");
            timeInMaxRange = format.parse("10:00:00");

            timeInMax1 = format.parse("11:30:00");
            timeInMaxRange1 = format.parse("14:00:00");
            timeInMax2 = format.parse("12:30:00");

            timeOutMin1 = format.parse("12:15:00");
            timeOutMin2 = format.parse("16:30:00");

        }

        if (thisDate.compareTo(normalWorkHour) > 0) {

            timeInMax = format.parse("08:16:00");

            timeOutMin1 = format.parse("17:00:00");

        }

        if (thisDate.compareTo(normalWorkHour) == 0) {

            timeInMax = format.parse("08:16:00");

            timeOutMin1 = format.parse("17:00:00");

        }

        Date timeOutZero = format.parse("00:00:00");

        String q = "";

        if (StaffDAO.getInfoDepartmentByHeadID(log) != null && !(CheckDAO.isMenuAccess(log, 1))) {
            q = "AND b.departmentID = '" + StaffDAO.getInfoDepartmentByHeadID(log).getId() + "'";
        }

        if (CheckDAO.isMenuAccess(log, 0)) {
            q = "";
        }

        if (!staffID.equals("none")) {
            q = " AND a.staffID = '" + staffID + "'";
        }

        io.setColorEO("success");
        io.setColorLI("success");
        io.setColor("success");
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from checkmaster a, co_staff b WHERE a.staffID=b.staffID AND a.date = ? " + q);
            stmt.setString(1, date);
            Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "---adaketakni-- " + stmt);
            rs = stmt.executeQuery();
            while (rs.next()) {

                Date timeIn = format.parse(rs.getString("timevalid"));
                Date timeOut = format.parse(rs.getString("timeout"));

                Logger.getLogger(CheckDAO.class.getName()).log(Level.INFO, "--timein -" + rs.getString("timevalid") + "-- timeout - " + rs.getString("timeout"));

                if (thisDate.compareTo(normalWorkHour) < 0) {

                    if (((timeIn.before(timeInMaxRange) && timeIn.after(timeInMax)) || timeIn.after(timeInMax2)) || (timeOut.before(timeOutMin1) || ((timeIn.after(timeInMax1) && timeIn.before(timeInMaxRange1)) && timeOut.before(timeOutMin2)))) {

                        if ((timeIn.before(timeInMaxRange) && timeIn.after(timeInMax)) || timeIn.after(timeInMax2)) {
                            //masuk lewat

                            io.setColorLI("danger");

                        } else {
                            io.setColorLI("success");
                        }

                        if (timeOut.before(timeOutMin1) || ((timeIn.after(timeInMax1) && timeIn.before(timeInMaxRange1)) && timeOut.before(timeOutMin2))) {

                            io.setColorEO("danger");

                        } else {
                            io.setColorEO("success");
                        }

                        long difference = timeOut.getTime() - timeIn.getTime();

                        //long diffMins = difference / (60 * 1000) % 60;
                        long diffInMin = difference / (60 * 1000);
                        long diffMinutes = difference / (60 * 1000) % 60;
                        long diffHours = difference / (60 * 60 * 1000) % 24;
                        io.setWorkinghour(String.valueOf(diffHours + "j " + diffMinutes + "m"));

                        if (diffInMin > 241) {
                            io.setIsCompleteWorkingHour(true);
                            io.setColor("success");
                        } else {
                            io.setIsCompleteWorkingHour(false);
                            io.setColor("danger");
                        }

                        if (rs.getString("timeout").equals("00:00:00")) {
                            io.setWorkinghour("N/A");
                        }

                        io.setCm(getInfo(rs));

                    }
                } else {

                    if (timeIn.after(timeInMax) || timeOut.before(timeOutMin1)) {
                        //if (((timeIn.before(timeInMaxRange) && timeIn.after(timeInMax)) || timeIn.after(timeInMax2)) || (timeOut.before(timeOutMin1) || ((timeIn.after(timeInMax1) && timeIn.before(timeInMaxRange1)) && timeOut.before(timeOutMin2)))) {

                        if (timeIn.after(timeInMax)) {
                            //masuk lewat

                            io.setColorLI("danger");

                        } else {
                            io.setColorLI("success");
                        }

                        if (timeOut.before(timeOutMin1)) {

                            io.setColorEO("danger");

                        } else {
                            io.setColorEO("success");
                        }

                        long difference = timeOut.getTime() - timeIn.getTime();

                        //long diffMins = difference / (60 * 1000) % 60;
                        long diffInMin = difference / (60 * 1000);
                        long diffMinutes = difference / (60 * 1000) % 60;
                        long diffHours = difference / (60 * 60 * 1000) % 24;
                        io.setWorkinghour(String.valueOf(diffHours + "j " + diffMinutes + "m"));

                        if (diffInMin > 466) {
                            io.setIsCompleteWorkingHour(true);
                            io.setColor("success");
                        } else {
                            io.setIsCompleteWorkingHour(false);
                            io.setColor("danger");
                        }

                        if (rs.getString("timeout").equals("00:00:00")) {
                            io.setWorkinghour("N/A");
                        }

                        io.setCm(getInfo(rs));

                    }

                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return io;
    }

    public static List<InOutSummary> getAllLateInEarlyOut(LoginProfile log, String date) throws Exception {

        ResultSet rs = null;
        List<InOutSummary> CVi;
        CVi = new ArrayList();

        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat formatD = new SimpleDateFormat("yyyy-MM-dd");

        Date timeInMax = null;
        Date timeInMaxRange = null;

        Date timeInMax1 = null;
        Date timeInMaxRange1 = null;
        Date timeInMax2 = null;

        Date timeOutMin1 = null;
        Date timeOutMin2 = null;

        Date lastFasting = formatD.parse("2020-05-24");
        Date normalWorkHour = formatD.parse("2020-06-10");
        Date thisDate = formatD.parse(date);

        if (thisDate.compareTo(lastFasting) > 0) {

            timeInMax = format.parse("08:31:00");
            timeInMaxRange = format.parse("10:00:00");

            timeInMax1 = format.parse("11:30:00");
            timeInMaxRange1 = format.parse("14:00:00");
            timeInMax2 = format.parse("13:01:00");

            timeOutMin1 = format.parse("12:30:00");
            timeOutMin2 = format.parse("17:00:00");

        }

        if (thisDate.compareTo(lastFasting) < 0) {

            timeInMax = format.parse("08:16:00");
            timeInMaxRange = format.parse("10:00:00");

            timeInMax1 = format.parse("11:30:00");
            timeInMaxRange1 = format.parse("14:00:00");
            timeInMax2 = format.parse("12:31:00");

            timeOutMin1 = format.parse("12:15:00");
            timeOutMin2 = format.parse("16:30:00");

        }

        if (thisDate.compareTo(normalWorkHour) > 0) {

            timeInMax = format.parse("08:16:00");

            timeOutMin1 = format.parse("17:00:00");

        }

        if (thisDate.compareTo(normalWorkHour) == 0) {

            timeInMax = format.parse("08:16:00");

            timeOutMin1 = format.parse("17:00:00");

        }

        Date now = format.parse(AccountingPeriod.getCurrentTime());

        String q = "";

        if (StaffDAO.getInfoDepartmentByHeadID(log) != null && !(CheckDAO.isMenuAccess(log, 1))) {
            q = "AND b.departmentID = '" + StaffDAO.getInfoDepartmentByHeadID(log).getId() + "'";
        }

        if (CheckDAO.isMenuAccess(log, 0)) {
            q = "";
        }

        // Date now = formatD.parse(AccountingPeriod.getCurrentTime());
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from checkmaster a, co_staff b WHERE a.staffID=b.staffID AND a.date = ? " + q);
            stmt.setString(1, date);
            Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "---adaketakni-- " + stmt);
            rs = stmt.executeQuery();
            while (rs.next()) {

                InOutSummary io = new InOutSummary();

                Date timeIn = format.parse(rs.getString("timevalid"));
                Date timeOut = format.parse(rs.getString("timeout"));

                if (thisDate.compareTo(normalWorkHour) < 0) {

                    if (((timeIn.before(timeInMaxRange) && timeIn.after(timeInMax)) || timeIn.after(timeInMax2)) || (timeOut.before(timeOutMin1) || ((timeIn.after(timeInMax1) && timeIn.before(timeInMaxRange1)) && timeOut.before(timeOutMin2)))) {

                        if ((timeIn.before(timeInMaxRange) && timeIn.after(timeInMax)) || timeIn.after(timeInMax2)) {
                            //masuk lewat

                            io.setColorLI("danger");

                        } else {
                            io.setColorLI("success");
                        }

                        if (timeOut.before(timeOutMin1) || ((timeIn.after(timeInMax1) && timeIn.before(timeInMaxRange1)) && timeOut.before(timeOutMin2))) {

                            io.setColorEO("danger");

                        } else {
                            io.setColorEO("success");
                        }

                        long difference = timeOut.getTime() - timeIn.getTime();

                        //long diffMins = difference / (60 * 1000) % 60;
                        long diffInMin = difference / (60 * 1000);
                        long diffMinutes = difference / (60 * 1000) % 60;
                        long diffHours = difference / (60 * 60 * 1000) % 24;
                        io.setWorkinghour(String.valueOf(diffHours + "j " + diffMinutes + "m"));

                        if (diffInMin > 241) {
                            io.setIsCompleteWorkingHour(true);
                            io.setColor("success");
                        } else {
                            io.setIsCompleteWorkingHour(false);
                            io.setColor("danger");
                        }

                        if (rs.getString("timeout").equals("00:00:00")) {
                            io.setWorkinghour("N/A");
                        }

                        io.setCm(getInfo(rs));
                        CVi.add(io);

                    }
                } else {

                    if (timeIn.after(timeInMax) || timeOut.before(timeOutMin1)) {
                        //if (((timeIn.before(timeInMaxRange) && timeIn.after(timeInMax)) || timeIn.after(timeInMax2)) || (timeOut.before(timeOutMin1) || ((timeIn.after(timeInMax1) && timeIn.before(timeInMaxRange1)) && timeOut.before(timeOutMin2)))) {

                        if (timeIn.after(timeInMax)) {
                            //masuk lewat

                            io.setColorLI("danger");

                        } else {
                            io.setColorLI("success");
                        }

                        if (timeOut.before(timeOutMin1)) {

                            io.setColorEO("danger");

                        } else {
                            io.setColorEO("success");
                        }

                        long difference = timeOut.getTime() - timeIn.getTime();

                        //long diffMins = difference / (60 * 1000) % 60;
                        long diffInMin = difference / (60 * 1000);
                        long diffMinutes = difference / (60 * 1000) % 60;
                        long diffHours = difference / (60 * 60 * 1000) % 24;
                        io.setWorkinghour(String.valueOf(diffHours + "j " + diffMinutes + "m"));

                        if (diffInMin > 526) {
                            io.setIsCompleteWorkingHour(true);
                            io.setColor("success");
                        } else {
                            io.setIsCompleteWorkingHour(false);
                            io.setColor("danger");
                        }

                        if (rs.getString("timeout").equals("00:00:00")) {
                            io.setWorkinghour("N/A");
                        }

                        io.setCm(getInfo(rs));

                        if (rs.getString("timeout").equals("00:00:00")) {

                            if (now.after(timeOutMin1)) {
                                CVi.add(io);
                            }

                        } else {
                            CVi.add(io);
                        }

                    }

                }

//                if (((timeIn.before(timeInMaxRange) && timeIn.after(timeInMax)) || timeIn.after(timeInMax2)) || (timeOut.before(timeOutMin1) || ((timeIn.after(timeInMax1) && timeIn.before(timeInMaxRange1)) && timeOut.before(timeOutMin2)))) {
//
//                    if ((timeIn.before(timeInMaxRange) && timeIn.after(timeInMax)) || timeIn.after(timeInMax2)) {
//                        //masuk lewat
//                        io.setColorLI("danger");
//
//                    } else {
//                        io.setColorLI("success");
//                    }
//
//                    if (timeOut.before(timeOutMin1) || ((timeIn.after(timeInMax1) && timeIn.before(timeInMaxRange1)) && timeOut.before(timeOutMin2))) {
//
//                        io.setColorEO("danger");
//
//                    } else {
//                        io.setColorEO("success");
//                    }
//
//                    long difference = timeOut.getTime() - timeIn.getTime();
//
//                    //long diffMins = difference / (60 * 1000) % 60;
//                    long diffInMin = difference / (60 * 1000);
//                    long diffMinutes = difference / (60 * 1000) % 60;
//                    long diffHours = difference / (60 * 60 * 1000) % 24;
//                    io.setWorkinghour(String.valueOf(diffHours + "j " + diffMinutes + "m"));
//
//                    if (diffInMin > 241) {
//                        io.setIsCompleteWorkingHour(true);
//                        io.setColor("success");
//                    } else {
//                        io.setIsCompleteWorkingHour(false);
//                        io.setColor("danger");
//                    }
//
//                    if (rs.getString("timeout").equals("00:00:00")) {
//                        io.setWorkinghour("N/A");
//                    }
//
//                    io.setCm(getInfo(rs));
//
//                    //if (now.after(timeOutMin2) && rs.getString("timeout").equals("00:00:00")) {
//                    CVi.add(io);
//                    //}
//
//                }
//                if (((timeIn.compareTo(timeInMax) > 0 && timeIn.compareTo(timeInMaxRange) < 0) || timeIn.compareTo(timeInMax2) > 0) || ((timeOut.compareTo(timeOutMin1) < 0 || ((timeIn.compareTo(timeInMax1) > 0 && timeIn.compareTo(timeInMaxRange1) < 0) && timeOut.compareTo(timeOutMin2) < 0)) && timeOut.compareTo(timeOutZero) != 0)) {
//
//                    if (((timeIn.compareTo(timeInMax) > 0 && timeIn.compareTo(timeInMaxRange) < 0) || timeIn.compareTo(timeInMax2) > 0)) {
//                        io.setColorLI("danger");
//                    } else {
//                        io.setColorLI("success");
//                    }
//                    //12:22 compare 12:30:00 ||             08:33:00 compare 11:30:00               08:33:00  compare 14:00:00
//                    if ((timeOut.compareTo(timeOutMin1) < 0 || ((timeIn.compareTo(timeInMax1) > 0 && timeIn.compareTo(timeInMaxRange1) < 0) && timeOut.compareTo(timeOutMin2) < 0)) && timeOut.compareTo(timeOutZero) != 0) {
//                        io.setColorEO("danger");
//                    } else {
//                        io.setColorEO("success");
//                    }
//
//                    long difference = timeOut.getTime() - timeIn.getTime();
//
//                    //long diffMins = difference / (60 * 1000) % 60;
//                    long diffInMin = difference / (60 * 1000);
//                    long diffMinutes = difference / (60 * 1000) % 60;
//                    long diffHours = difference / (60 * 60 * 1000) % 24;
//                    io.setWorkinghour(String.valueOf(diffHours + "j " + diffMinutes + "m"));
//
//                    if (diffInMin > 241) {
//                        io.setIsCompleteWorkingHour(true);
//                        io.setColor("success");
//                    } else {
//                        io.setIsCompleteWorkingHour(false);
//                        io.setColor("danger");
//                    }
//
//                    io.setCm(getInfo(rs));
//                    CVi.add(io);
//                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static List<CoStaff> getAllStaffLateIn(LoginProfile log, String filter, String param_filter, String date) throws Exception {

        String q = "";

        if (filter.equals("location")) {

            if (param_filter.equals("IBU PEJABAT")) {

                q = " AND (" + filter + " = 'IBU PEJABAT' OR " + filter + " = 'CAWANGAN INDERA MAHKOTA')";

            }

        }

        if (filter.equals("departmentID")) {

            if (!param_filter.equals("none")) {

                q = " AND " + filter + " = '" + param_filter + "'";

            } else {
                q = " AND email <> ''";
            }

        }

        if (log.getAccessLevel() == 0) {
            q += " AND staffID = '" + log.getUserID() + "'";
        } else if (log.getAccessLevel() == 2 || log.getAccessLevel() == 3) {
            // q += " AND staffID = '" + log.getUserID() + "' OR staffID IN " + LeaveDAO.getSuperviseeID(log);
        }

        ResultSet rs = null;
        List<CoStaff> CVi;
        CVi = new ArrayList();
        CoStaff c = new CoStaff();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff  WHERE  staffiD IN " + getLateInStaffID(log, date) + " ORDER BY name ASC  ");
            //stmt.setString(1, staffid);
            Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "---adaketak-- " + stmt);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(StaffDAO.getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static String getLateInStaffID(LoginProfile log, String date) throws SQLException, Exception {

        String s = "(";
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT pm.staffID as stf,pm.name,pm.location,b.timevalid FROM co_staff pm, checkmaster b WHERE pm.email <> '' AND (location = 'IBU PEJABAT' OR location = 'CAWANGAN INDERA MAHKOTA') AND pm.staffID = b.staffID AND b.date BETWEEN '" + date + "' AND '" + date + "' AND (timevalid BETWEEN '08:31:00' AND '10:01:00' OR timevalid > '13:00:00' )");
        //stmt.setString(1, no);
        //stmt.setString(1, departmentID);
        rs = stmt.executeQuery();

        while (rs.next()) {

            if (rs.isLast()) {
                s += "'" + rs.getString("stf") + "'";
            } else {
                s += "'" + rs.getString("stf") + "'" + ",";
            }

        }

        s += ")";

        Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "------string" + String.valueOf(s));

        return s;
    }

    public static List<CoStaff> getAllStaffAbsent(LoginProfile log, String filter, String param_filter, String date) throws Exception {

        String q = "";

        if (filter.equals("location")) {

            if (param_filter.equals("IBU PEJABAT")) {

                q = " AND (" + filter + " = 'IBU PEJABAT' OR " + filter + " = 'CAWANGAN INDERA MAHKOTA') AND position <> 'PENYELIA'";

            }

        }

        if (filter.equals("departmentID")) {

            if (!param_filter.equals("none")) {

                q = " AND " + filter + " = '" + param_filter + "'";

            } else {
                q = " AND email <> ''";
            }

        }

        if (log.getAccessLevel() == 0) {
            q += " AND staffID = '" + log.getUserID() + "'";
        } else if (log.getAccessLevel() == 2 || log.getAccessLevel() == 3) {
            // q += " AND staffID = '" + log.getUserID() + "' OR staffID IN " + LeaveDAO.getSuperviseeID(log);
        }

        ResultSet rs = null;
        List<CoStaff> CVi;
        CVi = new ArrayList();
        CoStaff c = new CoStaff();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff  WHERE  staffiD IN " + getAbsentStaffID(log, date) + " ORDER BY name ASC  ");
            //stmt.setString(1, staffid);
            Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "---adaketak-- " + stmt);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(StaffDAO.getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static String getAbsentStaffID(LoginProfile log, String date) throws SQLException, Exception {

        String q1 = "";

        if (StaffDAO.getInfoDepartmentByHeadID(log) != null && !(CheckDAO.isMenuAccess(log, 1))) {
            q1 = "AND pm.departmentID = '" + StaffDAO.getInfoDepartmentByHeadID(log).getId() + "'";
        }

        if (CheckDAO.isMenuAccess(log, 0)) {
            q1 = "";
        }

        String s = "(";
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT pm.staffID as stf,pm.name,pm.location FROM co_staff pm WHERE pm.email <> '' AND (location = 'IBU PEJABAT' OR location = 'CAWANGAN INDERA MAHKOTA') AND position <> 'PENYELIA' " + q1 + " and pm.staffID NOT IN (SELECT distinct(pd.staffID) FROM leave_master pd where ('" + date + "' BETWEEN datestart AND dateend) and status='Approved') and pm.staffID NOT IN (SELECT distinct(ps.staffID) FROM checkmaster ps where date = '" + date + "')  and pm.staffID NOT IN (SELECT distinct(po.staffID) FROM checkoutstation po where date = '" + date + "') ");
        //stmt.setString(1, no);
        //stmt.setString(1, departmentID);
        rs = stmt.executeQuery();

        while (rs.next()) {

            if (rs.isLast()) {
                s += "'" + rs.getString("stf") + "'";
            } else {
                s += "'" + rs.getString("stf") + "'" + ",";
            }

        }

        s += ")";

        Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "------string" + String.valueOf(s));

        return s;
    }

    public static int getKehadiran(LoginProfile log, String date, String dept) throws SQLException, Exception {
        int c = 0;

        String q = "";
        if (!dept.equals("Semua")) {
            q = " AND staffID IN " + StaffDAO.getDepartmentMemberID(log, dept);
        }

        String q1 = "";

        if (StaffDAO.getInfoDepartmentByHeadID(log) != null && !(CheckDAO.isMenuAccess(log, 1))) {
            q1 = "AND b.departmentID = '" + StaffDAO.getInfoDepartmentByHeadID(log).getId() + "'";
        }

        if (CheckDAO.isMenuAccess(log, 0)) {
            q1 = "";
        }

        try {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT count(*) as cnt FROM checkmaster a, co_staff b WHERE a.staffID=b.staffID " + q1 + " AND date=? " + q);
            stmt.setString(1, date);
            Logger.getLogger(CheckDAO.class.getName()).log(Level.INFO, "-------------------------lolo----------" + String.valueOf(stmt));
            rs = stmt.executeQuery();

            if (rs.next()) {
                c = rs.getInt("cnt");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    public static int getSumByStatus(LoginProfile log, String date, String status, String dept) throws SQLException, Exception {
        int c = 0;

        String q = "";
        if (!dept.equals("Semua")) {
            q = " AND staffID IN " + StaffDAO.getDepartmentMemberID(log, dept);
        }

        String q1 = "";

        if (StaffDAO.getInfoDepartmentByHeadID(log) != null && !(CheckDAO.isMenuAccess(log, 1))) {
            q1 = "AND b.departmentID = '" + StaffDAO.getInfoDepartmentByHeadID(log).getId() + "'";
        }

        if (CheckDAO.isMenuAccess(log, 0)) {
            q1 = "";
        }

        try {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT count(*) as cnt FROM checkmaster a, co_staff b WHERE a.staffID=b.staffID  " + q1 + "  AND date=? and a.status=?" + q);
            stmt.setString(1, date);
            stmt.setString(2, status);
            rs = stmt.executeQuery();
            Logger.getLogger(CheckDAO.class.getName()).log(Level.INFO, "-------------------------pupuooo----------" + String.valueOf(stmt));
            if (rs.next()) {
                c = rs.getInt("cnt");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    public static int getInOut(LoginProfile log, String date) throws SQLException, Exception {
        int c = 0;

        try {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM checkmaster WHERE date=? and");
            stmt.setString(1, date);
            rs = stmt.executeQuery();
            Logger.getLogger(CheckDAO.class.getName()).log(Level.INFO, "-------------------------asdsad----------" + String.valueOf(stmt));
            if (rs.next()) {
                c = rs.getInt("cnt");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    public static InOutSummary getInOutSummary(LoginProfile log, Checkmaster cm) throws SQLException, Exception {

        InOutSummary io = new InOutSummary();

        io.setTimevalid(cm.getTimevalid());
        io.setTimeout(cm.getTimeout());

        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        Date date1 = format.parse(cm.getTimevalid());
        Date date2 = format.parse(cm.getTimeout());
        Date dateIn = format.parse("08:30:00");
        Date dateOut = format.parse("12:30:00");

        //Date dateIn = format.parse("08:30:00");
        //Date dateOut = format.parse("12:30:00");
        long difference = date2.getTime() - date1.getTime();

        //long diffMins = difference / (60 * 1000) % 60;
        long diffInMin = difference / (60 * 1000);
        long diffMinutes = difference / (60 * 1000) % 60;
        long diffHours = difference / (60 * 60 * 1000) % 24;
        io.setWorkinghour(String.valueOf(diffHours + "j " + diffMinutes + "m"));

        if (diffInMin > 241) {
            io.setIsCompleteWorkingHour(true);
            io.setColor("success");
        } else {
            io.setIsCompleteWorkingHour(false);
            io.setColor("danger");
        }

        LocalTime start = LocalTime.parse(cm.getTimevalid());
        LocalTime stop = LocalTime.parse(cm.getTimeout());
        Duration duration = Duration.between(start, stop);

        if (date1.compareTo(dateIn) > 0) {
            io.setColorLI("danger");
        } else if (date1.compareTo(dateIn) < 0) {
            io.setColorLI("success");
        }
//        else if (date1.compareTo(dateIn) == 0) {
//            f = false;
//        }

        if (date2.compareTo(dateIn) > 0) {
            io.setColorLI("danger");
        } else if (date2.compareTo(dateIn) < 0) {
            io.setColorLI("success");
        }

        //Instant start = Instant.now();
        //(cm.getTimevalid() <){
        // }
        //io.setSyif(checkID);
        return io;
    }

    public static boolean isMenuAccess(LoginProfile log, int lvl) throws SQLException, Exception {
        boolean c = false;

        if (CheckDAO.getAccessByStaffID(log, lvl) != null) {
            c = true;
        }
        return c;
    }

    public static String getLocation(LoginProfile log, String id) throws Exception {

        String loc = "";

        if (id == null || id.equals(null) || id.equals("null")) {
            loc = "IBU PEJABAT";
        } else {
            loc = EstateDAO.getEstateInfo(log, id, "estatecode").getEstatedescp();
        }

        return loc;

    }

    public static Checkoutstation getRSCheckoutstation(ResultSet rs) throws SQLException {

        Checkoutstation g = new Checkoutstation();

        g.setDate(rs.getString("date"));
        g.setStaffID(rs.getString("staffID"));
        g.setStatus(rs.getString("status"));
        g.setApproveID(rs.getString("approveID"));
        g.setApprovedate(rs.getString("approvedate"));
        g.setApprovetime(rs.getString("approvetime"));
        g.setCheckID(rs.getString("checkID"));
        g.setDate(rs.getString("date"));
        g.setId(rs.getString("id"));
        g.setLocationID(rs.getString("locationID"));
        g.setReason(rs.getString("reason"));
        g.setStatus(rs.getString("status"));
        g.setTimein(rs.getString("timein"));
        g.setTimeout(rs.getString("timeout"));

        return g;
    }

    public static void saveCheckOutStation(LoginProfile log, Checkoutstation c, String deptID) throws Exception {

        try {

            String id = AutoGenerate.get10digitNo(log, "checkoutstation", "id");

            String q = ("INSERT INTO checkoutstation(checkID, date, id, locationID, reason,staffID, status, timein) VALUES (?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, c.getCheckID());
            ps.setString(2, c.getDate());
            ps.setString(3, id);
            ps.setString(4, c.getLocationID());
            ps.setString(5, c.getReason());
            ps.setString(6, c.getStaffID());
            ps.setString(7, c.getStatus());
            ps.setString(8, c.getTimein());

            Logger.getLogger(CheckDAO.class.getName()).log(Level.INFO, "---saveCheckOutStation-- " + ps);

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static boolean isStaffCheckInOutsideTheirBase(LoginProfile log, String staffID, String locationID) throws SQLException, Exception {
        boolean c = false;

        CoStaff co = (CoStaff) StaffDAO.getInfo(log, staffID);

        if (co.getLocation().equals("IBU PEJABAT") && (locationID == null || locationID.equals(null) || locationID.equals("null"))) {

        } else {
            c = true;
        }

        return c;
    }

    public static List<Checkoutstation> getAllOutStationByDate(LoginProfile log, String date) throws Exception {

        ResultSet rs = null;
        List<Checkoutstation> CVi;
        CVi = new ArrayList();
        CoStaff c = new CoStaff();

        String q1 = "";

        if (StaffDAO.getInfoDepartmentByHeadID(log) != null && !(CheckDAO.isMenuAccess(log, 1))) {
            q1 = "AND b.departmentID = '" + StaffDAO.getInfoDepartmentByHeadID(log).getId() + "'";
        }

        if (CheckDAO.isMenuAccess(log, 0)) {
            q1 = "";
        }

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from checkoutstation a, co_staff b WHERE a.staffID=b.staffID AND a.date = ? " + q1);
            stmt.setString(1, date);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getRSCheckoutstation(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static int getCountOutStationByDate(LoginProfile log, String date) throws SQLException, Exception {

        //date  = date.substring(6, 10) + "-" + date.substring(3, 5) + "-" + date.substring(0, 2);
        String q = "";

        if (StaffDAO.getInfoDepartmentByHeadID(log) != null && !(CheckDAO.isMenuAccess(log, 1))) {
            q = "AND b.departmentID = '" + StaffDAO.getInfoDepartmentByHeadID(log).getId() + "'";
        }

        if (CheckDAO.isMenuAccess(log, 0)) {
            q = "";
        }

        int i = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from checkoutstation a, co_staff b WHERE a.staffID=b.staffID AND a.date = ? " + q);
        stmt.setString(1, date);
//        stmt.setString(2, log.getUserID());
        rs = stmt.executeQuery();

        Logger.getLogger(LeaveDAO.class.getName()).log(Level.INFO, "------string" + String.valueOf(stmt));

        while (rs.next()) {
            i = rs.getInt("cnt");
        }

        return i;
    }
    
    public static Checkoutstation getCheckoutstation(LoginProfile log, String no, String checkID) throws SQLException, Exception {
        Checkoutstation c = null;

        try {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM checkoutstation WHERE staffID=? AND checkID = ?");
            stmt.setString(1, no);
            stmt.setString(2, checkID);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = CheckDAO.getRSCheckoutstation(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }
}
